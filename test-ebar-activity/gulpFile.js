
'use strict';

// libraries
const gulp = require('gulp');
const $ = require('gulp-load-plugins')();
const del = require('del');
const runSequence = require('run-sequence');
const merge = require('merge-stream');
const path = require('path');
const concat = require('gulp-concat');
const eventStream = require('event-stream');
const console = require('gulp-util');
const os = require('os');
const rsync = require('gulp-rsync');
const fs = require('fs');
const OutputPath = '/Users/mcaballero/Proyectos/sumus-platform/out/production/test-ebar-activity/www/test-ebar';
const WorkingDirectory = '/Users/mcaballero/Proyectos/sumus-platform/test-ebar-activity';
const ActivityName = 'test-ebar';
const WatchPort = 35989;

// root tasks
gulp.task('dev', cb => {
runSequence('clean', 'copy-lib-to-output', 'package-dev', 'watch', cb);
});

gulp.task('deploy', cb => {
runSequence('clean', 'package-deploy', cb);
});

// sub tasks
gulp.task('clean', () => {
return del([outputPath()], {force:true});
});

gulp.task('copy-lib-to-output', () => {
return eventStream.concat(
    unionLibs(workingPath('lib/**/*'), outputPath("/lib"))
);
});

gulp.task('package-dev', cb => {
runSequence('union-dev', 'resolve-dev-url-dependencies', cb);
});

gulp.task('union-dev', () => {
return eventStream.concat(
    unionWidgets(workingPath('src/widgets/**/*'), outputPath("/src/widgets")),
    unionFonts(workingPath('src/fonts/**/*'), outputPath("/fonts")),
    unionStyles(workingPath('src/styles/**/*'), outputPath("/styles")),
    unionImages(workingPath('src/images/**/*'), outputPath("/images")),
    gulp.src([workingPath('src/*.{html,js}'), '!' + workingPath('**/.DS_Store')]).pipe(rsync({ silent: true, incremental: true, root: 'src', destination: outputPath('')}))
);
});

gulp.task('package-deploy', cb => {
runSequence('union-deploy', 'resolve-deploy-url-dependencies', cb);
});

gulp.task('union-deploy', cb => {
return eventStream.concat(
    unionFonts(workingPath('src/fonts/**/*'), outputPath("/fonts")),
    unionStyles(workingPath('src/styles/**/*'), outputPath("/styles")),
    unionImages(workingPath('src/images/**/*'), outputPath("/images")),
    gulp.src([workingPath('src/*.{html,js}'), '!' + workingPath('src/components.html'), '!' + workingPath('**/.DS_Store')]).pipe(rsync({ silent: true, incremental: true, root: 'src', destination: outputPath('')})),
    unionLib(),
    compileFile(workingPath('src'), '/components.html'),
    obfuscateFile(outputPath('/components.html'))
);
});

gulp.task('resolve-deploy-url-dependencies', () => {
return gulp.src([outputPath('*.html'), '!' + outputPath('/components.html')])
    .pipe($.replace('src="', 'src="$url/'))
    .pipe($.replace('href="', 'href="$url/'))
    .pipe($.replace('href-absolute=', 'href='))
    .pipe(gulp.dest(outputPath()));
});

gulp.task('resolve-dev-url-dependencies', () => {
gulp.src([outputPath('*.html'), '!' + outputPath('components.html')])
    .pipe($.replace('src="', 'src="/' + ActivityName + '/'))
    .pipe($.replace('href="', 'href="/' + ActivityName + '/'))
    .pipe($.replace('href-absolute=', 'href='))
    .pipe(gulp.dest(outputPath()));
gulp.src([outputPath('components.html')])
    .pipe($.replace('src="', 'src="/' + ActivityName + '/src/'))
    .pipe($.replace('href="', 'href="/' + ActivityName + '/src/'))
    .pipe($.replace('href-absolute=', 'href='))
    .pipe(gulp.dest(outputPath()));
});

gulp.task('watch', () => {
const tasksToLaunch = ['refresh-dev-server'];

gulp.watch([workingPath('src/**/*.html')], tasksToLaunch);
gulp.watch([workingPath('src/styles/**/*.css')], tasksToLaunch);
gulp.watch([workingPath('src/widgets/**/*.css')], tasksToLaunch);
gulp.watch([workingPath('src/widgets/**/*.html')], tasksToLaunch);
gulp.watch([workingPath('src/components/**/*.css')], tasksToLaunch);
gulp.watch([workingPath('src/components/**/*.html')], tasksToLaunch);
gulp.watch([workingPath('src/images/**/*')], tasksToLaunch);

$.livereload.listen({ port: WatchPort, basePath: '.' });
});

gulp.task('refresh-dev-server', ['package-dev', 'refresh-browser']);
gulp.task('refresh-browser', () => gulp.src(outputPath('/*.html')).pipe($.livereload()));

// constants
const outputPath = subpath => !subpath ? OutputPath : path.join(OutputPath, subpath);
const workingPath = subpath => !subpath ? WorkingDirectory : path.join(WorkingDirectory, subpath);

const compileFile = (path, src) => {
var isWin = (os.type().toLowerCase().startsWith() == "win");
var vulcanizeOptions = { stripComments: true, inlineCss: true, inlineScripts: true };

if (isWin) vulcanizeOptions.abspath = path;
else src = path + src;

return gulp.src(src)
    .pipe($.vulcanize(vulcanizeOptions))
    .pipe($.replace('<iron-a11y-keys target="{{}}" keys="space enter" on-keys-pressed="toggleOpened"><\/iron-a11y-keys>', '<!--<iron-a11y-keys target="{{}}" keys="space enter" on-keys-pressed="toggleOpened"><\/iron-a11y-keys>-->')) // fix important paper-chip bug
    .pipe(gulp.dest(outputPath()))
    .pipe($.size({title: 'vulcanize'}));
};

const obfuscateFile = (src) => {
return gulp.src(src)
    .pipe($.htmlmin({
        collapseWhitespace: true,
        removeComments: true,
        removeTagWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
        trimCustomFragments : true
    }))
    .pipe(gulp.dest(outputPath()));
};

const unionLib = () => {
return gulp.src([workingPath('lib/{jquery,webcomponentsjs,moment,konos-server-web,alexandria-activity-elements,numeral,promise-polyfill}/**/*'),
    workingPath('lib/{cotton-cookies,cotton-push}/*.js')])
    .pipe(rsync({ silent: true, incremental: true, root: 'lib', destination: outputPath('lib') }));
};

const unionLibs = (src, dest) => {
return gulp.src(src)
    .pipe(gulp.dest(dest))
    .pipe($.size({title: 'libs'}));
};

const unionWidgets = (src, dest) => {
if (!fs.existsSync(outputPath("src"))) fs.mkdirSync(outputPath("src"));
return gulp.src(src)
    .pipe(rsync({ silent: true, incremental: true, root: 'src/widgets', destination: dest }))
    .pipe($.size({title: 'widgets'}));
};

const unionStyles = (src, dest) => {
return gulp.src(src)
    .pipe(gulp.dest(dest))
    .pipe($.size({title: 'styles'}));
};

const unionImages = (src, dest) => {
return gulp.src(src)
    .pipe(rsync({ silent: true, incremental: true, root: 'src/images', destination: dest }))
    .pipe($.size({title: 'images'}));
};

const unionFonts = (src, dest) => {
return gulp.src(src)
    .pipe(rsync({ silent: true, incremental: true, root: 'src/fonts', destination: dest }))
    .pipe($.size({title: 'fonts'}));
};

require('web-component-tester').gulp.init(gulp);