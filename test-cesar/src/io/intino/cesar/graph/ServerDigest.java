package io.intino.cesar.graph;

import io.intino.sumus.datawarehouse.store.Digest;

import java.time.Instant;

public class ServerDigest {

	private static final String USED_CPU = "usedCPU";
	private static final String USED_DISK = "usedDisk";
	private static final String INBOUND = "inboundConnections";
	private static final String OUTBOUND = "outboundConnections";
	private static final String USED_MEMORY = "usedMemory";
	private static final String COUNT = "count";
	private final Digest digest;

	public ServerDigest(Digest digest) {
		this.digest = digest;
	}

	public int count() {
		return digest.intOf(COUNT);
	}

	public ServerDigest addCount() {
		digest.add(COUNT, 1);
		return this;
	}

	public double usedCPU() {
		return digest.floatOf(USED_CPU);
	}

	public double usedMemory() {
		return digest.floatOf(USED_MEMORY);
	}

	public int inboundConnections() {
		return digest.intOf(INBOUND);
	}

	public int outboundConnections() {
		return digest.intOf(OUTBOUND);
	}

	public double usedDisk() {
		return digest.floatOf(USED_DISK);
	}

	public Instant created() {
		return digest.ts();
	}

	public ServerDigest addUsedCPU(double value) {
		digest.add(USED_CPU, (float) value);
		return this;
	}

	public ServerDigest addUsedDisk(double value) {
		digest.add(USED_DISK, (float) value);
		return this;
	}

	public ServerDigest addUsedMemory(double value) {
		digest.add(USED_MEMORY, (float) value);
		return this;
	}

	public ServerDigest addInboundConnections(Integer value) {
		digest.add(INBOUND, value);
		return this;
	}

	public ServerDigest addOutboundConnections(Integer value) {
		digest.add(OUTBOUND, value);
		return this;
	}

	public ServerDigest save() {
		digest.save();
		return this;
	}

	public String ref() {
		return digest.ref();
	}
}
