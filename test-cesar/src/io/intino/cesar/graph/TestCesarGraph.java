package io.intino.cesar.graph;

import io.intino.tara.magritte.Graph;

public class TestCesarGraph extends io.intino.cesar.graph.AbstractGraph {

	public TestCesarGraph(Graph graph) {
		super(graph);
	}

	public TestCesarGraph(Graph graph, TestCesarGraph wrapper) {
		super(graph, wrapper);
	}
}