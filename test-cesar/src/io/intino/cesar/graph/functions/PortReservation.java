package io.intino.cesar.graph.functions;

@FunctionalInterface
public interface PortReservation {

	int reservePort();
}
