package io.intino.cesar.graph.functions;

@FunctionalInterface
public interface SaveImage {

	void save(byte[] image);
}
