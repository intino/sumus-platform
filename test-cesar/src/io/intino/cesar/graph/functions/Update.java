package io.intino.cesar.graph.functions;

@FunctionalInterface
public interface Update {

	void update();
}
