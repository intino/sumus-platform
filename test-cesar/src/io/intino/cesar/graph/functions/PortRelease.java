package io.intino.cesar.graph.functions;

@FunctionalInterface
public interface PortRelease {

	void release(int port);
}
