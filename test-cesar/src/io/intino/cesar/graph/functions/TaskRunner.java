package io.intino.cesar.graph.functions;

@FunctionalInterface
public interface TaskRunner extends Runnable {

	void run();
}
