package io.intino.cesar.graph.functions;


@FunctionalInterface
public interface FitsSystem {

	boolean fits(io.intino.cesar.graph.System.Deployment deployment);
}
