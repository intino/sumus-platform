package io.intino.cesar.graph;

public abstract class Asset extends AbstractAsset {

	public Asset(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public boolean isCompromised() {
		throw new UnsupportedOperationException("Implement in lower levels");
	}

	public abstract Status status();

	public abstract boolean isRunningOutOfDisk();

	public abstract boolean isRunningOutOfMemory();

	public abstract boolean isRunningOutOfCpu();
}