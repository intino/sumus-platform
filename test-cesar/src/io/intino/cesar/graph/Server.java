package io.intino.cesar.graph;

import io.intino.tara.magritte.Node;

import java.time.Instant;
import java.util.List;

import static java.time.temporal.ChronoUnit.HOURS;

public class Server extends AbstractServer {
	protected ServerStatus status;

	public Server(Node node) {
		super(node);
	}

	public ServerStatus status() {
		if (status != null) return status;
		if (statusRef() == null || statusRef().isEmpty()) return null;
		Node node = graph().core$().clone().load(statusRef(), false);
		return node != null ? status = node.as(ServerStatus.class) : null;
	}

	public Server status(ServerStatus status) {
		this.statusRef = status.core$().id();
		this.status = status;
		return this;
	}

	@Override
	protected void load$(String name, List<?> values) {
		super.load$(name, values);
		if (name.equals("statusRef")) status();
	}

	public boolean isCompromised() {
		return isDisconnected() || isRunningOutOfDisk() || isRunningOutOfMemory();
	}

	public boolean isDisconnected() {
		return status == null || (status() != null && status().created().until(Instant.now(), HOURS) > graph().configuration().disconnectedTimeThreshold());
	}

	public boolean isRunningOutOfDisk() {
		return status().diskUsage() > 90;
	}

	public boolean isRunningOutOfMemory() {
		return status().memoryUsage() > 90;
	}

	@Override
	public boolean isRunningOutOfCpu() {
		return false;
	}
}