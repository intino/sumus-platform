package io.intino.cesar.graph;

import io.intino.tara.magritte.Node;

import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;

import static java.time.temporal.ChronoUnit.HOURS;

public class Device extends AbstractDevice {

	private DeviceStatus status;

	public Device(Node node) {
		super(node);
	}

	public void update(Consumer<Device> updater) {
		DeviceStatus status = status();
		if (status != null) {
			final double temperature = status.temperature();
			if (temperature < temperature().min()) temperature().min(temperature);
			else if (temperature > temperature().max()) temperature().max(temperature);
			updater.accept(this);
		}
	}

	public DeviceStatus status() {
		if (status != null) return status;
		if (statusRef() == null || statusRef().isEmpty()) return null;
		Node node = graph().core$().clone().load(statusRef(), false);
		return node != null ? status = node.as(DeviceStatus.class) : null;
	}

	public Asset status(DeviceStatus status) {
		this.statusRef = status.core$().id();
		this.status = status;
		return this;
	}

	@Override
	protected void load$(String name, List<?> values) {
		super.load$(name, values);
		if (name.equals("statusRef")) status();
	}

	public boolean isCompromised() {
		return isDisconnected() || lowBattery() || isHot() || isUnPlugged() || !isScreenOn();
	}

	public boolean isDisconnected() {
		return status == null || (status() != null && status().created().until(Instant.now(), HOURS) > graph().configuration().disconnectedTimeThreshold());
	}

	public boolean veryLowBattery() {
		return status().battery() < graph().configuration().deviceLowBatteryThreshold();
	}

	public boolean lowBattery() {
		return status().battery() < graph().configuration().deviceBatteryThreshold();
	}

	public boolean isHot() {
		return status().temperature() >= graph().configuration().deviceTemperatureThreshold();
	}

	public boolean isUnPlugged() {
		return !status().isPlugged();
	}

	public boolean isScreenOn() {
		return status().isScreenOn();
	}

	public boolean isRunningOutOfMemory() {
		return false;
	}

	public boolean isRunningOutOfCpu() {
		return false;
	}

	public boolean isRunningOutOfDisk() {
		return false;
	}
}