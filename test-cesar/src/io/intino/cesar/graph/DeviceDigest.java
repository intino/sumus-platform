package io.intino.cesar.graph;

import io.intino.sumus.datawarehouse.store.Digest;

import java.time.Instant;

public class DeviceDigest {

	private static final String CPU_USAGE = "cpuUsage";
	private static final String BATTERY = "battery";
	private static final String BATTERY_MIN = "batteryMin";
	private static final String IS_SCREEN_ON = "isScreenOn";
	private static final String TEMPERATURE = "temperature";
	private static final String IS_PLUGGED = "isPlugged";
	private static final String COUNT = "count";
	private final Digest digest;

	public DeviceDigest(Digest digest) {
		this.digest = digest;
	}

	public int isScreenOn() {
		return digest.intOf(IS_SCREEN_ON);
	}

	public DeviceDigest addIsScreenOn(boolean value) {
		digest.add(IS_SCREEN_ON, value ? 1 : 0);
		return this;
	}

	public double battery() {
		return digest.floatOf(BATTERY);
	}

	public DeviceDigest addBattery(double value) {
		digest.add(BATTERY, (float) value);
		return batteryMin(value);
	}

	private DeviceDigest batteryMin(double value) {
		digest.set(BATTERY_MIN, (float) Math.min(batteryMin(), value));
		return this;
	}

	private float batteryMin() {
		return digest.floatOf(BATTERY_MIN);
	}

	public double cpuUsage() {
		return digest.floatOf(CPU_USAGE);
	}

	public DeviceDigest addCpuUsage(double value) {
		digest.add(CPU_USAGE, (float) value);
		return this;
	}

	public DeviceDigest addTemperature(double value) {
		digest.add(TEMPERATURE, (float) value);
		return this;
	}

	public double temperature() {
		return digest.floatOf(TEMPERATURE);
	}

	public int isPlugged() {
		return digest.intOf(IS_PLUGGED);
	}

	public DeviceDigest addIsPlugged(boolean value) {
		digest.add(IS_PLUGGED, value ? 1 : 0);
		return this;
	}

	public int count() {
		return digest.intOf(COUNT);
	}

	public DeviceDigest addCount() {
		digest.add(COUNT, 1);
		return this;
	}

	public Instant created() {
		return digest.ts();
	}

	public DeviceDigest save() {
		digest.save();
		return this;
	}
}
