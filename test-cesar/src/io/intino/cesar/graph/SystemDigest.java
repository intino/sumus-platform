package io.intino.cesar.graph;

import io.intino.sumus.datawarehouse.store.Digest;

import java.time.Instant;

public class SystemDigest {

	private static final String USED_CPU = "usedCPU";
	private static final String THREADS = "threads";
	private static final String USED_MEMORY = "usedMemory";
	private static final String COUNT = "count";

	private final Digest digest;

	public SystemDigest(Digest digest) {
		this.digest = digest;
	}

	public Instant created() {
		return digest.ts();
	}

	public double usedCPU() {
		return digest.floatOf(USED_CPU);
	}

	public double usedMemory() {
		return digest.floatOf(USED_MEMORY);
	}

	public int threads() {
		return digest.intOf(THREADS);
	}

	public SystemDigest addUsedCPU(double value) {
		digest.set(USED_CPU, (float) value);
		return this;
	}

	public SystemDigest addUsedMemory(double value) {
		digest.set(USED_MEMORY, (float) value);
		return this;
	}

	public SystemDigest addThreads(int value) {
		digest.set(THREADS, value);
		return this;
	}

	public SystemDigest addCount() {
		digest.add(COUNT, 1);
		return this;
	}


	public SystemDigest save() {
		digest.save();
		return this;
	}

	public String ref() {
		return digest.ref();
	}
}
