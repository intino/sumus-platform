package io.intino.cesar.graph;

import io.intino.tara.magritte.Node;

public class System extends AbstractSystem {

	private SystemStatus status;

	public System(Node node) {
		super(node);
	}

	public SystemStatus status() {
		if (statusRef() == null || statusRef().isEmpty()) return null;
		Node node = graph().core$().clone().load(statusRef(), false);
		return node != null ? node.as(SystemStatus.class) : null;
	}

	public System status(SystemStatus status) {
		this.statusRef = status.core$().id();
		this.status = status;
		return this;
	}

	public boolean isRunningOutOfMemory() {
		return false;
	}

	public boolean isRunningOutOfCpu() {
		return false;
	}
}