package io.intino.cesar.graph;

import io.intino.tara.magritte.Node;

import java.util.List;

public class Feeder extends AbstractFeeder {

	public Feeder(Node node) {
		super(node);
	}


	public FeederStatus status() {
		if (status != null) return (FeederStatus) status;
		if (statusRef() == null || statusRef().isEmpty()) return null;
		Node node = graph().core$().clone().load(statusRef(), false);
		return node != null ? (FeederStatus) (status = node.as(FeederStatus.class)) : null;
	}

	public Feeder status(FeederStatus status) {
		this.statusRef = status.core$().id();
		this.status = status;
		return this;
	}

	@Override
	protected void load$(String name, List<?> values) {
		super.load$(name, values);
		if (name.equals("statusRef")) status();
	}

	public boolean isHot() {
		return status() != null && status().temperature() > 70;
	}
}