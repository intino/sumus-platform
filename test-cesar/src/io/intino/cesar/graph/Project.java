package io.intino.cesar.graph;

import java.util.List;
import java.util.stream.Collectors;

public class Project extends AbstractProject {

	public Project(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public List<Server> servers() {
		return this.graph().serverList(s -> this.equals(s.owner()) && !s.i$(Feeder.class)).collect(Collectors.toList());
	}

	public List<Feeder> feeders() {
		return this.graph().feederList(s -> this.equals(s.owner())).collect(Collectors.toList());
	}

	public List<Device> devices() {
		return this.graph().deviceList(s -> this.equals(s.owner())).collect(Collectors.toList());
	}
}