package io.intino.cesar;


import io.intino.cesar.graph.AbstractSystem.Deployment;
import io.intino.cesar.graph.*;
import io.intino.cesar.graph.System;
import io.intino.sumus.datawarehouse.store.Digest;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Olap;
import io.intino.tara.magritte.Layer;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static java.util.Collections.singletonList;

public class Graph {

	public static boolean fitsNewSystem(Consul consul, System.Deployment system) {
//		final Server server = consul.owner().as(Server.class);
//		return server.as(Server.class).memory().free() > system.prerequisites().memory() &&
//				server.as(Server.class).hDD().free() > system.prerequisites().hdd() &&
//				!isAlreadyDeployed(system);
		return true;
	}

	public static boolean isAlive(ServerConsul self) {
		return !self.core$().ownerAs(Server.class).isDisconnected();
	}

	public static int reservePort(Server self) {
		final Integer last = self.usedPorts(self.usedPorts().size() - 1);
		final int newPort = unusedPort(self.usedPorts(), last);
		self.usedPorts().add(newPort);
		return newPort;
	}

	private static int unusedPort(List<Integer> ports, Integer last) {
		int newPort = last;
		while (ports.contains(newPort)) newPort = newPort - 1;
		return newPort;
	}

	public static void shutdown(ShutDown self) {
		;
	}

	public static void moveService(Move self) {
		;
	}

	public static void releasePort(Server self, int port) {
		self.usedPorts().remove((Integer) port);
	}

	public static void saveScreen(Device.Screen self, byte[] image) {
		self.current(new ByteArrayInputStream(image), "infrastructure/devices/" + self.core$().owner().name() + "/screens/" + Instant.now().atOffset(UTC).getHour() + ".jpg");
	}

	public static Boolean isRunning(System self) {
		return !self.deploymentList().isEmpty() && self.deploymentList().stream().anyMatch(Deployment::success);
	}

	public static String statusCategorization(Categorization self, Layer item) {
		return item.core$().as(Asset.class).isCompromised() ? "Compromised" : "OK";
	}

	public static String screenCategorization(Categorization self, Layer item) {
		Device device = item.core$().as(Device.class);
		return device.status() != null && device.isScreenOn() ? "On" : "Off";
	}

	public static String batteryCategorization(Categorization self, Layer item) {
		Device device = item.core$().as(Device.class);
		return device.status() != null && device.lowBattery() ? "Low" : "High";
	}

	public static String pluggedCategorization(Categorization self, Layer item) {
		Device device = item.core$().as(Device.class);
		return device.status() != null && device.isUnPlugged() ? "Off" : "On";
	}

	public static String connectionCategorization(Categorization self, Layer item) {
		Device device = item.core$().as(Device.class);
		return device.status() != null && device.isDisconnected() ? "Disconnected" : "Connected";
	}

	public static double screenDigest(MeasureIndicator.Formula self, List<Digest> digests) {
		return ratio(digests, digests.stream().mapToDouble(s -> new DeviceDigest(s).isScreenOn()).sum()) * 100;
	}

	public static double batteryDigest(MeasureIndicator.Formula self, List<Digest> digests) {
		return ratio(digests, digests.stream().mapToDouble(s -> new DeviceDigest(s).battery()).sum());
	}

	public static double temperatureDigest(MeasureIndicator.Formula self, List<Digest> digests) {
		return ratio(digests, digests.stream().mapToDouble(s -> new DeviceDigest(s).temperature()).sum());
	}

	private static double ratio(List<Digest> digests, double sum) {
		int count = digests.stream().mapToInt(s -> new DeviceDigest(s).count()).sum();
		return count == 0 ? 0 : (sum / count);
	}

	public static List<NameSpace> cesarOlapNameSpaces(Olap self, String username) {
		return singletonList(self.graph().core$().as(TestCesarGraph.class).default$());
	}

}
