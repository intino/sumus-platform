package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;

import static io.intino.sumus.box.SumusDisplayHelper.entities;
import static io.intino.sumus.box.SumusDisplayHelper.entity;
import static io.intino.sumus.box.SumusDisplayHelper.sumusBox;

public class TestCesarSimpleSystemCatalog extends AbstractTestCesarSimpleSystemCatalog {

	public TestCesarSimpleSystemCatalog(TestCesarBox box) {
		super(box);
	}

	public static class Toolbar {
	}

	public static class Source {
		public static java.util.List<io.intino.cesar.graph.System> systemList(TestCesarBox box, io.intino.konos.alexandria.activity.model.catalog.Scope scope, String condition, String username) {
			return entities(sumusBox(box), io.intino.cesar.graph.System.class, scope, condition, username);
		}

		public static io.intino.cesar.graph.System system(TestCesarBox box, String id, String username) {
			return entity(sumusBox(box), io.intino.cesar.graph.System.class, id, username);
		}

		public static io.intino.cesar.graph.System rootSystem(TestCesarBox box, java.util.List<io.intino.cesar.graph.System> objects, String username) {
			return null;
		}

		public static io.intino.cesar.graph.System defaultSystem(TestCesarBox box, String id, String username) {
			return null;
		}

		public static String systemId(TestCesarBox box, io.intino.cesar.graph.System system) {
			return system.core$().id();
		}

		public static String systemName(TestCesarBox box, io.intino.cesar.graph.System system) {
			return system.name$();
		}
	}

	public static class Events {
	}

	public static class Views {
	}

	public static class Arrangements {
	}

}