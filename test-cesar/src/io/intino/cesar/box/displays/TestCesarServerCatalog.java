package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Server;
import io.intino.sumus.box.SumusDisplayHelper;

import static io.intino.sumus.box.SumusDisplayHelper.sumusBox;

public class TestCesarServerCatalog extends AbstractTestCesarServerCatalog {

	public TestCesarServerCatalog(TestCesarBox box) {
		super(box);
	}

	public static class Toolbar {
	}

	public static class Source {
		public static java.util.List<io.intino.cesar.graph.Server> serverList(TestCesarBox box, io.intino.konos.alexandria.activity.model.catalog.Scope scope, String condition, String username) {
			return SumusDisplayHelper.entities(sumusBox(box), Server.class, scope, condition, username);
		}

		public static io.intino.cesar.graph.Server server(TestCesarBox box, String id, String username) {
			return SumusDisplayHelper.entity(sumusBox(box), Server.class, id, username);
		}

		public static io.intino.cesar.graph.Server rootServer(TestCesarBox box, java.util.List<io.intino.cesar.graph.Server> objects, String username) {
			return null;
		}

		public static io.intino.cesar.graph.Server defaultServer(TestCesarBox box, String id, String username) {
			return null;
		}

		public static String serverId(TestCesarBox box, io.intino.cesar.graph.Server server) {
			return server.core$().id();
		}

		public static String serverName(TestCesarBox box, io.intino.cesar.graph.Server server) {
			return server.name$();
		}
	}

	public static class Events {
	}

	public static class Views {
	}

	public static class Arrangements {
	}
}