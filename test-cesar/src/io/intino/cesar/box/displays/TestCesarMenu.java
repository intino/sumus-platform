package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Project;
import io.intino.cesar.graph.TestCesarGraph;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.renders.RenderObjects;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class TestCesarMenu extends AbstractTestCesarMenu {

	public TestCesarMenu(TestCesarBox box) {
		super(box);
	}

	@Override
	protected void init() {
		super.init();
	}

	public static class ProjectsGroup {
		public static class Projects {
			public static String label(TestCesarBox box, Element element, Object object) {
				return ((Project)object).label();
			}

			public static String icon(TestCesarBox box, Element element, Object object) {
				return null;
			}

			public static Integer bubble(TestCesarBox box, Element element, Object object) {
				return -1;
			}

			public static List<RenderObjects.Source.Entry> objects(TestCesarBox box) {
				TestCesarGraph graph = box.graph().core$().as(TestCesarGraph.class);
				return graph.projectList().stream().map(Projects::entryOf).collect(toList());
			}

			private static RenderObjects.Source.Entry entryOf(Project p) {
				return new RenderObjects.Source.Entry() {
					@Override
					public String id() {
						return p.core$().id();
					}

					@Override
					public String name() {
						return p.name$();
					}

					@Override
					public Object object() {
						return p;
					}
				};
			}
		}
	}
}