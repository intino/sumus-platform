package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Device;
import io.intino.sumus.box.SumusDisplayHelper;

import static io.intino.sumus.box.SumusDisplayHelper.entity;
import static io.intino.sumus.box.SumusDisplayHelper.sumusBox;

public class TestCesarSimpleDeviceCatalog extends AbstractTestCesarSimpleDeviceCatalog {

	public TestCesarSimpleDeviceCatalog(TestCesarBox box) {
		super(box);
	}

	public static class Toolbar {
	}

	public static class Source {
		public static java.util.List<io.intino.cesar.graph.Device> deviceList(TestCesarBox box, io.intino.konos.alexandria.activity.model.catalog.Scope scope, String condition, String username) {
			return SumusDisplayHelper.entities(sumusBox(box), io.intino.cesar.graph.Device.class, scope, condition, username);
		}

		public static io.intino.cesar.graph.Device device(TestCesarBox box, String id, String username) {
			return entity(sumusBox(box), Device.class, id, username);
		}

		public static io.intino.cesar.graph.Device rootDevice(TestCesarBox box, java.util.List<io.intino.cesar.graph.Device> objects, String username) {
			return null;
		}

		public static io.intino.cesar.graph.Device defaultDevice(TestCesarBox box, String id, String username) {
			return null;
		}

		public static String deviceId(TestCesarBox box, io.intino.cesar.graph.Device device) {
			return device.core$().id();
		}

		public static String deviceName(TestCesarBox box, io.intino.cesar.graph.Device device) {
			return device.name$();
		}
	}

	public static class Events {
	}

	public static class Views {
	}

	public static class Arrangements {
	}

}