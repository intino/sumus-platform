package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.helpers.OlapHelper;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;

import java.util.function.Consumer;

public class TestCesarOverviewPanel extends AbstractTestCesarOverviewPanel {

	public TestCesarOverviewPanel(TestCesarBox box) {
		super(box);
	}

	public static class Toolbar {

	}

	public static class Views {
		public static AlexandriaDisplay cesarOlap(TestCesarBox box, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener) {
			return OlapHelper.cesarOlap(box, loadingListener, instantListener);
		}
	}
}