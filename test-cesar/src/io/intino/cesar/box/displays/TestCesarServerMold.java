package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.*;

import java.time.Instant;

public class TestCesarServerMold extends AbstractTestCesarServerMold {

	public TestCesarServerMold(TestCesarBox box) {
		super(box);
	}

	public static class Stamps {

		public static class Title {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.label();
			}
		}

		public static class NoData {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.status() == null ? "warning" : null;
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.label();
			}
		}

		public static class CpuIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.a$(Asset.class).status() == null ? null : "icons:work";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				ServerStatus status = server.status();
				return status == null ? "" : "color:green;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				Status status = server.a$(Asset.class).status();
				return status == null ? null : status.a$(ServerStatus.class).cpuUsage() + " %";
			}
		}

		public static class HddIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.a$(Asset.class).status() == null ? null : "notification:disc-full";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				Asset asset = server.a$(Asset.class);
				if (asset.status() == null) return "";
				return asset.isRunningOutOfDisk() ? "color:red;" : "color:green;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				Asset asset = server.a$(Asset.class);
				Status status = asset.status();
				if (status == null || asset.i$(Device.class)) return "";
				return status.a$(ServerStatus.class).diskUsage() + " %";
			}
		}

		public static class TimeIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				return server.status() == null ? null : "device:access-time";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				boolean disconnected = server.isDisconnected();
				return !disconnected ? "color:green;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Server server, String username) {
				if (server.status() == null) return null;
				Instant instant = server.status().created();
				return String.format("date(%s,LLL)", instant.toEpochMilli());
			}
		}

	}
}