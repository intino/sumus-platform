package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;

import static java.util.Collections.singletonList;

public class TestCesarProjectDashboard extends AbstractTestCesarProjectDashboard {

	public TestCesarProjectDashboard(TestCesarBox box) {
		super(box);
	}
	public static class Stamps {


		public static class OverviewPicture {
			public static java.util.List<java.net.URL> value(TestCesarBox box, java.lang.Object object, String username) {
				return singletonList(TestCesarProjectDashboard.class.getResource("/images/dashboard.png"));
			}
		}


		public static class SystemsStampCatalog {
		}

		public static class ServersStampCatalog {
		}

		public static class DeviceStampCatalog {
		}

	}
}