package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Configuration;
import io.intino.cesar.graph.TestCesarGraph;

import static java.lang.String.valueOf;

public class TestCesarAdministrationMold extends AbstractTestCesarAdministrationMold {

	public TestCesarAdministrationMold(TestCesarBox box) {
		super(box);
	}

	public static class Stamps {

		public static class Notifications {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration self, String username) {
				return configuration(box).notifications() ? "Notifications active" : "Notifications off";
			}
			public static String color(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return configuration(box).notifications() ? "#388e3c" : "#9d9d9d";
			}
		}

		public static class Icono {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return configuration(box).notifications() ? "social:notifications-active" : "social:notifications-off";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return "";
			}
		}

		public static class DeviceTemperatureThreshold {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return valueOf(configuration(box).deviceTemperatureThreshold());
			}
		}

		public static class DeviceBatteryThreshold {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return valueOf(configuration(box).deviceBatteryThreshold()) + "%";
			}
		}

		public static class DeviceLowBatteryThreshold {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return valueOf(configuration(box).deviceLowBatteryThreshold()) + "%";
			}
		}

		public static class DeviceDisconnectedTimeThreshold {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return valueOf(configuration(box).disconnectedTimeThreshold()) + " h";
			}
		}

		public static class Dummy {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return null;
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Configuration configuration, String username) {
				return "";
			}
		}

		public static class EditAlert {
			public static String path(TestCesarBox box, String configuration, String username) {
				return "configuration/alert/" + configuration;
			}
		}

		private static Configuration configuration(TestCesarBox box) {
			return box.graph().core$().as(TestCesarGraph.class).configuration();
		}

	}

}