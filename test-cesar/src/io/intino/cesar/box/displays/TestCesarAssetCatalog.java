package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Asset;
import io.intino.konos.alexandria.activity.Resource;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.model.toolbar.TaskSelection;
import io.intino.sumus.Category;
import io.intino.sumus.box.SumusDisplayHelper;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

import static io.intino.sumus.box.SumusDisplayHelper.*;

public class TestCesarAssetCatalog extends AbstractTestCesarAssetCatalog {

	public TestCesarAssetCatalog(TestCesarBox box) {
		super(box);
	}

	public static class Toolbar {

		public static TaskSelection.Refresh taskSelection(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element element, String option, java.util.List<io.intino.cesar.graph.Asset> selection, String username) {
			selection.forEach(r -> java.lang.System.out.println("borrar elemento: " + r.name$()));;
			return TaskSelection.Refresh.Catalog;
		}

		public static Resource download(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element element, String option, String username) {
			return new Resource() {
				@Override
				public String label() {
					return "descarga.pdf";
				}

				@Override
				public InputStream content() {
					return new ByteArrayInputStream(new byte[0]);
				}
			};
		}

		public static Resource downloadSelection(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element element, String option, java.util.List<io.intino.cesar.graph.Asset> selection, String username) {
			return new Resource() {
				@Override
				public String label() {
					return "descargaElementos.pdf";
				}

				@Override
				public InputStream content() {
					return new ByteArrayInputStream(new byte[0]);
				}
			};
		}

		public static Resource export(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element element, java.time.Instant from, java.time.Instant to, String username) {
			return new Resource() {
				@Override
				public String label() {
					return "exporta.pdf";
				}

				@Override
				public InputStream content() {
					return new ByteArrayInputStream(new byte[0]);
				}
			};
		}

		public static Resource exportSelection(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element element, java.time.Instant from, java.time.Instant to, java.util.List<io.intino.cesar.graph.Asset> selection, String username) {
			return new Resource() {
				@Override
				public String label() {
					return "exportaElementos.pdf";
				}

				@Override
				public InputStream content() {
					return new ByteArrayInputStream(new byte[0]);
				}
			};
		}
	}


	public static class Source {
		public static java.util.List<io.intino.cesar.graph.Asset> assetList(TestCesarBox box, io.intino.konos.alexandria.activity.model.catalog.Scope scope, String condition, String username) {
			return entities(sumusBox(box), Asset.class, scope, condition, username);
		}

		public static io.intino.cesar.graph.Asset asset(TestCesarBox box, String id, String username) {
			return entity(sumusBox(box), Asset.class, id, username);
		}

		public static io.intino.cesar.graph.Asset rootAsset(TestCesarBox box, java.util.List<io.intino.cesar.graph.Asset> objects, String username) {
			return null;
		}

		public static io.intino.cesar.graph.Asset defaultAsset(TestCesarBox box, String id, String username) {
			return null;
		}

		public static String assetId(TestCesarBox box, io.intino.cesar.graph.Asset asset) {
			return asset.core$().id();
		}

		public static String assetName(TestCesarBox box, io.intino.cesar.graph.Asset asset) {
			return asset.name$();
		}
	}

	public static class Events {

	}

	public static class Views {
		public static AlexandriaDisplay cesarOlapScope(TestCesarBox box, io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.model.catalog.Scope scope) {
			return null;//TODO
		}

		public static AlexandriaDisplay cesarOlap(TestCesarBox box, io.intino.konos.alexandria.activity.model.Element context, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener, String username) {
			return null;//TODO
		}
	}

	public static class Arrangements {
		public static java.util.List<io.intino.konos.alexandria.activity.model.catalog.arrangement.Group> unit(TestCesarBox box, java.util.List<io.intino.cesar.graph.Asset> items, String username) {
			List<Category> categoryList = box.graph().unitCategorization().calculate(items, userKeys(sumusBox(box), username));
			return SumusDisplayHelper.groups(categoryList, items);
		}

		public static java.util.List<io.intino.konos.alexandria.activity.model.catalog.arrangement.Group> status(TestCesarBox box, java.util.List<io.intino.cesar.graph.Asset> items, String username) {
			List<Category> categoryList = box.graph().statusCategorization().calculate(items, userKeys(sumusBox(box), username));
			return SumusDisplayHelper.groups(categoryList, items);
		}

		public static void createGroup(TestCesarBox box, io.intino.konos.alexandria.activity.model.Catalog infrastructure, String grouping, io.intino.konos.alexandria.activity.model.catalog.arrangement.Group group, String username) {
			SumusDisplayHelper.createGroup(sumusBox(box), infrastructure, grouping, group, username);
		}

		public static io.intino.konos.alexandria.activity.model.Catalog.ArrangementFilterer filterer(TestCesarBox box, String username) {
			return SumusDisplayHelper.createArrangementFilterer(username);
		}
	}

}