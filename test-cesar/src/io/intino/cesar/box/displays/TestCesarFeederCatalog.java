package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Feeder;

import static io.intino.sumus.box.SumusDisplayHelper.*;

public class TestCesarFeederCatalog extends AbstractTestCesarFeederCatalog {

	public TestCesarFeederCatalog(TestCesarBox box) {
		super(box);
	}

	public static class Source {
		public static java.util.List<io.intino.cesar.graph.Feeder> feederList(TestCesarBox box, io.intino.konos.alexandria.activity.model.catalog.Scope scope, String condition, String username) {
			return entities(sumusBox(box), Feeder.class, scope, condition, username);
		}

		public static io.intino.cesar.graph.Feeder feeder(TestCesarBox box, String id, String username) {
			return entity(sumusBox(box), Feeder.class, id, username);
		}

		public static io.intino.cesar.graph.Feeder rootFeeder(TestCesarBox box, java.util.List<io.intino.cesar.graph.Feeder> objects, String username) {
			return null;
		}

		public static io.intino.cesar.graph.Feeder defaultFeeder(TestCesarBox box, String id, String username) {
			return null;
		}

		public static String feederId(TestCesarBox box, io.intino.cesar.graph.Feeder feeder) {
			return feeder.core$().id();
		}

		public static String feederName(TestCesarBox box, io.intino.cesar.graph.Feeder feeder) {
			return feeder.name$();
		}
	}

	public static class Events {
	}

	public static class Views {
	}

	public static class Arrangements {
	}

}