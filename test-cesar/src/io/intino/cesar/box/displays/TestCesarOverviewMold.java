package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;

import static java.util.Collections.singletonList;

public class TestCesarOverviewMold extends AbstractTestCesarOverviewMold {

	public TestCesarOverviewMold(TestCesarBox box) {
		super(box);
	}
	public static class Stamps {

		public static class OverviewPicture {

			public static java.util.List<java.net.URL> value(TestCesarBox box, java.lang.Object object, String username) {
				return singletonList(TestCesarOverviewMold.class.getResource("/images/overview.png"));
			}
		}

	}
}