package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Asset;

import java.time.Instant;

public class TestCesarSystemMold extends AbstractTestCesarSystemMold {

	public TestCesarSystemMold(TestCesarBox box) {
		super(box);
	}

	public static class Stamps {

		public static class Name {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.label();
			}
		}

		public static class NoData {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.status() == null ? "warning" : null;
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.label();
			}
		}

		public static class CpuIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return !system.isRunningOutOfCpu() ? "color:green;" : "color:red;";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return !system.isRunningOutOfCpu() ? "color:green;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return String.valueOf(system.status().cpuUsage()) + " %";
			}
		}

		public static class Memory {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.i$(Asset.class) && system.a$(Asset.class).status() == null || system.status() == null ? null : "hardware:memory";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return !system.isRunningOutOfMemory() ? "color:green;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return String.valueOf(system.status().memoryUsage()) + " %";
			}
		}

		public static class Threads {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.status() == null ? null : "device:usb";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return "color:green;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return "";
				return String.valueOf(system.status().threads()) + " threads";
			}
		}

		public static class TimeIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.status() == null ? null : "device:access-time";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				return system.isRunning() ? "color:green;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.System system, String username) {
				if (system.status() == null) return null;
				Instant instant = system.status().created();
				return String.format("date(%s,LLL)", instant.toEpochMilli());
			}
		}

	}
}