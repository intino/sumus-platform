package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.konos.alexandria.activity.displays.AlexandriaLayout;

public class TestCesarDesktop extends AbstractTestCesarDesktop {

    public TestCesarDesktop(TestCesarBox box) {
        super(box);
    }

	public static AlexandriaLayout layout(TestCesarBox box) {
		return new TestCesarMenu(box);
	}
}