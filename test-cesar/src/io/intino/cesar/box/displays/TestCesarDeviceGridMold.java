package io.intino.cesar.box.displays;

import io.intino.cesar.Graph;
import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.helpers.DeviceMoldHelper;
import io.intino.cesar.graph.DeviceStatus;

import static java.util.Collections.singletonList;

public class TestCesarDeviceGridMold extends AbstractTestCesarDeviceGridMold {

	public TestCesarDeviceGridMold(TestCesarBox box) {
		super(box);
	}
	public static class Stamps {

		public static class Image {
			public static java.util.List<java.net.URL> value(TestCesarBox box, io.intino.cesar.graph.Device device, String username) {
				DeviceStatus status = device.status();
				if (status == null) return singletonList(Graph.class.getResource("/images/tablet_no_status.png"));
				if (!status.isScreenOn())
					return singletonList(Graph.class.getResource("/images/tablet_turnoff.png"));

				return device.screen().current() != null ? singletonList(device.screen().current()) : null;
			}
		}

		public static class Name {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device device, String username) {
				return DeviceMoldHelper.Name.value(box, device, username);
			}
		}

		public static class NoData {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.title(box, asset, username);
			}
		}

		public static class BatteryIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.value(box, asset, username);
			}

			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.style(box, asset, username);
			}

			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.title(box, asset, username);
			}
		}

		public static class HotIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.title(box, asset, username);
			}
		}

		public static class TimeIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.title(box, asset, username);
			}
		}

	}
}