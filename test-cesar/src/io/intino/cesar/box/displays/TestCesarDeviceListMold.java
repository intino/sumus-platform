package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.helpers.DeviceMoldHelper;

public class TestCesarDeviceListMold extends AbstractTestCesarDeviceListMold {

	public TestCesarDeviceListMold(TestCesarBox box) {
		super(box);
	}

	public static class Stamps {

		public static class Name {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device device, String username) {
				return DeviceMoldHelper.Name.value(box, device, username);
			}
		}

		public static class NoData {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.NoData.title(box, asset, username);
			}
		}

		public static class BatteryIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.value(box, asset, username);
			}

			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.style(box, asset, username);
			}

			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.BatteryIcon.title(box, asset, username);
			}
		}

		public static class HotIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.HotIcon.title(box, asset, username);
			}
		}

		public static class TimeIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.value(box, asset, username);
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.style(box, asset, username);
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
				return DeviceMoldHelper.TimeIcon.title(box, asset, username);
			}
		}

	}
}