package io.intino.cesar.box.displays;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.*;

import java.time.Instant;

public class TestCesarFeederMold extends AbstractTestCesarFeederMold {

	public TestCesarFeederMold(TestCesarBox box) {
		super(box);
	}

	public static class Stamps {

		public static class Title {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.label();
			}
		}

		public static class NoData {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.status() == null ? "warning" : null;
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.label();
			}
		}

		public static class HotIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return !feeder.i$(Device.class) || feeder.a$(Device.class).status() == null ? null : "social:whatshot";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				if (feeder.status() == null) return "";
				return !feeder.isHot() ? "color:blue;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				if (feeder.status() == null) return "";
				return String.valueOf(feeder.status().temperature()) + " º";
			}
		}

		public static class CpuIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.status() == null ? "" : "color:green;";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.status() == null ? "" : "color:green;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				Status status = feeder.a$(Asset.class).status();
				return status == null ? null : status.a$(ServerStatus.class).cpuUsage() + " %";
			}
		}

		public static class HddIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.a$(Asset.class).status() == null ? null : "notification:disc-full";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				Asset asset = feeder.a$(Asset.class);
				if (asset.status() == null) return "";
				return asset.isRunningOutOfDisk() ? "color:red;" : "color:green;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				Asset asset = feeder.a$(Asset.class);
				Status status = asset.status();
				if (status == null || asset.i$(Device.class)) return "";
				return status.a$(ServerStatus.class).diskUsage() + " %";
			}
		}

		public static class TimeIcon {
			public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				return feeder.a$(Asset.class).status() == null ? null : "device:access-time";
			}
			public static String style(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				if (feeder.a$(Asset.class).status() == null) return "";
				boolean disconnected = feeder.i$(Server.class) ? feeder.a$(Server.class).isDisconnected() : feeder.a$(Device.class).isDisconnected();
				return !disconnected ? "color:green;" : "color:red;";
			}
			public static String title(TestCesarBox box, io.intino.cesar.graph.Feeder feeder, String username) {
				if (feeder.a$(Asset.class).status() == null) return null;
				Instant instant = feeder.i$(Server.class) ? feeder.a$(Server.class).status().created() : feeder.a$(Device.class).status().created();
				return String.format("date(%s,LLL)", instant.toEpochMilli());
			}
		}

	}
}