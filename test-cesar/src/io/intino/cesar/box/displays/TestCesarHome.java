package io.intino.cesar.box.displays;

import io.intino.konos.alexandria.exceptions.*;
import io.intino.cesar.box.*;
import io.intino.cesar.box.schemas.*;
import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.displays.notifiers.TestCesarHomeNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.services.push.User;


public class TestCesarHome extends AlexandriaDisplay<TestCesarHomeNotifier> {
    private TestCesarBox box;

    public TestCesarHome(TestCesarBox box) {
        super();
        this.box = box;
    }


}