package io.intino.cesar.box.actions;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.displays.TestCesarDesktop;
import io.intino.konos.alexandria.activity.spark.actions.AlexandriaPageAction;

public class TestCesarHomePageAction extends AlexandriaPageAction {

	public TestCesarBox box;


	public TestCesarHomePageAction() { super("test-cesar"); }

	public String execute() {
		return super.template("testCesarHomePage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
	    return new io.intino.konos.alexandria.activity.displays.Soul(session) {
			@Override
			public void personify() {
				TestCesarDesktop display = new TestCesarDesktop(box);
				register(display);
				display.personify();
			}
		};
	}

	@Override
	protected String title() {
		return "";
	}

	@Override
	protected java.net.URL favicon() {
		return null;
	}
}