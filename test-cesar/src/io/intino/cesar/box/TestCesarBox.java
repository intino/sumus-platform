package io.intino.cesar.box;

import io.intino.cesar.graph.TestCesarGraph;
import io.intino.tara.magritte.Graph;

public class TestCesarBox extends AbstractBox {
	private TestCesarGraph graph;

	public TestCesarBox(String[] args) {
		super(args);
	}

	public TestCesarBox(TestCesarConfiguration configuration) {
		super(configuration);
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		if (o instanceof Graph) this.graph = ((Graph) o).as(TestCesarGraph.class);
		super.put(o);
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		return super.open();
	}

	public void close() {
		super.close();
	}

	public TestCesarGraph graph() {
		return graph;
	}

	static io.intino.konos.alexandria.activity.services.AuthService authService(java.net.URL authServiceUrl) {
		//TODO add your authService
		return null;
	}	
}