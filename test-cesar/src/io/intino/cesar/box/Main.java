package io.intino.cesar.box;

import io.intino.cesar.graph.Project;
import io.intino.cesar.graph.TestCesarGraph;
import io.intino.sumus.SumusStore;
import io.intino.tara.io.Stash;
import io.intino.tara.magritte.Graph;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		ArrayList<String> arguments = new ArrayList<>(Arrays.asList(args));
		arguments.add("logo=/images/logo.png");
		arguments.add("title=Cesar");
		arguments.add("subtitle=monentia.devOps()");
		TestCesarConfiguration configuration = new TestCesarConfiguration(arguments.toArray(new String[0]));
		TestCesarGraph cesarGraph = new Graph(store(configuration)).loadStashes("TestCesar", "Configuration", "infrastructure/devices/", "Responsibles", "Systems", "Projects").as(TestCesarGraph.class);
		if (cesarGraph.configuration() == null) cesarGraph.create("Configuration").configuration().core$().save();
		createProjects(cesarGraph);
		io.intino.konos.alexandria.Box box = new TestCesarBox(configuration).put(cesarGraph.core$()).open();
		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}

	private static void createProjects(TestCesarGraph graph) {
		if (graph.projectList().size() > 0) return;
		Project project = graph.create("Projects").project("Proyecto 1");
		project.devices().addAll(graph.deviceList());
		project.servers().addAll(graph.serverList());
		//project.systems().addAll(graph.systemList());
		project.core$().save();
	}

	private static io.intino.tara.magritte.Store store(TestCesarConfiguration configuration) {
		return new SumusStore(new File(configuration.store().getPath())) {

			boolean firstLoad = true;

			@Override
			public void writeStash(Stash stash, String path) {
				stash.language = stash.language == null || stash.language.isEmpty() ? "Cesar" : stash.language;
				super.writeStash(stash, path);
			}

			@Override
			public Stash stashFrom(String path) {
				Stash stash = super.stashFrom(path);
				if (firstLoad) stash.uses.addAll(allStashesInStore());
				firstLoad = false;
				return stash;
			}

			private List<String> allStashesInStore() {
				try {
					if (!file.exists()) return new ArrayList<>();
					List<String> result = new ArrayList<>();
					File infrastructure = fileOf("infrastructure");
					if (!infrastructure.exists()) return result;
					Files.walkFileTree(infrastructure.toPath(), new SimpleFileVisitor<Path>() {

						@Override
						public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
							if (file.toString().toLowerCase().endsWith(".stash")) result.add(relativePathOf(file.toUri().toURL()));
							return FileVisitResult.CONTINUE;
						}
					});
					return result;
				} catch (IOException e) {
					e.printStackTrace();
					return new ArrayList<>();
				}
			}
		};
	}
}