package io.intino.cesar.box.helpers;

import io.intino.cesar.box.TestCesarBox;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.displays.SumusOlap;

import java.util.function.Consumer;

import static io.intino.sumus.box.SumusDisplayHelper.sumusBox;

public class OlapHelper {

	public static AlexandriaDisplay cesarOlap(TestCesarBox box, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener) {
		SumusOlap display = new SumusOlap(sumusBox(box));
		display.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox(box)));
		display.olap(box.graph().cesarOlap());
		display.onLoading(loadingListener);
		display.onSelect(instantListener::accept);
		return display;
	}

}
