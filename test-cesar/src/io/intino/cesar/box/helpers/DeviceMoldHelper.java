package io.intino.cesar.box.helpers;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.graph.Device;
import io.intino.cesar.graph.DeviceStatus;
import io.intino.cesar.graph.Server;
import io.intino.cesar.graph.System;

import java.time.Instant;

public class DeviceMoldHelper {

	public static class Name {
		public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			return asset.label();
		}
	}

	public static class NoData {

		public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			return asset.status() == null ? "warning" : null;
		}

		public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			return "color:red;";
		}

		public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			return asset.label();
		}
	}
	public static class BatteryIcon {

		public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (!asset.i$(Device.class)) return null;
			DeviceStatus status = asset.a$(Device.class).status();
			if (status == null) return null;
			String prefix = "device:battery-" + (status.isPlugged() ? "charging-" : "");
			if (status.battery() == 100) return prefix + "full";
			return prefix + nearestLevel(status.battery());
		}

		public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (!asset.i$(Device.class)) return "";
			Device device = asset.a$(Device.class);
			if (device.status() == null) return "";
			if (!device.lowBattery()) return "color:green;";
			else if (!device.veryLowBattery()) return "color:orange;";
			return "color:red;";
		}

		public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (!asset.i$(Device.class)) return "";
			Device device = asset.a$(Device.class);
			if (device.status() == null) return "";
			return String.valueOf((int) device.status().battery()) + "%";
		}

		private static String nearestLevel(double battery) {
			int[] levels = {20, 30, 50, 60, 80, 90};
			int closest = 200;
			for (int level : levels) if (Math.abs(battery - level) < closest) closest = level;
			return String.valueOf(closest);
		}
	}
	public static class HotIcon {

		public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			return !asset.i$(Device.class) || asset.a$(Device.class).status() == null ? null : "social:whatshot";
		}

		public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (!asset.i$(Device.class)) return "";
			Device device = asset.a$(Device.class);
			if (device.status() == null) return "";
			return !device.isHot() ? "color:blue;" : "color:red;";
		}

		public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (!asset.i$(Device.class)) return "";
			Device device = asset.a$(Device.class);
			if (device.status() == null) return "";
			return String.valueOf(device.status().temperature()) + " º";
		}
	}
	public static class TimeIcon {

		public static java.lang.String value(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (asset.i$(Device.class))
				return asset.a$(Device.class).status() == null ? null : "device:access-time";
			return asset.a$(System.class).status() == null ? null : "device:access-time";
		}

		public static String style(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (asset.i$(Device.class)) {
				if (asset.a$(Device.class).status() == null) return "";
				boolean disconnected = asset.i$(Server.class) ? asset.a$(Server.class).isDisconnected() : asset.a$(Device.class).isDisconnected();
				return !disconnected ? "color:green;" : "color:red;";
			} else return asset.a$(System.class).isRunning() ? "color:green;" : "color:red;";
		}

		public static String title(TestCesarBox box, io.intino.cesar.graph.Device asset, String username) {
			if (asset.a$(Device.class).status() == null) return null;
			Instant instant = asset.i$(Server.class) ? asset.a$(Server.class).status().created() : asset.a$(Device.class).status().created();
			return String.format("date(%s,LLL)", instant.toEpochMilli());
		}
	}

}
