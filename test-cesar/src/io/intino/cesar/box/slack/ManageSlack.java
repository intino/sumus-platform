package io.intino.cesar.box.slack;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.slack.*;
import io.intino.konos.slack.Bot;
import io.intino.konos.slack.Bot.MessageProperties;

import java.util.Map;

public class ManageSlack {

	private TestCesarBox box;

	public ManageSlack(TestCesarBox box) {
		this.box = box;
	}

	public void init(com.ullink.slack.simpleslackapi.SlackSession session) {

	}

	public String addResponsible(MessageProperties properties, String user, String email, String[] fullName) {
		return "";
	}

	public String removeResponsible(MessageProperties properties, String user) {
		return "";
	}

	public String addDevices(MessageProperties properties, String[] devices) {
		return "";
	}

	public String addDevice(MessageProperties properties, String deviceId, String name) {
		return "";
	}

	public String removeDevice(MessageProperties properties, String deviceId) {
		return "";
	}

	public String addServer(MessageProperties properties, String Type, String label, String url, String ip) {
		return "";
	}

	public String removeServer(MessageProperties properties, String name) {
		return "";
	}

	public String removeSystem(MessageProperties properties, String name) {
		return "";
	}
}