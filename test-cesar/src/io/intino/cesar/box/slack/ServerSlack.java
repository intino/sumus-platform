package io.intino.cesar.box.slack;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.slack.*;
import io.intino.konos.slack.Bot;
import io.intino.konos.slack.Bot.MessageProperties;

import java.util.Map;

public class ServerSlack {

	private TestCesarBox box;

	public ServerSlack(TestCesarBox box) {
		this.box = box;
	}

	public void init(com.ullink.slack.simpleslackapi.SlackSession session) {

	}

	public String server(MessageProperties properties, String name) {
		return "";
	}

	public String name(MessageProperties properties, String newName) {
		return "";
	}

	public String status(MessageProperties properties) {
		return "";
	}

	public String remoteConnection(MessageProperties properties, String url, Integer port, String user) {
		return "";
	}

	public String shutdown(MessageProperties properties) {
		return "";
	}

	public String reboot(MessageProperties properties) {
		return "";
	}

	public String log(MessageProperties properties) {
		return "";
	}

	public String updateConsul(MessageProperties properties) {
		return "";
	}

	public String responsibles(MessageProperties properties) {
		return "";
	}

	public String setResponsibles(MessageProperties properties, String[] users) {
		return "";
	}
}