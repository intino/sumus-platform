package io.intino.cesar.box.slack.helpers;

import io.intino.cesar.box.schemas.Status;
import io.intino.cesar.graph.Device;
import io.intino.cesar.graph.DeviceStatus;
import io.intino.cesar.graph.Server;
import io.intino.cesar.graph.ServerStatus;
import io.intino.consul.schemas.SystemStatus;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.logging.Logger;

import static java.time.ZoneId.of;
import static java.time.format.DateTimeFormatter.ofPattern;


public class SlackMessageFormatter {
	private static final String NO_INFO_YET = "> No info yet";
	public static final String NO_STATUS = "no status";
	private static Logger logger = Logger.getGlobal();
	private static final DecimalFormat df = new DecimalFormat("0.0#");

	public static String format(DeviceStatus status, String timeZone) {
		return status == null ? NO_STATUS :
				":battery:`" + df.format(status.battery()) +
				"`   :electric_plug:`" + state(status.isPlugged()) +
				"`   :thermometer:`" + df.format(status.temperature()) +
				"`   :hammer_and_pick:`" + df.format(status.cpuUsage()) +
				"`   :desktop_computer:`" + state(status.isScreenOn()) +
				"`   :clock10:`" + formatInstant(timeZone, status.created()) + "`";
	}

	public static String time(Instant instant, String timeZone) {
		return ":clock10:`" + formatInstant(timeZone, instant) + "`";
	}

	public static String format(Status status, String timeZone) {
		return status == null ? NO_STATUS : ":battery:`" + df.format(status.battery()) +
				"`   :electric_plug:`" + state(status.isPlugged()) +
				"`   :thermometer:`" + df.format(status.temperature()) +
				"`   :hammer_and_pick:`" + df.format(status.cpuUsage()) +
				"`   :desktop_computer:`" + state(status.isScreenOn()) +
				"`   :clock10:`" + formatInstant(timeZone, status.ts() == null ? Instant.now() : status.ts()) + "`";
	}

	public static String format(Server server, ServerStatus status, String timeZone) {
		StringBuilder builder = new StringBuilder();
		if (status == null) builder.append(NO_INFO_YET);
		else if (server.isDisconnected()) builder.append("> *Disconnected*");
		else builder.append("> ").append(cpu(status.cpuUsage())).
					append("   ").append(memory(status.memoryUsage())).
					append("   ").append(hdd(status.diskUsage())).
					append("   ").append(connections(status.inboundConnections(), status.outboundConnections())).
					append("   ").append(time(status.created(), timeZone));
		return builder.toString();
	}

	public static String format(ServerStatus status, String timeZone) {
		StringBuilder builder = new StringBuilder();
		if (status == null) builder.append(NO_STATUS);
		else builder.append("\n\t").append(cpu(status.cpuUsage())).
				append("   ").append(memory(status.memoryUsage())).
				append("   ").append(hdd(status.diskUsage())).
				append("   ").append(connections(status.inboundConnections(), status.outboundConnections())).
				append("   ").append(time(status.created(), timeZone));
		return builder.toString();
	}

	public static String format(io.intino.consul.schemas.ServerStatus status, String timeZone) {
		StringBuilder builder = new StringBuilder();
		if (status == null) builder.append(NO_STATUS);
		else builder.append("\n\t").append(cpu(status.cpu())).
				append("   ").append(memory(status.memory())).
				append("   ").append(hdd(status.hdd())).
				append("   ").append(connections(status.inboundConnections(), status.outboundConnections())).
				append("   ").append(time(status.ts(), timeZone));
		return builder.toString();
	}

	public static String format(SystemStatus status, String timeZone) {
		String result = "\n\t";
		if (status == null) return result + NO_INFO_YET;
		return result + cpu(status.cpu()) + " " + memoryInAmount(status.memory()) + " " + threads(status.threads()) + " " + time(status.ts(), timeZone);
	}

	public static String format(io.intino.cesar.graph.System system, String timeZone) {
		String result = "\n\t";
		io.intino.cesar.graph.SystemStatus status = system.status();
		if (status == null) return result + NO_INFO_YET;
		return result + cpu(status.cpuUsage()) + " " + memoryInAmount(status.memoryUsage()) + " " + threads(status.threads()) + " " + time(status.created(), timeZone);
	}

	public static String formatCompromised(Device device, String timeZone) {
		DeviceStatus status = device.status();
		final DecimalFormat df = new DecimalFormat("0.0#");
		StringBuilder builder = new StringBuilder();
		if (device.status() == null) return builder.append("`Disconnected. No data`").toString();
		if (device.isDisconnected())
			return builder.append("Disconnected since ").append("`").append(formatInstant(timeZone, status.created())).append("`").toString();
		if (device.lowBattery()) builder.append(":battery:`").append(df.format(status.battery())).append("`   ");
		if (device.isUnPlugged()) builder.append(":electric_plug:`").append(state(status.isPlugged())).append("`   ");
		if (device.isHot()) builder.append(":thermometer:`").append(df.format(status.temperature())).append("`   ");
		if (!status.isScreenOn()) builder.append(":desktop_computer:`").append(state(status.isScreenOn())).append("`   ");
		builder.append(":clock10:`").append(formatInstant(timeZone, status.created())).append("`");
		return builder.toString();
	}

	public static String formatCompromised(Server server, String timeZone) {
		ServerStatus status = server.status();
		StringBuilder builder = new StringBuilder();
		if (server.status() == null) return builder.append("`Disconnected. No data`").toString();
		if (server.isDisconnected())
			return builder.append("Disconnected since ").append("`").append(formatInstant(timeZone, status.created())).append("`").toString();
		if (server.isRunningOutOfMemory()) builder.append(hdd(status.diskUsage())).append("`   ");
		if (server.isRunningOutOfDisk()) builder.append(memory(status.memoryUsage())).append("`   ");
		builder.append(":clock10:`").append(formatInstant(timeZone, status.created())).append("`");
		return builder.toString();
	}

	public static String formatInstant(String timeZone, Instant instant) {
		return ofPattern("dd/MM/yyyy HH:mm:ss").withZone(of(timeZone)).format(instant);
	}

	private static String hdd(double value) {
		return ":harddisk:`" + df.format(value) + "%`";
	}

	private static String memory(double value) {
		return ":memory:`" + df.format(value) + "%`";
	}

	private static String memoryInAmount(double value) {
		return ":memory:`" + df.format(value) + "mb`";
	}

	private static String connections(double in, double out) {
		return ":network: (`" + in + "`:small_red_triangle_down: `" + out + "`:small_red_triangle:)";
	}

	private static String cpu(double value) {
		return ":hammer_and_pick: `" + df.format(value) + "%`";
	}

	private static String threads(int value) {
		return ":threads: `" + value + "`";
	}

	private static String state(boolean state) {
		return state ? "ON" : "OFF";
	}
}
