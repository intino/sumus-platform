package io.intino.cesar.box.slack;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.slack.*;
import io.intino.konos.slack.Bot;
import io.intino.konos.slack.Bot.MessageProperties;

import java.util.Map;

public class CesarBotSlack {

	private TestCesarBox box;

	public CesarBotSlack(TestCesarBox box) {
		this.box = box;
	}

	public void init(com.ullink.slack.simpleslackapi.SlackSession session) {

	}

	public String units(MessageProperties properties) {
		return "";
	}

	public String devices(MessageProperties properties) {
		return "";
	}

	public String devicesId(MessageProperties properties) {
		return "";
	}

	public String systems(MessageProperties properties) {
		return "";
	}

	public String servers(MessageProperties properties) {
		return "";
	}

	public String responsibles(MessageProperties properties) {
		return "";
	}

	public String developToken(MessageProperties properties) {
		return "";
	}

	public String notifications(MessageProperties properties, String state) {
		return "";
	}

	public String issues(MessageProperties properties) {
		return "";
	}

	public String manage(MessageProperties properties) {
		return "";
	}

	public String device(MessageProperties properties, String deviceId) {
		return "";
	}

	public String server(MessageProperties properties, String name) {
		return "";
	}

	public String system(MessageProperties properties, String systemId) {
		return "";
	}
}