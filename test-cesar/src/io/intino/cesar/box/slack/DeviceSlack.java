package io.intino.cesar.box.slack;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.slack.*;
import io.intino.konos.slack.Bot;
import io.intino.konos.slack.Bot.MessageProperties;

import java.util.Map;

public class DeviceSlack {

	private TestCesarBox box;

	public DeviceSlack(TestCesarBox box) {
		this.box = box;
	}

	public void init(com.ullink.slack.simpleslackapi.SlackSession session) {

	}

	public String name(MessageProperties properties, String newName) {
		return "";
	}

	public String status(MessageProperties properties) {
		return "";
	}

	public String reboot(MessageProperties properties) {
		return "";
	}

	public String captureScreen(MessageProperties properties) {
		return "";
	}

	public String captureGrid(MessageProperties properties) {
		return "";
	}

	public String screen(MessageProperties properties, String state) {
		return "";
	}

	public String tap(MessageProperties properties, String square) {
		return "";
	}

	public String tapPixel(MessageProperties properties, Integer x, Integer y) {
		return "";
	}

	public String drag(MessageProperties properties, String fromSquare, String toSquare) {
		return "";
	}

	public String kill(MessageProperties properties, String packageName) {
		return "";
	}

	public String launch(MessageProperties properties, String packageName) {
		return "";
	}

	public String uninstall(MessageProperties properties, String packageName) {
		return "";
	}

	public String install(MessageProperties properties, String url) {
		return "";
	}

	public String update(MessageProperties properties, String url) {
		return "";
	}

	public String execute(MessageProperties properties, String[] command) {
		return "";
	}

	public String responsibles(MessageProperties properties) {
		return "";
	}

	public String setResponsibles(MessageProperties properties, String[] users) {
		return "";
	}

	public String setNotifications(MessageProperties properties, String unplug, String temperature, String battery, String lowBattery, String disconnected) {
		return "";
	}
}