package io.intino.cesar.box.slack;

import io.intino.cesar.box.TestCesarBox;
import io.intino.cesar.box.slack.*;
import io.intino.konos.slack.Bot;
import io.intino.konos.slack.Bot.MessageProperties;

import java.util.Map;

public class SystemSlack {

	private TestCesarBox box;

	public SystemSlack(TestCesarBox box) {
		this.box = box;
	}

	public void init(com.ullink.slack.simpleslackapi.SlackSession session) {

	}

	public String status(MessageProperties properties) {
		return "";
	}

	public String log(MessageProperties properties) {
		return "";
	}

	public String parameter(MessageProperties properties, String name, String value) {
		return "";
	}

	public String start(MessageProperties properties) {
		return "";
	}

	public String debug(MessageProperties properties) {
		return "";
	}

	public String stop(MessageProperties properties) {
		return "";
	}

	public String restart(MessageProperties properties) {
		return "";
	}

	public String parameters(MessageProperties properties) {
		return "";
	}

	public String responsibles(MessageProperties properties) {
		return "";
	}

	public String setResponsibles(MessageProperties properties, String[] users) {
		return "";
	}

	public String invoke(MessageProperties properties, String operation, String[] parameters) {
		return "";
	}

	public String operations(MessageProperties properties) {
		return "";
	}
}