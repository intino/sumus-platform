var SumusFilterListBehaviors = SumusFilterListBehaviors || {};

SumusFilterListBehaviors.Requester = {

    addFilter : function() {
    	this.carry("addFilter");
    },
    saveFilter : function(value) {
    	this.carry("saveFilter", { "value" : value });
    },
    removeFilter : function(value) {
    	this.carry("removeFilter", { "value" : value });
    },
    selectFilter : function(value) {
    	this.carry("selectFilter", { "value" : value });
    },
    applyFilter : function(value) {
    	this.carry("applyFilter", { "value" : value });
    },
    quitFilter : function(value) {
    	this.carry("quitFilter", { "value" : value });
    }

};