var SumusFilterListBehaviors = SumusFilterListBehaviors || {};

SumusFilterListBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshFilterList").toSelf().execute(function(parameters) {
        	widget._refreshFilterList(parameters.value);
        });
        this.when("refreshSelected").toSelf().execute(function(parameters) {
        	widget._refreshSelected(parameters.value);
        });
        this.when("refreshApplied").toSelf().execute(function(parameters) {
        	widget._refreshApplied(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};