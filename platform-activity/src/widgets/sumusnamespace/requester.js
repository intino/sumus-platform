var SumusNameSpaceBehaviors = SumusNameSpaceBehaviors || {};

SumusNameSpaceBehaviors.Requester = {

    selectNameSpace : function(value) {
    	this.carry("selectNameSpace", { "value" : value });
    }

};