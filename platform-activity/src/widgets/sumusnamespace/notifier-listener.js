var SumusNameSpaceBehaviors = SumusNameSpaceBehaviors || {};

SumusNameSpaceBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshNameSpaces").toSelf().execute(function(parameters) {
        	widget._refreshNameSpaces(parameters.value);
        });
        this.when("refreshSelectedNameSpace").toSelf().execute(function(parameters) {
        	widget._refreshSelectedNameSpace(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};