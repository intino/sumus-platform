var SumusTimeCrossTableDialogBehaviors = SumusTimeCrossTableDialogBehaviors || {};

SumusTimeCrossTableDialogBehaviors.Requester = {

    query : function(value) {
    	this.carry("query", { "value" : value });
    }

};