var SumusTimeCrossTableDialogBehaviors = SumusTimeCrossTableDialogBehaviors || {};

SumusTimeCrossTableDialogBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshCategorizationList(parameters.value);
        });
        this.when("refreshTemporalCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshTemporalCategorizationList(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};