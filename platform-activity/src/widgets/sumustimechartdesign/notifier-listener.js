var SumusTimeChartDesignBehaviors = SumusTimeChartDesignBehaviors || {};

SumusTimeChartDesignBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshTicketList").toSelf().execute(function(parameters) {
        	widget._refreshTicketList(parameters.value);
        });
        this.when("refreshSelectedTicketList").toSelf().execute(function(parameters) {
        	widget._refreshSelectedTicketList(parameters.value);
        });
        this.when("refreshSelectedChart").toSelf().execute(function(parameters) {
        	widget._refreshSelectedChart(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};