var SumusCategorizationBehaviors = SumusCategorizationBehaviors || {};

SumusCategorizationBehaviors.Requester = {

    toggle : function(value) {
    	this.carry("toggle", { "value" : value });
    },
    select : function(value) {
    	this.carry("select", { "value" : value });
    },
    selectAll : function() {
    	this.carry("selectAll");
    },
    selectNone : function() {
    	this.carry("selectNone");
    }

};