var SumusCategorizationBehaviors = SumusCategorizationBehaviors || {};

SumusCategorizationBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshCategorization").toSelf().execute(function(parameters) {
        	widget._refreshCategorization(parameters.value);
        });
        this.when("refreshSelected").toSelf().execute(function(parameters) {
        	widget._refreshSelected(parameters.value);
        });
        this.when("refreshMaxSelection").toSelf().execute(function(parameters) {
        	widget._refreshMaxSelection(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};