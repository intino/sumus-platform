var SumusTimeScatterChartBehaviors = SumusTimeScatterChartBehaviors || {};

SumusTimeScatterChartBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshTicketList").toSelf().execute(function(parameters) {
        	widget._refreshTicketList(parameters.value);
        });
        this.when("refreshCategorization").toSelf().execute(function(parameters) {
        	widget._refreshCategorization(parameters.value);
        });
        this.when("refreshHistogram").toSelf().execute(function(parameters) {
        	widget._refreshHistogram(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};