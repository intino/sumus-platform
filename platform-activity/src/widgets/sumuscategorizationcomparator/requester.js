var SumusCategorizationComparatorBehaviors = SumusCategorizationComparatorBehaviors || {};

SumusCategorizationComparatorBehaviors.Requester = {

    includeGlobalSerie : function(value) {
    	this.carry("includeGlobalSerie", { "value" : value });
    },
    apply : function() {
    	this.carry("apply");
    },
    quit : function() {
    	this.carry("quit");
    }

};