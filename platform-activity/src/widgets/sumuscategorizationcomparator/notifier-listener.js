var SumusCategorizationComparatorBehaviors = SumusCategorizationComparatorBehaviors || {};

SumusCategorizationComparatorBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshCategorizationList(parameters.value);
        });
        this.when("refreshSelection").toSelf().execute(function(parameters) {
        	widget._refreshSelection(parameters.value);
        });
        this.when("refreshIncludeGlobalSerie").toSelf().execute(function(parameters) {
        	widget._refreshIncludeGlobalSerie(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};