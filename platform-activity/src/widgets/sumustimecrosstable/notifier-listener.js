var SumusTimeCrossTableBehaviors = SumusTimeCrossTableBehaviors || {};

SumusTimeCrossTableBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshTicketCount").toSelf().execute(function(parameters) {
        	widget._refreshTicketCount(parameters.value);
        });
        this.when("refreshData").toSelf().execute(function(parameters) {
        	widget._refreshData(parameters.value);
        });
        this.when("loading").toSelf().execute(function(parameters) {
        	widget._loading(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};