'use strict';

const SumusTimeCrossTableGenerator = (function () {

    var unitLabel = "";
    var language = "es";
    var decimalPlaces = { absolute: 2, percentage: 2 };
    var cols = [ { label: "Country", values: ["España", "Alemania", "Inglaterra"] },
                 { label: "Area", values: ["Area1", "Area2"] }];

    var rows = [ { label: "Gender", values: ["Hombre", "Mujer"] },
                 { label: "Age", values: ["Mayor 30", "Menor 30"] } ];

    var data = [ { values: [1, 2, 3, 4, 5, 6] } ];

    var heatMap = null;

    var range = {
        min: 1,
        max: 6
    };

    const self = {};
    const columnStyle = "padding: 3px 10px;";
    const headerStyle = "background:#efefef;font-size:11pt;border:1px solid white;";

    const Dictionary = {
        "es" : {
            UnitMessage: "valores en ::unitLabel::"
        },
        "en" : {
            UnitMessage: "values in ::unitLabel::"
        }
    };

    self.generate = function (options, data) {

        if (options.container == null) {
            console.log("must define container for table generation");
            return;
        }

        unitLabel = options.unitLabel ? options.unitLabel : "";
        language = options.language ? options.language : "es";
        cols = options.cols;
        rows = options.rows;
        heatMap = options.heatMap;
        decimalPlaces = options.decimalPlaces != null ? options.decimalPlaces : decimalPlaces;

        calculateDataRange(data);
        generateTable(data, options.container);
    };

    function generateTable(data, element) {
        element.innerHTML = "";

        if (data.length == 0)
            return;

        var table = document.createElement("table");
        table.cellSpacing = 0;
        table.cellPadding = 0;
        table.style.width = "100%";

        createHeader(table);
        createBody(table, data);

        element.appendChild(table);
    }

    function createHeader(table) {
        var tableElement = $(table);
        for (var i=0; i < cols.length; i++) {
            var column = cols[i];
            var colSpan = i+1 < cols.length ? cols[i+1].values.length : 1;
            var countIterations = i > 0 ? cols[i-1].values.length : 1;
            tableElement.append(createHeaderRow(column, colSpan, countIterations));
        }
    }

    function createBody(table, data) {
        var flattenRowsValues = cartesianProductOf(rowsValues());
        var tableElement = $(table);

        for (var i=0; i<flattenRowsValues.length; i++) {
            var row = flattenRowsValues[i];
            tableElement.append(createRow(row, i, entryOf(data, i)));
        }
    }

    function createHeaderRow(column, colSpan, countIterations) {
        var result = "";

        for (var j=0; j<countNoDataCols(); j++) {
            var legendValue = j==0 ? legend() : "";
            result += "<th style='" + headerStyle + columnStyle + "background:white;color:#999;font-size:10pt;font-weight:normal;'>" + legendValue + "</th>";
        }

        for (var i=0; i<countIterations; i++)
            for (var j=0; j<column.values.length; j++) {
                var width = Math.floor(100/column.values.length);
                result += "<th style='" + headerStyle + columnStyle + "width:" + width + "%;' colspan='" + colSpan + "'>" + column.values[j] + "</th>";
            }

        return "<tr style='height:20px;'>" + result + "</tr>";
    }

    function legend() {
        if (unitLabel == "") return "";
        return Dictionary[language].UnitMessage.replace("::unitLabel::", unitLabel);
    }

    function createRow(rowHeaders, pos, entry) {
        var rowsCountArray = rowsCount();
        var result = "";

        for (var i=0; i<rowHeaders.length; i++) {
            var header = pos%rowsCountArray[i] == 0 ? rowHeaders[i] : "";
            result += "<td style='" + headerStyle + columnStyle + "width:100px;'>" + header + "</td>";
        }

        for (var i=0; i<entry.values.length; i++) {
            var color = heatMap != null ? colorForDataEntry(entry.values[i]) : "transparent";
            var value = formattedValue(entry.values[i]);
            var columnBehavior = heatMap != null ? "onMouseOver=\"this.querySelector('span').style.display='block';\" onMouseOut=\"this.querySelector('span').style.display='none';\"": "";
            var valueStyles = heatMap != null ? " style='display:none;'": "";
            result += "<td " + columnBehavior + "style='text-align:center;font-size:10pt;border-bottom:1px solid #efefef;border-right:1px solid #efefef;background-color:" + color + "'><span" + valueStyles + ">" + value + "</span></td>";
        }

        return "<tr style='height:20px;'>" + result + "</tr>";
    }

    function rowsCount() {
        var result = [];
        for (var i=0; i<rows.length; i++) {
            result.push(1);
            for (var j=rows.length-1; j>0; j--) {
                if (j>i) result[i] = result[i] * rows[j].values.length;
            }
        }
        return result;
    }

    function countNoDataCols() {
        return rows.length;
    }

    function entryOf(data, index) {
        return data[index];
    }

    function calculateDataRange(data) {
        var min = null;
        var max = null;
        for (var i=0;i<data.length; i++) {
            for (var j=0;j<data[i].values.length; j++) {
                var value = data[i].values[j];
                if (min == null || min > value) min = value;
                if (max == null || max < value) max = value;
            }
        }
        range = { min: min, max: max };
    }

    function rowsValues() {
        var result = [];
        for (var i=0; i<rows.length; i++) result.push(rows[i].values);
        return result;
    }

    function cartesianProductOf(a) { // a = array of array
        var i, j, l, m, a1, o = [];
        if (!a || a.length == 0) return a;

        a1 = a.splice(0, 1)[0]; // the first array of a
        a = cartesianProductOf(a);
        for (i = 0, l = a1.length; i < l; i++) {
            if (a && a.length) for (j = 0, m = a.length; j < m; j++)
                o.push([a1[i]].concat(a[j]));
            else
                o.push([a1[i]]);
        }
        return o;
    }

    function colorForDataEntry(value) {
        return window.SumusColorUtil.color(value, heatMap, range.max);
    }

    function formattedValue(value) {
        return value.toFixed(decimalPlaces.absolute);
    }

    return self;
});

window.SumusTimeCrossTableGenerator = SumusTimeCrossTableGenerator();
