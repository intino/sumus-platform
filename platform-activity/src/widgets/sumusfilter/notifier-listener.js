var SumusFilterBehaviors = SumusFilterBehaviors || {};

SumusFilterBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshCategorizationList(parameters.value);
        });
        this.when("refreshTemporalCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshTemporalCategorizationList(parameters.value);
        });
        this.when("refreshFilter").toSelf().execute(function(parameters) {
        	widget._refreshFilter(parameters.value);
        });
        this.when("showTemporalCategorizations").toSelf().execute(function(parameters) {
        	widget._showTemporalCategorizations();
        });
        this.when("hideTemporalCategorizations").toSelf().execute(function(parameters) {
        	widget._hideTemporalCategorizations();
        });
        this._listeningToDisplay = true;
    }
};