var SumusFilterBehaviors = SumusFilterBehaviors || {};

SumusFilterBehaviors.Requester = {

    addCategorization : function(value) {
    	this.carry("addCategorization", { "value" : value });
    },
    removeCategorization : function(value) {
    	this.carry("removeCategorization", { "value" : value });
    },
    apply : function() {
    	this.carry("apply");
    },
    quit : function() {
    	this.carry("quit");
    }

};