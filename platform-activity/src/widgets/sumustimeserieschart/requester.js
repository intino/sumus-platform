var SumusTimeSeriesChartBehaviors = SumusTimeSeriesChartBehaviors || {};

SumusTimeSeriesChartBehaviors.Requester = {

    updateRangeAndScale : function(value) {
    	this.carry("updateRangeAndScale", { "value" : value });
    },
    moveLeft : function(value) {
    	this.carry("moveLeft", { "value" : value });
    },
    moveRight : function(value) {
    	this.carry("moveRight", { "value" : value });
    },
    showDialog : function() {
    	this.carry("showDialog");
    },
    selectCatalogInstant : function(value) {
    	this.carry("selectCatalogInstant", { "value" : value });
    }

};