var SumusTimeSeriesChartBehaviors = SumusTimeSeriesChartBehaviors || {};

SumusTimeSeriesChartBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("olapRange").toSelf().execute(function(parameters) {
        	widget._olapRange(parameters.value);
        });
        this.when("timeRange").toSelf().execute(function(parameters) {
        	widget._timeRange(parameters.value);
        });
        this.when("formats").toSelf().execute(function(parameters) {
        	widget._formats(parameters.value);
        });
        this.when("updateScale").toSelf().execute(function(parameters) {
        	widget._updateScale(parameters.value);
        });
        this.when("clear").toSelf().execute(function(parameters) {
        	widget._clear();
        });
        this.when("refreshTicketCount").toSelf().execute(function(parameters) {
        	widget._refreshTicketCount(parameters.value);
        });
        this.when("refreshEventListCount").toSelf().execute(function(parameters) {
        	widget._refreshEventListCount(parameters.value);
        });
        this.when("refreshEventList").toSelf().execute(function(parameters) {
        	widget._refreshEventList(parameters.value);
        });
        this.when("addSeries").toSelf().execute(function(parameters) {
        	widget._addSeries(parameters.value);
        });
        this.when("removeSeries").toSelf().execute(function(parameters) {
        	widget._removeSeries(parameters.value);
        });
        this.when("addPoints").toSelf().execute(function(parameters) {
        	widget._addPoints(parameters.value);
        });
        this.when("refreshCatalogList").toSelf().execute(function(parameters) {
        	widget._refreshCatalogList(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};