'use strict';

const sumusDateFormatter = (function (formats) {

    const self = {};

    function numberOfWeek(instant) {
        const firstDayOfYear = new Date(instant.getFullYear(), 0, 1);
        const millisecondsInDay = 86400000;
        return Math.ceil((((instant - firstDayOfYear) /millisecondsInDay) + firstDayOfYear.getDay() + 1) / 7);
    }

    const formatters = {
        Year: function (format, instant) {
            const year = instant.getFullYear();
            if (format.indexOf('yyyy') > -1) {
                return format.replace('yyyy', year.toString());
            }
            return format.replace('yy', year.toString().substr(2, 2));
        },
        QuarterOfYear: function (format, instant) {
            const result = formatters.Year(format, instant);
            return result.replace('qN', Math.ceil((instant.getMonth() + 1) / 3));
        },
        Month: function (format, instant) {
            const result = formatters.Year(format, instant);
            const month = instant.getMonth() + 1;
            return result.replace('MM', month.toString().replace(/^(\d)$/, '0$1'));
        },
        Week: function (format, instant) {
            const result = formatters.Year(format, instant);
            const week = numberOfWeek(instant);
            return result.replace('wN', week.toString().replace(/^(\d)$/, '0$1'));
        },
        Day: function (format, instant) {
            const result = formatters.Month(format, instant);
            const day = instant.getDate();
            return result.replace('dd', day.toString().replace(/^(\d)$/, '0$1'));
        },
        SixHours: function (format, instant) {
            const result = formatters.Day(format, instant);
            const hours = instant.getHours();
            const startOfQuarter = (Math.floor(hours / 6) * 6).toString();
            const endOfQuarter = ((Math.floor(hours / 6) + 1) * 6).toString();
            return result.replace('qDS', startOfQuarter.replace(/^(\d)$/, '0$1'))
            .replace('qDE', endOfQuarter.replace(/^(\d)$/, '0$1'));
        },
        Hour: function (format, instant) {
            let result = formatters.Day(format, instant);
            let hours = instant.getHours();
            if (result.indexOf('HH') > -1) {
                return result.replace(new RegExp('HH', 'g'), hours.toString().replace(/^(\d)$/, '0$1'));
            }
            if (result.indexOf('t') > -1) {
                result = result.replace(new RegExp('t', 'g'), hours > 11 ? 'pm' : 'am');
            }
            hours = hours > 12 ? hours - 12 : hours;
            hours = hours === 0 ? 12 : hours;
            return result.replace(new RegExp('hh', 'g'), hours.toString().replace(/^(\d)$/, '0$1'));
        },
        FifteenMinutes: function (format, instant) {
            const result = formatters.Hour(format, instant);
            const minutes = instant.getMinutes();
            const startOfQuarter = (Math.floor(minutes / 15) * 15).toString();
            const endOfQuarter = ((Math.floor(minutes / 15) + 1) * 15).toString();
            return result.replace('qHS', startOfQuarter.replace(/^(\d)$/, '0$1'))
            .replace('qHE', endOfQuarter.replace(/^(\d)$/, '0$1'))
            .replace(/(\d\d):60/, function(value) { return (parseInt(value.substring(0,2)) + 1) + ":00"; })
            .replace(/ (\d):/, ' 0$1:');
        },
        Minute: function (format, instant) {
            const result = formatters.Hour(format, instant);
            const minutes = instant.getMinutes();
            return result.replace('mm', minutes.toString().replace(/^(\d)$/, '0$1'));
        },
        Second: function (format, instant) {
            const result = formatters.Minute(format, instant);
            const seconds = instant.getSeconds();
            return result.replace('ss', seconds.toString().replace(/^(\d)$/, '0$1'));
        }
    };

    self.formatter = function (scale) {
        return {
            format: function (instant) {
                return formatters[scale](this.formatOf(scale), instant);
            },
            formatOf : function() {
                for (var i=0; i < formats.length; i++)
                    if (formats[i].name == scale) return formats[i].format;
                return null;
            }
        };
    };

    return self;
});

window.sumusDateFormatter = sumusDateFormatter;
