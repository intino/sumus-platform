const sumusNavigationHandler = (function() {

    const self = {};

    let focus;

    function calculateDelta(e) {
        if (e.wheelDelta) {
            return e.wheelDelta / 120;
        } else if (e.detail) {
            return -e.detail / 3;
        }
    }

    function calculateScale(delta) {
        if (delta < 0) {
            return 1 - (delta / 5);
        }
        return 1 / (1 + (delta / 5));
    }

    self.focus = (point) => focus = point;

    self.point = () => { return focus };

    self.calculateRange = (e, currentRange) => {
        const delta = calculateDelta(e);
        if (delta) {
            const scale = calculateScale(delta);
            const min = focus + (currentRange.min - focus) * scale;
            const max = focus + (currentRange.max - focus) * scale;
            return (isNaN(min) || isNaN(max)) ? null : {from: min, to: max};
        }
    };

    return self;
});

window.sumusNavigationHandler = sumusNavigationHandler;
