const SumusHighchartsFormatter = (function () {

    const self = {};
    var addingPoints = false;

    function formatPoints (points, group) {
        return points.map(point => {
            return { x: point.created, y: point.value, group: group };
        });
    }

    function absoluteValue(point, decimalPlaces) {
        var decimalPlaces = decimalPlaces(point.series.name);
        return decimalPlaces != null ? point.y.toFixed(decimalPlaces.absolute) : 0;
    }

    function percentValue(point, total, decimalPlaces) {
        if (total == 0) return 0;
        return (point.y * 100 / total).toFixed(decimalPlaces(point.series.name).percentage);
    }

    function tooltipLabel(points, point, metric, decimalPlaces, shouldDisplayPercentage) {
        if (point.series.name == '_eventsSeries') return '';
        const symbolAndSeriesName = `<span style="color: ${point.series.color}; font-size: 14pt">\u25CF</span>${point.series.name}`;
        let value = `${absoluteValue(point, decimalPlaces)}`;
        if (shouldDisplayPercentage(point.series.name)) {
            value += ` (${percentValue(point, groupTotal(points, point), decimalPlaces)} %)`;
        }
        return `${symbolAndSeriesName}: <b>${value} ${metric(point.series.name)}</b><br/>`;
    }

    function groupTotal(points, p) {
        var groupForPoint = pointGroup(findPoint(points, p.x, p.y));
        return points.filter(p => p.point.group == groupForPoint).map(point => point.y).reduce((l, r) => l + r);
    }

    function findPoint(points, x, y) {
        return points.filter(point => point.x == x && point.y == y)[0];
    }

    function pointGroup(p) {
        return p.point.group;
    }

    self.stockChart = function (options) {
        return {
            chart: {
                renderTo: options.container
            },

            navigator: {
                enabled: false
            },

            credits: {
                enabled: false
            },

            scrollbar : {
                enabled : false
            },

            rangeSelector: {
                enabled: false
            },

            xAxis: {
                labels: {
                    enabled: false
                },
                minorTickLength: 0,
                tickLength: 0,
                minPadding: 0,
                maxPadding: 0
            },

            yAxis: {
                gridLineDashStyle: 'Dot',
                labels: {
                    style: {
                        fontSize: '8pt',
                        fontFamily: 'Rubik, Helvetica Neue, Helvetica, Arial, sans-serif'
                    }
                }
            },

            tooltip: {
                formatter: function() {
                    if (!this.points) {
                        return !this.point.text ? false : this.point.text;
                    }
                    return this.points
                    .map(point => tooltipLabel(this.points, point, options.metric, options.decimalPlaces, options.shouldDisplayPercentage))
                    .reduce((accumulated, newValue) => accumulated += newValue, `<b>${options.formatInstant(new Date(this.x))}</b><br/>`);
                }
            },

            plotOptions: {
                series: {
                    point: {
                        events: {
                            click : function(e) {
                                options.onClick(this.x);
                            },
                            mouseOver: function () {
                                options.onMouseOver(this.x);
                            }
                        }
                    }
                }
            }
        };
    };

    self.series = function (options) {
        if (options.indicator.style === 'absolute' || options.indicator.style === 'percentage') {
            return {
                name: options.indicator.label,
                type: 'area',
                data: formatPoints(options.indicator.dataList, options.indicator.group),
                color: options.indicator.color,
                fillColor: options.indicator.color,
                threshold: null,
                stacking: options.indicator.style === 'absolute' ? 'normal' : 'percent',
                yAxis: options.axis,
                zIndex: 1,
                marker : {
                    enabled : true,
                    radius : 2
                }
            };
        }
        return {
            name: options.indicator.label,
            data: formatPoints(options.indicator.dataList, options.indicator.group),
            color: options.indicator.color,
            yAxis: options.axis,
            lineWidth: 2,
            dashStyle: options.indicator.style === 'dotted' ? 'shortdash' : '',
            zIndex: 2,
            marker : {
                enabled : true,
                radius : 3
            }
        };
    };

    self.addPoints = function (series, group, points) {
        if (addingPoints) return;
        addingPoints = true;

        const instantsAdded = series.data != null ? series.data.filter(p => p != null).map(p => p.x) : [];
        let newPoints = points.filter(point => instantsAdded.indexOf(point.created) < 0);
        formatPoints(newPoints, group).forEach(point => series.addPoint(point, false, false, false));
        addingPoints = false;
    };

    return self;
})();

window.SumusHighchartsFormatter = SumusHighchartsFormatter;
