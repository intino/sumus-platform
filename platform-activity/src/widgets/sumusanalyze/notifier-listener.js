var SumusAnalyzeBehaviors = SumusAnalyzeBehaviors || {};

SumusAnalyzeBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshVisibility").toSelf().execute(function(parameters) {
        	widget._refreshVisibility(parameters.value);
        });
        this.when("enableComparePanel").toSelf().execute(function(parameters) {
        	widget._enableComparePanel();
        });
        this.when("disableComparePanel").toSelf().execute(function(parameters) {
        	widget._disableComparePanel();
        });
        this._listeningToDisplay = true;
    }
};