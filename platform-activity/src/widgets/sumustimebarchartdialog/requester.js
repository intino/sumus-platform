var SumusTimeBarChartDialogBehaviors = SumusTimeBarChartDialogBehaviors || {};

SumusTimeBarChartDialogBehaviors.Requester = {

    selectCategorization : function(value) {
    	this.carry("selectCategorization", { "value" : value });
    }

};