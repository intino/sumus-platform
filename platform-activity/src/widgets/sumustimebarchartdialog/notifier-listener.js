var SumusTimeBarChartDialogBehaviors = SumusTimeBarChartDialogBehaviors || {};

SumusTimeBarChartDialogBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("refreshCategorizationList").toSelf().execute(function(parameters) {
        	widget._refreshCategorizationList(parameters.value);
        });
        this.when("refreshCategorization").toSelf().execute(function(parameters) {
        	widget._refreshCategorization(parameters.value);
        });
        this._listeningToDisplay = true;
    }
};