'use strict';

const SumusColorUtil = (function () {
    const self = {};

    self.color = function(value, colors, maxValue) {
        let val = Math.floor(value * 100 / maxValue);
        return makeGradientColor(hexToRgb(colors.min), hexToRgb(colors.max), val).cssColor;
    };

    function makeGradientColor(color1, color2, percent) {
        let newColor = {};

        function makeChannel(a, b) {
            return(a + Math.round((b-a)*(percent/100)));
        }

        function makeColorPiece(num) {
            num = Math.min(num, 255);   // not more than 255
            num = Math.max(num, 0);     // not less than 0
            let str = num.toString(16);
            if (str.length < 2) {
                str = "0" + str;
            }
            return(str);
        }

        newColor.r = makeChannel(color1.r, color2.r);
        newColor.g = makeChannel(color1.g, color2.g);
        newColor.b = makeChannel(color1.b, color2.b);
        newColor.cssColor = "#" +
            makeColorPiece(newColor.r) +
            makeColorPiece(newColor.g) +
            makeColorPiece(newColor.b);

        return(newColor);
    }

    function hexToRgb(hex) {
        let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
    }

    return self;
});

window.SumusColorUtil = SumusColorUtil();