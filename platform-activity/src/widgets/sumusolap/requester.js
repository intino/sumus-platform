var SumusOlapBehaviors = SumusOlapBehaviors || {};

SumusOlapBehaviors.Requester = {

    addTicket : function(value) {
    	this.carry("addTicket", { "value" : value });
    },
    removeTicket : function(value) {
    	this.carry("removeTicket", { "value" : value });
    },
    moveTickets : function(value) {
    	this.carry("moveTickets", { "value" : value });
    },
    updateScale : function(value) {
    	this.carry("updateScale", { "value" : value });
    },
    updateRange : function(value) {
    	this.carry("updateRange", { "value" : value });
    },
    export : function(value) {
    	this.download("export", { "value" : value });
    },
    removeDrillCategory : function(value) {
    	this.carry("removeDrillCategory", { "value" : value });
    },
    selectChart : function(value) {
    	this.carry("selectChart", { "value" : value });
    },
    removeFilter : function(value) {
    	this.carry("removeFilter", { "value" : value });
    }

};