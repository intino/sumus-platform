var SumusOlapBehaviors = SumusOlapBehaviors || {};

SumusOlapBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("olapRange").toSelf().execute(function(parameters) {
        	widget._olapRange(parameters.value);
        });
        this.when("timeRange").toSelf().execute(function(parameters) {
        	widget._timeRange(parameters.value);
        });
        this.when("scaleList").toSelf().execute(function(parameters) {
        	widget._scaleList(parameters.value);
        });
        this.when("updateScale").toSelf().execute(function(parameters) {
        	widget._updateScale(parameters.value);
        });
        this.when("enableDrill").toSelf().execute(function(parameters) {
        	widget._enableDrill();
        });
        this.when("disableDrill").toSelf().execute(function(parameters) {
        	widget._disableDrill();
        });
        this.when("enableFilter").toSelf().execute(function(parameters) {
        	widget._enableFilter();
        });
        this.when("disableFilter").toSelf().execute(function(parameters) {
        	widget._disableFilter();
        });
        this.when("filtering").toSelf().execute(function(parameters) {
        	widget._filtering();
        });
        this.when("notFiltering").toSelf().execute(function(parameters) {
        	widget._notFiltering();
        });
        this.when("drilling").toSelf().execute(function(parameters) {
        	widget._drilling();
        });
        this.when("notDrilling").toSelf().execute(function(parameters) {
        	widget._notDrilling();
        });
        this.when("refreshDrill").toSelf().execute(function(parameters) {
        	widget._refreshDrill(parameters.value);
        });
        this.when("refreshFilterList").toSelf().execute(function(parameters) {
        	widget._refreshFilterList(parameters.value);
        });
        this.when("ticketList").toSelf().execute(function(parameters) {
        	widget._ticketList(parameters.value);
        });
        this.when("selectTicketList").toSelf().execute(function(parameters) {
        	widget._selectTicketList(parameters.value);
        });
        this.when("byCategorization").toSelf().execute(function(parameters) {
        	widget._byCategorization(parameters.value);
        });
        this.when("chartList").toSelf().execute(function(parameters) {
        	widget._chartList(parameters.value);
        });
        this.when("selectChart").toSelf().execute(function(parameters) {
        	widget._selectChart(parameters.value);
        });
        this.when("openAnalyzeDialog").toSelf().execute(function(parameters) {
        	widget._openAnalyzeDialog();
        });
        this.when("closeAnalyzeDialog").toSelf().execute(function(parameters) {
        	widget._closeAnalyzeDialog();
        });
        this._listeningToDisplay = true;
    }
};