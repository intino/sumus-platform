var SumusDesktopBehaviors = SumusDesktopBehaviors || {};

SumusDesktopBehaviors.NotifierListener = {

	properties : {
		_listeningToDisplay : { type: Boolean, value: function() { return false; } }
	},

    listenToDisplay : function() {
		if (this.display == null || this._listeningToDisplay) return;
        var widget = this;
        this.when("loading").execute(function(parameters) {
        	widget._loading(parameters.value);
        });
        this.when("loaded").execute(function(parameters) {
        	widget._loaded();
        });
        this._listeningToDisplay = true;
    }
};