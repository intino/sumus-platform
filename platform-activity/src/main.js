"use strict";

var launchApplication = function (configuration) {

    window.Application = (function (configuration) {
        var services = {};

        var self = {};
        self.configuration = configuration;

        return self;

    })(configuration);

    window.addEventListener('WebComponentsReady', function () {
        PushService.openConnection(configuration.pushServiceUrl);
    });

};

function isImage(file) {
    if (file.name == null) return false;
    return file.name.match(/\.(jpg|jpeg|png|gif)$/);
};

NodeList.prototype.forEach = Array.prototype.forEach;
HTMLCollection.prototype.forEach = Array.prototype.forEach;

function addBrowserClasses(element) {
    let ua = navigator.userAgent.toLowerCase();
    if (ua.match(/msie/i) || ua.match(/trident/i) ){
        $(element).addClass("ie");
    }
    if (ua.indexOf('safari') != -1 && ua.indexOf('chrome') == -1) {
        $(element).addClass("safari");
    }
}
