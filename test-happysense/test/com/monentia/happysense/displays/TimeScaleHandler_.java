package com.monentia.happysense.displays;

import io.intino.sumus.SumusStore;
import io.intino.sumus.analytics.TimeRange;
import io.intino.sumus.graph.Olap;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.graph.functions.InstantLoader;
import io.intino.sumus.graph.rules.TimeScale;
import io.intino.sumus.helpers.TimeScaleHandler;
import io.intino.sumus.helpers.TimeScaleHandler.Bounds;
import io.intino.tara.magritte.Graph;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;

import static io.intino.sumus.analytics.matchers.SumusMatchers.is;
import static io.intino.sumus.graph.rules.Mode.FromTheBeginning;
import static io.intino.sumus.graph.rules.TimeScale.*;
import static io.intino.sumus.helpers.Bounds.Zoom;
import static io.intino.sumus.helpers.UtcDateTime.of;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TimeScaleHandler_ {

	private static Olap olap;
	private static List<Olap.ZoomGroup.Zoom> zoomList;
	private static List<TimeScale> scales;
	private static Olap.Range range;

	private TimeScaleHandler timeScaleHandler;
	@Mock
	private Consumer<TimeRange> timeRangeListener;
	@Captor
	private ArgumentCaptor<TimeRange> rangeCaptor;

	@BeforeClass
	public static void setUpClass() {
		Graph model = new Graph(new SumusStore(new File(""))).loadStashes("Model");
		SumusGraph engine = model.as(SumusGraph.class);
		olap = engine.olapList().get(0);
		range = rangeOf(olap);
		zoomList = zoomGroup(olap).zoomList();
		scales = asList(Month, Week, Day);
	}

	@Before
	public void setUp() {
		timeScaleHandler = new TimeScaleHandler(bounds(range, zoomList), scales, scales.get(0));
	}

	@Test
	public void should_give_initial_range_within_boundaries() {
		TimeScaleHandler handler = handlerWithScaleStartingOnDays();

		TimeRange range = handler.range();

		assertThat(range, is(new TimeRange(instant(2010, 1, 1), instant(2010, 1, 16), Day)));
		assertThat(Day.instantsBetween(range.from(), range.to()), Is.is(16L));
	}

	@Test
	public void should_give_initial_range_of_size_of_range_if_instant_range_is_greater_than_range() {
		Olap.Range olapRange = mediumRange(olap);
		Olap.ZoomGroup zoomGroup = olap.create().zoomGroup();
		zoomGroup.create().zoom(Day).create().instantRange(10, 10);
		zoomGroup.create().zoom(SixHours).create().instantRange(56 , 84);

		TimeScaleHandler timeScaleHandler = new TimeScaleHandler(bounds(olapRange, zoomGroup.zoomList()), asList(Day, SixHours, Hour, FifteenMinutes), Day);
		TimeRange range = timeScaleHandler.range();

		assertThat(range, is(new TimeRange(instant(2010, 1, 1), instant(2010, 1, 5), Day)));
	}

	@Test
	public void should_notify_range_change_if_new_range_is_valid_for_current_scale() {
		timeScaleHandler.onScaleChange(timeRangeListener);
		timeScaleHandler.onRangeChange(timeRangeListener);
		timeScaleHandler.updateRange(instant(2010, 4, 1), instant(2010, 12, 1));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 4, 1), instant(2010, 12, 1), Month)));
	}

	@Test
	public void should_go_to_next_scale_if_new_range_is_lower_than_boundary() {
		timeScaleHandler.onScaleChange(timeRangeListener);
		timeScaleHandler.updateRange(instant(2010, 7, 1), instant(2010, 8, 1));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 5, 27), instant(2010, 9, 2), Week)));
	}

	@Test
	public void should_go_to_upper_scale_if_new_range_is_greater_than_boundary() {
		timeScaleHandler.updateRange(instant(2010, 7, 1), instant(2010, 8, 1));

		timeScaleHandler.onScaleChange(timeRangeListener);
		timeScaleHandler.updateRange(instant(2010, 1, 1), instant(2010, 8, 1));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 1, 1), instant(2010, 9, 1), Month)));
	}

	@Test
	public void should_keep_range_if_new_range_is_out_of_range_and_notify_not_valid_range() {
		timeScaleHandler.onNotValidRange(timeRangeListener);
		timeScaleHandler.updateRange(instant(2011, 7, 1), instant(2011, 8, 1));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 1, 1), instant(2010, 5, 1), Month)));
		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 1, 1), instant(2010, 5, 1), Month)));
	}

	@Test
	public void should_update_range_when_a_different_scale_is_selected() {
		timeScaleHandler.onScaleChange(timeRangeListener);
		timeScaleHandler.updateScale(Day);

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 2, 16), instant(2010, 3, 16), Day)));
	}

	@Test
	public void should_stay_on_same_range_if_new_range_is_bigger_than_upper_bound_and_no_more_scales_are_available() {
		timeScaleHandler.onNotValidRange(timeRangeListener);
		timeScaleHandler.updateRange(instant(2010, 1, 1), instant(2010, 12, 1));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue().scale(), is(Month));
	}

	@Test
	public void should_give_range_of_the_same_size_of_current_range_on_left() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));

		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		assertThat(timeScaleHandler.leftRange(), is(new TimeRange(instant(2010, 1, 1), instant(2010, 3, 1), Week)));
	}

	@Test
	public void should_give_range_of_the_same_size_of_current_range_on_right() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));

		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		assertThat(timeScaleHandler.rightRange(), is(new TimeRange(instant(2010, 6, 7), instant(2010, 9, 13), Week)));
	}

	@Test
	public void should_give_smaller_range_on_left_if_it_is_outside_range() {
		timeScaleHandler.updateRange(instant(2010, 1, 12), instant(2010, 1, 30));

		assertThat(timeScaleHandler.leftRange(), is(new TimeRange(instant(2010, 1, 1), instant(2010, 1, 1), Week)));
	}

	@Test
	public void should_give_smaller_range_on_right_if_it_is_outside_range() {
		timeScaleHandler.updateRange(instant(2010, 12, 12), instant(2010, 12, 30));

		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 10, 31), instant(2010, 12, 31), Week)));
		assertThat(timeScaleHandler.rightRange(), is(new TimeRange(instant(2010, 12, 31), instant(2010, 12, 31), Week)));
	}

	@Test
	public void should_notify_range_change_on_move() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));
		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		timeScaleHandler.onRangeChange(timeRangeListener);

		timeScaleHandler.move(instant(2010, 2, 1), instant(2010, 5, 7));

		verify(timeRangeListener).accept(rangeCaptor.capture());
		assertThat(rangeCaptor.getValue(), is(new TimeRange(instant(2010, 1, 29), instant(2010, 5, 7), Week)));
	}

	@Test
	public void should_return_range_to_load_twice_taking_into_account_previous_load_moving_left() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));
		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		TimeRange firstRange = timeScaleHandler.move(instant(2010, 2, 21), instant(2010, 5, 30));
		assertThat(firstRange, is(new TimeRange(instant(2010, 2, 7), instant(2010, 2, 21), Week)));

		TimeRange secondRange = timeScaleHandler.move(instant(2010, 2, 10), instant(2010, 5, 19));
		assertThat(secondRange, is(new TimeRange(instant(2010, 1, 24), instant(2010, 2, 7), Week)));
	}

	@Test
	public void should_return_range_to_load_twice_taking_into_account_previous_load_moving_right() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));

		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		TimeRange firstRange = timeScaleHandler.move(instant(2010, 4, 1), instant(2010, 7, 14));
		assertThat(firstRange, is(new TimeRange(instant(2010, 7, 14), instant(2010, 8, 18), Week)));

		TimeRange secondRange = timeScaleHandler.move(instant(2010, 5, 1), instant(2010, 8, 7));
		assertThat(secondRange, is(new TimeRange(instant(2010, 8, 18), instant(2010, 9, 22), Week)));
	}

	@Test
	public void should_restart_last_loaded_point_on_scale_change() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));

		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		TimeRange firstRange = timeScaleHandler.move(instant(2010, 2, 21), instant(2010, 5, 30));
		assertThat(firstRange, is(new TimeRange(instant(2010, 2, 7), instant(2010, 2, 21), Week)));

		timeScaleHandler.updateRange(instant(2010, 3, 1), instant(2010, 9, 1));
		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 2, 1), instant(2010, 10, 1), Month)));

		TimeRange secondRange = timeScaleHandler.move(instant(2010, 3, 1), instant(2010, 11, 1));
		assertThat(secondRange, is(new TimeRange(instant(2010, 11, 1), instant(2010, 12, 31), Month)));
	}

	@Test
	public void should_keep_range_size_if_move_range_gives_smaller_range() {
		timeScaleHandler.updateRange(instant(2010, 4, 12), instant(2010, 4, 30));
		assertThat(timeScaleHandler.range(), is(new TimeRange(instant(2010, 3, 1), instant(2010, 6, 7), Week)));

		Instant initialFrom = timeScaleHandler.range().from();
		Instant initialTo = timeScaleHandler.range().to();

		timeScaleHandler.move(of(initialFrom).plusWeeks(10).toInstant(), of(initialTo).plusWeeks(1).toInstant());

		assertThat(timeScaleHandler.range(), is(new TimeRange(of(initialFrom).plusWeeks(10).toInstant(), of(initialTo).plusWeeks(10).toInstant(), Week)));
	}

	@Test
	public void should_not_reset_last_loaded_range_if_update_range_does_not_change_scale() {
		TimeScaleHandler handler = spy(timeScaleHandler);

		handler.updateRange(instant(2010, 4, 1), instant(2010, 12, 1));

		verify(handler, never()).resetLoadedPoints();
	}
	
	private TimeScaleHandler handlerWithScaleStartingOnDays() {
		Olap.Range range = initialRange(olap);
		Olap.ZoomGroup zoomGroup = olap.create().zoomGroup();
		zoomGroup.create().zoom(Day).create().instantRange(10, 30);
		zoomGroup.create().zoom(SixHours).create().instantRange(56 , 84);
		zoomGroup.create().zoom(Hour).create().instantRange(24, 168);
		zoomGroup.create().zoom(FifteenMinutes).create().instantRange(32, 96);

		return new TimeScaleHandler(bounds(range, zoomGroup.zoomList()), asList(Day, SixHours, Hour, FifteenMinutes), Day);
	}

	private static Olap.Range rangeOf(Olap olap) {
		Olap.Range range = olap.create().range();
		range.from(new RangeFrom());
		range.to(new RangeTo());
		return range;
	}

	private static Olap.Range initialRange(Olap olap) {
		Olap.Range range = olap.create().range();
		range.from(new RangeInitialFrom());
		range.to(new RangeInitialTo());
		return range;
	}

	private static Olap.Range mediumRange(Olap olap) {
		Olap.Range range = olap.create().range();
		range.from(new RangeInitialFrom());
		range.to(new RangeMediumTo());
		return range;
	}

	private static Olap.ZoomGroup zoomGroup(Olap olap) {
		Olap.ZoomGroup zoomGroup = olap.create().zoomGroup();
		zoomGroup.create().zoom(Year).create().instantRange(6, 12);
		zoomGroup.create().zoom(Month).create().instantRange(4, 9);
		zoomGroup.create().zoom(Week).create().instantRange(4, 15);
		zoomGroup.create().zoom(Day).create().instantRange(7, 28);
		return zoomGroup;
	}

	private static Instant instant(int year, int month, int day) {
		return instant(year, month, day, 0, 0);
	}

	private static Instant instant(int year, int month, int day, int hour, int minute) {
		return of(year, month, day, hour, minute).toInstant();
	}

	public static class RangeFrom implements InstantLoader {
		@Override
		public Instant load(String username) {
			return instant(2010, 1, 1);
		}
	}

	public static class RangeInitialFrom implements InstantLoader {
		@Override
		public Instant load(String username) {
			return instant(2010, 1, 1, 0, 0);
		}
	}

	public static class RangeTo implements InstantLoader {
		@Override
		public Instant load(String username) {
			return instant(2010, 12, 31);
		}
	}

	public static class RangeInitialTo implements InstantLoader {
		@Override
		public Instant load(String username) {
			return instant(2010, 12, 31, 23, 59);
		}
	}

	public static class RangeMediumTo implements InstantLoader {
		@Override
		public Instant load(String username) {
			return instant(2010, 1, 5, 0, 0);
		}
	}

	private Bounds bounds(Olap.Range range, List<Olap.ZoomGroup.Zoom> zoomList) {
		Bounds bounds = new Bounds();
		bounds.range(new TimeRange(range.from(""), range.to(""), TimeScale.Day));
		bounds.mode(FromTheBeginning);
		bounds.zooms(zoomList.stream().collect(toMap(Olap.ZoomGroup.Zoom::scale, zoom -> new Zoom().min(zoom.instantRange().min()).max(zoom.instantRange().max()))));
		return bounds;
	}

}
