package com.monentia.happysense.displays;


import io.intino.sumus.box.displays.TimeSeriesChartDisplay;
import io.intino.sumus.graph.Olap;

import java.util.List;

import static io.intino.sumus.graph.rules.TimeScale.Day;

//@RunWith(MockitoJUnitRunner.class)
public class TimeLineDisplay_ extends DisplayTest {

	private static Olap olap;
	private TimeSeriesChartDisplay display;

//	@Mock
//	private MessageCarrier messageCarrier;
//	@Captor
//	private ArgumentCaptor<Map<String, Object>> mapCaptor;
//	private static TimeScaleHandler handler;
//
//	@BeforeClass
//	public static void setUpClass() {
//		Graph model = Graph.use(new SumusStore(new File("")), TestSumus.class, Sumus.class).load("Model");
//		Sumus platform = model.wrapper(Sumus.class);
//		olap = platform.olapList().get(0);
//		handler = new TimeScaleHandler(timeline(olap), Olap.Select.Range.FromTheBeginning, zoomList(olap), singletonList(Day), Day);
//	}
//
//	@Before
//	public void setUp() {
//		categorizationDisplays = new TimeLineDisplay(mock(SumusBox.class));
//		categorizationDisplays.timeScaleHandler(handler);
//		categorizationDisplays.instantFormatter(formatter(olap));
//	}
//
//	private static Olap.Timeline timeline(Olap olap) {
////		return olap.newTimeline(instant(2010, 1, 1), instant(2010, 12, 31));
//		return olap.create().timeline();
//	}

	private static List<Olap.ZoomGroup.Zoom> zoomList(Olap olap) {
		Olap.ZoomGroup zoomGroup = olap.create().zoomGroup();
		zoomGroup.create().zoom(Day).create().instantRange(7, 28);
		return zoomGroup.zoomList();
	}

	private static Olap.InstantFormatter formatter(Olap olap) {
		Olap.InstantFormatter formatter = olap.create().instantFormatter();
		formatter.create().formatter(Day, "dd/MM/yyyy");
		return formatter;
	}

}
