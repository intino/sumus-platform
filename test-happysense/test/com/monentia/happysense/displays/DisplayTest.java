package com.monentia.happysense.displays;

import io.intino.sumus.graph.SumusGraph;
import io.intino.tara.magritte.Graph;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class DisplayTest {
	static final Graph graph;
	static final SumusGraph platform;

	static {
		graph = new Graph().loadStashes("Model");
		platform = graph.as(SumusGraph.class);
	}

	static void assertThatWasSentToSelf(Map<String, Object> data) {
		assertThat(data.containsKey("id"), is(true));
	}

}
