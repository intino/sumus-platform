package com.monentia.happysense.analytics.query;

import io.intino.sumus.analytics.metricscalers.LinearScaler;
import io.intino.sumus.analytics.metricscalers.TimeScaler;
import io.intino.sumus.graph.Metric;
import io.intino.sumus.graph.TemporalMetric;
import io.intino.tara.magritte.Graph;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MetricScaler_ {

	private static io.intino.sumus.analytics.MetricScaler scaler;
	private static List<Metric.Unit> units;
	private static TimeScaler temporalScaler;
	private static List<TemporalMetric.Unit> temporalUnits;

	@BeforeClass
	public static void setUpClass() {
		Graph wrap = new Graph().loadStashes("Metric");
		scaler = new LinearScaler();
		units = units(wrap.rootList(Metric.class));
		temporalScaler = new TimeScaler();
		temporalUnits = temporalUnits(wrap.rootList(TemporalMetric.class));
	}

	private static List<Metric.Unit> units(List<Metric> metrics) {
		return metrics.stream()
				.map(Metric::metricUnitList)
				.flatMap(Collection::stream)
				.collect(toList());
	}

	private static List<TemporalMetric.Unit> temporalUnits(List<TemporalMetric> metrics) {
		return metrics.stream()
				.map(TemporalMetric::temporalMetricUnitList)
				.flatMap(Collection::stream)
				.collect(toList());
	}

	@Test
	public void when_value_is_between_1_KWh_and_1_MWh_should_return_KWh() {
		assertThat(scaler.scaler(1, unitForLabel("KWh")).unitLabel(), is("KWh"));
		assertThat(scaler.scaler(1200, unitForLabel("Wh")).unitLabel(), is("KWh"));
		assertThat(scaler.scaler(1200, unitForLabel("Wh")).scale(1200), is(1.2));
	}

	@Test
	public void when_value_is_between_1_MWh_and_1_TWh_should_return_KWh() {
		assertThat(scaler.scaler(1, unitForLabel("MWh")).unitLabel(), is("MWh"));
		assertThat(scaler.scaler(1200000, unitForLabel("Wh")).unitLabel(), is("MWh"));
		assertThat(scaler.scaler(1200000, unitForLabel("Wh")).scale(1200000), is(1.2));
	}

	@Test
	public void when_value_is_between_1_Wh_and_1000_Wh_should_return_Wh() {
		assertThat(scaler.scaler(5, unitForLabel("Wh")).unitLabel(), is("Wh"));
		assertThat(scaler.scaler(1200, unitForLabel("mWh")).unitLabel(), is("Wh"));
		assertThat(scaler.scaler(1200, unitForLabel("mWh")).scale(1200), is(1.2));
	}

	@Test
	public void when_value_is_0_Wh_should_return_Wh() {
		assertThat(scaler.scaler(0, unitForLabel("Wh")).unitLabel(), is("Wh"));
		assertThat(scaler.scaler(0, unitForLabel("KWh")).unitLabel(), is("Wh"));
		assertThat(scaler.scaler(0, unitForLabel("pWh")).unitLabel(), is("Wh"));
	}

	@Test
	public void when_temporal_scales_are_used_results_are_right() {
		assertThat(temporalScaler.scaler(1, temporalUnit("s")).unitLabel(), is("s"));
		assertThat(temporalScaler.scaler(75, temporalUnit("s")).unitLabel(), is("m"));
		assertThat(temporalScaler.scaler(75, temporalUnit("s")).scale(75), is(1.25));
		assertThat(temporalScaler.scaler(4500, temporalUnit("s")).unitLabel(), is("h"));
		assertThat(temporalScaler.scaler(4500, temporalUnit("s")).scale(4500), is(1.25));
		assertThat(temporalScaler.scaler(3600 * 24 * 1.25, temporalUnit("s")).unitLabel(), is("h"));
		assertThat(temporalScaler.scaler(3600 * 24 * 1.25, temporalUnit("s")).scale(3600 * 24 * 1.25), is(30.0));
		assertThat(temporalScaler.scaler(3600 * 24 * 365 * 1.25, temporalUnit("s")).unitLabel(), is("y"));
		assertThat(temporalScaler.scaler(3600 * 24 * 365 * 1.25, temporalUnit("s")).scale(3600 * 24 * 365 * 1.25), is(1.25));
	}

	private Metric.Unit unitForLabel(String label) {
		return units.stream()
				.filter(unit -> unit.label().equals(label)).findFirst().get();
	}

	private TemporalMetric.Unit temporalUnit(String label) {
		return temporalUnits.stream()
				.filter(unit -> unit.label().equals(label)).findFirst().get();
	}
}
