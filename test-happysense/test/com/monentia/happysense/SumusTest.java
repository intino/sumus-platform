package com.monentia.happysense;

import io.intino.sumus.Settings;
import io.intino.sumus.SumusStore;
import io.intino.sumus.graph.SumusGraph;
import io.intino.tara.magritte.Graph;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;

import java.io.File;
import java.io.IOException;

import static java.util.Collections.emptyMap;

public class SumusTest {
	protected static Graph graph;
	public static SumusGraph platform;

	private static final String Model = "Model";
	private static final String StoreDirectory = "../test-store-happysense";

	@BeforeClass
	public static void setUpClass() {
		//cleanStore();
		graph = new Graph(new SumusStore(new File(StoreDirectory))).loadStashes(Model);
		platform = graph.as(SumusGraph.class);
		SumusGraph.Settings = new Settings(emptyMap());
		platform.init();
	}

	private static void cleanStore() {
		try {
			File directory = new File(StoreDirectory);
			if (!directory.exists()) return;
			FileUtils.deleteDirectory(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
