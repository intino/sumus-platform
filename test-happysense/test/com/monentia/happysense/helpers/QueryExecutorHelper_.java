package com.monentia.happysense.helpers;

import com.monentia.happysense.SumusTest;
import io.intino.sumus.analytics.TimeRange;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.graph.rules.TimeScale;
import io.intino.sumus.helpers.TimeSeriesHelper;
import io.intino.sumus.helpers.UtcDateTime;
import io.intino.sumus.queries.TimeSeriesQuery;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

public class QueryExecutorHelper_ extends SumusTest {

	@Test
	public void should_resolve_simple_query_in_less_than_400_ms() throws Exception {
		TimeSeriesQuery query = createQuery();
		TimeSeriesHelper service = service();

		long start = System.nanoTime();
		service.execute(query);
		long end = System.nanoTime();

		assertThat((end - start) / 1e6, is(lessThan(400.0)));
	}

	@Test
	public void should_resolve_simple_query_launched_twice_in_less_than_50_ms() throws Exception {
		TimeSeriesQuery query = createQuery();
		TimeSeriesHelper service = service();

		service.execute(query);

		long start = System.nanoTime();
		service.execute(query);
		long end = System.nanoTime();

		assertThat((end - start) / 1e6, is(lessThan(50.0)));
	}

	@Test
	public void name() throws Exception {
		TimeSeriesQuery query = createQuery();
		TimeSeriesHelper service = service();

		List<Serie> result = service.execute(query);
		System.out.println(result.get(0).values());
	}

	private TimeSeriesHelper service() {
		return new TimeSeriesHelper();
	}

	private TimeSeriesQuery createQuery() {
		TimeSeriesQuery.Builder builder = new TimeSeriesQuery.Builder();
		builder.addTicket(happinessTicket());
		return builder.build(defaultNameSpace(), new TimeRange(from(), to(), TimeScale.Hour));
	}

	private NameSpace defaultNameSpace() {
		return graph.as(SumusGraph.class).nameSpaceList().stream().filter(ns -> ns.name$().equals("default")).findFirst().orElse(null);
	}

	private Instant from() {
		return UtcDateTime.of(2015, 1, 1, 0, 0).toInstant();
	}

	private Instant to() {
		return UtcDateTime.of(2015, 1, 1, 23, 59).toInstant();
	}

	private Ticket happinessTicket() {
		return platform.ticketList(t -> t.name$().equals("happinessTicket")).findFirst().orElse(null);
	}
}