package com.monentia.happysense.graph;

import io.intino.tara.magritte.Graph;

public class TestHappysenseGraph extends com.monentia.happysense.graph.AbstractGraph {

	public TestHappysenseGraph(Graph graph) {
		super(graph);
	}

	public TestHappysenseGraph(io.intino.tara.magritte.Graph graph, TestHappysenseGraph wrapper) {
	    super(graph, wrapper);
	}
}