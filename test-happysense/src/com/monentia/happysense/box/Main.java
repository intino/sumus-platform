package com.monentia.happysense.box;

import io.intino.konos.alexandria.Box;
import io.intino.konos.alexandria.activity.displays.providers.StampDisplayProvider;
import io.intino.sumus.Settings;
import io.intino.sumus.SumusStore;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.helpers.TranslatorHelper;
import io.intino.tara.magritte.Graph;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Main {

	public static void main(String[] args) {
		TestHappysenseConfiguration configuration = new TestHappysenseConfiguration(args);
		Settings settings = new Settings(configuration.args());
		SumusGraph.Settings = settings;
		Graph graph = new Graph(new SumusStore(configuration.store())).loadStashes(paths(configuration));
		Box box = new TestHappysenseBox(configuration).put(graph).put(settings).put(new TranslatorHelper(graph.as(SumusGraph.class))).open();
		injectStampDisplayProvider(box);
		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}

	private static String[] paths(TestHappysenseConfiguration configuration) {
		Map<String, String> map = configuration.args();
		return map.containsKey("graph_paths") ? map.get("graph_paths").split(" ") : new String[]{"Sumus"};
	}

	private static void injectStampDisplayProvider(Box box) {
		((SumusBox)box.owner()).stampDisplayProvider(new StampDisplayProvider() {
			private Map<String, Function<String, StampDisplay>> stampDisplayProviderMap = null;

			@Override
			public StampDisplay display(String stamp) {
				return null;
			}

			@Override
			public TemporalStampDisplay temporalDisplay(String stamp) {
				loadProviders();
				if (!stampDisplayProviderMap.containsKey(stamp))
					return null;
				return (TemporalStampDisplay) stampDisplayProviderMap.get(stamp).apply(stamp);
			}

			private void loadProviders() {
				if (stampDisplayProviderMap != null) return;
				stampDisplayProviderMap = new HashMap<>();
				stampDisplayProviderMap.put("evolucion", s -> new EvolucionDisplay((TestHappysenseBox) box));
				stampDisplayProviderMap.put("brothers", s -> new BrothersDisplay((TestHappysenseBox) box));
				stampDisplayProviderMap.put("cousins", s -> new CousinsDisplay((TestHappysenseBox) box));
				stampDisplayProviderMap.put("opinions", s -> new OpinionsDisplay((TestHappysenseBox) box));
			}
		});
	}

}