package com.monentia.happysense.box.dialogs;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.graph.ContactRequest;
import com.monentia.happysense.graph.Opinion;
import io.intino.konos.alexandria.activity.model.Dialog;

import java.time.Instant;

import static io.intino.konos.alexandria.activity.displays.DialogExecution.Modification;

public class HappysenseOpinionDialog extends HappysenseOpinionDialogDisplay {

	public HappysenseOpinionDialog(TestHappysenseBox box) {
		super(box);
	}

	public void contactRequest(String id) {
		TestHappysenseBox box = box();
		dialog().target(box.graph().core$().load(id).as(ContactRequest.class));
	}

	@Override
	public void prepare() {
		if (dialog().target() == null) return;
		dialog().input("felicidad").value("C");
		dialog().input("ejemplo.0").value("mcaballero");
	}

	public static class Toolbar {
		public static Modification send(TestHappysenseBox box, Dialog dialog, Dialog.Toolbar.Operation operation, String username) {
			Opinion felicidad = box.graph().create().opinion(dialog.input("felicidad").value().asString(), Instant.now());
			ContactRequest target = dialog.target();
			target.opinions().add(felicidad);
			return Modification.ItemModified;
		}

		public static Modification operacion2(TestHappysenseBox box, Dialog dialog, Dialog.Toolbar.Operation operation, String username) {
			return Modification.None;
		}

		public static Modification operacion1(TestHappysenseBox box, Dialog dialog, Dialog.Toolbar.Operation operation, String username) {
			return null;
		}
	}

	public static class Validators {
	}

	public static class Sources {
		public static java.util.List<String> otroValues(TestHappysenseBox box, Dialog.Tab.ComboBox self) {
			return java.util.Collections.emptyList();
		}
	}
}