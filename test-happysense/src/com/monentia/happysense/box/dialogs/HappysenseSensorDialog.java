package com.monentia.happysense.box.dialogs;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.graph.Sensor;
import io.intino.konos.alexandria.activity.model.Dialog;

import static io.intino.konos.alexandria.activity.displays.DialogExecution.Modification;

public class HappysenseSensorDialog extends HappysenseSensorDialogDisplay {

	public HappysenseSensorDialog(TestHappysenseBox box) {
		super(box);
	}

	public void sensor(String id) {
		TestHappysenseBox box = box();
		dialog().target(box.graph().sensorList().stream().filter(c -> c.core$().id().equals(id)).findFirst().orElse(null));
	}

	@Override
	public void prepare() {
		Sensor sensor = dialog().target();
		dialog().input("Título").value(sensor.label());
		dialog().input("Número de componentes").value(sensor.components());
		dialog().input("Peso").value(sensor.weight());
	}

	public static class Toolbar {
		public static Modification send(TestHappysenseBox box, Dialog dialog, Dialog.Toolbar.Operation operation, String username) {
			Sensor sensor = dialog.target();
			sensor.label(dialog.input("Título").value().asString());
			sensor.components(dialog.input("Número de componentes").value().asInteger());
			sensor.weight(dialog.input("Peso").value().asDouble());
			return Modification.ItemModified;
		}
	}

	public static class Validators {
	}

	public static class Sources {
	}
}