package com.monentia.happysense.box.displays;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.box.displays.notifiers.HappysenseEvolucionDisplayNotifier;
import com.monentia.happysense.graph.HappinessView;
import io.intino.konos.alexandria.activity.displays.AlexandriaTemporalStampDisplay;
import io.intino.sumus.graph.Record;


public class HappysenseEvolucionDisplay extends AlexandriaTemporalStampDisplay<HappysenseEvolucionDisplayNotifier> {
    private TestHappysenseBox box;

    public HappysenseEvolucionDisplay(TestHappysenseBox box) {
        super();
        this.box = box;
    }

    public HappinessView happinessView() {
        Record record = item().object();
        return record.a$(HappinessView.class);
    }

    @Override
    public void refresh() {
        super.refresh();
        notifier.refresh(happinessView().label());
    }

}