package com.monentia.happysense.box.displays;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.box.displays.notifiers.HappysenseApplicationDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;


public class HappysenseApplicationDisplay extends AlexandriaDisplay<HappysenseApplicationDisplayNotifier> {
    private TestHappysenseBox box;

    public HappysenseApplicationDisplay(TestHappysenseBox box) {
        super();
        this.box = box;
    }

	@Override
	protected void init() {
		super.init();

//		AlexandriaTemporalTimeCatalogDisplay display = new AlexandriaTemporalTimeCatalogDisplay(box);
//		display.catalog(box.graph().dashboard());
//		addAndPersonify(display);

//        AlexandriaTemporalRangeCatalogDisplay eventCatalogDisplay = new AlexandriaTemporalRangeCatalogDisplay(box);
//        eventCatalogDisplay.stampDisplayProvider(stamp -> {
//            if (!stampDisplayProviderMap.containsKey(stamp))
//                return null;
//            return stampDisplayProviderMap.get(stamp).apply(stamp);
//        });
//        eventCatalogDisplay.catalog(box.graph().contactRequests().asEventContainer());
//        addAndPersonify(eventCatalogDisplay);
	}

}