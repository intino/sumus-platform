package com.monentia.happysense.box.displays;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.box.displays.notifiers.HappysenseCousinsDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;


public class HappysenseCousinsDisplay extends AlexandriaDisplay<HappysenseCousinsDisplayNotifier> {
    private TestHappysenseBox box;

    public HappysenseCousinsDisplay(TestHappysenseBox box) {
        super();
        this.box = box;
    }

}