package com.monentia.happysense.box;

import com.monentia.happysense.graph.Area;
import com.monentia.happysense.graph.ContactRequest;
import com.monentia.happysense.graph.Country;
import com.monentia.happysense.graph.TestHappysenseGraph;
import io.intino.sumus.Settings;
import io.intino.sumus.SumusStore;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.datawarehouse.store.PathBuilder;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.queries.temporalrecord.Query;
import io.intino.sumus.queries.temporalrecord.QueryExecutor;
import io.intino.tara.magritte.Graph;

import java.net.URL;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static io.intino.konos.alexandria.activity.model.TimeScale.Day;

public class MockDataGenerator {

	public static void generate(String[] args) {
		TestHappysenseConfiguration configuration = new TestHappysenseConfiguration(args);
		Settings settings = new Settings(configuration.args());
		SumusGraph.Settings = settings;
		Graph graph = new Graph(new SumusStore(configuration.store())).loadStashes(paths(configuration));
		graph.as(SumusGraph.class).init();
		createMockData(graph.as(TestHappysenseGraph.class));
	}

	private static void createMockData(TestHappysenseGraph graph) {
		createMockContactRequests(graph);
		//createMockDigest(graph);
	}

	private static void createMockContactRequests(TestHappysenseGraph graph) {
		if(!QueryExecutor.execute(new Query(graph.global(), ContactRequest.class, now())).values().isEmpty()) return;

		Country germany = graph.countryList(c -> c.name$().equals("Germany")).findFirst().orElse(null);
		Country spain = graph.countryList(c -> c.name$().equals("Spain")).findFirst().orElse(null);
		Country mexico = graph.countryList(c -> c.name$().equals("Mexico")).findFirst().orElse(null);
		Area animacion = graph.areaList(c -> c.name$().equals("animacion")).findFirst().orElse(null);
		Area estancia = graph.areaList(c -> c.name$().equals("estancia")).findFirst().orElse(null);
		Area calidadDeLaComida = graph.areaList(c -> c.name$().equals("calidadDeLaComida")).findFirst().orElse(null);
		Area instalacionesDelHotel = graph.areaList(c -> c.name$().equals("instalacionesDelHotel")).findFirst().orElse(null);
		Area habitacion = graph.areaList(c -> c.name$().equals("habitacion")).findFirst().orElse(null);
		Area limpieza = graph.areaList(c -> c.name$().equals("limpieza")).findFirst().orElse(null);

		Instant minus = Instant.now().minus(1, ChronoUnit.DAYS);
		createMockContactRequest(graph, minus, "room 008", "Fran Hernández", "fhernandez@gmail.com", ContactRequest.Type.SatisfiedType, germany, animacion);
		createMockContactRequest(graph, minus, "room 009", "Sergio Alvarado", "salvarado@gmail.com", ContactRequest.Type.NotVerySatisfiedType, spain, estancia);
		createMockContactRequest(graph, minus, "room 010", "Sergio Alvarado", "salvarado@gmail.com", ContactRequest.Type.DissatisfiedType, mexico, calidadDeLaComida);

		Instant instant = Instant.now().truncatedTo(ChronoUnit.DAYS);
		createMockContactRequest(graph, instant, "room 001", "Mario Sánchez", "msanchez@gmail.com", ContactRequest.Type.SatisfiedType, germany, animacion);
		createMockContactRequest(graph, instant, "room 002", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.SatisfiedType, spain, estancia);
		createMockContactRequest(graph, instant, "room 003", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.NotVerySatisfiedType, mexico, calidadDeLaComida);
		createMockContactRequest(graph, instant, "room 004", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.DissatisfiedType, spain, instalacionesDelHotel);
		createMockContactRequest(graph, instant, "room 005", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.NotVerySatisfiedType, spain, habitacion);
		createMockContactRequest(graph, instant, "room 006", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.DissatisfiedType, spain, limpieza);
		createMockContactRequest(graph, instant, "room 007", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.SatisfiedType, spain, animacion);

		Instant plus = Instant.now().plus(1, ChronoUnit.DAYS);
		createMockContactRequest(graph, plus,"room 011", "Jose Falcón", "jfalcon@gmail.com", ContactRequest.Type.SatisfiedType, germany, animacion);
		createMockContactRequest(graph, plus, "room 012", "Auxiliadora Ramírez", "aramirez@gmail.com", ContactRequest.Type.SatisfiedType, germany, animacion);
	}

	private static void createMockContactRequest(TestHappysenseGraph graph, Instant created, String label, String fullName, String contact, ContactRequest.Type type, Country country, Area area) {
		graph.create(PathBuilder.temporalRecordPath(graph.global(), ContactRequest.class, new TimeStamp(created, Day)))
				.contactRequest(label, fullName, contact, type, contactRequestIcon(type), area, country, created.truncatedTo(ChronoUnit.DAYS))
				.save$();
	}

	private static URL contactRequestIcon(ContactRequest.Type type) {
		String icon = "satisfied.png";
		if (type == ContactRequest.Type.NotVerySatisfiedType) icon = "notverysatisfied.png";
		if (type == ContactRequest.Type.DissatisfiedType) icon = "dissatisfied.png";
		return MockDataGenerator.class.getResource("/images/" + icon);
	}

	private static void createMockDigest(TestHappysenseGraph graph) {
		Bucket bucket = graph.happysenseCube().getOrCreateBucket(graph.global(), Day, Instant.parse("2017-07-04T00:00:00Z"));
		bucket.digest(graph.area(0), graph.profile(0), graph.country(0))
				.add("count",  1);
		bucket.save();
	}

	private static TimeStamp now() {
		return new TimeStamp(Instant.now(), Day);
	}

	private static String[] paths(TestHappysenseConfiguration configuration) {
		Map<String, String> map = configuration.args();
		return map.containsKey("graph_paths") ? map.get("graph_paths").split(" ") : new String[]{"TestSumus"};
	}

}
