package com.monentia.happysense.box;

import com.monentia.happysense.graph.TestHappysenseGraph;
import io.intino.tara.magritte.Graph;

public class TestHappysenseBox extends AbstractBox {
	private TestHappysenseGraph graph;

	public TestHappysenseBox(String[] args) {
		super(args);
	}

	public TestHappysenseBox(TestHappysenseConfiguration configuration) {
		super(configuration);
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		super.put(o);
		if (o instanceof Graph) this.graph = ((Graph) o).as(TestHappysenseGraph.class);
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		return super.open();
	}

	public void close() {
		super.close();
	}

	public TestHappysenseGraph graph() {
		return graph;
	}

	static io.intino.konos.alexandria.activity.services.AuthService authService(java.net.URL authServiceUrl) {
		//TODO add your authService
		return null;
	}	
}