package com.monentia.happysense.box.actions;

import com.monentia.happysense.box.TestHappysenseBox;
import com.monentia.happysense.box.dialogs.HappysenseOpinionDialog;

public class OpinionPageAction extends PageAction {

	public TestHappysenseBox box;
	public String id;

	public String execute() {
		return super.template("opinionPage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
	    return new io.intino.konos.alexandria.activity.displays.Soul(session) {
			@Override
			public void personify() {
				HappysenseOpinionDialog display = new HappysenseOpinionDialog(box);
				display.contactRequest(id);
				register(display);
				display.personify();
			}
		};
	}
}