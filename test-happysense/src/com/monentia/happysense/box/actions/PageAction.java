package com.monentia.happysense.box.actions;

import com.monentia.happysense.box.TestHappysenseBox;

import java.net.URL;


public abstract class PageAction extends io.intino.konos.alexandria.activity.spark.actions.AlexandriaPageAction {
	public TestHappysenseBox box;

	public PageAction() {
		super("test-happysense");
	}

	@Override
	protected String title() {
		return "test application";
	}

	@Override
	protected URL favicon() {
		return null;
	}

}