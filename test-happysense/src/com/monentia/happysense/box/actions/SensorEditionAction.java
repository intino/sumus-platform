package com.monentia.happysense.box.actions;

import com.monentia.happysense.box.dialogs.HappysenseSensorDialog;


public class SensorEditionAction extends PageAction {
	public String name;

	public String execute() {
		return super.template("sensorEdition");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
	    return new io.intino.konos.alexandria.activity.displays.Soul(session) {
			@Override
			public void personify() {
				HappysenseSensorDialog display = new HappysenseSensorDialog(box);
				display.sensor(name);
				register(display);
				display.personify();
			}
		};
	}

}