package com.monentia.happysense.box.actions;

import com.monentia.happysense.box.displays.HappysenseApplicationDisplay;
import io.intino.konos.alexandria.activity.displays.Soul;

public class ApplicationPageAction extends PageAction {

	public String execute() {
		return template("applicationPage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
		return new Soul(session) {
			@Override
			public void personify() {
				HappysenseApplicationDisplay applicationDisplay = new HappysenseApplicationDisplay(box);
				register(applicationDisplay);
				applicationDisplay.personify();
			}
		};
	}

}