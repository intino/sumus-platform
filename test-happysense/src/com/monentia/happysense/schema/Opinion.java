package com.monentia.happysense.schema;

public class Opinion {

    private String opinion = "";
    private Boolean cancelled = true;
    private Boolean roomSet = true;
    private String room = "";
    private Boolean contact = true;
    private String question = "";
    private Integer touchCounter = 0;
    private String instant = "";

    public String opinion() {
        return this.opinion;
    }

    public Boolean cancelled() {
        return this.cancelled;
    }

    public Boolean roomSet() {
        return this.roomSet;
    }

    public String room() {
        return this.room;
    }

    public Boolean contact() {
        return this.contact;
    }

    public String question() {
        return this.question;
    }

    public Integer touchCounter() {
        return this.touchCounter;
    }

    public String instant() {
        return this.instant;
    }

    public Opinion opinion(String opinion) {
        this.opinion = opinion;
        return this;
    }

    public Opinion cancelled(Boolean cancelled) {
        this.cancelled = cancelled;
        return this;
    }

    public Opinion roomSet(Boolean roomSet) {
        this.roomSet = roomSet;
        return this;
    }

    public Opinion room(String room) {
        this.room = room;
        return this;
    }

    public Opinion contact(Boolean contact) {
        this.contact = contact;
        return this;
    }

    public Opinion question(String question) {
        this.question = question;
        return this;
    }

    public Opinion touchCounter(Integer touchCounter) {
        this.touchCounter = touchCounter;
        return this;
    }

    public Opinion instant(String instant) {
        this.instant = instant;
        return this;
    }

}