package com.monentia.happysense;

import com.monentia.happysense.graph.HappinessView;
import com.monentia.happysense.graph.TestHappysenseGraph;
import io.intino.sumus.graph.NameSpace;

import java.util.ArrayList;
import java.util.List;

public class Graph {

// TODO Mario -> Pasar a konos
//	public static String evolucionPath(io.intino.sumus.graph.Mold.Block.Stamp self, String object) {
//		return "/happiness/" + object + "/evolution";
//	}
//
//	public static String brothersPath(io.intino.sumus.graph.Mold.Block.Stamp self, String object) {
//		return "/happiness/" + object + "/brothers";
//	}
//
//	public static String cousinsPath(io.intino.sumus.graph.Mold.Block.Stamp self, String object) {
//		return "/happiness/" + object + "/cousins";
//	}
//
//	public static String addOpinionPath(io.intino.sumus.graph.Mold.Block.Stamp self, String object) {
//		return "/contact-request/" + object + "/opinion";
//	}
//
//	public static String editSensorPath(io.intino.sumus.graph.Mold.Block.Stamp self, String object) {
//		return "/sensor/" + object;
//	}
//
//	public static URL journalUrl(io.intino.sumus.graph.Mold.Block.Stamp self, String item) {
//		try {
//			return new URL("http://www.elmundo.es");
//		} catch (MalformedURLException e) {
//			return null;
//		}
//	}
//
//	public static Tree happinessViewTree(io.intino.sumus.graph.Mold.Block.Stamp self, Record record, String username) {
//		return treeOf(record.a$(HappinessView.class));
//	}
//
//	private static Tree treeOf(HappinessView view) {
//		Tree tree = new Tree();
//		List<HappinessView> parents = parents(view);
//		TreeItem item = treeItemOf(view);
//
//		for (HappinessView parent : parents) {
//			TreeItem parentItem = treeItemOf(parent);
//			parentItem.add(item);
//			item = parentItem;
//		}
//
//		tree.add(item, true);
//
//		return tree;
//	}

	private static List<HappinessView> parents(HappinessView view) {
		List<HappinessView> parents = new ArrayList<>();
		while (view.parent() != null) {
			parents.add(view.parent());
			view = view.parent();
		}
		return parents;
	}

// TODO Mario -> pasar a Konos
//	private static TreeItem treeItemOf(HappinessView view) {
//		return new TreeItem().name(view.core$().id()).label(view.label());
//	}
//
//	public static RecordLinks happinessViewChildren(io.intino.sumus.graph.Mold.Block.Stamp self, Record record) {
//		return hyperlinksOf(record.a$(HappinessView.class));
//	}
//
//	private static RecordLinks hyperlinksOf(HappinessView view) {
//		CatalogStampRecordLinks.RecordLinks links = new CatalogStampRecordLinks.RecordLinks();
//		view.children().forEach(child -> links.add(hyperlinkOf(child)));
//		return links;
//	}
//
//	private static RecordLink hyperlinkOf(HappinessView view) {
//		return new RecordLink().name(view.core$().id()).label(view.label());
//	}
//
//	public static Resource downloadHappinessView(io.intino.sumus.graph.Mold.Block.Stamp self, Record record, String option) {
//		return new Resource() {
//			@Override
//			public String label() {
//				return option + ".pdf";
//			}
//
//			@Override
//			public InputStream content() {
//				return new ByteArrayInputStream(new byte[0]);
//			}
//		};
//	}
//
//	public static Record defaultDashboardRecord(io.intino.sumus.graph.Catalog self, String name, String username, NameSpace nameSpace, TimeRange range) {
//		return self.graph().core$().clone().as(TestHappysenseGraph.class).create().happinessView("ejemplo", "", "", 0, null, Instant.now()).a$(Record.class);
//	}
//
//	public static boolean satisfiedContactRequests(io.intino.sumus.graph.Mold.Block.Stamp self, Record source, Record target) {
//		return target.a$(ContactRequest.class).type() == ContactRequest.Type.SatisfiedType;
//	}
//
//	public static boolean notVerySatisfiedContactRequests(io.intino.sumus.graph.Mold.Block.Stamp self, Record source, Record target) {
//		return target.a$(ContactRequest.class).type() == ContactRequest.Type.NotVerySatisfiedType;
//	}
//
//	public static boolean dissatisfiedContactRequests(io.intino.sumus.graph.Mold.Block.Stamp self, Record source, Record target) {
//		return target.a$(ContactRequest.class).type() == ContactRequest.Type.DissatisfiedType;
//	}
//
//	public static List<NameSpace> dashboardNameSpaces(io.intino.sumus.graph.Catalog self, String username) {
//		TestHappysenseGraph graph = self.graph().core$().as(TestHappysenseGraph.class);
//		return new ArrayList<NameSpace>() {{
//			add(graph.global());
//			add(graph.hotelStars());
//		}};
//	}
//
//	public static List<NameSpace> contactRequestsNameSpaces(io.intino.sumus.graph.Catalog self, String username) {
//		TestHappysenseGraph graph = self.graph().core$().as(TestHappysenseGraph.class);
//		return new ArrayList<NameSpace>() {{
//			add(graph.global());
//			add(graph.hotelStars());
//		}};
//	}

	public static List<NameSpace> happysenseOlapNameSpaces(io.intino.sumus.graph.Olap self, String username) {
		TestHappysenseGraph graph = self.graph().core$().as(TestHappysenseGraph.class);
		return new ArrayList<NameSpace>() {{
			add(graph.global());
			add(graph.hotelStars());
		}};
	}

	public static List<NameSpace> sensorOlapNameSpaces(io.intino.sumus.graph.Olap self, String username) {
		TestHappysenseGraph graph = self.graph().core$().as(TestHappysenseGraph.class);
		return new ArrayList<NameSpace>() {{
			add(graph.global());
			add(graph.hotelStars());
		}};
	}

// TODO Mario -> Pasar a konos
//	public static Resource exportHappinessView(io.intino.sumus.graph.Mold.Block.Stamp self, Record record, Instant from, Instant to, String option, String username) {
//		return new Resource() {
//			@Override
//			public String label() {
//				return "export.pdf";
//			}
//
//			@Override
//			public InputStream content() {
//				return new ByteArrayInputStream(new byte[0]);
//			}
//		};
//	}
//
//	public static StampSaveEvent.Refresh saveContactRequestTitle(io.intino.sumus.graph.Mold.Block.Title self, Record record, String value, String username) {
//		record.a$(ContactRequest.class).label(value);
//		return StampSaveEvent.Refresh.Record;
//	}

}
