var HappysenseBrothersBehaviors = HappysenseBrothersBehaviors || {};

HappysenseBrothersBehaviors.NotifierListener = {

    listenToDisplay : function() {
        var widget = this;
        this.when("refreshLabel").toSelf().execute(function(parameters) {
        	widget._refreshLabel(parameters.value);
        });
    }
};