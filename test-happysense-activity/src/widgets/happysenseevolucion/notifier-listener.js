var HappysenseEvolucionBehaviors = HappysenseEvolucionBehaviors || {};

HappysenseEvolucionBehaviors.NotifierListener = {

    listenToDisplay : function() {
        var widget = this;
        this.when("refresh").toSelf().execute(function(parameters) {
        	widget._refresh(parameters.value);
        });
    }
};