var HappysenseOpinionsBehaviors = HappysenseOpinionsBehaviors || {};

HappysenseOpinionsBehaviors.NotifierListener = {

    listenToDisplay : function() {
        var widget = this;
        this.when("refreshOpinions").toSelf().execute(function(parameters) {
        	widget._refreshOpinions(parameters.value);
        });
    }
};