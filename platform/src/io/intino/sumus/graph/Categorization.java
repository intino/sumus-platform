package io.intino.sumus.graph;

import io.intino.sumus.Category;
import io.intino.sumus.CategoryMap;
import io.intino.sumus.analytics.categorization.CategoryProcessor;
import io.intino.tara.magritte.Layer;

import java.util.List;

public class Categorization extends AbstractCategorization {

	public Categorization(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public List<Category> calculate(List<String> keys) {
		return CategoryProcessor.categorize(this, keys).toList();
	}

	public List<Category> calculate(List<? extends Layer> layers, List<String> keys) {
		return CategoryProcessor.categorize(this, layers, keys).toList();
	}

	public CategoryMap calculateMap(List<String> keys) {
		return CategoryProcessor.categorize(this, keys);
	}

	public CategoryMap calculateMap(List<? extends Layer> layers, List<String> keys) {
		return CategoryProcessor.categorize(this, layers, keys);
	}
	
}