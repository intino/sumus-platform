package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Node;
import io.intino.tara.lang.model.rules.NodeRule;

public class Named implements NodeRule {

    @Override
    public String errorMessage() {
        return "This node must be named";
    }

    public boolean accept(Node node) {
        return !node.isAnonymous();
    }
}
