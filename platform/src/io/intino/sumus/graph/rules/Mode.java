package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Rule;

public enum Mode implements Rule<Enum> {
    FromTheBeginning, ToTheLast;

    @Override
    public boolean accept(Enum value) {
        return value instanceof Mode;
    }
}
