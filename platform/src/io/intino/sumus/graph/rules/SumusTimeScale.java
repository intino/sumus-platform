package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Rule;

public enum SumusTimeScale implements Rule<Enum> {

	Year, QuarterOfYear, Month, Week, Day, SixHours, Hour, FifteenMinutes, Minute, Second;

	@Override
	public boolean accept(Enum value) {
		return value instanceof SumusTimeScale;
	}
}
