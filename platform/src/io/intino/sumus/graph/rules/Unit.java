package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Metric;

public enum Unit implements Metric<Integer> {
	Units(v -> v);

	private Converter<Integer> converter;

	Unit(Converter<Integer> converter) {
		this.converter = converter;
	}

	public Integer value(Integer value) {
		return converter.convert(value);
	}
}