package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Node;
import io.intino.tara.lang.model.rules.NodeRule;

public class GroupFilter implements NodeRule {

	public boolean accept(Node node) {
		return node.types().contains("Option") || node.types().contains("Options");
	}

	@Override
	public String errorMessage() {
		return "This element only accepts Option or Options";
	}

}
