package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Rule;

public enum Chart implements Rule<Enum> {
    TimeSeriesChart, TimeCrossTable, TimeBarChart, TimeScatterChart;

    @Override
    public boolean accept(Enum value) {
        return value instanceof Chart;
    }
}
