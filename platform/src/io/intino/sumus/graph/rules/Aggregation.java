package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.rules.variable.VariableRule;

import java.util.stream.DoubleStream;

public enum Aggregation implements VariableRule<Enum> {

	Sum {
		@Override
		public double aggregate(DoubleStream values) {
			try {
				return values.sum();
			}
			catch (NullPointerException ex) {
				return 0;
			}
		}
	};

	@Override
	public boolean accept(Enum value) {
		return value instanceof Aggregation;
	}

	public abstract double aggregate(DoubleStream values);
}
