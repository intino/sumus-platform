package io.intino.sumus.graph.rules;

import io.intino.tara.lang.model.Metric;

public enum Time implements Metric<Integer> {
	MilliSeconds(v -> v), Seconds(v -> v * 1000), Minutes(v -> 60 * 1000 * v),
	Hours(v -> 60 * 60 * 1000 * v), Days(v -> 24 * 60 * 60 * 1000 * v);

	private Converter<Integer> converter;

	Time(Converter<Integer> converter) {
		this.converter = converter;
	}

	public Integer value(Integer value) {
		return converter.convert(value);
	}
}