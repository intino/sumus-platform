package io.intino.sumus.graph.functions;

@FunctionalInterface
public interface RefreshListener {
	void onRefresh(io.intino.sumus.graph.functions.Command command);
}
