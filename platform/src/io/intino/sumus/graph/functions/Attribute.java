package io.intino.sumus.graph.functions;

public interface Attribute {
	String get(io.intino.tara.magritte.Layer item);
}
