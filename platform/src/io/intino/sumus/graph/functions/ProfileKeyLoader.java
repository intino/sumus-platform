package io.intino.sumus.graph.functions;

import java.util.List;

@FunctionalInterface
public interface ProfileKeyLoader {
	List<String> keys(String username);
}
