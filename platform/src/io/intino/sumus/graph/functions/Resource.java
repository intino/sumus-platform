package io.intino.sumus.graph.functions;

import java.io.InputStream;

public interface Resource {
	String label();
	InputStream content();
}
