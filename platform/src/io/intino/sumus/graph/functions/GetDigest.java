package io.intino.sumus.graph.functions;

import io.intino.sumus.datawarehouse.store.Digest;

@FunctionalInterface
public interface GetDigest {
    Digest digest(String ref);
}
