package io.intino.sumus.graph.functions;

@FunctionalInterface
public interface Converter {
	double convert(double value);
}
