package io.intino.sumus.graph.functions;

import io.intino.sumus.graph.NameSpace;

import java.util.List;

@FunctionalInterface
public interface NameSpacesLoader {
	List<NameSpace> nameSpaces(String username);
}
