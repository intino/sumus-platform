package io.intino.sumus.graph.functions;

import io.intino.sumus.graph.Record;

@FunctionalInterface
public interface ProfileChecker {
	boolean check(Record record, String username);
}
