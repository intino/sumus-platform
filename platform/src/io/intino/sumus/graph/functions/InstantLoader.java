package io.intino.sumus.graph.functions;

import java.time.Instant;

@FunctionalInterface
public interface InstantLoader {
	Instant load(String username);
}
