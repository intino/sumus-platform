package io.intino.sumus.graph.functions;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.graph.NameSpace;

import java.time.Instant;

@FunctionalInterface
public interface GetBucket {
    Bucket get(NameSpace nameSpace, TimeScale scale, Instant instant);
}
