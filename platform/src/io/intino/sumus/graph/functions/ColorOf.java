package io.intino.sumus.graph.functions;

import io.intino.sumus.graph.DataRetriever;
import io.intino.sumus.graph.Palette;

@FunctionalInterface
public interface ColorOf {
    String colorOf(DataRetriever dataRetriever, Palette.Color.Type type);
}
