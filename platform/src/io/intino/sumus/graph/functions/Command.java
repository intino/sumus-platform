package io.intino.sumus.graph.functions;

@FunctionalInterface
public interface Command {
	void execute();
}
