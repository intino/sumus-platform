package io.intino.sumus.graph.functions;

import io.intino.sumus.graph.Record;

@FunctionalInterface
public interface CheckRecord {
    boolean check(Record record);
}
