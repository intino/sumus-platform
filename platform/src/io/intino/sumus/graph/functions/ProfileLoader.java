package io.intino.sumus.graph.functions;

import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Record;

import java.util.List;

@FunctionalInterface
public interface ProfileLoader {
	io.intino.sumus.graph.functions.ProfileLoader.Profile load(String username);

	interface Profile {
		List<NameSpace> nameSpaces();
		List<String> keys();

		default boolean check(Record record) {
			return record.lock() == null || record.lock().isEmpty() || keys().contains(record.lock());
		}
	}
}
