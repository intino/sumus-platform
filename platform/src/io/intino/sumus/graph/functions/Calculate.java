package io.intino.sumus.graph.functions;

import io.intino.sumus.datawarehouse.store.Digest;

import java.util.List;

@FunctionalInterface
public interface Calculate {
    double calculate(List<Digest> digests);
}
