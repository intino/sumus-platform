package io.intino.sumus.graph;

import io.intino.sumus.analytics.catalog.CatalogProcessor;
import io.intino.tara.magritte.Graph;

import java.util.List;

import static java.util.Collections.emptyList;

public class SumusGraph extends io.intino.sumus.graph.AbstractGraph {
    public static io.intino.sumus.Settings Settings;

    public SumusGraph(Graph graph) {
        super(graph);
    }

	public SumusGraph(Graph graph, SumusGraph wrapper) {
		super(graph, wrapper);
	}

	public void init() {
        load();
        CatalogProcessor.addClusters(this);
    }

    private void load() {
        graph.loadStashes("filters.stash", "clusters.stash");
        this.update();
    }


	public boolean checkPermissions(Record record, String username) {
		return profileManager() == null || profileManager().check(record, username);
	}

	public List<NameSpace> userNameSpaces(String username) {
    	return profileManager() != null ? profileManager().profile(username).nameSpaces() : emptyList();
	}

	public List<String> userKeys(String username) {
		return profileManager() != null ? profileManager().profile(username).keys() : emptyList();
	}
}