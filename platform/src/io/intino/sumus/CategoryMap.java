package io.intino.sumus;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryMap extends HashMap<String, Category> {
	public List<Category> toList() {
		return this.values().stream().sorted(Comparator.comparing(Category::label)).collect(Collectors.toList());
	}
}
