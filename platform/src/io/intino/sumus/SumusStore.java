package io.intino.sumus;

import io.intino.sumus.helpers.UtcDateTime;
import io.intino.tara.io.Stash;
import io.intino.tara.magritte.stores.FileSystemStore;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Clock;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static io.intino.sumus.datawarehouse.store.PathBuilder.DigestsSuffix;
import static io.intino.sumus.datawarehouse.store.PathBuilder.EventsSuffix;
import static java.io.File.separator;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

public class SumusStore extends FileSystemStore {
	private Map<String, Instant> minNameSpaceInstants = new HashMap<>();
	private Map<String, Instant> maxNameSpaceInstants = new HashMap<>();

	public SumusStore(File file) {
		this(file, 60*60*1000);
	}

	public SumusStore(File file, long nameSpaceInstantCacheMillisPeriod) {
		super(file);
		createNameSpaceInstantReloader(nameSpaceInstantCacheMillisPeriod);
	}

	public File storeFolder(){
		return file;
	}

	@Override
	public void writeStash(Stash stash, String path) {
		super.writeStash(stash, path);
	}

	@Override
	public Stash stashFrom(String path) {
		Stash stash = super.stashFrom(path);
		return stash == null ? defaultStash() : stash;
	}

	private Stash defaultStash() {
		Stash defaultStash = new Stash();
		defaultStash.language = "Sumus";
		return defaultStash;
	}

	public Instant minInstantForNameSpace(String nameSpace) {
		if (!minNameSpaceInstants.containsKey(nameSpace)) {
			List<File> years = minYears(years(nameSpace));
			Instant instant = findInLowerThanDayScale(years)
					.findFirst().orElseGet(() -> findInLowerThanYearScale(years).findFirst().orElse(Instant.now(Clock.systemUTC())));
			minNameSpaceInstants.put(nameSpace, instant);
		}
		return minNameSpaceInstants.get(nameSpace);
	}

	public Instant maxInstantForNameSpace(String nameSpace) {
		if (!maxNameSpaceInstants.containsKey(nameSpace)) {
			List<File> years = maxYears(years(nameSpace));
			Instant instant = findInLowerThanDayScale(years)
					.reduce((l, r) -> r).orElseGet(() -> findInLowerThanYearScale(years).reduce((l, r) -> r).orElse(Instant.now(Clock.systemUTC())));
			maxNameSpaceInstants.put(nameSpace, instant);
		}
		return maxNameSpaceInstants.get(nameSpace);
	}

	public void clearNameSpaceRanges() {
		minNameSpaceInstants.clear();
		maxNameSpaceInstants.clear();
	}

	public String absoluteFilename(String storeFilename) {
		return file.getAbsoluteFile() + separator + storeFilename;
	}

	private Stream<Instant> findInLowerThanDayScale(List<File> years) {
		return years.stream()
				.map(File::toPath)
				.flatMap(p -> list(p).stream())
				.flatMap(p -> list(p).stream())
				.map(Path::toFile)
				.filter(this::isNotMacFile)
				.map(this::toInstant)
				.sorted();
	}

	private Stream<Instant> findInLowerThanYearScale(List<File> years) {
		return years.stream()
				.map(File::toPath)
				.flatMap(p -> list(p).stream())
				.map(Path::toFile)
				.filter(this::isNotMacFile)
				.map(this::toInstantUntilDay)
				.sorted();
	}

	private Instant toInstant(File file) {
		File dayFile = file.getParentFile();
		int day = Integer.valueOf(dayFile.getName());
		File yearFile = dayFile.getParentFile();
		int year = Integer.valueOf(yearFile.getName());
		String time = afterDash(file.getName());
		int hour = Integer.valueOf(time.substring(0, 2));
		int minutes = time.length() <= 2 ? 0 : Integer.valueOf(time.substring(2));
		return new UtcDateTime(year, 1, 1, hour, minutes).plusDays(day - 1).toInstant();
	}

	private Instant toInstantUntilDay(File file) {
		int day = Integer.valueOf(afterDash(file.getName()));
		File yearFile = file.getParentFile();
		int year = Integer.valueOf(yearFile.getName());
		return new UtcDateTime(year, 1, 1, 0, 0).plusDays(day - 1).toInstant();
	}

	private List<File> minYears(List<Path> years) {
		Pattern pattern = Pattern.compile("\\d+");
		int year = Calendar.getInstance().get(Calendar.YEAR);

		String min = String.valueOf(years.stream().mapToLong(d -> {
			Matcher matcher = pattern.matcher(d.toString());
			return (matcher.find()) ? Long.valueOf(matcher.group()) : 0;
		}).min().orElse(year));

		return years.stream().filter(d -> d.toString().contains(String.valueOf(min))).map(Path::toFile).sorted(File::compareTo).collect(toList());
	}

	private List<File> maxYears(List<Path> years) {
		Pattern pattern = Pattern.compile("\\d+");
		int year = Calendar.getInstance().get(Calendar.YEAR);

		String max = String.valueOf(years.stream().mapToLong(d -> {
			Matcher matcher = pattern.matcher(d.toString());
			return (matcher.find()) ? Long.valueOf(matcher.group()) : 0;
		}).max().orElse(year));

		return years.stream().filter(d -> d.toString().contains(String.valueOf(max))).map(Path::toFile).sorted(File::compareTo).collect(toList());
	}

	private List<Path> years(String nameSpace) {
		List<Path> directories = list(new File(file, nameSpace + DigestsSuffix).toPath(), directoryFilter());
		directories.addAll(list(new File(file, nameSpace + EventsSuffix).toPath(), directoryFilter()));
		return directories.stream().map(d -> list(d, directoryFilter()).stream()).flatMap(p -> p).collect(toList());
	}

	private boolean isMacFile(Path path) {
		return path.toString().contains(".DS_Store");
	}

	private boolean isNotMacFile(File file) {
		return !file.toString().contains(".DS_Store");
	}

	private List<Path> list(Path path) {
		return list(path, (entry) -> true);
	}

	private List<Path> list(Path path, DirectoryStream.Filter<Path> filter) {
		try {
			DirectoryStream<Path> paths = Files.newDirectoryStream(path, filter);
			Iterable<Path> iterable = paths::iterator;
			List<Path> result = stream(iterable.spliterator(), false).collect(toList());
			paths.close();
			return result;
		} catch (IOException e) {
			return emptyList();
		}
	}

	private DirectoryStream.Filter<Path> directoryFilter() {
		return (entry) -> Files.isDirectory(entry) && !isMacFile(entry);
	}

	private String afterDash(String fileName) {
		if (!fileName.contains("-"))
			return fileName;

		fileName = fileName.substring(fileName.indexOf("-") + 1, fileName.lastIndexOf("."));
		if (!fileName.contains("-"))
			return fileName;

		return fileName.substring(0, fileName.indexOf("-"));
	}

	private void createNameSpaceInstantReloader(long nameSpaceInstantCacheMillisPeriod) {
		Timer nameSpaceInstantReloader = new Timer();
		nameSpaceInstantReloader.schedule(new TimerTask() {
			@Override
			public void run() {
				clearNameSpaceRanges();
			}
		}, 0, nameSpaceInstantCacheMillisPeriod);
	}

}