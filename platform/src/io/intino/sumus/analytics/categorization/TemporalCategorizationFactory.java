package io.intino.sumus.analytics.categorization;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.temporal.*;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class TemporalCategorizationFactory {
    private static Map<TimeScale, Class<? extends TemporalCategorization>> categorizations = new HashMap<>();

    static {
        init();
    }

    public TemporalCategorizationFactory() {
        init();
    }

    private static void init() {
        categorizations.put(TimeScale.Year, YearCategorization.class);
        categorizations.put(TimeScale.QuarterOfYear, QuarterCategorization.class);
        categorizations.put(TimeScale.Month, MonthCategorization.class);
        categorizations.put(TimeScale.Week, WeekCategorization.class);
        categorizations.put(TimeScale.Day, DayCategorization.class);
        categorizations.put(TimeScale.SixHours, SixHoursCategorization.class);
        categorizations.put(TimeScale.Hour, HourCategorization.class);
        categorizations.put(TimeScale.FifteenMinutes, FifteenMinutesCategorization.class);
        categorizations.put(TimeScale.Minute, MinuteCategorization.class);
        categorizations.put(TimeScale.Second, SecondCategorization.class);
    }

    public TemporalCategorization get(TimeRange range) {
        try {
            Class<? extends TemporalCategorization> aClass = categorizations.get(range.scale());
            return aClass.getConstructor(TimeRange.class).newInstance(range);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

}
