package io.intino.sumus.analytics.categorization;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.CategoryMap;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.helpers.TranslatorHelper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TemporalCategorization extends Categorization {
    protected String name;
    protected String format;
    protected TranslatorHelper helper;
    protected String language;
    protected TimeRange range;
    private CategoryMap categoryMap;

    public TemporalCategorization(String name, String format, Categorization parent, TimeRange range) {
        super(null);
        this.name = name;
        this.parent = parent;
        this.format = format;
        update(range);
        createCategories();
    }

    public TemporalCategorization(TimeRange range) {
        this("TemporalCategorization", "", null, range);
    }

    public TemporalCategorization helper(TranslatorHelper helper) {
        this.helper = helper;
        setup();
        return this;
    }

    @Override
    public Categorization parent() {
        return parent;
    }

    private void update(TimeRange timeRange) {
        TimeScale scale = timeRange.scale();
        this.range = new TimeRange(scale.normalise(timeRange.from()), scale.normalise(timeRange.to()), scale);
        Optional.ofNullable(parent()).ifPresent(parent -> ((TemporalCategorization) parent).update(timeRange));
    }

    public TemporalCategorization language(String language) {
        this.language = language;
        setup();
        return this;
    }

    @Override
    public String name$() {
        return this.name;
    }

    public TimeRange range() {
        return range;
    }

    public String format() {
        return format;
    }

    public boolean isAvailableForScale(TimeScale scale) {
        return scale.ordinal() >= this.range().scale().ordinal();
    }

    public void range(TimeRange timeRange) {
        update(timeRange);
    }

    private void setup() {
        if (this.helper == null || this.language == null) return;
        this.label(helper.translate(name).into(language));
    }

    private void createCategories() {
        categoryMap = new CategoryMap();
        range.allInstants().forEach(instant -> {
            String label = labelOf(instant);
            if (!categoryMap.containsKey(label))
                categoryMap.put(label, new TemporalCategory(new TimeStamp(instant, range.scale()), label, range.scale().sortingWeight(instant, format)));
            categoryMap.get(label).recordIds().add(instant.toString());
        });
    }

    @Override
    public java.util.List<io.intino.sumus.Category> calculate(List<String> keys) {
        List<Category> categories = new ArrayList<>(categoryMap.values());
        categories.sort(Comparator.comparingInt(c -> ((TemporalCategory) c).weight()));
        return categories;
    }

    private String labelOf(Instant instant) {
        return range.scale().toString(instant);
    }

    public static class TemporalCategory extends Category {
        private final int weight;
        private final TimeStamp timeStamp;
        private String name;

        TemporalCategory(TimeStamp timeStamp, String name, int weight) {
            super(name);
            this.timeStamp = timeStamp;
            this.name = name;
            this.weight = weight;
        }

        @Override
        public String name() {
            return name;
        }

        public int weight() {
            return weight;
        }

        public TimeStamp timeStamp() {
            return timeStamp;
        }
    }

}
