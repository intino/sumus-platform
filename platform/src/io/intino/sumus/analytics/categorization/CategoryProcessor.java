package io.intino.sumus.analytics.categorization;

import io.intino.sumus.Category;
import io.intino.sumus.CategoryMap;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Record;
import io.intino.tara.magritte.Layer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CategoryProcessor {

	public static CategoryMap categorize(Categorization categorization, List<String> keys) {
		return new CategoryProcessor().doCategorize(categorization, keys);
	}

	public static CategoryMap categorize(Categorization categorization, List<? extends Layer> layers, List<String> keys) {
		return new CategoryProcessor().doCategorize(categorization, layers, keys);
	}

	private CategoryMap doCategorize(Categorization categorization, List<String> keys) {
		return doCategorize(categorization, categorization.graph().core$().find(categorization.record().layerClass()), keys);
	}

	private CategoryMap doCategorize(Categorization categorization, List<? extends Layer> recordList, List<String> keys) {
		Set<String> keySet = new HashSet<>(keys);
		CategoryMap categoryMap = new CategoryMap();
		categoryMap.clear();
		for (Layer record : recordList) {
			if (!record.i$(categorization.record()) || !keySetContains(keySet, record)) continue;
			String category = categorization.attribute(record);
			if (category == null) continue;
			if (!categoryMap.containsKey(category)) categoryMap.put(category, new Category(category));
			categoryMap.get(category).recordIds().add(record.core$().id());
		}
		return categoryMap;
	}

	private boolean keySetContains(Set<String> keySet, Layer record) {
		return keySet.isEmpty() || keySet.contains(record.a$(Record.class).lock());
	}
}
