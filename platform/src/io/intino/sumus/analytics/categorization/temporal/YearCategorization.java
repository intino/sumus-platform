package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

public class YearCategorization extends TemporalCategorization {

    public YearCategorization(TimeRange range) {
        super("YearCategorization", "Year", parent(range), new TimeRange(range.from(), range.to(), TimeScale.Year));
    }

    private static Categorization parent(TimeRange range) {
        return null;
    }

}
