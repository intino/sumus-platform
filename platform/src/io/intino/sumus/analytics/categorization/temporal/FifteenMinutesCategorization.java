package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

public class FifteenMinutesCategorization extends TemporalCategorization {

    public FifteenMinutesCategorization(TimeRange range) {
        super("FifteenMinutesCategorization", "FifteenMinutes", parent(range), new TimeRange(range.from(), range.to(), TimeScale.FifteenMinutes));
    }

    private static Categorization parent(TimeRange range) {
        return new TemporalCategorization(new TimeRange(range.from(), range.to(), TimeScale.Hour));
    }

}
