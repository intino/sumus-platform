package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

public class HourCategorization extends TemporalCategorization {

    public HourCategorization(TimeRange range) {
        super("HourCategorization", "Hour", parent(range), new TimeRange(range.from(), range.to(), TimeScale.Hour));
    }

    private static Categorization parent(TimeRange range) {
        return new TemporalCategorization(new TimeRange(range.from(), range.to(), TimeScale.SixHours));
    }

}
