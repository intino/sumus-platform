package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

import static io.intino.konos.alexandria.activity.model.TimeScale.Month;

public class MonthCategorization extends TemporalCategorization {

    public MonthCategorization(TimeRange range) {
        super("MonthCategorization", "Month", parent(range), new TimeRange(range.from(), range.to(), Month));
    }

    private static Categorization parent(TimeRange range) {
        return new TemporalCategorization(new TimeRange(range.from(), range.to(), TimeScale.QuarterOfYear));
    }

}
