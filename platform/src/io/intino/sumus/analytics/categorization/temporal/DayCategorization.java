package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

public class DayCategorization extends TemporalCategorization {

    public DayCategorization(TimeRange range) {
        super("DayOfYearCategorization", "DayOfYear", parent(range), new TimeRange(range.from(), range.to(), TimeScale.Day));
    }

    private static Categorization parent(TimeRange range) {
        return new TemporalCategorization(new TimeRange(range.from(), range.to(), TimeScale.Week));
    }

}
