package io.intino.sumus.analytics.categorization.temporal;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.graph.Categorization;

public class MinuteCategorization extends TemporalCategorization {

    public MinuteCategorization(TimeRange range) {
        super("MinuteCategorization", "Minute", parent(range), new TimeRange(range.from(), range.to(), TimeScale.Minute));
    }

    private static Categorization parent(TimeRange range) {
        return new TemporalCategorization(new TimeRange(range.from(), range.to(), TimeScale.FifteenMinutes));
    }

}
