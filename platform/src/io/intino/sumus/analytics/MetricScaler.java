package io.intino.sumus.analytics;

import io.intino.sumus.graph.AbstractMetric;

public interface MetricScaler {
    Scaler scaler(double value, AbstractMetric.Unit unit);
}
