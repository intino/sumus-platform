package io.intino.sumus.analytics.catalog;

import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.sumus.graph.Cluster;
import io.intino.sumus.graph.SumusGraph;

import java.util.List;

public class CatalogProcessor {
    private final List<Cluster> clusterList;

    private CatalogProcessor(SumusGraph sumus) {
        this.clusterList = sumus.clusterList();
    }

    public static void addClusters(SumusGraph sumus) {
        new CatalogProcessor(sumus).addClusters();
    }

    private void addClusters() {
        clusterList.forEach(cluster -> {
            Catalog catalog = cluster.catalog();
            if (catalog == null) return;
            // TODO MARIO catalog.analysis().create().clusterGrouping(cluster).histogram(Catalog.Analysis.AbstractGrouping.Histogram.Absolute);
        });
    }

}
