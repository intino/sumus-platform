package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;

import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class CategorizationView {

	private final String name;
	private final String label;
	private final List<CategoryView> categories;
	private final String entity;

	public CategorizationView(Categorization categorization, List<Category> categories) {
		this.name = categorization.name$();
		this.label = categorization.label();
		this.categories = categories.stream()
				.sorted(byLabel())
				.map(CategoryView::new)
				.collect(toList());
		this.entity = categorization.record() != null ? categorization.record().name() : "";
	}

	private Comparator<Category> byLabel() {
		return Comparator.comparing(Category::label);
	}

	public String name() {
		return name;
	}

	public String label() {
		return label;
	}

	public String entity() {
		return entity;
	}

	public List<CategoryView> categories() {
		return categories;
	}

}
