package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.StackedIndicator;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.analytics.Scaler;
import io.intino.tara.magritte.Node;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Serie {
    private final Map<Instant, Double> values = new ConcurrentHashMap<>();
    private final MeasureIndicator indicator;
    private final String label;
    private final Ticket.Style style;
    private final Ticket.DecimalPlaces decimalPlaces;
    private final Ticket.Min min;
    private final Ticket.Max max;
    private Scaler scaler;
    private String color;
    private boolean asGlobalSerie = false;
    private Double minValue = null;
    private Double maxValue = null;

    public Serie(Ticket ticket, MeasureIndicator indicator, String label, String color) {
        this.indicator = indicator;
        this.style = ticket.style();
        this.label = label;
        this.decimalPlaces = ticket.decimalPlaces();
        this.min = ticket.min();
        this.max = ticket.max();
        this.color = color;
    }

    public String label() {
        return label;
    }

    public String color() {
        return color;
    }

    public String style() {

        if (asGlobalSerie)
            return Ticket.Style.Line.Dotted.name().toLowerCase();

        if (style.stacked() == Ticket.Style.Stacked.None)
            return style.line().name().toLowerCase();

        return style.stacked().name().toLowerCase();
    }

    public Serie asGlobalSerie() {
        this.asGlobalSerie = true;
        return this;
    }

    public Ticket.Min min() {
        return min;
    }

    public Ticket.Max max() {
        return max;
    }

    public AbstractMetric.Unit unit() {
        return indicator.unit();
    }

    public Ticket.DecimalPlaces decimalPlaces() {
        return decimalPlaces;
    }

    public void scaleValues(Scaler scaler) {
        this.scaler = scaler;
        values.entrySet().forEach(e -> e.setValue(scaler.scale(e.getValue())));
    }

    public double maxValue() {
        return maxValue != null ? maxValue : -1;
    }

    public double minValue() {
        return this.minValue;
    }

    public String group() {
        Node owner = indicator.core$().owner();
        if (owner.is(StackedIndicator.class)) return owner.owner().as(Ticket.class).label();
        return owner.as(Ticket.class).label();
    }

    public Map<Instant, Double> values() {
        return values;
    }

    public String metric() {
        return scaler.unitLabel();
    }

    public void register(Instant time, Double value) {
        if (minValue == null || minValue > value) minValue = value;
        if (maxValue == null || maxValue < value) maxValue = value;
        values.put(time, value);
    }

}
