package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.*;
import io.intino.sumus.queries.TimeSeriesQuery.SortBy;
import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.Indicator;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.Ticket;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class Histogram {
	protected List<Category> categoryList = new ArrayList<>();
	protected SortBy sortBy = null;
	private final Map<Ticket, HistogramTicket> ticketList = new HashMap<>();

	public Histogram(List<Category> categoryList) {
		this(categoryList,null);
	}

	public Histogram(List<Category> categoryList, SortBy sortBy) {
		this.categoryList.addAll(categoryList);
		this.sortBy = sortBy;
	}

	public HistogramTicket histogramTicket(Ticket ticket) {
		if (!ticketList.containsKey(ticket))
			ticketList.put(ticket, new HistogramTicket(ticket));
		return ticketList.get(ticket);
	}

	public List<Category> categories() {
		if (sortBy == null) return categoryList;
		return HistogramIndicator.sort(categoryList, sortBy.mode());
	}

	public List<HistogramTicket> tickets() {
		return new ArrayList<>(ticketList.values());
	}

	private Optional<HistogramIndicator> histogramIndicator(Ticket ticket, MeasureIndicator indicator) {
		return Optional.ofNullable(histogramTicket(ticket).indicators.get(indicator));
	}

	public static class HistogramTicket {
		private final Ticket ticket;
		private final Map<MeasureIndicator, HistogramIndicator> indicators = new HashMap<>();

		HistogramTicket(Ticket ticket) {
			this.ticket = ticket;
		}

		public Ticket ticket() { return ticket; }

		public void register(HistogramIndicator indicator) {
			indicators.put(indicator.indicator, indicator);
		}

		public List<HistogramIndicator> indicators() {
			return new ArrayList<>(indicators.values());
		}

		public String name() {
			return ticket.name$();
		}

		public String label() {
			return ticket.label();
		}

		public String shortLabel() {
			return ticket.shortLabel();
		}

		public double maxValue() {
			return indicators.values().stream().mapToDouble(HistogramIndicator::maxValue).max().orElse(0D);
		}

		public List<Double> values() {
			List<Double> values = indicators.values().iterator().next().values().stream().map(i -> 0.0).collect(toList());
			for (HistogramIndicator indicator : indicators.values()) {
				List<Double> indicatorValues = indicator.values();
				for (int i = 0; i < indicatorValues.size(); i++) {
					values.set(i, values.get(i) + indicatorValues.get(i));
				}
			}
			return values;
		}
	}

	public static class HistogramIndicator {
		private final MeasureIndicator indicator;
		private final String color;
		private final List<Double> values = new ArrayList<>();
		private final Ticket.DecimalPlaces decimalPlaces;
		private Double minValue = null;
		private Double maxValue = null;

		public HistogramIndicator(MeasureIndicator indicator, String label, String color, Ticket.DecimalPlaces decimalPlaces) {
			this.indicator = indicator;
			this.color = color;
			this.decimalPlaces = decimalPlaces;
		}

		public Indicator indicator() { return indicator; }

		public String label() {
			return indicator.label();
		}

		public List<Double> values() {
			return this.values;
		}

		public AbstractMetric.Unit unit() {
			return indicator.unit();
		}

		public Ticket.DecimalPlaces decimalPlaces() {
			return decimalPlaces;
		}

		public double minValue() {
			return minValue != null ? minValue : 0;
		}

		public double maxValue() {
			return maxValue != null ? maxValue : 0;
		}

		public String color() {
			return color;
		}

		public void register(List<Double> values) {
			this.values.addAll(values.stream().map(v -> v == null ? 0D : v).collect(toList()));
			updateBounds();
		}

		static List<Category> sort(List<Category> categoryList, SortBy.Mode mode) {
			categoryList.sort(labelSorting(mode));
			return categoryList;
		}

		private void updateBounds() {
			boolean empty = values.isEmpty();
			minValue = !empty ? values.stream().filter(Objects::nonNull).min(Double::compare).orElse(0D) : 0D;
			maxValue = !empty ? values.stream().filter(Objects::nonNull).max(Double::compare).orElse(0D) : 0D;
		}

		private static Comparator<Category> labelSorting(SortBy.Mode mode) {
			return (o1, o2) -> {
				String label1 = o1.label();
				String label2 = o2.label();
				if (mode == SortBy.Mode.Ascendant)
					return label1.compareTo(label2);
				else
					return label2.compareTo(label1);
			};
		}

	}

}
