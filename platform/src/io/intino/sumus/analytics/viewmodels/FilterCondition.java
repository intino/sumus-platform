package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;

import java.util.List;

import static java.util.Collections.unmodifiableList;

public class FilterCondition {

	public final Categorization categorization;
	public final List<Category> categories;

	public FilterCondition(Categorization categorization, List<Category> categories) {
		this.categorization = categorization;
		this.categories = unmodifiableList(categories);
	}

}
