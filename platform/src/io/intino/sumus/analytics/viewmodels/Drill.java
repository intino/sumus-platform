package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;

public class Drill {

	public final Categorization categorization;
	public final List<Category> categories = new ArrayList<>();

	public Drill(Categorization categorization, List<Category> categories) {
		this.categorization = categorization;
		this.categories.addAll(categories);
	}

	public void removeCategory(String name) {
		Category category = categories.stream().filter(c -> c.name().equals(name)).findFirst().orElse(null);
		if (category == null) return;
		categories.remove(category);
	}

	public void removeCategory(Category category) {
		categories.remove(category);
	}

	public Map<Categorization, List<Category>> toMap() {
		return singletonMap(categorization, categories);
	}
}
