package io.intino.sumus.analytics.viewmodels;

public class ZoomRange {

	public final long max;
	public final long min;

	public ZoomRange(long max, long min) {
		this.max = max;
		this.min = min;
	}
}
