package io.intino.sumus.analytics.viewmodels;

import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableList;

public class Assertion {

	public final String type;
	public final Instant instant;
	public final List<Attribute> attributes;

	public Assertion(String type, List<Attribute> attributes) {
		this.type = type;
		this.instant = findInstant(attributes);
		this.attributes = unmodifiableList(attributes);
	}

	private static Instant findInstant(List<Attribute> attributes) {
		return attributes.stream().filter(a -> a.key().equals("instant"))
				.findFirst()
				.map(a -> instantOf((Long) a.value))
				.orElse(Instant.now(Clock.systemUTC()));
	}

	private static Instant instantOf(long milliseconds) {
		return Instant.ofEpochMilli(milliseconds);
	}

	public static class Attribute {
		private final String key;
		private final Object value;

		public Attribute(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		public String key() {
			return key;
		}

		public List<?> value() {
			return new ArrayList<>(singletonList(value));
		}
	}
}
