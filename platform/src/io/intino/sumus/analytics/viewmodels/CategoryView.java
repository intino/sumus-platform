package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.Category;

import java.util.List;

import static java.util.Collections.emptyList;

public class CategoryView {

	private final String name;
	private final String label;
	private final List<CategoryView> subCategories;

	public CategoryView(Category category) {
		this(category, emptyList());
	}

	public CategoryView(Category category, List<CategoryView> subCategories) {
		this.name = category.name();
		this.label = category.label();
		this.subCategories = subCategories;
	}

	public String name() {
		return name;
	}

	public String label() {
		return label;
	}

	public List<CategoryView> subCategories() {
		return subCategories;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CategoryView)) return false;
		return ((CategoryView) obj).name.equals(name) && ((CategoryView) obj).label.equals(label) && ((CategoryView) obj).subCategories.equals(subCategories);
	}
}
