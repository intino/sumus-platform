package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

public class CategorizationViewBuilder {

	public static CategorizationView build(Categorization categorization, List<String> keys) {
		return createCategorizationView(categorization, emptyList(), keys);
	}

	public static List<CategorizationView> build(List<? extends Categorization> categorizations, Map<Categorization, List<Category>> disallowed, List<String> keys) {
		return categorizations.stream()
				.map(categorization -> buildView(categorization, disallowed, keys))
				.flatMap(Collection::stream)
				.collect(toList());
	}

	private static List<CategorizationView> buildView(Categorization categorization, Map<Categorization, List<Category>> disallowed, List<String> keys) {
		return singletonList(createCategorizationView(categorization, disallowedCategories(disallowed.values()), keys));
	}

	private static CategorizationView createCategorizationView(Categorization categorization, List<Category> disallowedCategories, List<String> keys) {

		if (disallowedCategories.isEmpty()) {
			return new CategorizationView(categorization, categorization.calculate(keys));
		}

		List<Category> categories = categorization.calculate(keys).stream().filter(disallowedCategories::contains).collect(toList());
		return new CategorizationView(categorization, categories.isEmpty() ? categorization.calculate(keys) : categories);
	}

	private static List<Category> disallowedCategories(Collection<List<Category>> disallowed) {
		return disallowed.stream().flatMap(Collection::stream).collect(toList());
	}
}
