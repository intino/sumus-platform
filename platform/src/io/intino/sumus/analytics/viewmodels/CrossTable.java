package io.intino.sumus.analytics.viewmodels;

import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.Scaler;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CrossTable {
	private final AbstractMetric.Unit unit;
	private Double maxValue = null;
	private Map<String, Double> values = new HashMap<>();

	public CrossTable(AbstractMetric.Unit unit) {
		this.unit = unit;
	}

	public void register(List<Category> categories, double value) {
		String key = categoriesKey(categories);
		if (!values.containsKey(key)) values.put(key, 0D);
		value += values.get(key);
		values.put(key, value);
		updateBounds(value);
	}

	public Double value(List<String> categories) {
		String key = categoriesKeyFromIds(categories);
		return values.containsKey(key) ? values.get(key) : 0D;
	}

	public Scaler scaler() {
		AbstractMetric metric = unit.core$().ownerAs(AbstractMetric.class);
		Double maxValue = this.maxValue != null ? this.maxValue : -1D;
		return metric.scaler().scaler(maxValue, unit);
	}

	private String categoriesKeyFromIds(List<String> categories) {
		categories.sort(Comparator.naturalOrder());
		return categories.stream().collect(Collectors.joining());
	}

	private String categoriesKey(List<Category> categories) {
		categories.sort(Comparator.comparing(Category::label));
		return categories.stream().map(Category::label).collect(Collectors.joining());
	}

	private void updateBounds(double value) {
		if (maxValue == null || maxValue < value)
			maxValue = value;
	}

}
