package io.intino.sumus.analytics;

import io.intino.sumus.analytics.metricscalers.LinearScaler;
import io.intino.sumus.analytics.metricscalers.TimeScaler;
import io.intino.sumus.graph.Metric;
import io.intino.sumus.graph.TemporalMetric;

public class Insight {
    public static MetricScaler linearMetricScaler(Metric self) {
        return new LinearScaler();
    }

    public static MetricScaler temporalMetricScaler(TemporalMetric self) {
        return new TimeScaler();
    }

}
