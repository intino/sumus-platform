package io.intino.sumus.analytics.exporters;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;
import io.intino.sumus.QueryEngine.DocumentRow;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class CsvDocument extends AbstractDocument {

	private final List<DocumentRow> rows;
	private final String name;
	private final String csv;

	private CsvDocument(List<DocumentRow> rows, String language) {
		super(language);
		this.rows = rows;
		this.name = "Analysis-" + FORMATTER.format(Instant.now(Clock.systemUTC())) + ".csv";
		this.csv = buildDocument();
	}

	public static CsvDocument export(List<DocumentRow> rows, String language) {
		return new CsvDocument(rows, language);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public InputStream content() {
		return new ByteArrayInputStream(csv.getBytes());
	}

	@Override
	public File write(File directory) {
		return write(directory, name);
	}

	@Override
	public File write(File directory, String name) {
		File file = new File(directory, name);
		try {
			Files.write(file.toPath(), csv.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

	public void write(HttpServletResponse response) {
		write(response, name);
	}

	private void write(HttpServletResponse response, String name) {
		try {
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "attachment;filename=" + name);
			response.getOutputStream().write(csv.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String buildDocument() {
		StringBuilder csv = createHeader().append("\n");
		rows.forEach(row -> appendRow(csv, row));
		return csv.toString();
	}

	private StringBuilder createHeader() {
		return new StringBuilder(translate("Time")).append(";").append(translate("Ticket")).append(";").append(drillHeader(rows)).append(filterHeader(rows)).append(translate("Value")).append("\n");
	}

	private String drillHeader(List<DocumentRow> rows) {
		if (rows.size() == 0) {
			return "";
		}
		return rows.get(0).drill.entrySet().stream()
				.map(e -> translate("DrillCategorization") + ";" + translate("DrillCategory") + ";")
				.collect(joining(""));
	}

	private String filterHeader(List<DocumentRow> rows) {
		if (rows.size() == 0) {
			return "";
		}
		return rows.get(0).filter.entrySet().stream()
				.map(e -> {
					StringBuilder builder = new StringBuilder(translate("FilterCategorization") + ";");
					e.getValue().forEach(l -> builder.append(translate("FilterCategory")).append(";"));
					return builder.toString();
				})
				.collect(joining(""));
	}

	private StringBuilder appendRow(StringBuilder csv, DocumentRow row) {
		return csv.append(row.instant).append(";").append(row.indicator).append(";").append(drill(row.drill)).append(filter(row.filter)).append(row.value == null ? "" : row.value + " " + row.unitLabel).append("\n");
	}

	private String drill(Map<Categorization, Category> drill) {
		StringBuilder builder = new StringBuilder();
		drill.entrySet().forEach(e -> {
			builder.append(e.getKey().label()).append(";").append(e.getValue().label());
		});
		return builder.length() == 0 ? "" : builder.append(";").toString();
	}

	private String filter(Map<Categorization, List<Category>> filter) {
		StringBuilder builder = new StringBuilder();
		filter.entrySet().forEach(e -> {
			builder.append(e.getKey().label()).append(";");
			e.getValue().stream().map(Category::label).forEach(l -> builder.append(l).append(";"));
		});
		return builder.toString();
	}

}
