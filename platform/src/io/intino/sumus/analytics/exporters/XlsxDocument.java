package io.intino.sumus.analytics.exporters;

import io.intino.sumus.Category;
import io.intino.sumus.QueryEngine.DocumentRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.time.Clock;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.IntStream.rangeClosed;

public class XlsxDocument extends AbstractDocument {

	private final XSSFWorkbook workbook = new XSSFWorkbook();
	private final List<DocumentRow> rows;
	private final String name;
	private final XSSFCellStyle headerCellStyle;

	public XlsxDocument(List<DocumentRow> rows, String language) {
		super(language);
		this.rows = rows;
		this.name = "Analysis-" + FORMATTER.format(Instant.now(Clock.systemUTC())) + ".xlsx";
		this.headerCellStyle = headerCellStyle();
		buildDocument();
	}

	public static XlsxDocument export(List<DocumentRow> rows, String language) {
		return new XlsxDocument(rows, language);
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public InputStream content() {
		try {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			workbook.write(result);
			return new ByteArrayInputStream(result.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public File write(File directory) {
		return write(directory, name);
	}

	@Override
	public File write(File directory, String name) {
		File file = new File(directory, name);
		try (OutputStream stream = new FileOutputStream(file)) {
			workbook.write(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

	private void buildDocument() {
		XSSFSheet sheet = workbook.createSheet();
		writeHeaders(sheet.createRow(0));
		writeValues(sheet);
		rangeClosed(0, sheet.getRow(sheet.getFirstRowNum()).getLastCellNum()).forEach(sheet::autoSizeColumn);
	}

	private void writeHeaders(Row row) {
		writeHeader(row, translate("Time"));
		writeHeader(row, translate("Indicator"));
		if (rows.size() > 0) {
			writeDrillAndFilterHeaders(row, rows.get(0));
		}
		writeHeader(row, translate("Value"));
	}

	private void writeDrillAndFilterHeaders(Row row, DocumentRow documentRow) {
		documentRow.drill.entrySet().forEach(e -> {
			writeHeader(row, translate("DrillCategorization"));
			writeHeader(row, translate("DrillCategory"));
		});
		documentRow.filter.entrySet().forEach(e -> {
			writeHeader(row, translate("FilterCategorization"));
			e.getValue().stream().map(Category::label).forEach(entry -> writeHeader(row, translate("FilterCategory")));
		});
	}

	private XSSFCellStyle headerCellStyle() {
		XSSFCellStyle style = workbook.createCellStyle();
		XSSFFont font = workbook.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		style.setFont(font);
		return style;
	}

	private void writeHeader(Row row, String label) {
		final Cell cell = row.createCell(row.getLastCellNum() == -1 ? 0 : row.getLastCellNum());
		cell.setCellValue(label);
		cell.setCellStyle(headerCellStyle);
	}

	private void writeValues(XSSFSheet sheet) {
		indicatorsToListOfObjectsArray().forEach(values -> {
			Row row = sheet.createRow(sheet.getLastRowNum() + 1);
			for (Object value : values) {
				writeCell(value, row.createCell(Math.max(row.getLastCellNum(), 0)));
			}
		});
	}

	private void writeCell(Object value, Cell cell) {
		if (value instanceof String)
			cell.setCellValue((String) value);
		else if (value instanceof Double)
			cell.setCellValue((Double) value);
	}

	private Stream<Object[]> indicatorsToListOfObjectsArray() {
		return rows.stream().map(XlsxDocument::toObjectArray);
	}

	private static Object[] toObjectArray(DocumentRow row) {
		List<Object> objects = new ArrayList<>();
		objects.add(row.instant);
		objects.add(row.indicator);
		row.drill.entrySet().forEach(e -> {
			objects.add(e.getKey().label());
			objects.add(e.getValue().label());
		});
		row.filter.entrySet().forEach(e -> {
			objects.add(e.getKey().label());
			e.getValue().stream().map(Category::label).forEach(objects::add);
		});
		objects.add(row.value == null ? "" : row.value + " " + row.unitLabel);
		return objects.toArray();
	}

}
