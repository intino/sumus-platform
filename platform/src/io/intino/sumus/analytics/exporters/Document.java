package io.intino.sumus.analytics.exporters;

import java.io.File;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;

public interface Document {

	DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");

	String name();
	InputStream content();
	File write(File directory);
	File write(File directory, String name);
}
