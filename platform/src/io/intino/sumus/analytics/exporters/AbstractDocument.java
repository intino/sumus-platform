package io.intino.sumus.analytics.exporters;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDocument implements Document {
	private final String language;

	protected AbstractDocument(String language) {
		this.language = language;
	}

	protected Dictionary dictionary() {
		return new Dictionary().register(spanish()).register(english());
	}

	protected String translate(String word) {
		return dictionary().language(language).translate(word);
	}

	protected static class Dictionary {
		private Map<String, Language> languages = new HashMap<>();

		public Language language(String key) {
			return this.languages.get(key);
		}

		public Dictionary register(Language language) {
			this.languages.put(language.key, language);
			return this;
		}

		protected static class Language {
			String key;
			Map<String, String> translations = new HashMap<>();

			public Language key(String key) {
				this.key = key;
				return this;
			}

			public Language translation(String word, String value) {
				this.translations.put(word, value);
				return this;
			}

			public String translate(String word) {
				return translations.get(word);
			}
		}
	}

	private Dictionary.Language spanish() {
		return new Dictionary.Language().key("es")
				.translation("Time", "Instante")
				.translation("Indicator", "Indicador")
				.translation("Ticket", "Ticket")
				.translation("Value", "Valor")
				.translation("DrillCategorization", "Categorización")
				.translation("DrillCategory", "Categoría")
				.translation("FilterCategorization", "Filtro de categorización")
				.translation("FilterCategory", "Filtro de categoría");
	}

	private Dictionary.Language english() {
		return new Dictionary.Language().key("en")
				.translation("Time", "Instant")
				.translation("Indicator", "Indicator")
				.translation("Ticket", "Ticket")
				.translation("Value", "Value")
				.translation("DrillCategorization", "Categorization")
				.translation("DrillCategory", "Category")
				.translation("FilterCategorization", "Filter categorization")
				.translation("FilterCategory", "Filter category");
	}

}
