package io.intino.sumus.analytics;

import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.Ticket;

import java.util.List;

public interface FormulaLoader {
    List<MeasureIndicator.Formula> formulas(List<Ticket> tickets);
}
