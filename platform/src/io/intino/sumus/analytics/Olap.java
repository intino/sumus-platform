package io.intino.sumus.analytics;

import io.intino.sumus.datawarehouse.store.PathBuilder;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Palette;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.graph.DataRetriever;
import io.intino.sumus.graph.functions.Command;
import io.intino.sumus.helpers.ColorHelper;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;

public class Olap {
	private static final String ColorId = "%s.%s.%s";
	private static final HashMap<String, String> ColorMap = new HashMap<>();

	public static List createOlapListenerList(io.intino.sumus.graph.Olap self) {
		return new java.util.ArrayList<>();
	}

	public static void addOlapListener(io.intino.sumus.graph.Olap self, Command command) {
		self.listeners().add(command);
	}

	public static void notifyOlapListeners(io.intino.sumus.graph.Olap self) {
		self.listeners().forEach(listener -> ((Command) listener).execute());
	}

	public static Instant calculateOlapRangeFrom(io.intino.sumus.graph.Olap.Range self, String username) {
		io.intino.sumus.graph.Olap olap = self.core$().ownerAs(io.intino.sumus.graph.Olap.class);

		if (self.calculatedFrom() != null)
			return self.calculatedFrom();

		self.calculatedFrom(fromOf(olap, username));
		return self.calculatedFrom();
	}

	public static Instant calculateOlapRangeTo(io.intino.sumus.graph.Olap.Range self, String username) {
		io.intino.sumus.graph.Olap olap = self.core$().ownerAs(io.intino.sumus.graph.Olap.class);

		if (self.calculatedTo() != null)
			return self.calculatedTo();

		self.calculatedTo(toOf(olap, username));
		return self.calculatedTo();
	}

	public static boolean resetOlapRange(io.intino.sumus.graph.Olap.Range self) {
		self.calculatedFrom(null);
		self.calculatedTo(null);
		return true;
	}

	public static String ticketColor(Ticket self) {
		return self.graph().palette().colorOf(self.dataRetriever(), Palette.Color.Type.Main);
	}

	public static String paletteColorId(Palette.Color self) {
		DataRetriever dataRetriever = self.dataRetriever();
		return dataRetriever.core$().ownerAs(Ticket.class).name$() + "." + dataRetriever.name$() + "." + self.type().toString();
	}

	public static String paletteColorOf(Palette self, DataRetriever dataRetriever, Palette.Color.Type type) {
		String id = String.format(ColorId, dataRetriever.core$().ownerAs(Ticket.class).name$(), dataRetriever.name$(), type.toString());

		if (ColorMap.containsKey(id))
			return ColorMap.get(id);

		List<Palette.Color> colors = self.colorList(c -> c.id().equals(id));
		boolean colorDefined = colors.size() > 0;
		ColorMap.put(id, colorDefined ? colors.get(0).colorCode() : ColorHelper.newColor());

		return ColorMap.get(id);
	}

	private static Instant fromOf(io.intino.sumus.graph.Olap olap, String username) {
		return PathBuilder.range(nameSpacesOf(olap, username)).from();
	}

	private static Instant toOf(io.intino.sumus.graph.Olap olap, String username) {
		return PathBuilder.range(nameSpacesOf(olap, username)).to();
	}

	private static List<NameSpace> nameSpacesOf(io.intino.sumus.graph.Olap olap, String username) {
		return olap.nameSpaces(username);
	}

}
