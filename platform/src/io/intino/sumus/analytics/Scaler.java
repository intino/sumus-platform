package io.intino.sumus.analytics;

public interface Scaler {
    String unitLabel();
    double scale(double value);
}
