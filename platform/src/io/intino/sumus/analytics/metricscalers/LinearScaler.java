package io.intino.sumus.analytics.metricscalers;

import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.Metric;
import io.intino.sumus.analytics.MetricScaler;
import io.intino.sumus.analytics.Scaler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinearScaler implements MetricScaler {

    private static final Map<Metric.Unit, Metric> map = new HashMap<>();

    public Scaler scaler(double value, AbstractMetric.Unit unit) {
        if (map.isEmpty())
            unit.graph().metricList(m -> m.i$(Metric.class))
                    .forEach(metric -> metric.unitList().forEach(u -> map.put(u.a$(Metric.Unit.class), metric)));
        return value == 0 ? scalerWithFactorOne(unit.a$(Metric.Unit.class)) : findScaler(value, map.get(unit.a$(Metric.Unit.class)), unit.a$(Metric.Unit.class));
    }

    private Scaler findScaler(double value, Metric metric, Metric.Unit originalUnit) {
        List<Metric.Unit> units = metric.metricUnitList();
        double factor = 1;
        value = originalUnit.convert(value);

        for (Metric.Unit unit : units) {
            factor = unit.convert(1);
            double x = Math.abs(value / factor);
            if (x >= 1 && x < 1000) return createScaler(originalUnit, unit, factor);
        }

        return createScaler(originalUnit, units.get(units.size() - 1), factor);
    }

    private Scaler scalerWithFactorOne(Metric.Unit originalUnit) {
        return map.get(originalUnit).metricUnitList().stream()
                .filter(unit -> unit.convert(1) == 1)
                .map(unit -> createScaler(originalUnit, unit, 1))
                .findFirst().orElse(null);
    }

    private Scaler createScaler(final Metric.Unit originalUnit, final Metric.Unit unit, final double factor) {
        return new Scaler() {
            @Override
            public String unitLabel() {
                return unit.label();
            }

            @Override
            public double scale(double value) {
                return originalUnit.convert(value) / factor;
            }
        };
    }
}
