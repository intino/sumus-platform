package io.intino.sumus.analytics.metricscalers;

import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.TemporalMetric;
import io.intino.sumus.analytics.MetricScaler;
import io.intino.sumus.analytics.Scaler;

import java.util.HashMap;
import java.util.Map;

public class TimeScaler implements MetricScaler {

    private static final Map<TemporalMetric.Unit, TemporalMetric> map = new HashMap<>();

    public Scaler scaler(double value, AbstractMetric.Unit unit) {
        if (map.isEmpty())
            unit.graph().temporalMetricList().forEach(m -> m.temporalMetricUnitList().forEach(u -> map.put(u, m)));
        return createScaler(value, (TemporalMetric.Unit) unit);
    }

    private Scaler createScaler(double value, TemporalMetric.Unit originalUnit) {
        TemporalMetric.Unit result = getUnit(value, originalUnit);
        return new Scaler() {
            @Override
            public String unitLabel() {
                return result.label();
            }

            @Override
            public double scale(double value) {
                return value / result.conversionFactor();
            }
        };
    }

    private TemporalMetric.Unit getUnit(double value, TemporalMetric.Unit originalUnit) {
        TemporalMetric metric = map.get(originalUnit);
        TemporalMetric.Unit result = null;
        for (TemporalMetric.Unit unit : metric.temporalMetricUnitList()) {
            if (unit.limit() < value) continue;
            result = unit;
            break;
        }
        result = result != null ? result : metric.temporalMetricUnit(metric.temporalMetricUnitList().size() - 1);
        return result;
    }

}
