package io.intino.sumus.queries;

import io.intino.sumus.Category;
import io.intino.sumus.CategoryMap;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Record;
import io.intino.tara.magritte.Layer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class CatalogManager {

	private final List<Record> records;
	private final Map<String, Categorization> categorizations;
	private final Map<Categorization, CategoryMap> categories;
	private final List<String> keys;
	private Filter filter = null;
	private Map<String, List<String>> filteredCategorizations = new HashMap<>();

	public CatalogManager(List<Record> records, List<Categorization> categorizations, List<String> keys) {
		this.records = records;
		this.keys = keys;
		this.categorizations = categorizations.stream().collect(toMap(Layer::name$, c -> c));
		this.categories = calculateCategorizations(records);
	}

	private Map<Categorization, CategoryMap> calculateCategorizations(List<Record> records) {
		return this.categorizations.values().stream()
				   .collect(toMap(c -> c, c -> filteredCategorizations.keySet().contains(c.name$()) ? c.calculateMap(this.records, keys) : c.calculateMap(records, keys)));
	}

	public void filter(String categorizationName, List<String> categories) {
		Categorization categorization = categorizations.get(categorizationName);
		filter = filter == null ? new Filter() : filter;
		filter.add(collect(categories, categorization));
		filteredCategorizations.put(categorizationName, categories);
	}

	public void clearFilter(){
		filter = null;
		filteredCategorizations.clear();
	}

	public List<Record> records() {
		if (filter == null) return records;
		return records.stream().filter(record -> filter.contains(record.core$().id())).collect(toList());
	}

	public Map<Categorization, CategoryMap> categories(){
		if(filter == null) return categories;
		return calculateCategorizations(records());
	}

	public List<String> filteredCategories(Categorization categorization) {
		String key = categorization.name$();
		return filteredCategorizations.containsKey(key) ? filteredCategorizations.get(key) : emptyList();
	}

	public CategoryMap categories(Categorization categorization) {
		return categories(categorization.name$());
	}

	public CategoryMap categories(String categorization) {
		return categories().entrySet().stream()
						   .filter(e -> e.getKey().name$().equals(categorization) || e.getKey().label().equals(categorization))
						   .map(Map.Entry::getValue).findFirst().orElse(null);
	}

	private Category[] collect(List<String> categories, Categorization categorization) {
		return categories.stream().map(c -> this.categories.get(categorization).get(c)).toArray(Category[]::new);
	}

}
