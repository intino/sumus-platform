package io.intino.sumus.queries;

import io.intino.sumus.Category;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.analytics.categorization.TemporalCategorization.TemporalCategory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Drill {
    private final String id;
    private final List<Category> categories = new ArrayList<>();
    private TemporalCategory category = null;

    public Drill(String id, Category[] categories) {
        this.id = id;
        for (Category category : categories) {
            if (category instanceof TemporalCategory) this.category = (TemporalCategory) category;
            else this.categories.add(category);
        }
    }

    @Override
    public String toString() {
        return id;
    }

    public List<Category> categories() {
        return new ArrayList<>(categories);
    }

    public boolean contains(List<String> ids) {
        for (Category category : categories) if (!category.contains(ids)) return false;
        return true;
    }

    public boolean contains(String... ids) {
        return contains(asList(ids));
    }

    public List<String> ids() {
        List<String> result = new ArrayList<>();
        categories.forEach(c -> result.addAll(c.recordIds()));
        return result;
    }

    public TemporalCategory temporalCategory() {
        return category;
    }

    public TimeStamp timeStamp() {
        return category.timeStamp();
    }

    public boolean isTemporal() {
        return category != null;
    }
}
