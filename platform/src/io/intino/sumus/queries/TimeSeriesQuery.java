package io.intino.sumus.queries;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.analytics.FormulaLoader;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.digest.Query;

import java.util.*;

public class TimeSeriesQuery extends AbstractQuery {
	private final List<Ticket> tickets = new ArrayList<>();
	private final Map<Categorization, Set<Category>> drills = new HashMap<>();
	private final Map<Categorization, List<Category>> filters = new HashMap<>();
	private NameSpace nameSpace;
	private TimeRange timeRange;
	private TimeStamp timeStamp;
	private boolean includeGlobalSerie;

	private TimeSeriesQuery() {
	}

	public NameSpace nameSpace() {
		return nameSpace;
	}

	public TimeRange timeRange() {
		return timeRange;
	}

	public List<Ticket> tickets() {
		return tickets;
	}

	public Map<Categorization, List<Category>> filters() {
		return filters;
	}

	public Map<Categorization, Set<Category>> drills() {
		return drills;
	}

	public boolean includeGlobalSerie() {
		return includeGlobalSerie;
	}

	public Query toRawQuery(FormulaLoader loader) {
		Query result = new Query();

		addScope(result);

		loader.formulas(tickets).forEach(formula -> result.add(nameSpace, formula));

		if (timeRange != null)
			result.add(timeRange);

		if (timeStamp != null)
			result.add(timeStamp);

		if (!filters.values().isEmpty())
			result.filter(filters.values().stream().flatMap(Collection::stream).toArray(Category[]::new));

		drills.values().forEach(categories -> categories.forEach(result::drill));

		return result;
	}

	public TimeScale scale() {
		return timeRange != null ? timeRange.scale() : timeStamp.scale();
	}

	public static class Builder extends AbstractQuery.Builder<TimeSeriesQuery> {

		public Builder() {
			super(new TimeSeriesQuery());
		}

		public TimeSeriesQuery build(NameSpace nameSpace, TimeRange timeRange) {
			nameSpace(nameSpace);
			timeRange(timeRange);
			return query;
		}

		public TimeSeriesQuery build(NameSpace nameSpace, TimeStamp timeStamp) {
			nameSpace(nameSpace);
			timeStamp(timeStamp);
			return query;
		}

		public Builder addTicket(Ticket ticket) {
			query.tickets.add(ticket);
			return this;
		}

		public Builder addTickets(List<Ticket> tickets) {
			query.tickets.addAll(tickets);
			return this;
		}

		public Builder filter(Categorization categorization, List<Category> category) {
			query.filters.put(categorization, category);
			return this;
		}

		public Builder drill(Categorization categorization, Category category) {
			query.drills.putIfAbsent(categorization, new HashSet<>());

			Set<Category> categories = query.drills.get(categorization);
			categories.remove(category);
			categories.add(category);

			return this;
		}

		public Builder drill(Categorization categorization, List<Category> categories) {
			categories.forEach(c -> drill(categorization, c));
			return this;
		}

		public Builder drillWithKeys(Categorization categorization, List<String> keys) {
			categorization.calculate(keys).forEach(c -> drill(categorization, c));
			return this;
		}

		private Builder timeRange(TimeRange timeRange) {
			query.timeRange = timeRange;
			return this;
		}

		private Builder timeStamp(TimeStamp timeStamp) {
			query.timeStamp = timeStamp;
			return this;
		}

		public void includeGlobalSerie(boolean value) {
			query.includeGlobalSerie = value;
		}

		public void nameSpace(NameSpace nameSpace) {
			query.nameSpace = nameSpace;
		}
    }

	public static class SortBy {
		private final Ticket ticket;
		private final MeasureIndicator indicator;
		private final SortBy.Mode mode;

		public enum Mode { Ascendant, Descendant };

		public SortBy(Ticket ticket, MeasureIndicator indicator, SortBy.Mode mode) {
			this.ticket = ticket;
			this.indicator = indicator;
			this.mode = mode;
		}

		public Ticket ticket() {
			return ticket;
		}

		public MeasureIndicator indicator() {
			return indicator;
		}

		public Mode mode() {
			return mode;
		}
	}
}
