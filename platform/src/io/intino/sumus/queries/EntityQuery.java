package io.intino.sumus.queries;

import io.intino.tara.magritte.Concept;

public class EntityQuery extends AbstractQuery {
	private Concept entity;
	private String condition = null;
	private String username = null;

	private EntityQuery() {
	}

	public Concept entity() {
		return entity;
	}

	public String condition() {
		return condition;
	}

	public String username() {
		return username;
	}

	public static class Builder extends AbstractQuery.Builder<EntityQuery> {

		public Builder() {
			super(new EntityQuery());
		}

		public EntityQuery build(Concept entity, String username) {
			query.entity = entity;
			query.username = username;
			return query;
		}

		public EntityQuery.Builder condition(String condition) {
			query.condition = condition;
			return this;
		}
	}

}
