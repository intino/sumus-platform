package io.intino.sumus.queries;

import io.intino.sumus.Category;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Record;
import io.intino.sumus.queries.digest.Query;

import java.util.*;

public class AbstractQuery {
    protected Scope scope = null;

    public Optional<Scope>  scope() {
        return Optional.ofNullable(scope);
    }

    protected Map<Categorization, List<Category>> toMap(List<io.intino.sumus.analytics.viewmodels.FilterCondition> filters) {
        Map<Categorization, List<io.intino.sumus.Category>> result = new HashMap<>();

        filters.forEach(fc -> {
            Categorization categorization = fc.categorization;
            List<io.intino.sumus.Category> categories = fc.categories;

            if (!result.containsKey(categorization))
                result.put(categorization, new ArrayList<>());

            List<io.intino.sumus.Category> categorizationCategories = result.get(categorization);
            categorizationCategories.removeAll(categories);
            categorizationCategories.addAll(categories);
        });

        return result;
    }

    protected void addScope(Query query) {
        if (scope == null) return;
        scope.categories().forEach(categories -> query.filter(categories.toArray(new Category[0])));
        scope.records().forEach(records -> query.filter(records.toArray(new Record[0])));
    }

    protected void addScope(io.intino.sumus.queries.temporalrecord.Query query) {
        if (scope == null) return;
        scope.categories().forEach(categories -> query.filter(categories.toArray(new Category[0])));
        scope.records().forEach(records -> query.filter(records.toArray(new Record[0])));
    }

    public static class Builder<Q extends AbstractQuery> {
        protected final Q query;

        public Builder(Q query) {
            this.query = query;
        }

        public <B extends AbstractQuery.Builder> B scope(Scope scope) {
            query.scope = scope;
            return (B) this;
        }
    }

}
