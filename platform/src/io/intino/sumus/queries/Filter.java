package io.intino.sumus.queries;

import io.intino.sumus.Category;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.analytics.categorization.TemporalCategorization.TemporalCategory;

import java.util.*;

import static java.util.Collections.singletonList;

public class Filter {

    private final List<Set<String>> recordIds = new ArrayList<>();
    private final Map<TimeStamp, TemporalCategory> temporalCategories = new HashMap<>();

    public void add(Category... categories) {
        List<Category> recordCategories = new ArrayList<>();
        for (Category category : categories) {
            if (category instanceof TemporalCategory) add((TemporalCategory) category);
            else recordCategories.add(category);
        }
        add(recordCategories);
    }

    private void add(List<Category> recordCategories) {
        Set<String> records = new HashSet<>();
        for (Category category : recordCategories) records.addAll(category.recordIds());
        recordIds.add(records);
    }

    private void add(TemporalCategory category) {
        temporalCategories.put(category.timeStamp(), category);
    }

    public boolean contains(List<String> ids) {
        for (Set<String> record : recordIds) if (!containsAnyOf(ids, record)) return false;
        return true;
    }

    public boolean contains(String id) {
        return contains(singletonList(id));
    }

    private boolean containsAnyOf(List<String> ids, Set<String> records) {
        for (String id : ids) if (records.contains(id)) return true;
        return false;
    }

    public boolean isTemporal() {
        return !temporalCategories.isEmpty();
    }

    public boolean contains(TimeStamp timeStamp) {
        if (temporalCategories.isEmpty()) return true;
        for (TimeStamp stamp : temporalCategories.keySet()) if (stamp.contains(timeStamp)) return true;
        return false;
    }
}
