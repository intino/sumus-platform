package io.intino.sumus.queries.digest;

import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.Digest;
import io.intino.sumus.graph.MeasureIndicator.Formula;
import io.intino.sumus.graph.NameSpace;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class QueryResult {
    private Query query;
    private Map<String, Double> values = new ConcurrentHashMap<>();
    private List<Digest> digests = Collections.synchronizedList(new ArrayList<>());

    QueryResult(Query query) {
        this.query = query;
    }

    void register(List<Digest> digest) {
        digests.addAll(digest);
    }

    void register(Query.SubQuery query, TimeStamp stamp, double value) {
        values.put(query.toString() + stamp.toString(), value);
    }

    void register(Query.SubQuery query, io.intino.sumus.queries.Drill drill, TimeStamp stamp, double value) {
        if (drill.isTemporal()) values.put(query.toString() + drill.toString(), value);
        else values.put(query.toString() + drill.toString() + stamp.toString(), value);
    }

    public Double value(NameSpace nameSpace, Formula formula, TimeStamp stamp) {
        return values.get(query.subQuery(nameSpace, formula).toString() + stamp.toString());
    }

    public Double value(NameSpace nameSpace, Formula formula, io.intino.sumus.queries.Drill drill) {
        return values.get(query.subQuery(nameSpace, formula).toString() + drill.toString());
    }

    public Double value(NameSpace nameSpace, Formula formula, io.intino.sumus.queries.Drill drill, TimeStamp stamp) {
        return values.get(query.subQuery(nameSpace, formula).toString() + drill.toString() + stamp.toString());
    }

    public Map<TimeStamp, Double> values(NameSpace nameSpace, Formula formula) {
        return values(nameSpace, formula, null);
    }

    public Map<TimeStamp, Double> values(NameSpace nameSpace, Formula formula, io.intino.sumus.queries.Drill drill) {
        return values(query.subQuery(nameSpace, formula), drill);
    }

    private Map<TimeStamp, Double> values(Query.SubQuery query, io.intino.sumus.queries.Drill drill) {
        String prefix = query.toString() + (drill != null ? drill.toString() : "");
        Map<TimeStamp, Double> values = new LinkedHashMap<>();
        for (TimeStamp stamp : query.timeStamps()) values.put(stamp, this.values().get(prefix + stamp.toString()));
        return values;
    }

    public Query query() {
        return query;
    }

    public Map<String, Double> values() {
        return values;
    }

    public List<Digest> digests() {
        return digests;
    }
}
