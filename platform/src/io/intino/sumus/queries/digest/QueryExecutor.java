package io.intino.sumus.queries.digest;

import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.datawarehouse.store.Digest;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.stream.Collectors.toList;

public class QueryExecutor {

    private final Query query;
    private final QueryResult result;
    private final Map<String, List<io.intino.sumus.queries.Drill>> drillMap = new ConcurrentHashMap<>();

    private QueryExecutor(Query query) {
        this.query = query.commit();
        this.result = new QueryResult(query);
    }

    public static QueryResult execute(Query query) {
        QueryExecutor executor = new QueryExecutor(query);
        executor.execute();
        return executor.result;
    }

    private void execute() {
        for (NameSpace nameSpace : query.nameSpaces()) processNamespaces(nameSpace);
    }

    private void processNamespaces(NameSpace nameSpace) {
        List<Query.SubQuery> queries = query.subQueries().stream().filter(s -> s.nameSpace.equals(nameSpace)).collect(toList());
        queryTimeStamps(queries).parallelStream().forEach(i -> processNamespace(i, queries));
    }

    private void processNamespace(TimeStamp stamp, Collection<Query.SubQuery> queries) {
        Collection<Query.SubQuery> availableQueries = availableQueries(stamp, queries);
        Map<Cube, BucketWithDrills> bucketMap = bucketByCubeType(stamp, availableQueries);
        for (Query.SubQuery query : availableQueries) {
            BucketWithDrills bucket = bucketMap.get(query.cube());
            if (bucket == null) continue;
            if (bucket.drilledDigests.isEmpty())
                result.register(query, stamp, query.formula.calculate(bucket.filteredDigests));
            else
                bucket.drilledDigests.forEach((drill, value) -> result.register(query, drill, stamp, query.formula.calculate(value)));
        }
    }

    private Collection<Query.SubQuery> availableQueries(TimeStamp stamp, Collection<Query.SubQuery> queries) {
        if (query.timeStamps().contains(stamp)) return queries;
        List<Query.SubQuery> subQueries = new ArrayList<>();
        for (Query.SubQuery subQuery : queries) if (subQuery.ts.contains(stamp)) subQueries.add(subQuery);
        return subQueries;
    }

    private Map<io.intino.sumus.queries.Drill, List<Digest>> groupDigestsByDrill(List<Digest> digests, List<io.intino.sumus.queries.Drill> temporalDrills) {
        Map<io.intino.sumus.queries.Drill, List<Digest>> digestsByDrill = new HashMap<>();
        if (!query.drills().isEmpty()) groupDigestsByDrill(digests, digestsByDrill);
        if (temporalDrills != null) groupDigestsByDrill(digests, digestsByDrill, temporalDrills);
        return digestsByDrill;
    }

    private synchronized void groupDigestsByDrill(List<Digest> digests, Map<io.intino.sumus.queries.Drill, List<Digest>> digestsByDrill) {
        query.drills().forEach(d -> digestsByDrill.put(d, new ArrayList<>()));
        for (Digest digest : digests) {
            if (!drillMap.containsKey(digest.entityIds())) indexDigest(digest);
            for (io.intino.sumus.queries.Drill drill : drillMap.get(digest.entityIds())) digestsByDrill.get(drill).add(digest);
        }
    }

    private void groupDigestsByDrill(List<Digest> digests, Map<io.intino.sumus.queries.Drill, List<Digest>> digestsByDrill, List<io.intino.sumus.queries.Drill> temporalDrills) {
        temporalDrills.forEach(d -> digestsByDrill.put(d, new ArrayList<>()));
        for (Digest digest : digests)
            for (io.intino.sumus.queries.Drill temporalDrill : temporalDrills)
                if (temporalDrill.contains(digest.entityIds())) digestsByDrill.get(temporalDrill).add(digest);
    }

    private void indexDigest(Digest digest) {
        drillMap.put(digest.entityIds(), new ArrayList<>());
        for (io.intino.sumus.queries.Drill drill : query.drills())
            if (drill.contains(digest.entityIdsSplit())) drillMap.get(digest.entityIds()).add(drill);
    }

    private Collection<TimeStamp> queryTimeStamps(List<Query.SubQuery> queries) {
        Set<TimeStamp> stamps = new LinkedHashSet<>();
        queries.forEach(q -> stamps.addAll(q.ts));
        return stamps;
    }

    private List<Digest> filter(List<Digest> digests) {
        if (query.filter() == null) return digests;
        List<Digest> result = new ArrayList<>();
        for (Digest digest : digests) if (query.filter().contains(digest.entityIdsSplit())) result.add(digest);
        return result;
    }

    private Map<Cube, BucketWithDrills> bucketByCubeType(TimeStamp stamp, Collection<Query.SubQuery> queries) {
        Map<Cube, BucketWithDrills> cubes = new HashMap<>();
        for (Cube cube : cubes(queries)) {
            Bucket bucket = new Bucket(cube, queries.iterator().next().nameSpace, stamp);
            if (!bucket.exists()) continue;
            cubes.put(cube, new BucketWithDrills(bucket));
        }
        return cubes;
    }

    private Set<Cube> cubes(Collection<Query.SubQuery> queries) {
        Set<Cube> concepts = new HashSet<>();
        for (Query.SubQuery query : queries) concepts.add(query.cube());
        return concepts;
    }

    class BucketWithDrills {

        List<Digest> filteredDigests;
        Map<io.intino.sumus.queries.Drill, List<Digest>> drilledDigests;

        BucketWithDrills(Bucket bucket) {
            filteredDigests = filter(bucket.digests());
            result.register(filteredDigests);
            drilledDigests = groupDigestsByDrill(filteredDigests, query.drills(bucket.timeStamp()));
        }
    }

}
