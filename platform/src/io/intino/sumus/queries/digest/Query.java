package io.intino.sumus.queries.digest;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.MeasureIndicator.Formula;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Record;
import io.intino.sumus.queries.Filter;

import java.time.Instant;
import java.util.*;

public class Query {

    private final Map<String, SubQuery> subQueries = new LinkedHashMap<>();
    private final Set<TimeStamp> ts = new LinkedHashSet<>();
    private final Set<io.intino.sumus.queries.Drill> drills = new LinkedHashSet<>();
    private final Map<TimeStamp, List<io.intino.sumus.queries.Drill>> temporalDrills = new HashMap<>();
    private int drillCount = 0;
    private Filter filter;

    private static String id(NameSpace nameSpace, Formula formula) {
        return nameSpace.name$() + formula.name$();
    }

    private static Set<TimeStamp> timeStampsOf(TimeRange range) {
        Set<TimeStamp> stamps = new HashSet<>();
        Instant instant = range.scale().normalise(range.from());
        while (instant.isBefore(range.to())) {
            stamps.add(timeStampOf(instant, range.scale()));
            instant = range.scale().nextTime(instant);
        }
        return stamps;
    }

    private static TimeStamp timeStampOf(Instant instant, TimeScale scale) {
        return new TimeStamp(instant, scale);
    }

    public Query add(NameSpace nameSpace, Formula formula) {
        if (!subQueries.containsKey(id(nameSpace, formula)))
            subQueries.put(id(nameSpace, formula), new SubQuery(nameSpace, formula, new HashSet<>()));
        return this;
    }

    public Query add(NameSpace nameSpace, Formula formula, TimeRange range) {
        add(nameSpace, formula);
        subQueries.get(id(nameSpace, formula)).add(timeStampsOf(range));
        return this;
    }

    public Query add(NameSpace nameSpace, Formula formula, Instant instant, TimeScale scale) {
        add(nameSpace, formula);
        subQueries.get(id(nameSpace, formula)).add(timeStampOf(instant, scale));
        return this;
    }

    public Query add(TimeRange timeRange) {
        ts.addAll(timeStampsOf(timeRange));
        return this;
    }

    public Query add(Instant instant, TimeScale scale) {
        ts.add(timeStampOf(instant, scale));
        return this;
    }

    public Query add(TimeStamp timeStamp) {
        ts.add(timeStamp);
        return this;
    }

    public Query drill(Category... categories) {
        io.intino.sumus.queries.Drill drill = new io.intino.sumus.queries.Drill("label" + drillCount++, categories);
        if (drill.isTemporal()) processTemporalDrill(drill);
        else drills.add(drill);
        return this;
    }

    private void processTemporalDrill(io.intino.sumus.queries.Drill drill) {
        if (!temporalDrills.containsKey(drill.timeStamp())) temporalDrills.put(drill.timeStamp(), new ArrayList<>());
        temporalDrills.get(drill.timeStamp()).add(drill);
    }

    public Query drill(List<Category> categories) {
        return drill(categories.toArray(new Category[categories.size()]));
    }

    public Query filter(Category... categories) {
        filter = filter == null ? new Filter() : filter;
        filter.add(categories);
        return this;
    }

    public Query filter(Record... records) {
        Category category = new Category("Custom Filter");
        for (Record record : records) category.recordIds().add(record.core$().id());
        filter(category);
        return this;
    }

    public List<NameSpace> nameSpaces() {
        return new ArrayList<>(subQueries.values().stream().map(s -> s.nameSpace).collect(LinkedHashSet::new, HashSet::add, LinkedHashSet::addAll));
    }

    public List<Formula> formulas() {
        return new ArrayList<>(subQueries.values().stream().map(s -> s.formula).collect(LinkedHashSet::new, HashSet::add, LinkedHashSet::addAll));
    }

    public Collection<SubQuery> subQueries() {
        return subQueries.values();
    }

    SubQuery subQuery(NameSpace nameSpace, Formula formula) {
        return subQueries.get(id(nameSpace, formula));
    }

    public List<TimeStamp> timeStamps() {
        return new ArrayList<>(ts);
    }

    public List<TimeStamp> timeStamps(NameSpace nameSpace, Formula formula) {
        return new ArrayList<>(subQueries.get(id(nameSpace, formula)).timeStamps());
    }

    public List<io.intino.sumus.queries.Drill> drills() {
        return new ArrayList<>(drills);
    }

    public List<TimeStamp> temporalStamps() {
        return new ArrayList<>(temporalDrills.keySet());
    }

    public Filter filter() {
        return filter;
    }

    public List<io.intino.sumus.queries.Drill> drills(TimeStamp timeStamp) {
        return temporalDrills.get(timeStamp);
    }

    Query commit() {
        Set<TimeStamp> ts = filteredStamps(queryTimeStamps());
        for (SubQuery subQuery : subQueries.values()) commit(subQuery, ts);
        return this;
    }

    private Set<TimeStamp> queryTimeStamps() {
        for (TimeStamp ts : temporalDrills.keySet()) add(ts);
        return ts;
    }

    private Set<TimeStamp> filteredStamps(Set<TimeStamp> ts) {
        if (filter == null || !filter.isTemporal()) return ts;
        List<TimeStamp> toRemove = new ArrayList<>();
        for (TimeStamp t : ts) if (!filter.contains(t)) toRemove.add(t);
        ts.removeAll(toRemove);
        return ts;
    }

    private void commit(SubQuery subQuery, Set<TimeStamp> ts) {
        subQuery.ts = filteredStamps(subQuery.ts);
        subQuery.ts.addAll(ts);
    }

    class SubQuery {

        NameSpace nameSpace;
        Formula formula;
        Set<TimeStamp> ts;

        SubQuery(NameSpace nameSpace, Formula formula, Set<TimeStamp> ts) {
            this.nameSpace = nameSpace;
            this.formula = formula;
            this.ts = ts;
        }

        void add(Set<TimeStamp> timeStamps) {
            this.ts.addAll(timeStamps);
        }

        void add(TimeStamp timeStamp) {
            ts.add(timeStamp);
        }

        Set<TimeStamp> timeStamps() {
            HashSet<TimeStamp> stamps = new HashSet<>(ts);
            stamps.addAll(Query.this.ts);
            return stamps;
        }

        Cube cube() {
            return formula.cube();
        }

        @Override
        public String toString() {
            return id(nameSpace, formula);
        }
    }
}
