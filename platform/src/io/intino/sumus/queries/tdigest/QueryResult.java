package io.intino.sumus.queries.tdigest;

import io.intino.sumus.graph.Distribution;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class QueryResult {
    private Query query;
    private Map<String, List<Double>> values = new ConcurrentHashMap<>();

    QueryResult(Query query) {
        this.query = query;
    }

    void register(Distribution.Source source, List<Double> value) {
        values.put(source.name$(), value);
    }

    void register(Distribution.Source source, io.intino.sumus.queries.Drill drill, List<Double> value) {
        values.put(source.name$() + drill, value);
    }

    public List<Double> value(Distribution.Source source) {
        return values.get(source.name$());
    }

    public List<Double> value(Distribution.Source source, io.intino.sumus.queries.Drill drill) {
        return values.get(source.name$() + drill);
    }

    public Query query() {
        return query;
    }

    public Map<String, List<Double>> values() {
        return values;
    }
}
