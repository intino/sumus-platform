package io.intino.sumus.queries.tdigest;

import com.tdunning.math.stats.AVLTreeDigest;
import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.Distribution;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.datawarehouse.store.TDigest;
import io.intino.sumus.queries.Drill;

import java.time.Instant;
import java.util.*;

public class QueryExecutor {

    private final Query query;
    private final QueryResult result;
    private final Map<String, List<Drill>> drillMap = new HashMap<>();

    private QueryExecutor(Query query) {
        this.query = query;
        this.result = new QueryResult(query);
    }

    public static QueryResult execute(Query query) {
        QueryExecutor executor = new QueryExecutor(query);
        executor.execute();
        return executor.result;
    }

    private void execute() {
        prepareDrills();
        processNamespace(query.nameSpace());
    }

    private void prepareDrills() {
        for (String id : allIds()) {
            for (Drill drill : query.drills()) {
                if (!drill.contains(id)) continue;
                if (!drillMap.containsKey(id)) drillMap.put(id, new ArrayList<>());
                drillMap.get(id).add(drill);
            }
        }
    }

    private List<String> allIds() {
        List<String> ids = new ArrayList<>();
        for (Drill drill : query.drills()) ids.addAll(drill.ids());
        return ids;
    }

    private void processNamespace(NameSpace nameSpace) {
        Map<Cube, Bucket> buckets = bucketByCubeType(query.instant(), nameSpace);
        for (Distribution.Source source : query.sources()) {
            Bucket bucket = buckets.get(cubeOf(source));
            if (bucket == null) continue;
            registerDistribution(source, bucket);
        }
    }

    private void registerDistribution(Distribution.Source source, Bucket bucket) {
        List<TDigest> tDigests = filter(bucket.tDigests(source.property().label()));
        if(query.drills().isEmpty()) result.register(source, percentilesOf(tDigests));
        drillMap(tDigests).forEach((key, value) -> result.register(source, key, percentilesOf(value)));
    }

    private List<Double> percentilesOf(List<TDigest> tDigests) {
        AVLTreeDigest avlDigest = (AVLTreeDigest) AVLTreeDigest.createDigest(100);
        for (TDigest tDigest : tDigests) avlDigest.add(tDigest.avlTreeDigest());
        return percentilesOf(avlDigest);
    }

    private List<Double> percentilesOf(AVLTreeDigest avlDigest) {
        List<Double> result = new ArrayList<>();
        for (double i = 0; i <= 1; i += 0.01) result.add(avlDigest.quantile(i));
        result.add(avlDigest.quantile(1));
        return result;
    }

    private List<TDigest> filter(List<TDigest> tDigests) {
        if (query.filter() == null) return tDigests;
        List<TDigest> result = new ArrayList<>();
        for (TDigest tDigest : tDigests) if (query.filter().contains(tDigest.entityIds())) result.add(tDigest);
        return result;
    }

    private Map<Drill, List<TDigest>> drillMap(List<TDigest> digests) {
        Map<Drill, List<TDigest>> digestMap = digestMap();
        for (TDigest digest : digests)
            for (String id : digest.entityIds())
                for (Drill drill : drillMap.getOrDefault(id, new ArrayList<>())) digestMap.get(drill).add(digest);
        return digestMap;
    }

    private Map<Drill, List<TDigest>> digestMap() {
        Map<Drill, List<TDigest>> digestMap = new HashMap<>();
        for (Drill drill : query.drills()) digestMap.put(drill, new ArrayList<>());
        return digestMap;
    }

    private Map<Cube, Bucket> bucketByCubeType(Instant instant, NameSpace nameSpace) {
        Map<Cube, Bucket> cubes = new HashMap<>();
        for (Cube cube : cubes()) {
            Bucket bucket = new Bucket(cube, nameSpace, new TimeStamp(instant, query.scale()));
            if (bucket.exists()) cubes.put(cube, bucket);
        }
        return cubes;
    }

    private Set<Cube> cubes() {
        Set<Cube> concepts = new HashSet<>();
        for (Distribution.Source source : query.sources()) concepts.add(cubeOf(source));
        return concepts;
    }

    private Cube cubeOf(Distribution.Source source) {
        return source.property().core$().ownerAs(Cube.class);
    }


}
