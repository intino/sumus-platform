package io.intino.sumus.queries.tdigest;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.graph.Distribution;
import io.intino.sumus.graph.Entity;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.queries.Drill;
import io.intino.sumus.queries.Filter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Collections.addAll;

public class Query {
    private final Set<Distribution.Source> sources = new HashSet<>();
    private final List<Drill> drills = new ArrayList<>();
    private final Instant instant;
    private final TimeScale scale;
    private NameSpace nameSpace;
    private io.intino.sumus.queries.Filter filter;

    public Query(Instant instant, TimeScale scale, NameSpace nameSpace) {
        this.instant = instant;
        this.scale = scale;
        this.nameSpace = nameSpace;
    }

    public Instant instant() {
        return instant;
    }

    public TimeScale scale() {
        return scale;
    }

    public NameSpace nameSpace() {
        return nameSpace;
    }

    public List<Distribution.Source> sources() {
        return new ArrayList<>(sources);
    }

    public List<Drill> drills() {
        return drills;
    }

    public io.intino.sumus.queries.Filter filter() {
        return filter;
    }

    public Query source(Distribution.Source... sources) {
        addAll(this.sources, sources);
        return this;
    }

    public Drill drill(Category... categories) {
        Drill drill = new Drill("drill" + drills.size(), categories);
        drills.add(drill);
        return drill;
    }

    public Query filter(Category... categories) {
        filter = filter == null ? new Filter() : filter;
        filter.add(categories);
        return this;
    }

    public Query filter(Entity... entities) {
        Category category = new Category("Custom Filter");
        for (Entity entity : entities) category.recordIds().add(entity.core$().id());
        filter(category);
        return this;
    }

}
