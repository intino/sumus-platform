package io.intino.sumus.queries;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.queries.temporalrecord.Query;
import io.intino.tara.magritte.Concept;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TemporalRecordQuery extends AbstractQuery {
	private Concept record;
	private Map<Categorization, List<Category>> filters = new HashMap<>();
	private String condition;
	private TimeRange range = null;
	private TimeScale scale = null;
	private Instant instant = null;
	private NameSpace nameSpace;
	private String username;

	private TemporalRecordQuery() {
	}

	public Concept record() {
		return record;
	}

	public String condition() {
		return condition;
	}

	public TimeRange timeRange() {
		return range;
	}

	public Map<Categorization, List<Category>> filters() {
		return filters;
	}

	public NameSpace nameSpace() {
		return nameSpace;
	}

	public String username() {
		return username;
	}

	public Query toRawQuery() {
		Query result = range != null ? new Query(nameSpace, record, range) : new Query(nameSpace, record, scale, instant);

		addScope(result);

		if (condition != null)
			result.condition(condition);

		if (!filters.values().isEmpty())
			result.filter(filters.values().stream().flatMap(Collection::stream).toArray(Category[]::new));

		return result;
	}

	public static class Builder extends AbstractQuery.Builder<TemporalRecordQuery> {

		public Builder() {
			super(new TemporalRecordQuery());
		}

		public TemporalRecordQuery build(Concept record, String username) {
			query.record = record;
			query.username = username;
			return query;
		}

		public TemporalRecordQuery.Builder nameSpace(NameSpace nameSpace) {
			query.nameSpace = nameSpace;
			return this;
		}

		public TemporalRecordQuery.Builder timeRange(TimeRange range) {
			query.range = range;
			return this;
		}

		public TemporalRecordQuery.Builder instant(Instant instant, TimeScale scale) {
			query.instant = instant;
			query.scale = scale;
			return this;
		}

		public TemporalRecordQuery.Builder filter(List<FilterCondition> filters) {
			query.filters = query.toMap(filters);
			return this;
		}

		public TemporalRecordQuery.Builder condition(String condition) {
			query.condition = condition;
			return this;
		}

	}

}
