package io.intino.sumus.queries.temporalrecord;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.*;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Graph;
import io.intino.tara.magritte.Layer;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static io.intino.sumus.datawarehouse.store.PathBuilder.temporalRecordPath;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class CategorizationSeeker {

	public static List<Categorization> categorizationsOf(NameSpace nameSpace, Concept temporalRecord, TimeScale scale, Instant instant) {
		return categorizationsOf(nameSpace, temporalRecord, new TimeStamp(instant, scale));
	}

	public static List<Categorization> categorizationsOf(NameSpace nameSpace, Concept temporalRecord, TimeStamp timeStamp) {
		List<TemporalRecord> temporalRecords = temporalRecords(temporalRecord, nameSpace, timeStamp);
		if (temporalRecords.isEmpty()) return emptyList();
		List<Record> records = recordsOf(temporalRecords.get(0));
		return nameSpace.graph().categorizationList(c -> canBeUsedOn(records, c)).collect(toList());
	}

	public static List<Record> recordsOf(TemporalRecord record) {
		return record.core$().variables().values().stream()
				.filter(CategorizationSeeker::isDimension)
				.map(CategorizationSeeker::toRecord)
				.collect(toList());
	}

	public static List<String> recordIdsOf(TemporalRecord record) {
		return recordsOf(record).stream().map(e -> e.core$().id()).collect(Collectors.toList());
	}

	private static List<TemporalRecord> temporalRecords(Concept temporalRecord, NameSpace nameSpace, TimeStamp timeStamp) {
		Graph graph = nameSpace.graph().core$().clone();
		graph.loadStashes(temporalRecordPath(nameSpace, temporalRecord, timeStamp));
		return graph.rootList(TemporalRecord.class);
	}

	private static boolean isDimension(List<?> varValues) {
		return !varValues.isEmpty() && varValues.get(0) instanceof Layer && ((Layer) varValues.get(0)).i$(Entity.class);
	}

	private static Record toRecord(List<?> varValues) {
		return ((Layer) varValues.get(0)).a$(Record.class);
	}

	private static boolean canBeUsedOn(List<Record> records, Categorization categorization) {
		for (Record record : records) if (record.i$(categorization.record())) return true;
		return false;
	}
}
