package io.intino.sumus.queries.temporalrecord;

import io.intino.sumus.graph.TemporalRecord;

import java.util.ArrayList;
import java.util.List;

public class QueryResult {
    private Query query;
    private List<TemporalRecord> values = new ArrayList<>();

    QueryResult(Query query) {
        this.query = query;
    }

    public void register(TemporalRecord temporalRecord) {
        if (values.contains(temporalRecord)) return;
        values.add(temporalRecord);
    }

    public List<TemporalRecord> values() {
        return values;
    }

    public Query query() {
        return query;
    }
}
