package io.intino.sumus.queries.temporalrecord;

import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.TemporalRecord;
import io.intino.tara.magritte.Graph;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static io.intino.sumus.datawarehouse.store.PathBuilder.temporalRecordPath;

public class QueryExecutor {

	private final Query query;
	private final QueryResult result;

	private QueryExecutor(Query query) {
		this.query = query;
		this.result = new QueryResult(query);
	}

	public static QueryResult execute(Query query) {
		QueryExecutor executor = new QueryExecutor(query);
		executor.execute();
		return executor.result;
	}

	private void execute() {
		for (TimeStamp timeStamp : query.timeStamps()) {
			Graph clone = query.nameSpace().graph().core$().clone();
			clone.loadStashes(temporalRecordPath(query.nameSpace(), query.temporalRecord(), timeStamp));
			clone.rootList(query.temporalRecord().layerClass()).stream()
					.map(e -> e.a$(TemporalRecord.class))
					.filter(this::isInFilter)
					.filter(this::hasCondition)
					.forEach(result::register);
		}
	}

	private boolean hasCondition(TemporalRecord temporalRecord) {
		String condition = query.condition();
		if (condition == null || condition.isEmpty()) return true;
		final List<String> keys = Arrays.asList(condition.toLowerCase().split(" "));
		List<String> words = temporalRecord.core$().variables().values().stream().flatMap(Collection::stream).filter(s -> s instanceof String).map(s -> (String) s).collect(Collectors.toList());
		return keys.stream().filter(key -> words.stream().filter(word -> word.toLowerCase().contains(key)).count() > 0).count() == keys.size();
	}

	private boolean isInFilter(TemporalRecord temporalRecord) {
		return query.filter() == null ||
				query.filter().contains(temporalRecord.core$().id()) ||
				query.filter().contains(CategorizationSeeker.recordIdsOf(temporalRecord));
	}

}
