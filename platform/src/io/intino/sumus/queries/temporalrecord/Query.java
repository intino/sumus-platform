package io.intino.sumus.queries.temporalrecord;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Record;
import io.intino.sumus.queries.Filter;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Layer;

import java.time.Instant;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.util.Collections.singletonList;

public class Query {

    private final Concept temporalRecord;
    private final NameSpace nameSpace;
	private final Set<TimeStamp> timeStamps;
	private Filter filter;
    private String condition;

    public Query(NameSpace nameSpace, Concept temporalRecord, TimeStamp timeStamp) {
        this.temporalRecord = temporalRecord;
        this.nameSpace = nameSpace;
		this.timeStamps = new LinkedHashSet<>(singletonList(timeStamp));
	}

    public Query(NameSpace nameSpace, Concept temporalRecord, TimeRange timeRange) {
        this.temporalRecord = temporalRecord;
        this.nameSpace = nameSpace;
		this.timeStamps = timeStampsOf(timeRange);
	}

	private static Set<TimeStamp> timeStampsOf(TimeRange range) {
		Set<TimeStamp> stamps = new LinkedHashSet<>();
		Instant instant = range.scale().normalise(range.from());
		while (instant.isBefore(range.to())) {
			stamps.add(new TimeStamp(instant, range.scale()));
			instant = range.scale().nextTime(instant);
		}
		return stamps;
	}

    public Query(NameSpace nameSpace, Concept temporalRecord, TimeScale scale, Instant instant) {
        this(nameSpace, temporalRecord, new TimeStamp(instant, scale));
    }

    public Query(NameSpace nameSpace, Class<? extends Layer> recordClass, TimeStamp timeStamp) {
        this(nameSpace, nameSpace.core$().graph().concept(recordClass), timeStamp);
    }

    public Query(NameSpace nameSpace, Class<? extends Layer> recordClass, TimeScale scale, Instant instant) {
        this(nameSpace, recordClass, new TimeStamp(instant, scale));
    }
    
    public Query filter(Category... categories) {
        filter = filter == null ? new Filter() : filter;
        filter.add(categories);
        return this;
    }

    public Query filter(Record... records) {
        Category category = new Category("Custom Filter");
        for (Record record : records) category.recordIds().add(record.core$().id());
        filter(category);
        return this;
    }

    Concept temporalRecord() {
        return temporalRecord;
    }

    public NameSpace nameSpace() {
        return nameSpace;
    }

    Collection<TimeStamp> timeStamps() {
        return timeStamps;
    }

    public Filter filter() {
        return filter;
    }

    public String condition() {
        return this.condition;
    }

    public Query condition(String condition) {
        this.condition = condition;
        return this;
    }
}
