package io.intino.sumus.queries;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.FormulaLoader;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.MathHelper;
import io.intino.sumus.queries.digest.Query;

import java.util.*;

import static java.util.Collections.singletonList;

public class CrossTableQuery extends AbstractQuery {
	private Ticket ticket = null;
	private final Map<Categorization, List<Category>> columns = new HashMap<>();
	private final Map<Categorization, List<Category>> rows = new HashMap<>();
	private final Map<Categorization, List<Category>> filters = new HashMap<>();
	private NameSpace nameSpace;
	private TimeRange timeRange;

	private CrossTableQuery() {
	}

	public Ticket ticket() {
		return ticket;
	}

	public Map<Categorization, List<Category>> columns() {
		return columns;
	}

	public Map<Categorization, List<Category>> rows() {
		return rows;
	}

	public Map<Categorization, List<Category>> filters() {
		return filters;
	}

	public NameSpace nameSpace() {
		return nameSpace;
	}

	public TimeRange timeRange() {
		return timeRange;
	}

	public Query toRawQuery(FormulaLoader loader) {
		Query result = new Query();

		addScope(result);

		loader.formulas(singletonList(ticket)).forEach(formula -> result.add(nameSpace, formula));
		result.add(timeRange);

		filters.values().forEach((List<Category> categories) -> result.filter(categories.toArray(new Category[categories.size()])));
		Set<Set<Category>> cartesianColumns = cartesianProduct(new ArrayList<>(this.columns.values()));
		Set<Set<Category>> cartesianRows = cartesianProduct(new ArrayList<>(this.rows.values()));

		cartesianColumns.forEach(cartesianColumn -> cartesianRows.forEach(cartesianRow -> {
			List<Category> categories = mergeSets(cartesianColumn, cartesianRow);
			result.drill(categories);
		}));

		return result;
	}

	private Set<Set<Category>> cartesianProduct(List<List<Category>> values) {
		Set[] set = new Set[values.size()];

		for (int i=0; i<values.size(); i++)
			set[i] = new LinkedHashSet<>(values.get(i));

		if (set.length == 1)
			return new LinkedHashSet<Set<Category>>() {{
				values.get(0).forEach(value -> add(new LinkedHashSet<Category>() {{
					add(value);
				}}));
			}};

		return MathHelper.cartesianProduct(set);
	}


	private List<Category> mergeSets(Set<Category> column, Set<Category> row) {
		List<Category> categories = new ArrayList<>(column);
		categories.addAll(row);
		return categories;
	}

	public static class Builder extends AbstractQuery.Builder<CrossTableQuery> {

		public Builder() {
			super(new CrossTableQuery());
		}

		public CrossTableQuery build(NameSpace nameSpace, Ticket ticket, TimeRange timeRange) {
			nameSpace(nameSpace);
			ticket(ticket);
			timeRange(timeRange);
			return query;
		}

		private Builder ticket(Ticket ticket) {
			query.ticket = ticket;
			return this;
		}

		private Builder nameSpace(NameSpace nameSpace) {
			query.nameSpace = nameSpace;
			return this;
		}

		private Builder timeRange(TimeRange timeRange) {
			query.timeRange = timeRange;
			return this;
		}

		public Builder addColumn(Categorization categorization, List<Category> categories) {
			query.columns.put(categorization, categories);
			return this;
		}

		public Builder addRow(Categorization categorization, List<Category> categories) {
			query.rows.put(categorization, categories);
			return this;
		}

		public Builder filter(List<FilterCondition> filterConditions) {
			filterConditions.forEach(filter -> query.filters.put(filter.categorization, filter.categories));
			return this;
		}
	}
}
