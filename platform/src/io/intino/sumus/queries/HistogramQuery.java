package io.intino.sumus.queries;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.FormulaLoader;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.digest.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistogramQuery extends AbstractQuery {
	private final List<Ticket> tickets = new ArrayList<>();
	private final Map<Categorization, List<Category>> filters = new HashMap<>();
	private TimeSeriesQuery.SortBy sortBy = null;
	private Categorization categorization = null;
	private NameSpace nameSpace;
	private TimeRange timeRange;
	private List<String> keys = new ArrayList<>();

	private HistogramQuery() {
	}

	public List<Ticket> tickets() {
		return tickets;
	}

	public Categorization categorization() {
		return categorization;
	}

	public Map<Categorization, List<Category>> filters() {
		return filters;
	}

	public TimeSeriesQuery.SortBy sortBy() {
		return sortBy;
	}

	public NameSpace nameSpace() {
		return nameSpace;
	}

	public TimeRange timeRange() {
		return timeRange;
	}

	public List<String> keys() { return keys; }

	public Query toRawQuery(FormulaLoader loader, List<Category> categories) {
		Query result = new Query();

		addScope(result);

		loader.formulas(tickets).forEach(formula -> result.add(nameSpace, formula));
		result.add(timeRange);

		filters().values().forEach(filterCategories -> result.filter(filterCategories.toArray(new Category[filterCategories.size()])));
		categories.forEach(result::drill);

		return result;
	}

	public TimeScale scale() {
		return timeRange.scale();
	}

	public static class Builder extends AbstractQuery.Builder<HistogramQuery> {

		public Builder() {
			super(new HistogramQuery());
		}

		public HistogramQuery build(NameSpace nameSpace, Categorization categorization, TimeRange timeRange, TimeSeriesQuery.SortBy sortBy) {
			nameSpace(nameSpace);
			categorization(categorization);
			timeRange(timeRange);
			sortBy(sortBy);
			return query;
		}

		public Builder addTickets(List<Ticket> tickets) {
			query.tickets.addAll(tickets);
			return this;
		}

		public Builder addTicket(Ticket ticket) {
			query.tickets.add(ticket);
			return this;
		}

		private Builder nameSpace(NameSpace nameSpace) {
			query.nameSpace = nameSpace;
			return this;
		}

		private Builder timeRange(TimeRange timeRange) {
			query.timeRange = timeRange;
			return this;
		}

		private Builder categorization(Categorization categorization) {
			query.categorization = categorization;
			return this;
		}

		private Builder sortBy(TimeSeriesQuery.SortBy sortBy) {
			query.sortBy = sortBy;
			return this;
		}

		public Builder filter(Categorization categorization, List<Category> category) {
			query.filters.put(categorization, category);
			return this;
		}

		public Builder filter(List<FilterCondition> filterConditions) {
			filterConditions.forEach(filter -> query.filters.put(filter.categorization, filter.categories));
			return this;
		}

		public Builder keys(List<String> keys) {
			query.keys = keys;
			return this;
		}
	}
}
