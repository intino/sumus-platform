package io.intino.sumus.queries;

import io.intino.sumus.Category;
import io.intino.sumus.graph.Record;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scope {
	private Map<String, List<Category>> categories = new HashMap<>();
	private Map<String, List<Record>> records = new HashMap<>();

	public Scope clear() {
		this.categories.clear();
		this.records.clear();
		return this;
	}

	public Collection<List<Category>> categories() {
		return this.categories.values();
	}

	public Scope categories(Map<String, List<Category>> categories) {
		this.categories.clear();
		this.categories.putAll(categories);
		return this;
	}

	public Collection<List<Record>> records() {
		return this.records.values();
	}

	public Scope records(Map<String, List<Record>> records) {
		this.records.clear();
		this.records.putAll(records);
		return this;
	}
}
