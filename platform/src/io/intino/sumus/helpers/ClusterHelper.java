package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Cluster;
import io.intino.sumus.graph.Entity;

import java.util.List;

public class ClusterHelper {

	public static Cluster registerClusterGroup(SumusBox box, Catalog catalog, String grouping, String group, List<Entity> entities, String username) {
		io.intino.sumus.graph.Cluster cluster = ClusterHelper.loadCluster(box, catalog, grouping, username);

		if (cluster == null)
			cluster = ClusterHelper.createCluster(box, catalog, grouping, username);

		ClusterHelper.createClusterGroup(cluster, group, entities);

		return cluster;
	}

	private static io.intino.sumus.graph.Cluster loadCluster(SumusBox box, Catalog catalog, String key, String username) {
		return box.graph().clusterList().stream().filter(c -> (c.username() == null || (c.username() != null && c.username().equals(username)))
				&& c.label().equals(key)).findFirst().orElse(null);
	}

	private static io.intino.sumus.graph.Cluster createCluster(SumusBox box, Catalog catalog, String key, String username) {
		Cluster cluster = box.graph().create("clusters").cluster(catalog, key, username);
		cluster.catalog(catalog);
//TODO MARIO		cluster.catalog().analysis().create().clusterGrouping(cluster).histogram(AbstractGrouping.Histogram.Absolute);
		return cluster;
	}

	private static void createClusterGroup(Cluster cluster, String name, List<Entity> entities) {
		Cluster.Group group = cluster.create().group(name);
		entities.forEach(entity -> group.entities().add(entity));
		cluster.save$();
	}

}
