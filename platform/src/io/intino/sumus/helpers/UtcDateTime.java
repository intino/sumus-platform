package io.intino.sumus.helpers;

import java.time.*;
import java.time.temporal.TemporalField;

public class UtcDateTime {
    private LocalDateTime dateTime;

    public UtcDateTime(long epochMilli) {
        this.dateTime = dateTimeOf(epochMilli);
    }

    public UtcDateTime(Instant instant) {
        this.dateTime = dateTimeOf(instant);
    }

    public UtcDateTime(int year, int month, int day, int hour, int minute) {
        this.dateTime = LocalDateTime.of(year, month, day, hour, minute);
    }

    public UtcDateTime(int year, int month, int day, int hour, int minute, int second) {
        this.dateTime = LocalDateTime.of(year, month, day, hour, minute, second);
    }

    public int year() {
        return dateTime.getYear();
    }

    public static int yearOf(Instant instant) {
        return dateTimeOf(instant).getYear();
    }

    public java.time.Month month() {
        return dateTime.getMonth();
    }

    public static java.time.Month monthOf(Instant instant) {
        return dateTimeOf(instant).getMonth();
    }

    public int monthNumber() {
        return dateTime.getMonthValue();
    }

    public static int monthNumberOf(Instant instant) {
        return dateTimeOf(instant).getMonthValue();
    }

    public int dayOfYear() {
        return dateTime.getDayOfYear();
    }

    public static int dayOfYearOf(Instant instant) {
        return dateTimeOf(instant).getDayOfYear();
    }

    public int dayOfMonth() {
        return dateTime.getDayOfMonth();
    }

    public DayOfWeek dayOfWeek() {
        return dateTime.getDayOfWeek();
    }

    public static int dayOfMonthOf(Instant instant) {
        return dateTimeOf(instant).getDayOfMonth();
    }

    public static DayOfWeek dayOfWeek(Instant instant) {
        return dateTimeOf(instant).getDayOfWeek();
    }

    public int hour() {
        return dateTime.getHour();
    }

    public static int hourOf(Instant instant) {
        return dateTimeOf(instant).getHour();
    }

    public int minute() {
        return dateTime.getMinute();
    }

    public static int minuteOf(Instant instant) {
        return dateTimeOf(instant).getMinute();
    }

    public int second() {
        return dateTime.getSecond();
    }

    public static int secondOf(Instant instant) {
        return dateTimeOf(instant).getSecond();
    }

    public boolean isBefore(UtcDateTime other) {
        return dateTime.isBefore(other.dateTime);
    }

    public boolean isEqual(UtcDateTime other) {
        return dateTime.isEqual(other.dateTime);
    }

    public UtcDateTime plusWeeks(int week) {
        dateTime = dateTime.plusWeeks(week);
        return this;
    }

    public UtcDateTime plusDays(int day) {
        dateTime = dateTime.plusDays(day);
        return this;
    }

    public UtcDateTime plusHours(int hours) {
        dateTime = dateTime.plusHours(hours);
        return this;
    }

    public UtcDateTime plusMinutes(int minutes) {
        dateTime = dateTime.plusMinutes(minutes);
        return this;
    }

    public UtcDateTime minusDays(int day) {
        dateTime = dateTime.minusDays(day);
        return this;
    }

    public UtcDateTime with(TemporalField temporalField, int newValue) {
        dateTime = dateTime.with(temporalField, newValue);
        return this;
    }

    public int get(TemporalField temporalField) {
        return dateTime.get(temporalField);
    }

    public Instant toInstant() {
        return instantOf(dateTime);
    }

    public static UtcDateTime of(Instant initialInstant) {
        return new UtcDateTime(initialInstant);
    }

    public static UtcDateTime of(long epochMilli) {
        return new UtcDateTime(epochMilli);
    }

    public static UtcDateTime of(int year, int month, int day, int hour, int minute) {
        return new UtcDateTime(year, month, day, hour, minute);
    }

    public static UtcDateTime of(int year, int month, int day, int hour, int minute, int second) {
        return new UtcDateTime(year, month, day, hour, minute, second);
    }

    private static LocalDateTime dateTimeOf(long epochMilli) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(epochMilli), utc());
    }

    private static LocalDateTime dateTimeOf(Instant instant) {
        return dateTimeOf(instant.toEpochMilli());
    }

    private static ZoneId utc() {
        return ZoneId.of("UTC");
    }

    private static ZoneOffset utcOffset() {
        return ZoneOffset.UTC;
    }

    private static Instant instantOf(LocalDateTime dateTime) {
        return dateTime.toInstant(utcOffset());
    }

}
