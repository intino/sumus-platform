package io.intino.sumus.helpers;

import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.Indicator;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.queries.CrossTableQuery;
import io.intino.sumus.analytics.viewmodels.CrossTable;
import io.intino.sumus.queries.digest.Query;
import io.intino.sumus.queries.digest.QueryExecutor;
import io.intino.sumus.queries.digest.QueryResult;

import java.util.List;

public class CrossTableHelper extends QueryHelper {

	public CrossTable execute(CrossTableQuery crossTableQuery) {
		Query query = queryOf(crossTableQuery);
		QueryResult rawData = QueryExecutor.execute(query);
		CrossTable crossTable = new CrossTable(unitOf(query.formulas()));

		query.nameSpaces().forEach(nameSpace -> query.formulas().forEach(formula -> {
			query.drills().forEach(drill -> {
				crossTable.register(drill.categories(), rawData.values(nameSpace, formula, drill).values().iterator().next());
			});
// TODO JE
//			query.temporalStamps().forEach(stamp -> {
//				query.drills(stamp).forEach(drill -> {
//					crossTable.register(drill.categories(), rawData.values(nameSpace, formula, drill).values().iterator().next());
//				});
//			});
		}));

		return crossTable;
	}

	private Query queryOf(CrossTableQuery query) {
		return query.toRawQuery(tickets -> CrossTableHelper.this.formulas(measureIndicators(query.ticket().dataRetriever().a$(Indicator.class))));
	}

	private AbstractMetric.Unit unitOf(List<MeasureIndicator.Formula> formulas) {
		return formulas.get(0).core$().ownerAs(MeasureIndicator.class).unit();
	}

}
