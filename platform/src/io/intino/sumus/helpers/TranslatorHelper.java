package io.intino.sumus.helpers;

import io.intino.sumus.graph.SumusGraph;

public class TranslatorHelper extends Helper {
	private final SumusGraph graph;

	public TranslatorHelper(SumusGraph model) {
		this.graph = model;
	}

	public Translation translate(String message) {
		return language -> graph.i18n$().message(language, message);
	}

	@FunctionalInterface
	public interface Translation {
		String into(String language);
	}
}
