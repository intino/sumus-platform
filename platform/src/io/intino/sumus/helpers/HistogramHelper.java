package io.intino.sumus.helpers;

import io.intino.sumus.graph.Categorization;
import io.intino.sumus.Category;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.HistogramQuery;
import io.intino.sumus.analytics.viewmodels.Histogram;
import io.intino.sumus.queries.digest.Query;
import io.intino.sumus.queries.digest.QueryExecutor;
import io.intino.sumus.queries.digest.QueryResult;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class HistogramHelper extends QueryHelper {

	public Histogram execute(HistogramQuery histogramQuery) {
		Query query = queryOf(histogramQuery);
		QueryResult rawData = QueryExecutor.execute(query);
		Histogram histogram = new Histogram(categories(histogramQuery), histogramQuery.sortBy());

		query.nameSpaces().forEach(nameSpace -> query.formulas().forEach(formula -> {
			List<Double> values = query.drills().stream().map(d -> rawData.values(nameSpace, formula, d).values().iterator().next()).collect(toList());
			Histogram.HistogramTicket ticket = histogram.histogramTicket(ticketOf(formula));
			ticket.register(createIndicator(formula, query, values));
		}));

		return histogram;
	}

	private Histogram.HistogramIndicator createIndicator(MeasureIndicator.Formula formula, Query query, List<Double> values) {
		MeasureIndicator indicator = indicatorOf(formula);
		Ticket.DecimalPlaces decimalPlaces = ticketOf(formula).decimalPlaces();
		Histogram.HistogramIndicator result = new Histogram.HistogramIndicator(indicator, labelOf(formula), colorOf(indicator), decimalPlaces);
		result.register(values);
		return result;
	}

	private Query queryOf(HistogramQuery query) {
		return query.toRawQuery(tickets -> HistogramHelper.this.formulas(measureIndicators(query.tickets(), query.scale())), categories(query));
	}

	private List<Category> categories(HistogramQuery query) {
		Categorization categorization = query.categorization();
		if (query.filters().containsKey(categorization))
			return query.filters().get(categorization);
		return categorization.calculate(query.keys());
	}

}
