package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.datawarehouse.store.PathBuilder;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.TemporalRecord;
import io.intino.sumus.queries.TemporalRecordQuery;
import io.intino.sumus.queries.temporalrecord.QueryExecutor;
import io.intino.tara.magritte.Node;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class TemporalRecordHelper extends Helper {
    private final SumusBox box;

    public TemporalRecordHelper(SumusBox box) {
        this.box = box;
    }

    public TimeRange recordRange(List<NameSpace> nameSpaces) {
        return PathBuilder.range(nameSpaces);
    }

    public List<TemporalRecord> records(TemporalRecordQuery query) {
        return QueryExecutor.execute(query.toRawQuery()).values().stream().filter(tr -> checkPermissions(tr, query.username())).collect(toList());
    }

    public TemporalRecord record(String id, String username) {
        Node node = box.graph().core$().clone().load(id, false);
        if (node == null) return null;
        TemporalRecord record = node.as(TemporalRecord.class);
        return checkPermissions(record, username) ? record : null;
    }

    private boolean checkPermissions(TemporalRecord record, String username) {
        return box.graph().checkPermissions(record, username);
    }

}
