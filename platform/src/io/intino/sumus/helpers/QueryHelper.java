package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.Scaler;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.graph.*;
import io.intino.sumus.queries.Drill;
import io.intino.sumus.queries.digest.Query;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Node;

import java.util.*;

import static java.util.Collections.*;
import static java.util.stream.Collectors.toList;

abstract class QueryHelper extends Helper {

    private static final String DrillLabel = "%s (%s)";

    String labelOf(MeasureIndicator.Formula formula) {
        return formula.core$().ownerAs(MeasureIndicator.class).label();
    }

    String labelOf(MeasureIndicator.Formula formula, Drill drill) {
        List<String> categories = drill.categories().stream().map(Category::label).collect(toList());
        return String.format(DrillLabel, formula.core$().ownerAs(MeasureIndicator.class).label(), String.join(",", categories));
    }

    String colorOf(MeasureIndicator indicator) {
        return indicator.graph().palette().colorOf(indicator, Palette.Color.Type.Main);
    }

    String colorOf(MeasureIndicator indicator, int index) {
        return indicator.graph().palette().colorOf(indicator, Palette.Color.Type.valueOf("Drill" + index));
    }

    int drillIndexOf(Drill drill, Query query) {
        List<Drill> drills = query.drills();
        for (int i = 0; i < drills.size(); i++)
            if (drills.get(i) == drill) return i+1;
        return 0;
    }

    List<MeasureIndicator.Formula> formulas(List<MeasureIndicator> measureIndicatorList) {
        if (measureIndicatorList.isEmpty()) return emptyList();
        List<Categorization> categorizationList = measureIndicatorList.get(0).graph().categorizationList();
        return new ArrayList<>(categorizationsByCube(categorizationList, measureIndicatorList).values());
    }

    void scaleValues(List<Serie> serieList) {
        if (serieList.isEmpty()) return;

        serieList.forEach(serie -> {
            AbstractMetric.Unit unit = serie.unit();
            AbstractMetric metric = unit.core$().ownerAs(AbstractMetric.class);
            Scaler indicatorsScale = metric.scaler().scaler(serie.maxValue(), unit);
            serie.scaleValues(indicatorsScale);
        });
    }

    Ticket ticketOf(MeasureIndicator.Formula formula) {
        Node owner = formula.core$().owner();
        if (owner.is(MeasureIndicator.class)) return owner.ownerAs(Ticket.class);
        else return owner.owner().ownerAs(Ticket.class);
    }

    MeasureIndicator indicatorOf(MeasureIndicator.Formula formula) {
        return formula.core$().ownerAs(MeasureIndicator.class);
    }

    List<MeasureIndicator> measureIndicators(List<Ticket> ticketList, TimeScale scale) {
        return ticketList.parallelStream()
                .filter(ticket -> isTicketVisible(ticket, scale))
                .filter(ticket -> ticket.dataRetriever().i$(Indicator.class))
                .map(ticket -> measureIndicators(ticket.dataRetriever().a$(Indicator.class)))
                .flatMap(Collection::stream).collect(toList());
    }

    List<MeasureIndicator> measureIndicators(Indicator indicator) {
        if (indicator.i$(MeasureIndicator.class)) return singletonList(indicator.a$(MeasureIndicator.class));
        return indicator.a$(StackedIndicator.class).measureIndicatorList();
    }

    private boolean isTicketVisible(Ticket ticket, TimeScale scale) {
        int queryScale = scale.ordinal();
        return ticket.range().max().ordinal() <= queryScale && queryScale <= ticket.range().min().ordinal();
    }

    public Map<MeasureIndicator, MeasureIndicator.Formula> categorizationsByCube(List<Categorization> categorizations, List<MeasureIndicator> indicators) {
        if (indicators.isEmpty()) return emptyMap();
        List<Cube> cubes = commonCubesOf(indicators);
        if (cubes.isEmpty()) throw new RuntimeException("There are no common cubes in indicators provided");
        Cube cube = cubeWithMostCategorizationCriteria(categorizations, cubes);
        return indicators.stream().map(i -> formulaOf(i, cube)).collect(LinkedHashMap::new, (map, f) -> map.put(f.core$().ownerAs(MeasureIndicator.class), f), LinkedHashMap::putAll);
    }

    @SuppressWarnings("ConstantConditions")
    private MeasureIndicator.Formula formulaOf(MeasureIndicator indicator, Cube cube) {
        return indicator.formulaList().stream().filter(f -> f.cube().equals(cube)).findFirst().get();
    }

    private Cube cubeWithMostCategorizationCriteria(List<Categorization> categorizations, List<Cube> cubes) {
        Cube maxCube = null;
        int maxCount = -1;
        for (Cube cube : cubes) {
            int count = categorizationCountAt(categorizations, cube);
            if (count > maxCount) {
                maxCount = count;
                maxCube = cube;
            }
        }
        return maxCube;
    }

    private int categorizationCountAt(List<Categorization> categorizations, Cube cube) {
        List<Concept> cubeEntities = cube.dimensionList().stream().map(Cube.Dimension::entity).collect(toList());
        List<Concept> categorizationEntities = categorizations.stream().map(Categorization::record).collect(toList());
        final int[] coincidenceCount = {0};
        cubeEntities.forEach(c1 -> categorizationEntities.forEach(c2 -> {
            if (c2.equals(c1)) coincidenceCount[0]++;
        }));
        return coincidenceCount[0];
    }

    private List<Cube> commonCubesOf(List<MeasureIndicator> indicators) {
        Map<Cube, List<Indicator>> cubeMap = new HashMap<>();
        indicators.get(0).graph().cubeList().forEach(c -> cubeMap.put(c, new ArrayList<>()));
        indicators.forEach(i -> i.formulaList().forEach(f -> cubeMap.get(f.cube()).add(i)));
        return cubeMap.entrySet().stream()
                .filter(e -> e.getValue().size() == indicators.size())
                .map(Map.Entry::getKey)
                .collect(toList());
    }

}
