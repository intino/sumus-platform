package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.Scope;
import io.intino.tara.magritte.Concept;

import java.util.*;

import static java.util.stream.Collectors.toList;

public class ChartSpec {
    private List<Ticket> ticketList = new LinkedList<>();
    private Map<Ticket, List<String>> ticketTags = new HashMap<>();
    private List<Categorization> categorizationList = new LinkedList<>();
    private Map<Categorization, List<String>> categorizationTags = new HashMap<>();
    private List<FilterCondition> filterConditionList = new LinkedList<>();
    private List<Concept> eventTypeList = new ArrayList<>();
    private Drill drill = null;
    private TimeRange range = null;
    private boolean includeGlobalSerie = true;
    private HeatMap heatMap = null;
    private Scope scope = null;

    public Scope scope() {
        return scope;
    }

    public void scope(Scope scope) {
        this.scope = scope;
    }

    public List<Ticket> ticketList() {
        return this.ticketList;
    }

    public List<Ticket> ticketList(String tag) {
        return ticketTags.entrySet().stream().filter(entry -> {
            Ticket ticket = entry.getKey();
            return entry.getValue().contains(tag) && ticketList.contains(ticket);
        }).map(Map.Entry::getKey).collect(toList());
    }

    public void clearTicketsTag(String tag) {
        ticketTags.values().forEach(tt -> tt.remove(tag));
    }

    public void clearTags(Ticket ticket) {
        if (!ticketTags.containsKey(ticket)) return;
        ticketTags.clear();
    }

    public void addTags(Ticket ticket, List<String> tagList) {
        if (!ticketTags.containsKey(ticket))
            ticketTags.put(ticket, new ArrayList<>());
        ticketTags.get(ticket).addAll(tagList);
    }

    public List<Categorization> categorizationList() {
        return this.categorizationList;
    }

    public List<Categorization> categorizationList(String tag) {
        return categorizationTags.entrySet().stream().filter(entry -> {
            Categorization categorization = entry.getKey();
            return entry.getValue().contains(tag) && categorizationList.contains(categorization);
        }).map(Map.Entry::getKey).collect(toList());
    }

    public void clearCategorizationsTag(String tag) {
        categorizationTags.values().forEach(ct -> ct.remove(tag));
    }

    public void addTags(Categorization categorization, List<String> tagList) {
        if (!categorizationTags.containsKey(categorization))
            categorizationTags.put(categorization, new ArrayList<>());
        categorizationTags.get(categorization).addAll(tagList);
    }

    public Drill drill() {
        return this.drill;
    }

    public boolean includeGlobalSerie() {
        return this.includeGlobalSerie;
    }

    public void drill(Drill drill) {
        this.drill = drill;
    }

    public void drill(Drill drill, boolean includeGlobalSerie) {
        this.drill = drill;
        this.includeGlobalSerie = includeGlobalSerie;
    }

    public List<FilterCondition> filterList() {
        return this.filterConditionList;
    }

    public TimeRange range() {
        return this.range;
    }

    public void range(TimeRange range) {
        this.range = range;
    }

    public HeatMap heatMap() {
        return this.heatMap;
    }

    public void heatMap(HeatMap heatMap) {
        this.heatMap = heatMap;
    }

    public List<Concept> eventTypes() {
        return eventTypeList;
    }

    public void eventTypes(List<Concept> eventTypeList) {
        this.eventTypeList = eventTypeList;
    }

    public boolean equals(ChartSpec spec) {
        if (!equalsTickets(spec.ticketList())) return false;
        if (!equalsCategorizations(spec.categorizationList())) return false;
        if (!equalsFilters(spec.filterList())) return false;
        if (!equalsDrill(spec.drill())) return false;
        if (!equalsRange(spec.range())) return false;
        return true;
    }

    public boolean equalsTickets(List<Ticket> ticketList) {
        if (ticketList.size() != this.ticketList.size()) return false;
        for (int i = 0; i < ticketList.size(); i++) {
            Ticket ticket = ticketList.get(i);
            Ticket currentTicket = this.ticketList.get(i);
            if (!ticket.name$().equals(currentTicket.name$())) return false;
        }
        return true;
    }

    public boolean equalsCategorizations(List<Categorization> categorizationList) {
        if (categorizationList.size() != this.categorizationList.size()) return false;
        for (int i = 0; i < categorizationList.size(); i++) {
            Categorization categorization = categorizationList.get(i);
            Categorization currentCategorization = this.categorizationList.get(i);
            if (!categorization.name$().equals(currentCategorization.name$())) return false;
        }
        return true;
    }

    public boolean equalsFilters(List<FilterCondition> filterConditionList) {
        if (filterConditionList.size() != this.filterConditionList.size()) return false;
        for (int i = 0; i < filterConditionList.size(); i++) {
            FilterCondition condition = filterConditionList.get(i);
            FilterCondition currentFilterCondition = this.filterConditionList.get(i);
            if (condition != currentFilterCondition) return false;
        }
        return true;
    }

    public boolean equalsDrill(Drill drill) {
        if (drill == null && this.drill == null) return true;
        if (drill == null) return false;
        if (this.drill == null) return false;
        return drill.categorization == this.drill.categorization && drill.categories.size() == this.drill.categories.size();
    }

    public boolean equalsRange(TimeRange range) {
        if (this.range == null && range == null) return true;
        if (this.range == null || range == null) return false;
        if (this.range.scale() != range.scale()) return false;
        if (this.range.from() != range.from()) return false;
        if (this.range.to() != range.to()) return false;
        return true;
    }

    public interface HeatMap {
        String minColor();
        String maxColor();
    }
}
