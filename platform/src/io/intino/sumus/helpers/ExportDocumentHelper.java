package io.intino.sumus.helpers;

import io.intino.sumus.*;
import io.intino.sumus.analytics.Scaler;
import io.intino.sumus.queries.TimeSeriesQuery;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.graph.AbstractMetric;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.Drill;
import io.intino.sumus.queries.digest.Query;
import io.intino.sumus.queries.digest.QueryExecutor;
import io.intino.sumus.queries.digest.QueryResult;

import java.time.Instant;
import java.util.*;

import static io.intino.sumus.QueryEngine.DocumentRow;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;

public class ExportDocumentHelper extends QueryHelper {
    private final Map<String, Scaler> scalers = new HashMap<>();

    public List<DocumentRow> execute(TimeSeriesQuery timeSeriesQuery) {
        Query query = queryOf(timeSeriesQuery);
        QueryResult rawData = QueryExecutor.execute(query);
        List<Serie> serieList = new ArrayList<>();

        query.nameSpaces().forEach(nameSpace -> query.formulas().forEach(formula -> {
            if (query.drills().isEmpty())
                serieList.add(createSerie(formula, query, rawData.values(nameSpace, formula)));

            query.drills().forEach(drill -> serieList.add(createSerie(formula, query, rawData.values(nameSpace, formula, drill), drill)));
        }));

        return toDocument(serieList, timeSeriesQuery);
    }

    private List<DocumentRow> toDocument(List<Serie> serieList, TimeSeriesQuery timeSeriesQuery) {
        Comparator<DocumentRow> byInstant = Comparator.comparing(r -> r.internalInstant);
        Comparator<DocumentRow> byTicket = Comparator.comparing(r -> r.indicator);
        Comparator<DocumentRow> byInstantAndTicket = byInstant.thenComparing(byTicket);

        List<DocumentRow> rows = new ArrayList<>();
        serieList.forEach(serie -> serie.values().entrySet().stream().map(e -> rows.add(buildRow(e, unitLabelOf(serie), emptyMap(), serie, timeSeriesQuery))));

        return rows.stream().sorted(byInstantAndTicket).collect(toList());
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, Map<TimeStamp, Double> values) {
        TimeStamp timeStamp = query.timeStamps().get(0);
        return createSerie(formula, query, labelOf(formula), colorOf(indicatorOf(formula)), values);
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, Map<TimeStamp, Double> values, Drill drill) {
        int drillIndex = drillIndexOf(drill, query);
        return createSerie(formula, query, labelOf(formula, drill), colorOf(indicatorOf(formula), drillIndex), values);
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, String label, String color, Map<TimeStamp, Double> values) {
        Ticket ticket = ticketOf(formula);
        Serie serie = new Serie(ticket, indicatorOf(formula), label, color);
        boolean fillWithZeros = ticket.style().line() == Ticket.Style.Line.ContinuousWithZeros;

        query.timeStamps().forEach(timeStamp -> {
            if (!values.containsKey(timeStamp) && !fillWithZeros) return;
            serie.register(timeStamp.instant(), fillWithZeros ? 0d : values.get(timeStamp));
        });

        scalers.put(serie.label(), scaler(serie));
        serie.scaleValues(scalers.get(serie.label()));

        return serie;
    }

    private String unitLabelOf(Serie indicator) {
        return scalers.get(indicator.label()).unitLabel();
    }

    private DocumentRow buildRow(Map.Entry<Instant, Double> entryValue, String unitLabel, Map<Categorization, Category> drill, Serie indicator, TimeSeriesQuery query) {
        Double value = entryValue.getValue();

        if (indicator.decimalPlaces().absolute() != 0)
            value = MathHelper.round(value, indicator.decimalPlaces().absolute());

        return new DocumentRow(entryValue.getKey(), indicator.label(), drill, query.filters(), value, unitLabel, query.timeRange().scale());
    }

    private Scaler scaler(Serie indicator) {
        return scaler(indicator, indicator.maxValue());
    }

    private Scaler scaler(Serie indicator, double maxValue) {
        scalers.putIfAbsent(indicator.label(), indicator.unit().core$().ownerAs(AbstractMetric.class).scaler().scaler(maxValue, indicator.unit()));
        return scalers.get(indicator.label());
    }

    private Query queryOf(TimeSeriesQuery query) {
        return query.toRawQuery(tickets -> ExportDocumentHelper.this.formulas(measureIndicators(query.tickets(), query.scale())));
    }

}
