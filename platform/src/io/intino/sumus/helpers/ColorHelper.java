package io.intino.sumus.helpers;

import java.util.Random;

public class ColorHelper {

    private static Random RandomGenerator = new Random();
    private static final char[] hex = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    public static String newColor() {
        char[] s = new char[7];
        int n = RandomGenerator.nextInt(0x1000000);

        s[0] = '#';
        for (int i=1;i<7;i++) {
            s[i] = hex[n & 0xf];
            n >>= 4;
        }

        return new String(s);
    }

}
