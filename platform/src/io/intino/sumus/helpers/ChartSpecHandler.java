package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec.HeatMap;
import io.intino.sumus.queries.Scope;
import io.intino.tara.magritte.Concept;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ChartSpecHandler {
    private boolean dirty = false;
    private List<Ticket> olapTicketList = new ArrayList<>();
    private List<Categorization> olapCategorizationList = new ArrayList<>();
    private List<TemporalCategorization> olapTemporalCategorizationList = new ArrayList<>();
    private List<Consumer<ChartSpec>> changeListeners = new ArrayList<>();
    private ChartSpec spec = new ChartSpec();

    public void onChange(Consumer<ChartSpec> listener) {
        this.changeListeners.add(listener);
    }

    public ChartSpec specification() {
        return spec;
    }

    public List<Ticket> olapTickets() {
        return olapTicketList;
    }

    public void olapTickets(List<Ticket> olapTicketList) {
        this.olapTicketList.clear();
        this.olapTicketList.addAll(olapTicketList);
    }

    public List<Categorization> olapCategorizations() {
        return olapCategorizationList;
    }

    public void olapCategorizations(List<Categorization> olapCategorizationList) {
        this.olapCategorizationList.clear();
        this.olapCategorizationList.addAll(olapCategorizationList);
    }

    public List<TemporalCategorization> olapTemporalCategorizations() {
        return olapTemporalCategorizationList;
    }

    public void olapTemporalCategorizations(List<TemporalCategorization> olapTemporalCategorizationList) {
        this.olapTemporalCategorizationList.clear();
        this.olapTemporalCategorizationList.addAll(olapTemporalCategorizationList);
    }

    public List<Ticket> selectedTickets() {
        return spec.ticketList();
    }

    public void clearTicketsTag(String tag) {
        spec.clearTicketsTag(tag);
    }

    public void setScope(Scope scope) {
        spec.scope(scope);
        dirty = true;
    }

    public void setTicketTags(Ticket ticket, List<String> tags) {
        spec.clearTags(ticket);
        spec.addTags(ticket, tags);
        dirty = true;
    }

    public void selectTickets(String... tickets) {
        List<Ticket> ticketList = Stream.of(tickets).map(this::ticketOf).collect(toList());
        selectTickets(ticketList);
        dirty = true;
    }

    public void selectTickets(List<Ticket> ticketList) {
        if (spec.equalsTickets(ticketList)) return;
        List<Ticket> specTicketList = spec.ticketList();
        specTicketList.clear();
        ticketList.forEach(specTicketList::add);
        dirty = true;
    }

    public void selectTickets(Map<Ticket, List<String>> taggedTicketList) {
        List<Ticket> specTicketList = spec.ticketList();
        specTicketList.clear();
        taggedTicketList.entrySet().forEach(entry -> {
            Ticket ticket = entry.getKey();
            specTicketList.add(ticket);
            spec.addTags(ticket, entry.getValue());
        });
        dirty = true;
    }

    public void addTicket(Ticket ticket) {
        List<Ticket> specTicketList = spec.ticketList();

        if (specTicketList.contains(ticket))
            return;

        specTicketList.add(ticket);
        dirty = true;
    }

    public void removeTicket(Ticket ticket) {
        spec.ticketList().remove(ticket);
        dirty = true;
    }

    public void clearCategorizationsTag(String tag) {
        spec.clearCategorizationsTag(tag);
        dirty = true;
    }

    public void selectCategorizations(Map<Categorization, List<String>> taggedCategorizationList) {
        List<Categorization> specCategorizationList = spec.categorizationList();
        taggedCategorizationList.entrySet().forEach(entry -> {
            Categorization categorization = entry.getKey();
            if (!specCategorizationList.contains(categorization)) specCategorizationList.add(categorization);
            spec.addTags(categorization, entry.getValue());
        });
        dirty = true;
    }

    public void range(TimeRange range) {
        spec.range(range);
        dirty = true;
    }

    public void heatMap(HeatMap heatMap) {
        spec.heatMap(heatMap);
        dirty = true;
    }

    public void filter(List<FilterCondition> conditionList) {
        spec.filterList().clear();
        spec.filterList().addAll(conditionList);
        dirty = true;
    }

    public Drill drill() {
        return spec.drill();
    }

    public void drill(Drill drill) {
        drill(drill, spec.includeGlobalSerie());
    }

    public void drill(Drill drill, boolean includeGlobalSerie) {
        if (spec.equalsDrill(drill) && spec.includeGlobalSerie() == includeGlobalSerie) return;
        spec.drill(drill, includeGlobalSerie);
        dirty = true;
    }

    public Drill removeDrillCategory(String categoryName) {
        Drill drill = spec.drill();
        drill.removeCategory(categoryName);
        dirty = true;
        return drill;
    }

    public void eventTypes(List<Concept> eventTypes) {
        spec.eventTypes(eventTypes);
        dirty = true;
    }

    public void update() {
        if (!dirty) return;
        notifySpecChanged();
        dirty = false;
    }

    private void notifySpecChanged() {
        changeListeners.parallelStream().forEach(l -> l.accept(spec));
    }

    private Ticket ticketOf(String key) {
        return olapTicketList.stream().filter(t -> t.name$().equals(key) || t.label().equals(key)).findFirst().orElse(null);
    }

    private Categorization categorizationOf(String key) {
        return allCategorizations().stream().filter(c -> c.name$().equals(key) || c.label().equals(key)).findFirst().orElse(null);
    }

    private List<Categorization> allCategorizations() {
        List<Categorization> result = new ArrayList<>();
        result.addAll(olapCategorizationList);
        result.addAll(olapTemporalCategorizationList);
        return result;
    }

}
