package io.intino.sumus.helpers;

import io.intino.konos.alexandria.activity.services.push.ActivitySession;

import java.net.MalformedURLException;
import java.net.URL;

public class Asset {

    public static URL baseAssetUrl(ActivitySession session) {
        try {
            return new URL(session.browser().baseAssetUrl());
        } catch (MalformedURLException e) {
            return null;
        }
    }

}
