package io.intino.sumus.helpers;

import io.intino.sumus.SumusStore;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.ProfileManager;
import io.intino.sumus.graph.SumusGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class NameSpaceHandler {
	private final SumusGraph graph;
	private Map<String, NameSpace> selection = new HashMap<>();
	private List<Consumer<NameSpace>> listeners = new ArrayList<>();

	public NameSpaceHandler(SumusGraph graph) {
		this.graph = graph;
	}

	public void onSelect(Consumer<NameSpace> listener) {
		this.listeners.add(listener);
	}

	public void removeSelectListener(Consumer<NameSpace> listener) {
		this.listeners.remove(listener);
	}

	public NameSpace selectedNameSpace(String username) {

		if (!selection.containsKey(username)) {
			List<NameSpace> nameSpaces = nameSpaces(username);
			if (nameSpaces.size() > 0)
				selection.put(username, nameSpaces.get(0));
		}

		return selection.get(username);
	}

	public NameSpaceHandler select(String username, String key) {
		select(username, nameSpaces(username).stream().filter(n -> n.name$().equals(key) || n.label().equals(key)).findFirst().orElse(null));
		return this;
	}

	public NameSpaceHandler select(String username, NameSpace nameSpace) {
		selection.put(username, nameSpace);
		clearNameSpaceRanges();
		this.listeners.forEach(l -> l.accept(nameSpace));
		return this;
	}

	public List<NameSpace> nameSpaces(String username) {
		ProfileManager manager = graph.profileManager();
		if (manager == null || username == null) return graph.nameSpaceList();
		return manager.profile(username).nameSpaces();
	}

	private void clearNameSpaceRanges() {
		SumusStore store = (SumusStore) graph.core$().store();
		store.clearNameSpaceRanges();
	}

}
