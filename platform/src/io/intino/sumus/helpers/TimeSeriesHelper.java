package io.intino.sumus.helpers;

import io.intino.sumus.TimeStamp;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.graph.MeasureIndicator;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.queries.Drill;
import io.intino.sumus.queries.TimeSeriesQuery;
import io.intino.sumus.queries.digest.Query;
import io.intino.sumus.queries.digest.QueryExecutor;
import io.intino.sumus.queries.digest.QueryResult;

import java.util.*;

public class TimeSeriesHelper extends QueryHelper {

    private static final String GlobalSerieColor = "#DDD";

    public synchronized List<Serie> execute(TimeSeriesQuery timeSeriesQuery) {
        Query query = queryOf(timeSeriesQuery);
        QueryResult rawData = QueryExecutor.execute(query);
        List<Serie> result = new ArrayList<>();

        query.nameSpaces().forEach(nameSpace -> query.formulas().forEach(formula -> {

            if (!query.drills().isEmpty()) {
                if (timeSeriesQuery.includeGlobalSerie())
                    result.add(createGlobalSerie(formula, query, rawData.values(nameSpace, formula)));
            }
            else
                result.add(createSerie(formula, query, rawData.values(nameSpace, formula)));

            query.drills().forEach(drill -> result.add(createSerie(formula, query, rawData.values(nameSpace, formula, drill), drill)));

        }));

        scaleValues(result);

        return result;
    }

    public List<String> entities(TimeSeriesQuery timeSeriesQuery) {
        Query query = queryOf(timeSeriesQuery);
        QueryResult rawData = QueryExecutor.execute(query);
        Set<String> entities = new HashSet<>();
        rawData.digests().forEach(d -> entities.addAll(d.entityIdsSplit()));
        return new ArrayList<>(entities);
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, Map<TimeStamp, Double> values) {
        return createSerie(formula, query, labelOf(formula), colorOf(indicatorOf(formula)), values);
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, Map<TimeStamp, Double> values, Drill drill) {
        int drillIndex = drillIndexOf(drill, query);
        return createSerie(formula, query, labelOf(formula, drill), colorOf(indicatorOf(formula), drillIndex), values);
    }

    private Serie createGlobalSerie(MeasureIndicator.Formula formula, Query query, Map<TimeStamp, Double> values) {
        Serie serie = createSerie(formula, query, labelOf(formula), GlobalSerieColor, values);
        serie.asGlobalSerie();
        return serie;
    }

    private Serie createSerie(MeasureIndicator.Formula formula, Query query, String label, String color, Map<TimeStamp, Double> values) {
        Ticket ticket = ticketOf(formula);
        Serie serie = new Serie(ticket, indicatorOf(formula), label, color);
        boolean fillWithZeros = ticket.style().line() == Ticket.Style.Line.ContinuousWithZeros;

        query.timeStamps().forEach(timeStamp -> {
            if (!fillWithZeros && !values.containsKey(timeStamp)) return;
            Double value = values.get(timeStamp);
            serie.register(timeStamp.instant(), value != null ? value : 0d);
        });

        return serie;
    }

    private Query queryOf(TimeSeriesQuery query) {
        return query.toRawQuery(tickets -> TimeSeriesHelper.this.formulas(measureIndicators(query.tickets(), query.scale())));
    }

}
