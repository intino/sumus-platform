package io.intino.sumus.helpers;

import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Record;
import io.intino.tara.magritte.Node;

public class RecordHelper extends Helper {
    private final SumusBox box;

    public RecordHelper(SumusBox box) {
        this.box = box;
    }

    public Record record(String id, String username) {
        Node node = box.graph().core$().clone().load(id, false);
        if (node == null) return null;
        Record record = node.as(Record.class);
        return checkPermissions(record, username) ? record : null;
    }

    private boolean checkPermissions(Record entity, String username) {
        return box.graph().checkPermissions(entity, username);
    }

}
