package io.intino.sumus.helpers;

import io.intino.sumus.Category;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Entity;
import io.intino.sumus.graph.Record;
import io.intino.sumus.queries.EntityQuery;
import io.intino.sumus.queries.Scope;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Layer;
import io.intino.tara.magritte.Node;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class EntityHelper extends Helper {
    private final SumusBox box;

    public EntityHelper(SumusBox box) {
        this.box = box;
    }

    public List<Entity> entities(EntityQuery query) {
        Concept entity = query.entity();
        Optional<Scope> scope = query.scope();

        return box.graph().core$().find(entity.layerClass()).stream()
                .filter(e -> checkPermissions(e.a$(Record.class), query.username()))
                .filter(e -> !scope.isPresent() || conforms(e, scope.get()))
                .filter(e -> hasCondition(e, query.condition())).map(e -> e.a$(Entity.class))
                .collect(toList());
    }

    public Entity entity(String id, String username) {
        Node node = box.graph().core$().clone().load(id, false);
        if (node == null) return null;
        Entity entity = node.as(Entity.class);
        return checkPermissions(entity, username) ? entity : null;
    }

    private boolean checkPermissions(Record entity, String username) {
        return box.graph().checkPermissions(entity, username);
    }

    private boolean hasCondition(Layer entity, String condition) {
        if (condition == null || condition.isEmpty()) return true;
        final List<String> keys = Arrays.asList(condition.toLowerCase().split(" "));
        List<String> words = words(entity);
        return keys.stream().filter(key -> words.stream().filter(word -> word.toLowerCase().contains(key)).count() > 0).count() == keys.size();
    }

    private List<String> words(Layer entity) {
        return words(entity, 0);
    }

    private List<String> words(Layer entity, int depth) {
        if (depth == 2) return Collections.emptyList();

        List<?> values = entity.core$().variables().values().stream().flatMap(Collection::stream).collect(toList());
        List<String> words = values.stream().filter(v -> v instanceof String).map(v -> (String) v).collect(toList());
        values.stream().filter(v -> v instanceof Layer).map(v -> (Layer)v).forEach(e -> words.addAll(words(e, depth+1)));

        return words;
    }

    private boolean conforms(Layer entity, Scope scope) {
        List<String> entities = entity.core$().variables().values().stream().flatMap(Collection::stream).filter(s -> s instanceof Entity).map(s -> ((Entity) s).core$().id()).collect(Collectors.toList());
        List<List<String>> records = records(scope);

        for (List<String> recordList : records) {
            if (recordList.contains(entity.core$().id())) continue;
            recordList.retainAll(entities);
            if (recordList.size() <= 0) return false;
        }

        return true;
    }

    private List<List<String>> records(Scope scope) {
        List<List<String>> result = new ArrayList<>();

        scope.categories().forEach(categoryList -> result.add(recordsOfCategories(categoryList)));
        scope.records().forEach(recordList -> result.add(records(recordList)));

        return result;
    }

    private List<String> recordsOfCategories(List<Category> categories) {
        return categories.stream().map(Category::recordIds).flatMap(Collection::stream).collect(toList());
    }

    private List<String> records(List<Record> recordList) {
        return recordList.stream().map(r -> r.core$().id()).collect(toList());
    }

}
