package io.intino.sumus.helpers;

import java.util.LinkedHashSet;
import java.util.Set;

public class MathHelper extends Helper {

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public static <T extends Object> Set<Set<T>> cartesianProduct(Set<T>... sets) {
        if (sets.length < 2)
            throw new IllegalArgumentException(
                    "Can't have a product of fewer than two sets (got " +
                            sets.length + ")");

        return _cartesianProduct(0, sets);
    }

    private static <T extends Object> Set<Set<T>> _cartesianProduct(int index, Set<T>... sets) {
        Set<Set<T>> ret = new LinkedHashSet<>();
        if (index == sets.length) {
            ret.add(new LinkedHashSet<>());
        } else {
            for (Object obj : sets[index]) {
                for (Set<T> set : _cartesianProduct(index+1, sets)) {
                    set.add((T) obj);
                    ret.add(set);
                }
            }
        }
        return ret;
    }

}
