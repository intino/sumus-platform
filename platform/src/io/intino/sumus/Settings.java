package io.intino.sumus;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class Settings {
    private final Map<String, String> args;

    private static final String Icon = "/images/favicon.png";

    public Settings(Map<String, String> args) {
        this.args = args;
    }

    public String title() {
        return args.containsKey("title") ? args.get("title") : "no title";
    }

    public String subtitle() {
        return args.containsKey("subtitle") ? args.get("subtitle") : "";
    }

    public URL logo() {
        try {
            if (!args.containsKey("logo"))
                return null;

            return new URL(args.get("logo"));
        } catch (MalformedURLException e) {
            return this.getClass().getResource(args.get("logo"));
        }
    }

    public URL icon() {
        try {
            if (!args.containsKey("icon"))
                return this.getClass().getResource(Icon);

            return new URL(args.get("icon"));
        } catch (MalformedURLException e) {
            return this.getClass().getResource(args.get("icon"));
        }
    }

}
