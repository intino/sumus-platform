package io.intino.sumus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Category {

    private Set<String> recordIds = new HashSet<>();
    private String label;

    private static final String AlphaAndDigits = "[^a-zA-Z0-9]+";

    public Category(String label) {
        this.label = label;
    }

    public String name() {
        return label.replaceAll(AlphaAndDigits,"");
    }

    public String label() {
        return label;
    }

    public void label(String label) {
        this.label = label;
    }

    public Set<String> recordIds() {
        return recordIds;
    }

    public void recordIds(List<String> recordIds) {
        this.recordIds = new HashSet<>(recordIds);
    }

    public boolean contains(String id){
        return recordIds.contains(id);
    }

    public boolean contains(List<String> ids) {
        for (String id : ids) if(recordIds.contains(id)) return true;
        return false;
    }
}
