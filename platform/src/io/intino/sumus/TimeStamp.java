package io.intino.sumus;

import io.intino.konos.alexandria.activity.model.TimeScale;

import java.time.Instant;

public class TimeStamp {

    private Instant instant;
    private TimeScale scale;

    public TimeStamp(Instant instant, TimeScale scale) {
        this.instant = instant;
        this.scale = scale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TimeStamp timeStamp = (TimeStamp) o;

        if (!instant.equals(timeStamp.instant)) return false;
        return scale == timeStamp.scale;
    }

    @Override
    public int hashCode() {
        int result = instant.hashCode();
        result = 31 * result + scale.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return instant.toString() + scale.toString();
    }

    public Instant instant() {
        return instant;
    }

    public TimeScale scale() {
        return scale;
    }

    public boolean contains(TimeStamp timeStamp) {
        if (this.scale.ordinal() > timeStamp.scale.ordinal()) return false;
        if (this.equals(timeStamp)) return true;
        return this.scale.normalise(timeStamp.instant).equals(instant);
    }
}
