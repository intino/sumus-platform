package io.intino.sumus.datawarehouse;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.datawarehouse.store.Digest;
import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Record;

import java.time.Instant;
import java.util.List;

import static io.intino.sumus.datawarehouse.store.Bucket.KEY_SEPARATOR;
import static io.intino.sumus.datawarehouse.store.Bucket.REF_SEPARATOR;
import static io.intino.sumus.graph.functions.ProfileLoader.Profile;
import static java.util.Collections.emptyList;

public class Data {
	public static Bucket getBucket(Cube self, NameSpace nameSpace, TimeScale scale, Instant instant) {
		Bucket bucket = Bucket.get(self, nameSpace, new TimeStamp(instant, scale));
		return bucket.exists() ? bucket : null;
	}

	public static Bucket newBucket(Cube self, NameSpace nameSpace, TimeScale scale, Instant instant) {
		return Bucket.get(self, nameSpace, new TimeStamp(instant, scale)).create();
	}

	public static Digest getDigest(Cube self, String digestRef) {
		String[] split = digestRef.split(REF_SEPARATOR);
		NameSpace nameSpace = self.core$().graph().load(split[0]).as(NameSpace.class);
		TimeStamp timeStamp = new TimeStamp(Instant.parse(split[1]), TimeScale.valueOf(split[2]));
		Bucket bucket = Bucket.get(self, nameSpace, timeStamp);
		return bucket.exists() ? bucket.digest(split[3].split("\\" + KEY_SEPARATOR)) : null;
	}

	public static Profile defaultProfile(io.intino.sumus.graph.ProfileManager self, String username) {
		return new Profile() {
			@Override
			public List<NameSpace> nameSpaces() {
				return self.graph().nameSpaceList();
			}

			@Override
			public List<String> keys() {
				return emptyList();
			}
		};
	}

	public static boolean checkRecord(io.intino.sumus.graph.ProfileManager self, Record record, String username) {
		return self.profile(username).check(record);
	}
}