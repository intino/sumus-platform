package io.intino.sumus.datawarehouse.store;

import java.time.Instant;
import java.util.List;

public interface Digest {

    String entityIds();

    List<String> entityIdsSplit();

    String ref();

    int intOf(String col);

    float floatOf(String col);

    void set(String col, int value);

    void set(String col, float value);

    void add(String col, int value);

    void add(String col, float value);

    Instant ts();

    void save();
}
