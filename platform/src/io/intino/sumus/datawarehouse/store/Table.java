package io.intino.sumus.datawarehouse.store;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class Table {
    private Map<String, Integer> index;
    private List<String> columns;
    private List<String> rows;
    private List<int[]> cells;

    public Table(InputStream inputStream) {
        this.fromBytes(inputStream);
        this.createIndex();
    }

    public Table(String[] columns) {
        this.columns = asList(columns);
        this.rows = new ArrayList<>();
        this.cells = new ArrayList<>();
        this.createIndex();
    }

    public int cols() {
        return columns.size();
    }

    public int rows() {
        return rows.size();
    }

    public String col(int index) {
        return columns.get(index);
    }

    public int col(String column) {
        int colIndex = indexOf(column);
        if (colIndex == -1) throw new RuntimeException("Table does not have a column named " + column + ". Available columns: " + String.join(", ", columns));
        return colIndex;
    }

    public String row(int index) {
        return rows.get(index);
    }

    public int row(String row) {
        return indexOf(row);
    }

    public int cell(String row, String col) {
        return cell(row(row), col(col));
    }

    public int cell(int row, int col) {
        return cells.get(row)[col];
    }

    public int set(String row, String col, int value) {
        return cells.get(row(row))[col(col)] = value;
    }

    public int set(int row, int col, int value) {
        return cells.get(row)[col] = value;
    }

    public void createRowsWith(String... keys) {
        for (String key : keys) {
            this.rows.add(key);
            this.index.put(key, this.cells.size());
            this.cells.add(new int[cols()]);
        }
    }

    private int indexOf(String key) {
        return index.getOrDefault(key, -1);
    }

    void createIndex() {
        index = new HashMap<>();
        for (int i = 0; i < columns.size(); i++) index.put(columns.get(i), i);
        for (int i = 0; i < rows.size(); i++) index.put(rows.get(i), i);
    }

    public byte[] toBytes() {
        try {
            return serialize(new ByteArrayOutputStream(1024 * 64)).toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void fromBytes(InputStream inputStream) {
        try {
            deserialize(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ByteArrayOutputStream serialize(ByteArrayOutputStream out) throws IOException {
        DataOutputStream stream = new DataOutputStream(out);
        writeString(stream, String.join("\n", this.columns));
        writeString(stream, String.join("\n", this.rows));
        writeLongs(stream);
        stream.flush();
        return out;
    }

    private void deserialize(InputStream inputStream) throws IOException {
        DataInputStream stream = new DataInputStream(inputStream);
        this.columns = new ArrayList<>(asList(readString(stream).split("\n")));
        this.rows = new ArrayList<>(asList(readString(stream).split("\n")));
        this.cells = readLongs(stream);
        stream.close();
    }

    private void writeString(DataOutputStream stream, String columns) throws IOException {
        stream.writeInt(columns.length());
        stream.write(columns.getBytes("UTF-8"));
    }

    private void writeLongs(DataOutputStream stream) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[cellsSizeInBytes()]);
        for (int[] row : cells) for (int cell : row) buffer.putInt(cell);
        stream.write(buffer.array());
    }

    private String readString(DataInputStream stream) throws IOException {
        int bytes = stream.readInt();
        byte[] result = new byte[bytes];
        stream.read(result);
        return new String(result, "UTF-8");
    }

    public List<int[]> readLongs(DataInputStream stream) throws IOException {
        List<int[]> result = new ArrayList<>();
        byte[] bytes = new byte[cellsSizeInBytes()];
        stream.read(bytes);
        ByteBuffer wrap = ByteBuffer.wrap(bytes);
        for (int i = 0; i < rows.size(); i++) {
            result.add(new int[cols()]);
            for (int j = 0; j < columns.size(); j++) result.get(i)[j] = wrap.getInt();
        }
        return result;
    }

    private int cellsSizeInBytes() {
        return rows.size() * columns.size() * Integer.BYTES;
    }

}
