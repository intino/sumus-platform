package io.intino.sumus.datawarehouse.store;

import com.tdunning.math.stats.AVLTreeDigest;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.SumusStore;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.NameSpace;
import io.intino.tara.magritte.Layer;
import io.intino.tara.magritte.RemounterGraph;

import java.io.*;
import java.nio.file.Files;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Float.floatToIntBits;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;

public class Bucket {

	private static final Map<String, Bucket> BUCKET_MAP = new HashMap<>();
	public static final String REF_SEPARATOR = "@";
	public static final String KEY_SEPARATOR = "|";
	private final Cube cube;
	private final NameSpace nameSpace;
	private TimeStamp timeStamp;
	private Table table;
	private Map<String, TTable> tTables = new HashMap<>();
	private List<Digest> digests;
	private Map<String, List<TDigest>> tDigests = new HashMap<>();

	public Bucket(Cube cube, NameSpace nameSpace, TimeStamp timeStamp) {
		this.cube = cube;
		this.nameSpace = nameSpace;
		this.timeStamp = timeStamp;
		if (exists()) table = new Table(streamOf(digestFile()));
	}

	public Cube cube() {
		return cube;
	}

	public NameSpace nameSpace() {
		return nameSpace;
	}

	public TimeStamp timeStamp() {
		return timeStamp;
	}

	public TimeScale scale() {
		return timeStamp.scale();
	}

	public Instant instant() {
		return timeStamp.instant();
	}

	public Bucket create() {
		if (table != null) return this;
		table = new Table(cube.propertyList().stream().map(Cube.Property::label).toArray(String[]::new));
		return this;
	}

	private File digestFile() {
		return digestFile(cube, nameSpace, timeStamp);
	}

	private static File digestFile(Cube cube, NameSpace nameSpace, TimeStamp timeStamp) {
		File storeFolder = ((SumusStore) cube.graph().core$().store()).storeFolder();
		return new File(storeFolder, PathBuilder.digestPath(nameSpace, cube, timeStamp.scale(), timeStamp.instant()) + ".digest");
	}

	private File tDigestFile(String property) {
		File storeFolder = ((SumusStore) cube.graph().core$().store()).storeFolder();
		return new File(storeFolder, PathBuilder.digestPath(nameSpace, cube, scale(), instant()) + "." + property + ".tdigest");
	}

	public boolean exists() {
		return digestFile().exists();
	}

	public boolean contains(String key) {
		return table.row(key) >= 0;
	}

	public List<TDigest> tDigests(String property) {
		if (tDigests.containsKey(property)) return tDigests.get(property);
		loadTTable(property);
		return tDigestsOf(property);
	}

	private void loadTTable(String property) {
		if (tTables.containsKey(property)) return;
		File tDigestFile = tDigestFile(property);
		if (tDigestFile.exists()) tTables.put(property, tTableOf(tDigestFile));
	}

	private List<TDigest> tDigestsOf(String property) {
		TTable tTable = tTables.get(property);
		if (tTable == null) return emptyList();
		List<TDigest> tDigests = new ArrayList<>();
		for (int i = 0; i < tTable.rows(); i++) tDigests.add(tDigest(tTable, property, i));
		return tDigests;
	}

	private TDigest tDigest(TTable tTable, String property, int index) {
		return new TDigest() {
			private List<String> ids = asList(tTable.row(index).split("\\|"));

			@Override
			public List<String> entityIds() {
				return ids;
			}

			@Override
			public void add(double value) {
				tTable.value(index).add(value);
			}

			@Override
			public double rateBelow(double value) {
				return tTable.value(index).cdf(value);
			}

			@Override
			public double quantile(double value) {
				return tTable.value(index).quantile(value);
			}

			@Override
			public AVLTreeDigest avlTreeDigest() {
				return tTable.value(index);
			}

			@Override
			public void save() {
				Bucket.this.save(tDigestFile(property), tTable.toBytes());
			}
		};
	}

	private TTable tTableOf(File tDigestFile) {
		try {
			return new TTable(new FileInputStream(tDigestFile));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public TDigest tDigest(String property, String... keys) {
		loadTTable(property);
		if (!tTables.containsKey(property)) tTables.put(property, new TTable());
		return tDigestOf(property, check(String.join(KEY_SEPARATOR, keys), tTables.get(property)));
	}

	private TDigest tDigestOf(String property, String key) {
		return tDigest(tTables.get(property), property, tTables.get(property).row(key));
	}

	public TDigest tDigest(String property, Layer... layers) {
		return tDigest(property, stream(layers).map(l -> l.core$().id()).toArray(String[]::new));
	}

	public List<Digest> digests() {
		if (digests != null) return digests;
		digests = new ArrayList<>();
		for (int i = 0; i < table.rows(); i++) digests.add(digestOf(i));
		return digests;
	}

	public Digest digest(String... keys) {
		return digestOf(check(String.join("|", keys)));
	}

	public Digest digest(Layer... layers) {
		return digest(stream(layers).map(l -> l.core$().id()).toArray(String[]::new));
	}

	private Digest digestOf(String row) {
		return digestOf(table.row(row));
	}

	private Digest digestOf(int row) {
		return new Digest() {

			private List<String> ids;

			@Override
			public String entityIds() {
				return table.row(row);
			}

			@Override
			public List<String> entityIdsSplit() {
				return ids == null ? ids = asList(entityIds().split("\\|")) : ids;
			}

			@Override
			public String ref() {
				return Bucket.this.ref() + REF_SEPARATOR + entityIds();
			}

			@Override
			public int intOf(String col) {
				return table.cell(row, table.col(col));
			}

			@Override
			public float floatOf(String col) {
				return Float.intBitsToFloat(intOf(col));
			}

			@Override
			public void set(String col, int value) {
				table.set(row, table.col(col), value);
			}

			@Override
			public void set(String col, float value) {
				table.set(row, table.col(col), floatToIntBits(value));
			}

			@Override
			public void add(String col, int value) {
				set(col, intOf(col) + value);
			}

			@Override
			public void add(String col, float value) {
				set(col, floatOf(col) + value);
			}

			@Override
			public Instant ts() {
				return timeStamp.instant();
			}

			@Override
			public void save() {
				Bucket.this.save();
			}
		};
	}

	private String ref() {
		return nameSpace().core$().id() + REF_SEPARATOR + instant() + REF_SEPARATOR + scale();
	}

	private String check(String key) {
		if (!contains(key)) table.createRowsWith(key);
		return key;
	}

	private String check(String key, TTable tTable) {
		if (tTable.row(key) == -1) tTable.createRowsWith(key);
		return key;
	}

	public void save() {
		if (nameSpace.graph().core$().store().allowWriting())
			save(digestFile(), table.toBytes());
	}

	public void save(File file, byte[] bytes) {
		try {
			file.getParentFile().mkdirs();
			Files.write(file.toPath(), bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private InputStream streamOf(File digestFile) {
		try {
			return new FileInputStream(digestFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Bucket get(Cube cube, NameSpace nameSpace, TimeStamp timeStamp) {
		if (!(nameSpace.graph().core$() instanceof RemounterGraph)) return new Bucket(cube, nameSpace, timeStamp);
		String path = digestFile(cube, nameSpace, timeStamp).getAbsolutePath();
		if (!BUCKET_MAP.containsKey(path))
			BUCKET_MAP.put(path, new Bucket(cube, nameSpace, timeStamp));
		return BUCKET_MAP.get(path);
	}

	public static void saveAll(){
		for (Bucket bucket : BUCKET_MAP.values()) bucket.save();
		BUCKET_MAP.clear();
	}
}
