package io.intino.sumus.datawarehouse.store;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.SumusStore;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.graph.*;
import io.intino.sumus.helpers.UtcDateTime;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Graph;
import io.intino.tara.magritte.Layer;

import java.time.Clock;
import java.time.Instant;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static io.intino.sumus.helpers.UtcDateTime.*;
import static java.util.stream.Collectors.toList;

public class PathBuilder {
	public static final String DigestsSuffix = "-digests";
	public static final String EventsSuffix = "-events";
	public static final String ReportsSuffix = "-reports";
	private static final String PathSeparator = "/";
	private static Map<Class<?>, String> classMap = new HashMap<>();

	public static String digestPath(NameSpace nameSpace, Cube cube, TimeStamp timeStamp) {
		ScaleBasedPathBuilder builder = ScaleBasedPathBuilder.fileNameBuilders.get(timeStamp.scale());
		return nameSpace.name$() + DigestsSuffix + PathSeparator + cube.name$() + PathSeparator + builder.parentDirectory(timeStamp.instant()) + formatFileName(builder.code(), builder.formattedInstantForFileName(timeStamp.instant()), builder.toString());
	}

	public static String digestPath(NameSpace nameSpace, Cube cube, TimeScale scale, Instant instant) {
		return digestPath(nameSpace, cube, new TimeStamp(instant, scale));
	}


	public static String temporalRecordPath(NameSpace nameSpace, Concept temporalRecord, TimeStamp timeStamp) {
		Graph graph = nameSpace.graph().core$();
		ScaleBasedPathBuilder builder = ScaleBasedPathBuilder.fileNameBuilders.get(timeStamp.scale());
		String suffix = temporalRecord.is(graph.concept(Event.class).id()) ? EventsSuffix : ReportsSuffix;
		return nameSpace.name$() + suffix + PathSeparator + nameOf(temporalRecord).toLowerCase() + PathSeparator + builder.parentDirectory(timeStamp.instant()) + formatFileName(builder.code(), builder.formattedInstantForFileName(timeStamp.instant()), builder.toString());
	}

	private static String nameOf(Concept temporalRecord) {
		Class<? extends Layer> layerClass = temporalRecord.layerClass();
		if (!classMap.containsKey(layerClass)) classMap.put(layerClass, layerClass.getSimpleName());
		return classMap.get(layerClass);
	}

	public static String temporalRecordPath(NameSpace nameSpace, Concept temporalRecord, TimeScale scale, Instant instant) {
		return temporalRecordPath(nameSpace, temporalRecord, new TimeStamp(instant, scale));
	}

	public static String temporalRecordPath(NameSpace nameSpace, Class<? extends Layer> recordClass, TimeStamp timeStamp) {
		return temporalRecordPath(nameSpace, nameSpace.core$().graph().concept(recordClass), timeStamp);
	}

	public static String temporalRecordPath(NameSpace nameSpace, Class<? extends Layer> recordClass, TimeScale scale, Instant instant) {
		return temporalRecordPath(nameSpace, recordClass, new TimeStamp(instant, scale));
	}

	public static TimeRange range(List<NameSpace> nameSpaces) {
		if (nameSpaces.size() <= 0) return null;

		SumusGraph graph = nameSpaces.get(0).graph();
		SumusStore store = (SumusStore) graph.core$().store();
		List<String> names = nameSpaces.stream().map(Layer::name$).collect(toList());
		Instant from = names.stream().map(store::minInstantForNameSpace).min(Instant::compareTo).orElse(Instant.MIN);
		Instant to = names.stream().map(store::maxInstantForNameSpace).max(Instant::compareTo).orElse(Instant.now(Clock.systemUTC()));
		to = TimeScale.Day.addTo(to, 1).minusMillis(1);

		List<Instant> graphEventCreatedList = graph.core$().find(Event.class).stream().map(TemporalRecord::created).collect(toList());
		Optional<Instant> minGraphInstant = graphEventCreatedList.stream().min(Instant::compareTo);
		Optional<Instant> maxGraphInstant = graphEventCreatedList.stream().max(Instant::compareTo);

		if (minGraphInstant.isPresent() && from.isAfter(minGraphInstant.get())) from = TimeScale.Day.addTo(minGraphInstant.get(), -1).plusMillis(1);
		if (maxGraphInstant.isPresent() && to.isBefore(maxGraphInstant.get())) to = TimeScale.Day.addTo(maxGraphInstant.get(), 1).minusMillis(1);

		return new TimeRange(from, to, TimeScale.FifteenMinutes);
	}

	private static String formatFileName(String code, String instant, String scale) {
		return code + "-" + instant + "-" + scale;
	}

	private static String formatWith2Places(int number) {
		return number < 10 ? "0" + number : number + "";
	}

	private static String formatWith3Places(int number) {
		return number > 99 ? number + "" : number > 9 ? "0" + number : "00" + number;
	}

	public enum ScaleBasedPathBuilder {

		Year {
			@Override
			public String code() {
				return "A";
			}

			@Override
			String parentDirectory(Instant instant) {
				return "";
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.Year.normalise(instant);
				return formatWith2Places(yearOf(normalisedInstant));
			}

		},

		QuarterOfYear {
			@Override
			public String code() {
				return "D";
			}

			@Override
			String parentDirectory(Instant instant) {
				return yearOf(instant) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.QuarterOfYear.normalise(instant);
				return formatWith2Places(monthNumberOf(normalisedInstant));
			}

		},

		Month {
			@Override
			public String code() {
				return "C";
			}

			@Override
			String parentDirectory(Instant instant) {
				return yearOf(instant) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.Month.normalise(instant);
				return formatWith2Places(monthNumberOf(normalisedInstant));
			}

		},

		Week {
			@Override
			public String code() {
				return "B";
			}

			@Override
			String parentDirectory(Instant instant) {
				Instant normalisedInstant = TimeScale.Week.normalise(instant);
				return UtcDateTime.of(normalisedInstant).year() + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				UtcDateTime normalisedInstant = UtcDateTime.of(TimeScale.Week.normalise(instant));
				return formatWith2Places(normalisedInstant.get(WeekFields.ISO.weekOfWeekBasedYear()));
			}

		},

		Day {
			@Override
			public String code() {
				return "A";
			}

			@Override
			String parentDirectory(Instant instant) {
				return yearOf(instant) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.Day.normalise(instant);
				return formatWith3Places(dayOfYearOf(normalisedInstant));
			}

		},

		SixHours {
			@Override
			public String code() {
				return "D";
			}

			@Override
			String parentDirectory(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(TimeScale.Second.normalise(instant));
				return dateTime.year() + PathSeparator + formatWith3Places(dateTime.dayOfYear()) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.SixHours.normalise(instant);
				return formatWith2Places(hourOf(normalisedInstant));
			}

		},

		Hour {
			@Override
			public String code() {
				return "C";
			}

			@Override
			String parentDirectory(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(instant);
				return dateTime.year() + PathSeparator + formatWith3Places(dateTime.dayOfYear()) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				Instant normalisedInstant = TimeScale.Hour.normalise(instant);
				return formatWith2Places(hourOf(normalisedInstant));
			}

		},

		FifteenMinutes {
			@Override
			public String code() {
				return "B";
			}

			@Override
			String parentDirectory(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(instant);
				return dateTime.year() + PathSeparator + formatWith3Places(dateTime.dayOfYear()) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(TimeScale.FifteenMinutes.normalise(instant));
				return formatWith2Places(dateTime.hour()) + formatWith2Places(dateTime.minute());
			}

		},

		Minute {
			@Override
			public String code() {
				return "A";
			}

			@Override
			String parentDirectory(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(instant);
				return dateTime.year() + PathSeparator + formatWith3Places(dateTime.dayOfYear()) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(TimeScale.Minute.normalise(instant));
				return formatWith2Places(dateTime.hour()) + formatWith2Places(dateTime.minute());
			}

		},

		Second {
			@Override
			public String code() {
				return "A";
			}

			@Override
			String parentDirectory(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(instant);
				return dateTime.year() + PathSeparator + formatWith3Places(dateTime.dayOfYear()) + PathSeparator + formatWith2Places(dateTime.hour()) + formatWith2Places(dateTime.minute()) + PathSeparator;
			}

			@Override
			String formattedInstantForFileName(Instant instant) {
				UtcDateTime dateTime = UtcDateTime.of(TimeScale.Second.normalise(instant));
				return formatWith2Places(dateTime.second());
			}

		};

		static final Map<TimeScale, ScaleBasedPathBuilder> fileNameBuilders = new HashMap<>();
		static final Map<String, ScaleBasedPathBuilder> codeBuilders = new HashMap<>();

		static {
			fileNameBuilders.put(TimeScale.Second, ScaleBasedPathBuilder.Second);
			fileNameBuilders.put(TimeScale.Minute, ScaleBasedPathBuilder.Minute);
			fileNameBuilders.put(TimeScale.FifteenMinutes, ScaleBasedPathBuilder.FifteenMinutes);
			fileNameBuilders.put(TimeScale.Hour, ScaleBasedPathBuilder.Hour);
			fileNameBuilders.put(TimeScale.SixHours, ScaleBasedPathBuilder.SixHours);
			fileNameBuilders.put(TimeScale.Day, ScaleBasedPathBuilder.Day);
			fileNameBuilders.put(TimeScale.Week, ScaleBasedPathBuilder.Week);
			fileNameBuilders.put(TimeScale.Month, ScaleBasedPathBuilder.Month);
			fileNameBuilders.put(TimeScale.QuarterOfYear, ScaleBasedPathBuilder.QuarterOfYear);
			fileNameBuilders.put(TimeScale.Year, ScaleBasedPathBuilder.Year);
		}

		static {
			codeBuilders.put(ScaleBasedPathBuilder.Second.code(), ScaleBasedPathBuilder.Second);
			codeBuilders.put(ScaleBasedPathBuilder.Minute.code(), ScaleBasedPathBuilder.Minute);
			codeBuilders.put(ScaleBasedPathBuilder.FifteenMinutes.code(), ScaleBasedPathBuilder.FifteenMinutes);
			codeBuilders.put(ScaleBasedPathBuilder.Hour.code(), ScaleBasedPathBuilder.Hour);
			codeBuilders.put(ScaleBasedPathBuilder.SixHours.code(), ScaleBasedPathBuilder.SixHours);
			codeBuilders.put(ScaleBasedPathBuilder.Day.code(), ScaleBasedPathBuilder.Day);
			codeBuilders.put(ScaleBasedPathBuilder.Week.code(), ScaleBasedPathBuilder.Week);
			codeBuilders.put(ScaleBasedPathBuilder.Month.code(), ScaleBasedPathBuilder.Month);
			codeBuilders.put(ScaleBasedPathBuilder.QuarterOfYear.code(), ScaleBasedPathBuilder.QuarterOfYear);
			codeBuilders.put(ScaleBasedPathBuilder.Year.code(), ScaleBasedPathBuilder.Year);
		}

		abstract String code();

		abstract String parentDirectory(Instant instant);

		abstract String formattedInstantForFileName(Instant instant);


	}
}
