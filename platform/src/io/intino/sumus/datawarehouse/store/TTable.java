package io.intino.sumus.datawarehouse.store;

import com.tdunning.math.stats.AVLTreeDigest;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class TTable {
    private Map<String, Integer> index;
    private List<String> rows = new ArrayList<>();
    private List<AVLTreeDigest> digests = new ArrayList<>();

    public TTable(InputStream inputStream) {
        this.fromBytes(inputStream);
        this.createIndex();
    }

    public TTable() {
        this.createIndex();
    }

    public int rows() {
        return rows.size();
    }

    public String row(int index) {
        return rows.get(index);
    }

    public int row(String row) {
        return indexOf(row);
    }

    public AVLTreeDigest value(String row) {
        return value(row(row));
    }

    public AVLTreeDigest value(int row) {
        return digests.get(row);
    }

    public void createRowsWith(String... keys) {
        int i = rows();
        for (String key : keys) {
            this.rows.add(key);
            index.put(key, i++);
            this.digests.add(new AVLTreeDigest(100));
        }
    }

    private int indexOf(String key) {
        return index.getOrDefault(key, -1);
    }

    void createIndex() {
        index = new HashMap<>();
        for (int i = 0; i < rows.size(); i++) index.put(rows.get(i), i);
    }

    public byte[] toBytes() {
        try {
            return serialize(new ByteArrayOutputStream(1024 * 64)).toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void fromBytes(InputStream inputStream) {
        try {
            deserialize(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ByteArrayOutputStream serialize(ByteArrayOutputStream out) throws IOException {
        DataOutputStream stream = new DataOutputStream(out);
        writeString(stream, String.join("\n", this.rows));
        writeDigests(stream);
        stream.flush();
        return out;
    }

    private void deserialize(InputStream inputStream) throws IOException {
        DataInputStream stream = new DataInputStream(inputStream);
        this.rows = new ArrayList<>(asList(readString(stream).split("\n")));
        this.digests = readDigests(stream);
        stream.close();
    }

    private void writeString(DataOutputStream stream, String columns) throws IOException {
        stream.writeInt(columns.length());
        stream.write(columns.getBytes("UTF-8"));
    }

    private void writeDigests(DataOutputStream stream) throws IOException {
        for (AVLTreeDigest digest : digests) {
            byte[] bytes = bytesOf(digest);
            stream.writeInt(bytes.length);
            stream.write(bytes);
        }
    }

    private String readString(DataInputStream stream) throws IOException {
        int bytes = stream.readInt();
        byte[] result = new byte[bytes];
        stream.read(result);
        return new String(result, "UTF-8");
    }

    public List<AVLTreeDigest> readDigests(DataInputStream stream) throws IOException {
        List<AVLTreeDigest> result = new ArrayList<>();
        for (int i = 0; i < rows.size(); i++) {
            byte[] array = new byte[stream.readInt()];
            stream.read(array);
            result.add(digestOf(array));
        }
        return result;
    }

    private byte[] bytesOf(AVLTreeDigest digest) {
        ByteBuffer buffer = ByteBuffer.wrap(new byte[digest.smallByteSize()]);
        digest.asSmallBytes(buffer);
        return buffer.array();
    }

    private AVLTreeDigest digestOf(byte[] array) {
        return AVLTreeDigest.fromBytes(ByteBuffer.wrap(array));
    }

}
