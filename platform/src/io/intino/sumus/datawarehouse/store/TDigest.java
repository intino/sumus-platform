package io.intino.sumus.datawarehouse.store;

import com.tdunning.math.stats.AVLTreeDigest;

import java.util.List;

public interface TDigest {

    List<String> entityIds();

    void add(double value);

    double rateBelow(double value);

    double quantile(double value);

    AVLTreeDigest avlTreeDigest();

    void save();
}
