package io.intino.sumus.exceptions;

import java.util.Collections;
import java.util.Map;

public class CouldNotConnectWithFederation extends Exception implements io.intino.konos.alexandria.Error {

    @Override
    public String code() {
        return "err:cncwf";
    }

    @Override
    public String label() {
        return "could not connect with federation";
    }

    @Override
    public Map<String, String> parameters() {
        return Collections.emptyMap();
    }
}
