package io.intino.sumus.exceptions;

import java.util.Collections;
import java.util.Map;

public class DifferentScalesVersusZoomsInOlap extends Exception implements io.intino.konos.alexandria.Error {
    @Override
    public String code() {
        return "err:dsvzo";
    }

    @Override
    public String label() {
        return "Different scales versus available zooms in olap";
    }

    @Override
    public Map<String, String> parameters() {
        return Collections.emptyMap();
    }
}
