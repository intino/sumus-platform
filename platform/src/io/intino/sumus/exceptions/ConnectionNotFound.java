package io.intino.sumus.exceptions;

import java.util.Collections;
import java.util.Map;

public class ConnectionNotFound extends Exception implements io.intino.konos.alexandria.Error {

    @Override
    public String code() {
        return "err:cnf";
    }

    @Override
    public String label() {
        return "connection not found";
    }

    @Override
    public Map<String, String> parameters() {
        return Collections.emptyMap();
    }
}
