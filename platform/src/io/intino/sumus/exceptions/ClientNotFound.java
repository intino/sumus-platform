package io.intino.sumus.exceptions;

import java.util.Collections;
import java.util.Map;

public class ClientNotFound extends Exception implements io.intino.konos.alexandria.Error {
    private final String clientId;

    public ClientNotFound(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String code() {
        return "err:cnf";
    }

    @Override
    public String label() {
        return "client not found";
    }

    @Override
    public Map<String, String> parameters() {
        return Collections.singletonMap("client", clientId);
    }
}
