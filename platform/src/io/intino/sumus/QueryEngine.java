package io.intino.sumus;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.viewmodels.CrossTable;
import io.intino.sumus.analytics.viewmodels.Histogram;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.*;
import io.intino.sumus.helpers.*;
import io.intino.sumus.queries.*;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public class QueryEngine {
    private final SumusBox box;
    private Scope scope = null;

    public QueryEngine(SumusBox box) {
        this.box = box;
    }

    public List<Serie> timeSeries(TimeSeriesQuery query) {
        return new TimeSeriesHelper().execute(query);
    }

    public List<DocumentRow> document(TimeSeriesQuery query) {
        return new ExportDocumentHelper().execute(query);
    }

    public CrossTable crossTable(CrossTableQuery query) {
        return new CrossTableHelper().execute(query);
    }

    public Histogram histogram(HistogramQuery query) {
        return new HistogramHelper().execute(query);
    }

    public List<Entity> entities(EntityQuery query) {
        return new EntityHelper(box).entities(query);
    }

    public List<String> entities(TimeSeriesQuery query) {
        return new TimeSeriesHelper().entities(query);
    }

    public Entity entity(String id, String username) {
        return new EntityHelper(box).entity(id, username);
    }

    public TimeRange temporalRecordRange(List<NameSpace> nameSpaces) {
        return new TemporalRecordHelper(box).recordRange(nameSpaces);
    }

    public List<TemporalRecord> temporalRecords(TemporalRecordQuery query) {
        return new TemporalRecordHelper(box).records(query);
    }

    public TemporalRecord temporalRecord(String id, String username) {
        return new TemporalRecordHelper(box).record(id, username);
    }

    public Record record(String id, String username) {
        return new RecordHelper(box).record(id, username);
    }

    public static class DocumentRow {
        public final Instant internalInstant;
        public final String instant;
        public final String indicator;
        public final Map<Categorization, Category> drill;
        public final Map<Categorization, List<Category>> filter;
        public final Double value;
        public final String unitLabel;

        public DocumentRow(Instant instant, String indicator, Map<Categorization, Category> drill, Map<Categorization, List<Category>> filter, Double value, String unitLabel, TimeScale scale) {
            this.internalInstant = instant;
            this.instant = scale.toString(instant);
            this.indicator = indicator;
            this.drill = drill;
            this.filter = filter;
            this.value = value;
            this.unitLabel = unitLabel;
        }
    }

}
