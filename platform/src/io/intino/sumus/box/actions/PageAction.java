package io.intino.sumus.box.actions;

import io.intino.sumus.box.SumusBox;

import java.net.URL;

public abstract class PageAction extends io.intino.konos.alexandria.activity.spark.actions.AlexandriaPageAction {
	public SumusBox box;

	public PageAction() {
		super("platform");
	}

	@Override
	protected String title() {
		return "Sumus";
	}

	@Override
	protected URL favicon() {
		return PageAction.class.getResource("/images/favicon.png");
	}

}