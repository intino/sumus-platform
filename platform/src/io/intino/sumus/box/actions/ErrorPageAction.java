package io.intino.sumus.box.actions;

import io.intino.konos.alexandria.activity.displays.Soul;
import io.intino.sumus.box.displays.SumusError;
import io.intino.sumus.exceptions.CouldNotConnectWithFederation;

public class ErrorPageAction extends PageAction {

	public String execute() {
		String result = template("errorPage");
		result = result.replace("$errorCode", new CouldNotConnectWithFederation().code());
		return result;
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
		return new Soul(session) {
			@Override
			public void personify() {
				SumusError display = new SumusError(box);
				register(display);
				display.personify();
			}
		};
	}

}