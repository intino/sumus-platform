package io.intino.sumus.box.actions;

import io.intino.konos.alexandria.activity.displays.Soul;
import io.intino.sumus.box.displays.SumusDesktop;

public class AnalyticsPageAction extends PageAction {

	public String execute() {
		return template("analyticsPage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
		return new Soul(session) {
			@Override
			public void personify() {
				SumusDesktop display = new SumusDesktop(box);
				register(display);
				display.personify();
			}
		};
	}

}