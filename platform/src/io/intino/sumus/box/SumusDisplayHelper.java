package io.intino.sumus.box;

import io.intino.konos.alexandria.Box;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Item;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Group;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;
import io.intino.sumus.Category;
import io.intino.sumus.QueryEngine;
import io.intino.sumus.graph.*;
import io.intino.sumus.helpers.ClusterHelper;
import io.intino.sumus.helpers.NameSpaceHandler;
import io.intino.sumus.queries.EntityQuery;
import io.intino.sumus.queries.Filter;
import io.intino.sumus.queries.TemporalRecordQuery;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Layer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class SumusDisplayHelper {
	private static Map<String, QueryEngine> queryEngineMap = new HashMap<>();
	private static NameSpaceHandler nameSpaceHandler;

	public static QueryEngine queryEngine(SumusBox box, String id) {
		if (!queryEngineMap.containsKey(id))
			queryEngineMap.put(id, new QueryEngine(box));
		return queryEngineMap.get(id);
	}

	public static SumusBox sumusBox(Box box) {
		return (SumusBox)box.owner();
	}

	public static NameSpaceHandler nameSpaceHandler(SumusBox box) {
		if (nameSpaceHandler == null)
			nameSpaceHandler = new NameSpaceHandler(box.graph());
		return nameSpaceHandler;
	}

	public static io.intino.sumus.queries.Scope scopeOf(Scope scope) {
		if (scope == null) return null;
		return new io.intino.sumus.queries.Scope().categories(categories(scope.groups())).records(records(scope.objects()));
	}

	public static List<NameSpace> userNameSpaces(SumusBox box, String username) {
		return box.graph().core$().as(SumusGraph.class).userNameSpaces(username);
	}

	public static List<String> userKeys(SumusBox box, String username) {
		return box.graph().core$().as(SumusGraph.class).userKeys(username);
	}

	public static Catalog.ArrangementFilterer createArrangementFilterer(ActivitySession session) {
		return new Catalog.ArrangementFilterer() {
			Filter filter = null;

			@Override
			public ActivitySession session() {
				return session;
			}

			@Override
			public void add(String grouping, Group... groups) {
				filter = isEmpty() ? new Filter() : filter;
				List<Category> categories = categories(groups);
				filter.add(categories.toArray(new Category[categories.size()]));
			}

			@Override
			public boolean contains(Item item) {
				filter = isEmpty() ? new Filter() : filter;
				if (item == null) return false;
				return filter.contains(item.id());
			}

			@Override
			public void clear() {
				filter = null;
			}

			@Override
			public boolean isEmpty() {
				return filter == null;
			}
		};
	}

	private static Map<String, List<Category>> categories(Map<String, List<Group>> groups) {
		return groups.entrySet().stream().collect(toMap(Map.Entry::getKey, e -> categories(e.getValue())));
	}

	public static List<Category> categories(Group... groups) {
		return categories(Stream.of(groups));
	}

	public static List<Category> categories(List<Group> groups) {
		return categories(groups.stream());
	}

	public static Category category(Group group) {
		Category category = new Category(group.label());
		category.recordIds((group instanceof SumusGroup) ? ((SumusGroup)group).recordIds(): emptyList());
		return category;
	}

	public static List<Group> groups(List<Category> categoryList, List<? extends Layer> objects) {
		return categoryList.stream().map(c -> group(c, layersToRecords(objects))).collect(toList());
	}

	public static Group group(Category category, List<Record> records) {
		List<String> recordIds = records.stream().map(r -> r.core$().id()).filter(category::contains).collect(toList());
		return new SumusGroup().recordIds(recordIds).label(category.label()).countObjects(recordIds.size());
	}

	public static void createGroup(SumusBox box, Catalog catalog, String grouping, Group group, String username) {
		ClusterHelper.registerClusterGroup(box, catalog, grouping, group.label(), emptyList(), username);
	}

	private static List<Category> categories(Stream<Group> groups) {
		return groups.map(SumusDisplayHelper::category).collect(toList());
	}

	private static Map<String, List<Record>> records(Map<String, List<Object>> objects) {
		return objects.entrySet().stream().collect(toMap(Map.Entry::getKey, e -> objectsToRecords(e.getValue())));
	}

	private static List<String> objectsToRecordsIds(List<Object> objects) {
		return objects.stream().map(o -> ((Layer)o).core$().id()).collect(toList());
	}

	private static List<Record> objectsToRecords(List<Object> objects) {
		return objects.stream().map(o -> ((Layer)o).core$().as(Record.class)).collect(toList());
	}

	private static List<Entity> objectsToEntities(List<Object> objects) {
		return objects.stream().map(o -> ((Layer)o).core$().as(Entity.class)).collect(toList());
	}

	private static List<Record> layersToRecords(List<? extends Layer> objects) {
		return objects.stream().map(o -> o.a$(Record.class)).collect(toList());
	}

	public static <T extends Layer> List<T> entities(SumusBox box, Class<T> entityClass, Scope scope, String condition, String username) {
		Concept concept = box.graph().core$().concept(entityClass);
		EntityQuery.Builder builder = new EntityQuery.Builder();
		builder.scope(scopeOf(scope));
		builder.condition(condition);
		return queryEngine(box, username).entities(builder.build(concept, username)).stream().map(e -> e.a$(entityClass)).collect(toList());
	}

	public static <T extends Layer> T entity(SumusBox box, Class<T> entityClass, String id, String username) {
		Entity entity = queryEngine(box, username).entity(id, username);
		return entity != null ? entity.a$(entityClass) : null;
	}

	public static <T extends Layer> List<T> temporalRecords(SumusBox box, Class<T> temporalRecordClass, Scope scope, String condition, TimeRange timeRange, NameSpace nameSpace, String username) {
		Concept concept = box.graph().core$().concept(temporalRecordClass);
		TemporalRecordQuery.Builder builder = new TemporalRecordQuery.Builder();
		builder.scope(scopeOf(scope));
		builder.condition(condition);
		builder.timeRange(timeRange);
		builder.nameSpace(nameSpace);
		return queryEngine(sumusBox(box), username).temporalRecords(builder.build(concept, username)).stream().map(r -> r.a$(temporalRecordClass)).collect(toList());
	}

	public static <T extends Layer> T temporalRecord(SumusBox box, Class<T> temporalRecordClass, String id, String username) {
		TemporalRecord temporalRecord = queryEngine(box, username).temporalRecord(id, username);
		return temporalRecord != null ? temporalRecord.a$(temporalRecordClass) : null;
	}

	public static class SumusGroup extends Group {
		private List<String> recordIds;

		public List<String> recordIds() {
			return recordIds;
		}

		public SumusGroup recordIds(List<String> recordIds) {
			this.recordIds = recordIds;
			return this;
		}
	}
}
