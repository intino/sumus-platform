package io.intino.sumus.box;

import amidas.proxy.konos.core.AmidasKonosAccessor;
import io.intino.konos.alexandria.activity.displays.Soul;
import io.intino.konos.alexandria.activity.services.auth.Space;
import io.intino.konos.alexandria.activity.displays.AlexandriaAbstractCatalog;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Item;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Grouping;
import io.intino.sumus.Settings;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.helpers.TranslatorHelper;
import io.intino.tara.magritte.Graph;

import java.util.Collection;

public class SumusBox extends AbstractBox {
	private SumusGraph sumusGraph;
	private Settings settings;
	private TranslatorHelper translatorHelper;

	public SumusBox(String[] args) {
		super(args);
	}

	public SumusBox(SumusConfiguration configuration) {
		super(configuration);
		this.settings = new Settings(configuration.args());
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		if (o instanceof Graph) this.sumusGraph = ((Graph) o).as(SumusGraph.class);
		if (o instanceof TranslatorHelper) this.translatorHelper = (TranslatorHelper) o;
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		this.translatorHelper = new TranslatorHelper(sumusGraph);
		super.open();
		graph().init();
		return this;
	}

	public SumusGraph graph() {
		return sumusGraph;
	}

	public Settings actionsConfiguration() {
		return settings;
	}

	public void close() {
		super.close();
	}

	public TranslatorHelper translatorHelper() {
		return translatorHelper;
	}

	public void refreshDisplaysOf(Catalog catalog, Grouping grouping) {
		activitySouls.values().stream().map(Soul::getAll).flatMap(Collection::stream)
				.filter(d -> d instanceof AlexandriaAbstractCatalog && ((AlexandriaAbstractCatalog) d)
						.isFor(catalog)).forEach(d -> ((AlexandriaAbstractCatalog) d).refresh(grouping));
	}

	public void refreshDisplaysOf(Catalog catalog, boolean categorizationsHaveChanges, Item... items) {
		activitySouls.values().stream().map(Soul::getAll).flatMap(Collection::stream)
				.filter(d -> d instanceof AlexandriaAbstractCatalog && ((AlexandriaAbstractCatalog) d)
						.isFor(catalog)).forEach(d -> {
			AlexandriaAbstractCatalog display = (AlexandriaAbstractCatalog) d;
			display.dirty(true);
			if (categorizationsHaveChanges || items.length == 0)
				display.refresh();
			else
				display.refresh(items);
		});
	}

	static io.intino.konos.alexandria.activity.services.AuthService authService(java.net.URL authServiceUrl) {
		return authServiceUrl != null ? new AmidasKonosAccessor(new Space(authServiceUrl), authServiceUrl) : null;
	}

}