package io.intino.sumus.box;

import io.intino.konos.alexandria.Box;
import io.intino.sumus.Settings;
import io.intino.sumus.SumusStore;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.helpers.TranslatorHelper;
import io.intino.tara.magritte.Graph;

import java.util.Map;

public class Main {
	public static void main(String[] args) {
		SumusConfiguration configuration = new SumusConfiguration(args);
		Settings settings = new Settings(configuration.args());
		SumusGraph.Settings = settings;
		Graph graph = new Graph(new SumusStore(configuration.store())).loadStashes(paths(configuration));
		Box box = new SumusBox(configuration).put(graph).put(settings).put(new TranslatorHelper(graph.as(SumusGraph.class))).open();
		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}

	private static String[] paths(SumusConfiguration configuration) {
		Map<String, String> map = configuration.args();
		return map.containsKey("graph_paths") ? map.get("graph_paths").split(" ") : new String[]{"Sumus"};
	}
}