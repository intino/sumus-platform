package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.konos.alexandria.activity.services.push.ActivityClient;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;
import io.intino.konos.alexandria.activity.services.push.User;
import io.intino.sumus.QueryEngine;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.ProfileManager;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.graph.rules.SumusTimeScale;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class SumusDisplay<N extends AlexandriaDisplayNotifier> extends AlexandriaDisplay<N> {
    private QueryEngine queryEngine;
    final SumusBox box;

    public SumusDisplay(SumusBox box) {
        this.box = box;
    }

    public Optional<User> user() {
        return Optional.ofNullable(session().user());
    }

    public String username() {
        return user().isPresent() ? user().get().username() : session().id();
    }

    SumusGraph platform() {
        return box.graph();
    }

    List<String> keys() {
        ProfileManager profileManager = platform().profileManager();

        if (profileManager == null)
            return emptyList();

        return profileManager.profile(username()).keys();
    }

    String currentLanguage() {
        ActivitySession session = session();
        if (session == null) return "en";
        ActivityClient client = session.client();
        return client != null ? client.language() : session().browser().languageFromMetadata();
    }

    URL baseAssetUrl() {
        try {
            return new URL(session().browser().baseAssetUrl());
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public QueryEngine queryEngine() {
        if (queryEngine == null)
            queryEngine = new QueryEngine(box);
        return queryEngine;
    }

    public void queryEngine(QueryEngine engine) {
        this.queryEngine = engine;
    }

    int timezoneOffset() {
        return session().browser().timezoneOffset();
    }

    void timezoneOffset(int value) {
        session().browser().timezoneOffset(value/60);
    }

    TimeScale scaleOf(SumusTimeScale scale) {
        return TimeScale.values()[scale.ordinal()];
    }

    List<TimeScale> scalesOf(List<SumusTimeScale> scales) {
        return scales.stream().map(this::scaleOf).collect(toList());
    }

}
