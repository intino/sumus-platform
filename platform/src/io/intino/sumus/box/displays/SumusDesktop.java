package io.intino.sumus.box.displays;

import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.notifiers.SumusDesktopNotifier;

public class SumusDesktop extends SumusDisplay<SumusDesktopNotifier> {

	public SumusDesktop(SumusBox box) {
		super(box);
	}

}