package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.Scaler;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.exporters.Document;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.notifiers.SumusTimeCrossTableNotifier;
import io.intino.sumus.box.schemas.*;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.MathHelper;
import io.intino.sumus.queries.CrossTableQuery;
import io.intino.tara.magritte.Layer;

import java.time.Instant;
import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class SumusTimeCrossTable extends SumusChart<SumusTimeCrossTableNotifier, AlexandriaTimeNavigator> {
	public static final int TemporalCategorizationOffset = 2;
	private Map<String, List<String>> categoriesSelection = new HashMap<>();

	public static final String ColumnTag = "column";
	public static final String RowTag = "row";

	public SumusTimeCrossTable(SumusBox box) {
		super(box, new AlexandriaTimeNavigator(box));
	}

	@Override
	public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
		super.timeScaleHandler(timeScaleHandler);
		timeScaleHandler.onScaleChange(this::doQuery);
	}

	@Override
	public boolean isValidSpecification(ChartSpec spec) {
		return spec.ticketList().size() == 1 && spec.categorizationList(ColumnTag).size() > 0 && spec.categorizationList(RowTag).size() > 0;
	}

	@Override
	public Document export(Instant from, Instant to) {
		return null;
	}

	@Override
	public boolean isShowingEvents() {
		return false;
	}

	@Override
	public boolean allowTemporalFiltering() {
		return true;
	}

	@Override
	public Categorization byCategorization() {
		return null;
	}

	@Override
	public void refresh() {
		super.refresh();
		this.doQuery(timeScaleHandler().range());
	}

	@Override
	public void addNavigatorListeners(AlexandriaTimeNavigator display) {
		display.onMove(this::doQuery);
	}

	@Override
	public boolean allowMultipleTickets() {
		return false;
	}

	@Override
	public boolean allowDrill() {
		return false;
	}

	@Override
	protected void update() {
		notifier.refreshTicketCount(specification().ticketList().size());
		filter(specification().filterList());
	}

	private void doQuery() {
		notifyLoading(true);
		notifier.loading(true);
		notifier.refreshData(isValidQuery() ? data() : emptyData());
		notifyLoading(false);
		notifier.loading(false);
	}

	private void doQuery(Instant instant) {
		doQuery();
	}

	private void doQuery(TimeRange range) {
		doQuery();
	}

	private CrossTableData data() {
		List<Column> columns = columns();
		List<Row> rows = rows();

		io.intino.sumus.analytics.viewmodels.CrossTable crossTable = isValidQuery() ? queryEngine().crossTable(queryOf(range())) : null;
		Scaler scaler = crossTable != null ? crossTable.scaler() : null;
		ChartSpec.HeatMap heatMap = specification().heatMap();

		return new CrossTableData().decimalPlaces(decimalPlaces())
				.columnList(columns)
				.rowList(rows)
				.entryList(entries(crossTable, scaler, columns, rows))
				.unitLabel(scaler != null ? scaler.unitLabel() : "")
				.heatMap(heatMap != null ? heatMapOf(heatMap) : null);
	}

	private TimeRange range() {
		TimeScale scale = navigatorDisplay().scale();
		Instant date = navigatorDisplay().date();
		return new TimeRange(date, scale.addTo(date, 1), scale);
	}

	private CrossTableData emptyData() {
		return new CrossTableData().entryList(new ArrayList<>()).columnList(columns()).rowList(rows());
	}

	private DecimalPlaces decimalPlaces() {
		DecimalPlaces result = new DecimalPlaces();
		Ticket ticket = ticket();
		if (ticket == null) return result;
		Ticket.DecimalPlaces decimalPlaces = ticket.decimalPlaces();
		return result.absolute(decimalPlaces.absolute()).percentage(decimalPlaces.percentage());
	}

	private HeatMap heatMapOf(ChartSpec.HeatMap heatMap) {
		return new HeatMap().min(heatMap.minColor()).max(heatMap.maxColor());
	}

	private List<Column> columns() {
		List<String> cols = specification().categorizationList(ColumnTag).stream().map(Layer::name$).collect(toList());
		return filterValid(cols).stream().map(this::columnOf).collect(toList());
	}

	private Column columnOf(Categorization categorization) {
		List<String> selection = categoriesSelection.containsKey(categorization.label()) ? categoriesSelection.get(categorization.label()) : null;
		List<String> collect = categorization.calculate(keys()).stream().filter(c -> selection == null || selection.contains(c.name()) || selection.contains(c.label())).map(io.intino.sumus.Category::label).collect(toList());
		if (!(categorization instanceof TemporalCategorization)) collect.sort(Comparator.naturalOrder());
		return new Column().label(categorization.label()).values(collect);
	}

	private List<Row> rows() {
		List<String> rows = specification().categorizationList(RowTag).stream().map(Layer::name$).collect(toList());
		return filterValid(rows).stream().map(this::rowOf).collect(toList());
	}

	private Row rowOf(Categorization categorization) {
		List<String> selection = categoriesSelection.containsKey(categorization.label()) ? categoriesSelection.get(categorization.label()) : null;
		List<String> collect = categorization.calculate(keys()).stream().filter(c -> selection == null || selection.contains(c.name()) || selection.contains(c.label())).map(io.intino.sumus.Category::label).collect(toList());
		if (!(categorization instanceof TemporalCategorization)) collect.sort(Comparator.naturalOrder());
		return new Row().label(categorization.label()).values(collect);
	}

	private boolean isValidCategorization(Categorization categorization) {
		if (!(categorization instanceof TemporalCategorization)) return true;

		TimeScale scale = timeScaleHandler().range().scale();
		return isAvailableForScale(((TemporalCategorization)categorization), scale);
	}

	private List<io.intino.sumus.Category> categoriesOf(Categorization categorization, List<String> categories) {
		if (categories == null || categories.size() == 0) return categorization.calculate(keys());
		return categorization.calculate(keys()).stream().filter(c -> categories.contains(c.label()) || categories.contains(c.name())).collect(toList());
	}

	private List<Entry> entries(io.intino.sumus.analytics.viewmodels.CrossTable crossTable, Scaler scaler, List<Column> columns, List<Row> rows) {
		if (ticket() == null || crossTable == null)
			return emptyList();

		List<Entry> result = new ArrayList<>();
		Set<Set<Object>> columnsCartesianProduct = columnsCartesianProduct(columns);
		Set<Set<Object>> rowsCartesianProduct = rowsCartesianProduct(rows);

		rowsCartesianProduct.forEach(rowSet -> {
			List<String> rowCategories = rowSet.stream().map(Object::toString).collect(toList());
			Entry entry = new Entry();

			columnsCartesianProduct.forEach(columnSet -> {
				List<String> columnCategories = columnSet.stream().map(Object::toString).collect(toList());
				List<String> categories = new ArrayList<>(columnCategories);
				categories.addAll(rowCategories);
				entry.values().add(scaler.scale(crossTable.value(categories)));
			});

			result.add(entry);
		});

		return result;
	}

	private CrossTableQuery queryOf(TimeRange timeRange) {
		CrossTableQuery.Builder builder = new CrossTableQuery.Builder().filter(specification().filterList());
		columns().forEach(column -> {
			io.intino.sumus.graph.Categorization categorization = categorizationOf(column.label());
			builder.addColumn(categorization, categoriesOf(categorization, column.values()));
		});
		rows().forEach(row -> {
			Categorization categorization = categorizationOf(row.label());
			builder.addRow(categorization, categoriesOf(categorization, row.values()));
		});
		builder.scope(specification().scope());
		return builder.build(nameSpace(), ticket(), timeRange);
	}

	private List<Categorization> filterValid(List<String> categorizations) {
		return categorizations.stream().map(this::categorizationOf).filter(this::isValidCategorization).collect(toList());
	}

	private Set<Set<Object>> columnsCartesianProduct(List<Column> columns) {
		List<List<String>> cartesianColumns = new ArrayList<>();
		for (Column column : columns) {
			Categorization categorization = categorizationOf(column.label());
			List<String> collect = categoriesOf(categorization, column.values()).stream().map(io.intino.sumus.Category::label).collect(toList());
			if (!(categorization instanceof TemporalCategorization)) collect.sort(Comparator.naturalOrder());
			cartesianColumns.add(collect);
		}
		return cartesianProduct(cartesianColumns);
	}

	private Set<Set<Object>> rowsCartesianProduct(List<Row> rows) {
		List<List<String>> cartesianRows = new ArrayList<>();
		for (Row row : rows) {
			Categorization categorization = categorizationOf(row.label());
			List<String> collect = categoriesOf(categorization, row.values()).stream().map(io.intino.sumus.Category::label).collect(toList());
			if (!(categorization instanceof TemporalCategorization)) collect.sort(Comparator.naturalOrder());
			cartesianRows.add(collect);
		}
		return cartesianProduct(cartesianRows);
	}

	private Set<Set<Object>> cartesianProduct(List<List<String>> values) {
		Set[] set = new Set[values.size()];

		for (int i=0; i<values.size(); i++)
			set[i] = new LinkedHashSet<>(values.get(i));

		if (set.length == 1)
			return new LinkedHashSet<Set<Object>>() {{
				values.get(0).forEach(value -> add(new LinkedHashSet<Object>() {{
					add(value);
				}}));
			}};

		return MathHelper.cartesianProduct(set);
	}

	private Ticket ticket() {
		List<Ticket> ticketList = specification().ticketList();
		return ticketList.size() > 0 ? ticketList.get(0) : null;
	}

	private void filter(List<io.intino.sumus.analytics.viewmodels.FilterCondition> filters) {
		this.categoriesSelection.clear();
		filters.forEach(filterCondition -> {
			Categorization categorization = filterCondition.categorization;
			String categorizationLabel = categorization.label();

			if (filterCondition.categories.size() == 0)
				this.categoriesSelection.remove(categorizationLabel);
			else
				this.categoriesSelection.put(categorizationLabel, filterCondition.categories.stream().map(io.intino.sumus.Category::label).collect(toList()));
		});

		this.doQuery(timeScaleHandler().range());
	}

	private boolean isValidQuery() {
		return specification().ticketList().size() == 1 && columns().size() > 0 && rows().size() > 0;
	}

	public void showDialog() {
		super.showDialog();
	}

	static boolean isAvailableForScale(TemporalCategorization categorization, TimeScale scale) {
		int ownScale = categorization.range().scale().ordinal();
		return ownScale >= scale.ordinal() && ownScale <= scale.ordinal()+TemporalCategorizationOffset;
	}

}