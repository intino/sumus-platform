package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.TicketBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeChartDesignNotifier;
import io.intino.sumus.graph.rules.Chart;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.util.List;
import java.util.Optional;

public class SumusTimeChartDesign extends SumusDisplay<SumusTimeChartDesignNotifier> {
	private TimeScaleHandler timeScaleHandler;
	private ChartSpecHandler chartSpecHandler;
	private Chart selectedChart = null;

	public SumusTimeChartDesign(SumusBox box) {
		super(box);
	}

	public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
		this.timeScaleHandler = timeScaleHandler;
	}

	public void chartSpecHandler(ChartSpecHandler chartSpecHandler) {
		this.chartSpecHandler = chartSpecHandler;
		this.chartSpecHandler.onChange(this::refresh);
	}

	public void selectChart(Chart chart) {
		selectedChart = chart;
		notifier.refreshSelectedChart(chart.toString());
	}

	public void selectTickets(String[] tickets) {
		dialogDisplay().ifPresent(d -> d.selectTickets(tickets));
	}

	@Override
	protected void init() {
		super.init();

		buildChartDialogs();
		notifier.refreshTicketList(TicketBuilder.buildList(chartSpecHandler.olapTickets()));
	}

	@Override
	public void refresh() {
		super.refresh();
		refresh(chartSpecHandler.specification());
	}

	private void refresh(ChartSpec chartSpec) {
		notifier.refreshSelectedTicketList(TicketBuilder.buildList(chartSpec.ticketList()));
	}

	private void buildChartDialogs() {
		add(new SumusTimeSeriesChartDialog(box));
		add(new SumusTimeCrossTableDialog(box));
		add(new SumusTimeBarChartDialog(box));
		add(new SumusTimeScatterChartDialog(box));
	}

	private void add(SumusChartDialog display) {
		display.timeScaleHandler(timeScaleHandler);
		display.chartSpecHandler(chartSpecHandler);
		addAndPersonify(display);
	}

	public void updateTemporalCategorizations(List<TemporalCategorization> categorizations) {
		child(SumusTimeCrossTableDialog.class).updateTemporalCategorizations(categorizations);
	}

	private Optional<SumusChartDialog> dialogDisplay() {
		return children(SumusChartDialog.class).stream().filter(d -> d.type() == selectedChart).findFirst();
	}
}