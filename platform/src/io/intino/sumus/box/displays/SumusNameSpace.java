package io.intino.sumus.box.displays;

import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.NameSpaceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusNameSpaceNotifier;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.helpers.NameSpaceHandler;

import java.util.List;

public class SumusNameSpace extends SumusDisplay<SumusNameSpaceNotifier> {
	private NameSpaceHandler nameSpaceHandler;

    public SumusNameSpace(SumusBox box) {
        super(box);
		this.nameSpaceHandler = new NameSpaceHandler(box.graph());
    }

    public SumusNameSpace nameSpaceHandler(NameSpaceHandler handler) {
		this.nameSpaceHandler = handler;
		return this;
	}

	@Override
	protected void init() {
		super.init();
		sendNameSpaceList();
	}

	private void sendNameSpaceList() {
		refreshNameSpaces(nameSpaceHandler.nameSpaces(username()));
		refreshSelectedNameSpace(nameSpaceHandler.selectedNameSpace(username()));
	}

	private void refreshNameSpaces(List<io.intino.sumus.graph.NameSpace> nameSpaces) {
		notifier.refreshNameSpaces(NameSpaceBuilder.buildList(nameSpaces));
	}

	private void refreshSelectedNameSpace(NameSpace nameSpace) {
		notifier.refreshSelectedNameSpace(NameSpaceBuilder.build(nameSpace));
	}

	public void selectNameSpace(String value) {
    	nameSpaceHandler.select(username(), value);
	}
}