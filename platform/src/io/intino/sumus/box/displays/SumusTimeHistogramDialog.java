package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.rules.Chart;
import io.intino.sumus.helpers.ChartSpecHandler;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

public abstract class SumusTimeHistogramDialog<DN extends AlexandriaDisplayNotifier> extends SumusChartDialog<DN, AlexandriaTimeNavigator> {
    private final String tag;

    public SumusTimeHistogramDialog(SumusBox box, Chart type, String tag) {
        super(box, type, new AlexandriaTimeNavigator(box));
        this.tag = tag;
    }

    @Override
    protected void init() {
        super.init();
        sendCategorizations();
    }

    public void selectCategorization(String value) {
        ChartSpecHandler chartSpecHandler = chartSpecHandler();
        chartSpecHandler.clearCategorizationsTag(tag);
        chartSpecHandler.selectCategorizations(singletonMap(categorizationOf(value), singletonList(tag)));
        sendCategorization();
        chartSpecHandler.update();
    }

    protected abstract void sendCategorizations();
    protected abstract void sendCategorization();

    protected Categorization categorization() {
        return specification().categorizationList(tag).get(0);
    }

}