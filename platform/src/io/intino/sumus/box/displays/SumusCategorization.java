package io.intino.sumus.box.displays;

import io.intino.sumus.Category;
import io.intino.sumus.analytics.viewmodels.CategorizationViewBuilder;
import io.intino.sumus.analytics.viewmodels.CategoryView;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.CategoryViewBuilder;
import io.intino.sumus.box.displays.notifiers.SumusCategorizationNotifier;
import io.intino.sumus.box.schemas.CategorizationToggleDialog;
import io.intino.sumus.graph.Categorization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import static io.intino.sumus.box.displays.builders.CategorizationViewBuilder.build;
import static java.util.stream.Collectors.toList;

public class SumusCategorization extends SumusDisplay<SumusCategorizationNotifier> {
	private Categorization categorization;
	private final List<Consumer<List<Category>>> selectListeners = new ArrayList<>();
	private final List<Category> categories = new ArrayList<>();

	public SumusCategorization(SumusBox box) {
		super(box);
	}

	public void categorization(Categorization categorization) {
		this.categorization = categorization;
	}

	public Categorization categorization() {
		return this.categorization;
	}

	public void toggle(CategorizationToggleDialog dialog) {
		this.select(dialog.categories());
	}

	public void onSelect(Consumer<List<Category>> consumer) {
		selectListeners.add(consumer);
		children(SumusCategorization.class).forEach(d -> d.onSelect(consumer));
	}

	public Drill asDrill() {
		return (categories.size() > 0) ? new Drill(categorization, categories) : null;
	}

	public boolean hasFilterCondition() {
		return categories.size() > 0;
	}

	public FilterCondition asFilterCondition() {
		return new FilterCondition(categorization, categories);
	}

	public void toggle(Category category) {

		if (categories.contains(category)) categories.remove(category);
		else categories.add(category);

		refreshSelected();
		notifySelection();
	}

	public void select(String[] categories) {
		select(Arrays.asList(categories));
	}

	public void select(List<String> categoryList) {
		categories.clear();
		List<Category> calculate = categorization.calculate(keys());
		categoryList.forEach(key -> {
			List<Category> foundCategories = calculate.stream().filter(c -> c.name().equals(key) || c.label().equals(key)).collect(toList());
			if (foundCategories.size() > 0) categories.add(foundCategories.get(0));
		});
		refreshSelected();
		notifySelection();
	}

	public void selectAll() {
		categories.clear();
		categories.addAll(categorization.calculate(keys()));
		refreshSelected();
		notifySelection();
	}

	public void selectNone() {
		categories.clear();
		refreshSelected();
		notifySelection();
	}

	public void maxSelection(int count) {
		notifier.refreshMaxSelection(count);
	}

	@Override
	public void refresh() {
		super.refresh();
		notifier.refreshCategorization(build(CategorizationViewBuilder.build(categorization, keys())));
	}

	private void notifySelection() {
		selectListeners.forEach(c -> c.accept(categories));
	}

	private void refreshSelected() {
		notifier.refreshSelected(CategoryViewBuilder.buildList(categories.stream().map(CategoryView::new).collect(toList())));
	}

//    private List<Category> filter(Collection<Category> values) {
//        if (categorization().parent() == null) return new ArrayList<>(values);
//
//        TemporalCategorization temporalParent = (TemporalCategorization)parent;
//        values.stream().filter(v -> {
//            TemporalCategory value = (TemporalCategory)v;
//            TimeScale scale = temporalParent.range.scale();
//            scale.normalise(value.timeStamp.instant());
//
//            v.
//        })
//    }

}