package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.notifiers.SumusAnalyzeNotifier;
import io.intino.sumus.graph.Filter;
import io.intino.sumus.graph.rules.Chart;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class SumusAnalyze extends SumusDisplay<SumusAnalyzeNotifier> {
	private TimeScaleHandler timeScaleHandler;
	private ChartSpecHandler chartSpecHandler;
	private List<Consumer<List<FilterCondition>>> filterChangeListeners = new ArrayList<>();
	private List<BiConsumer<List<Drill>, Boolean>> drillChangeListeners = new ArrayList<>();
	private Map<Filter, List<FilterCondition>> conditionsMap = new HashMap<>();
	private SumusFilter currentFilterDisplay = null;

	public SumusAnalyze(SumusBox box) {
		super(box);
	}

	public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
		this.timeScaleHandler = timeScaleHandler;
	}

	public void chartSpecHandler(ChartSpecHandler chartSpecHandler) {
		this.chartSpecHandler = chartSpecHandler;
	}

	public void onFilter(Consumer<List<FilterCondition>> consumer) {
		filterChangeListeners.add(consumer);
	}

	public void onDrill(BiConsumer<List<Drill>, Boolean> consumer) {
		drillChangeListeners.add(consumer);
	}

	public void allowTemporalCategorizations(boolean value) {
		child(SumusFilter.class).allowTemporalCategorizations(value);
	}

	public void selectChart(Chart chart) {
		child(SumusTimeChartDesign.class).selectChart(chart);
	}

	public void updateTemporalCategorizations(List<TemporalCategorization> availableCategorizations) {
		child(SumusTimeChartDesign.class).updateTemporalCategorizations(availableCategorizations);
		//child(FilterDisplay.class).updateTemporalCategorizations(availableCategorizations);
	}

	@Override
	protected void init() {
		super.init();
		createTimeChartDesignDisplay();
		createCategorizationComparatorDisplay();
		createFilterListDisplay();
		createFilterDisplay();
		refreshVisibility();
	}

	private void createTimeChartDesignDisplay() {
		SumusTimeChartDesign display = new SumusTimeChartDesign(box);
		display.timeScaleHandler(timeScaleHandler);
		display.chartSpecHandler(chartSpecHandler);
		addAndPersonify(display);
	}

	private void createCategorizationComparatorDisplay() {
		SumusCategorizationComparator display = new SumusCategorizationComparator(box);
		display.categorizations(chartSpecHandler.olapCategorizations());
		//display.temporalCategorizations(chartSpecHandler.olapTemporalCategorizations());
		display.onApply(this::applyDrill);
		display.onQuit(this::quitDrill);
		addAndPersonify(display);
	}

	private void createFilterListDisplay() {
		SumusFilterList display = new SumusFilterList(box);
		display.onSelect(this::refreshCurrentFilter);
		display.onApply(this::applyFilter);
		display.onQuit(this::quitFilter);
		display.onRemoveFilter(this::removeFilter);
		addAndPersonify(display);
	}

	private void createFilterDisplay() {
		currentFilterDisplay = new SumusFilter(box);
		currentFilterDisplay.categorizations(chartSpecHandler.olapCategorizations());
		//currentFilterDisplay.temporalCategorizations(chartSpecHandler.olapTemporalCategorizations());
		currentFilterDisplay.onApply(this::applyFilter);
		currentFilterDisplay.onChange(this::updateFilter);
		currentFilterDisplay.onQuit(this::quitFilter);
		currentFilterDisplay.onRemove(this::removeFilter);
		addAndPersonify(currentFilterDisplay);
	}

	private void refreshCurrentFilter(Filter filter) {
		if (currentFilterDisplay == null)
			return;

		currentFilterDisplay.filter(filter);
		currentFilterDisplay.refresh();
	}

	private void applyFilter(Filter filter) {
		conditionsMap.put(filter, currentFilterDisplay.conditions());
		notifyConditionsChange();
	}

	private void updateFilter(Filter filter) {
		if (!conditionsMap.containsKey(filter)) return;
		conditionsMap.put(filter, currentFilterDisplay.conditions());
		notifyConditionsChange();
	}

	public void quitFilter(String filter) {
		child(SumusFilterList.class).quitFilter(filter);
	}

	public void removeFilter(Filter filter) {
		quitFilter(filter);
		SumusFilterList display = child(SumusFilterList.class);
		display.refresh();

		List<Filter> filters = display.filterList();
		refreshCurrentFilter(filters.size() == 1 ? filters.get(0) : null);
	}

	public void quitFilter(Filter filter) {
		conditionsMap.remove(filter);
		notifyConditionsChange();
	}

	public void quitFilters() {
		conditionsMap.clear();
		notifyConditionsChange();
	}

	public List<Filter> filterList() {
		return conditionsMap.keySet().stream().collect(toList());
	}

	public void enableComparePanel() {
		notifier.enableComparePanel();
	}

	public void disableComparePanel() {
		notifier.disableComparePanel();
	}

	private void applyDrill(Drill drill, boolean includeGlobalSerie) {
		notifyDrillChange(drill, includeGlobalSerie);
	}

	private void quitDrill(Drill drill) {
		notifyDrillChange(null, false);
	}

	private void refreshVisibility() {
		notifier.refreshVisibility(!isEmpty());
	}

	private boolean isEmpty() {
		if (chartSpecHandler.olapTickets().isEmpty()) return true;
		if (chartSpecHandler.olapCategorizations().isEmpty() && chartSpecHandler.olapTemporalCategorizations().isEmpty()) return true;
		return false;
	}

	private List<FilterCondition> conditions() {
		return conditionsMap.values().stream().flatMap(Collection::stream).collect(toList());
	}

	private void notifyConditionsChange() {
		filterChangeListeners.forEach(l -> l.accept(conditions()));
	}

	private void notifyDrillChange(Drill drill, boolean includeGlobalSerie) {
		drillChangeListeners.forEach(l -> l.accept(drill != null ? new ArrayList<Drill>() {{ add(drill); }} : emptyList(), includeGlobalSerie));
	}

}