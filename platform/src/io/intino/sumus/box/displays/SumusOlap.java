package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.helpers.Bounds;
import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.konos.alexandria.activity.spark.ActivityFile;
import io.intino.sumus.Category;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.categorization.TemporalCategorizationFactory;
import io.intino.sumus.analytics.exporters.Document;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.*;
import io.intino.sumus.box.displays.notifiers.SumusOlapNotifier;
import io.intino.sumus.box.schemas.ChartOption;
import io.intino.sumus.box.schemas.RequestRange;
import io.intino.sumus.graph.*;
import io.intino.sumus.graph.rules.Chart;
import io.intino.sumus.graph.rules.Mode;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;
import io.intino.sumus.helpers.NameSpaceHandler;
import io.intino.sumus.queries.Scope;
import io.intino.tara.magritte.Concept;

import java.io.InputStream;
import java.time.Instant;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static io.intino.sumus.box.displays.builders.RangeBuilder.build;
import static io.intino.sumus.graph.Ticket.Range;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.IntStream.rangeClosed;

public class SumusOlap extends SumusDisplay<SumusOlapNotifier> {
	private static final Logger LOG = Logger.getLogger(SumusOlap.class.getName());
	private Olap olap;
	private NameSpaceHandler nameSpaceHandler;
	private TimeScaleHandler timeScaleHandler;
	private ChartSpecHandler chartSpecHandler;
	private List<Consumer<Boolean>> loadingListeners = new ArrayList<>();
	private List<Consumer<CatalogInstantBlock>> selectListeners = new ArrayList<>();
	private Map<Chart, Function<Chart, SumusChart>> chartDisplayBuilders = new HashMap<>();
	private Map<Chart, SumusChart> chartDisplayMap = new HashMap<>();
	private Chart selectedChart = null;
	private List<Ticket> lastSelectedTickets = new ArrayList<>();
	private Scope scope = null;

	public SumusOlap(SumusBox box) {
		super(box);
		fillBuilders();
	}

	public SumusOlap olap(Olap olap) {
		this.olap = olap;
		return this;
	}

	public Olap olap() {
		return this.olap;
	}

	public void nameSpaceHandler(NameSpaceHandler nameSpaceHandler) {
		this.nameSpaceHandler = nameSpaceHandler;
		this.nameSpaceHandler.onSelect(this::selectNameSpace);
	}

	public void scope(Scope scope) {
		chartSpecHandler.setScope(scope);
		chartSpecHandler.update();
	}

	@Override
	public void refresh() {
		super.refresh();

		if (selectedChart == null && olap.charts().size() > 0)
			this.selectChart(olap.charts().get(0), null);
		else
			chartDisplay().ifPresent(SumusChart::update);

		updateActions();
	}

	public void addTicket(String label) {
		if (label == null) return;
		addTicket(ticket(label));
	}

	public void addTicket(Ticket ticket) {
		chartSpecHandler.addTicket(ticket);
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();

		if (selectedTickets.size() == 1)
			notifyShowChart();

		checkEnabledDrill(canDrill());
		checkEnabledFilter(canFilter());
		updateAnalyzeDisplay();
		updateTickets();
		chartSpecHandler.update();
	}

	public void removeTicket(String label) {
		if (label == null) return;
		removeTicket(ticket(label));
	}

	public void removeTicket(Ticket ticket) {
		chartSpecHandler.removeTicket(ticket);
		checkEnabledDrill(canDrill());
		checkEnabledFilter(canFilter());
		updateTickets();
		updateAnalyzeDisplay();
		chartSpecHandler.update();
	}

	public void moveTickets(String[] tickets) {
		chartSpecHandler.selectTickets(tickets);
		chartSpecHandler.update();
	}

	public void updateScale(String scale) {
		updateScale(TimeScale.valueOf(scale));
	}

	public void updateScale(TimeScale scale) {
		timeScaleHandler.updateScale(scale);
	}

	public ActivityFile export(RequestRange range) {
		return new ActivityFile() {
			private Document document = export(range.from(), range.to());

			@Override
			public String label() {
				return document.name();
			}

			@Override
			public InputStream content() {
				return document.content();
			}
		};
	}

	public void drill(Categorization categorization, List<Category> categories) {
		Drill drill = new Drill(categorization, categories);
		chartSpecHandler.drill(drill);
		notifier.refreshDrill(DrillBuilder.build(drill));
		chartSpecHandler.update();
	}

	public void removeDrillCategory(String categoryName) {
		Drill drill = chartSpecHandler.removeDrillCategory(categoryName);

		if (drill.categories.size() <= 0) {
			removeDrill();
			return;
		}

		notifier.refreshDrill(DrillBuilder.build(drill));
		this.updateActions();
		chartSpecHandler.update();
	}

	public void removeDrill() {
		chartSpecHandler.drill(null);
		notifier.refreshDrill(null);
		chartSpecHandler.update();
	}

	public void selectNameSpace(NameSpace nameSpace) {
		children(SumusChart.class).forEach(display -> display.nameSpace(nameSpace));

		if (selectedChart != null)
			chartDisplay().ifPresent(SumusChart::refresh);
	}

	public void selectChart(ChartOption option) {
		selectChart(Chart.valueOf(option.name()), option.option());
	}

	public void selectChart(Chart chart, String option) {
		selectedChart = chart;
		notifier.selectChart(chart.toString());

		SumusChart chartDisplay = chartDisplay().get();

		if (analyzeDisplay().isPresent()) {
			SumusAnalyze display = analyzeDisplay().get();
			display.allowTemporalCategorizations(chartDisplay.allowTemporalFiltering());
			display.selectChart(chart);
		}

		if (!chartDisplay.isValidSpecification(chartSpecHandler.specification()))
			notifier.openAnalyzeDialog();
		else
			notifier.closeAnalyzeDialog();

		if (chartDisplay.allowDrill()) removeDrill();
		else if (chartSpecHandler.drill() != null) removeDrill();

		chartDisplay.specification(chartSpecHandler.specification());
		updateByCategorization();
		updateCompare();
	}

	public void updateRange(RequestRange range) {
		updateRange(range.from(), range.to());
	}

	public void updateRange(Instant from, Instant to) {
		timeScaleHandler.updateRange(from, to);
	}

	public void onLoading(Consumer<Boolean> listener) {
		loadingListeners.add(listener);
	}

	public void onSelect(Consumer<CatalogInstantBlock> listener) {
		selectListeners.add(listener);
	}

	public void removeFilter(String value) {
		analyzeDisplay().ifPresent(d -> d.quitFilter(value));
	}

	private List<Catalog> catalogs() {

		if (olap.events() == null || olap.events().onClickInstant() == null)
			return emptyList();

		return olap.events().onClickInstant().catalogs();
	}

	@Override
	protected void init() {
		super.init();
		setupOlap();
		sendCharts();
		sendRanges();
		if (olap.select().ticket() != null)
			addTicket(olap.select().ticket());
	}

	private static Stream<Indicator> indicators(List<Ticket> tickets) {
		return tickets.stream()
				.filter(ticket -> ticket.dataRetriever().i$(Indicator.class))
				.map(ticket -> ticket.dataRetriever().i$(MeasureIndicator.class) ? singletonList(ticket.dataRetriever().a$(MeasureIndicator.class)) : ticket.dataRetriever().a$(StackedIndicator.class).measureIndicatorList())
				.flatMap(Collection::stream);
	}

	private void checkZoomsAndTicketsScales(List<Olap.ZoomGroup.Zoom> zooms, List<TimeScale> scales) {
		List<Integer> ordinalScales = scales.stream().map(Enum::ordinal).collect(toList());

		if (zooms.size() != scales.size())
			LOG.severe(String.format("Olap %s: The number of scales defined by tickets are different from available zoom levels. It must be the same", olap.name$()));

		zooms.forEach(zoom -> {
			if (ordinalScales.contains(zoom.scale().ordinal())) return;
			LOG.severe(String.format("Olap %s: Scale %s not defined by tickets.", olap.name$(), zoom.scale().toString()));
		});
	}

	private void drill(List<Drill> drills, boolean includeGlobalSerie) {
		Drill drill = drills.size() > 0 ? drills.get(0) : null;
		chartSpecHandler.drill(drill, includeGlobalSerie);
		notifier.refreshDrill(DrillBuilder.build(drill));
		chartSpecHandler.update();
	}

	private void checkEnabledDrill(boolean enabled) {
		if (enabled && canDrill())
			notifier.enableDrill();
		else
			notifier.disableDrill();
	}

	private void checkEnabledFilter(boolean enabled) {
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();
		if (enabled && canFilter() && indicators(selectedTickets).count() > 0) {
			notifier.enableFilter();
		} else {
			notifier.disableFilter();
		}
	}

	private void updateAnalyzeDisplay() {
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();
		if (!selectedTickets.isEmpty()) return;

		analyzeDisplay().ifPresent(SumusAnalyze::quitFilters);
		updateActions();

		if (analyzeDisplay().isPresent())
			notifier.refreshFilterList(FilterBuilder.buildList(analyzeDisplay().get().filterList()));
	}

	private void updateActions() {
		if (isFiltering()) {
			notifier.filtering();
			if (canDrill())
				notifier.enableDrill();
			else
				notifier.disableDrill();
		} else if (isDrilling()) {
			notifier.drilling();
		} else {
			notifier.notDrilling();
			notifier.notFiltering();
			checkEnabledDrill(canDrill());

			if (canFilter())
				notifier.enableFilter();
			else
				notifier.disableFilter();
		}
	}

	private void updateCompare() {
		if (!analyzeDisplay().isPresent()) return;

		SumusAnalyze analyzeDisplay = analyzeDisplay().get();
		boolean allowDrill = chartDisplay().isPresent() && chartDisplay().get().allowDrill();

		if (canDrill() && allowDrill)
			analyzeDisplay.enableComparePanel();
		else
			analyzeDisplay.disableComparePanel();
	}

	private void updateTickets() {
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();
		List<Ticket> ticketList = ticketList();

		this.lastSelectedTickets.clear();
		this.lastSelectedTickets.addAll(selectedTickets);

		notifier.ticketList(TicketBuilder.buildList(ticketList));
		notifier.selectTicketList(TicketBuilder.buildList(selectedTickets));
	}

	private void updateByCategorization() {
		if (chartSpecHandler.selectedTickets().size() <= 0) return;
		chartDisplay().ifPresent(chartDisplay -> {
			Categorization byCategorization = chartDisplay.byCategorization();
			notifier.byCategorization(byCategorization != null ? ReferenceBuilder.build(byCategorization) : null);
		});
	}

	private List<Ticket> ticketList() {
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();
		return olap.tickets().stream().filter(t -> !selectedTickets.contains(t)).collect(toList());
	}

	private void notifyShowChart() {
		if (analyzeDisplay().isPresent()) return;
		if (olap.charts().size() > 0) selectChart(olap.charts().get(0), null);
		notifier.scaleList(ScaleBuilder.buildList(timeScaleHandler.scales(), currentLanguage()));
		buildAnalyzeDisplay();
		updateScale(olap.select().scale().toString());
	}

	private void buildAnalyzeDisplay() {
		SumusAnalyze analyzeDisplay = new SumusAnalyze(box);
		analyzeDisplay.timeScaleHandler(timeScaleHandler);
		analyzeDisplay.chartSpecHandler(chartSpecHandler);
		add(analyzeDisplay);
		analyzeDisplay.personifyOnce();
		analyzeDisplay.onFilter(this::filter);
		analyzeDisplay.onDrill(this::drill);
		analyzeDisplay.allowTemporalCategorizations(chartDisplay().isPresent() && chartDisplay().get().allowTemporalFiltering());
		analyzeDisplay.selectChart(selectedChart);
		updateTemporalCategorizations(timeScaleHandler.range());
	}

	private void filter(List<FilterCondition> conditions) {
		chartSpecHandler.filter(conditions);
		if (analyzeDisplay().isPresent())
			notifier.refreshFilterList(FilterBuilder.buildList(analyzeDisplay().get().filterList()));
		chartSpecHandler.update();
	}

	private void notifyLoading(Boolean value) {
		loadingListeners.forEach(l -> l.accept(value));
	}

	private void refreshOlapAndTemporalCategorizations(TimeRange timeRange) {
		refreshOlap(timeRange);
		updateTemporalCategorizations(timeRange);
	}

	private void refreshOlap(TimeRange timeRange) {
		notifier.updateScale(ScaleBuilder.build(timeRange.scale(), currentLanguage()));
		notifier.timeRange(build(timeRange));
		this.refresh();
	}

	private void updateTemporalCategorizations(TimeRange timeRange) {
		List<TemporalCategorization> temporalCategorizations = temporalCategorizations(timeRange);
		chartSpecHandler.olapTemporalCategorizations(temporalCategorizations);
		Optional.ofNullable(child(SumusAnalyze.class)).ifPresent(ad -> ad.updateTemporalCategorizations(temporalCategorizations));
	}

	private void sendCharts() {
		notifier.chartList(olap.charts().stream().map(this::chartOf).filter(Objects::nonNull).collect(toList()));
	}

	private io.intino.sumus.box.schemas.Chart chartOf(Chart chart) {
		return new io.intino.sumus.box.schemas.Chart().name(chart.toString());
	}

	private void sendRanges() {
		notifier.olapRange(build(timeScaleHandler.boundsRange()));
		notifier.timeRange(build(timeScaleHandler.range()));
	}

	private Optional<SumusAnalyze> analyzeDisplay() {
		return Optional.ofNullable(child(SumusAnalyze.class));
	}

	private Optional<SumusChart> chartDisplay() {
		return Optional.ofNullable(chartDisplay(selectedChart));
	}

	private SumusChart chartDisplay(Chart chart) {
		if (chart == null) return null;
		if (!chartDisplayMap.containsKey(chart))
			chartDisplayMap.put(chart, chartDisplayBuilders.get(chart).apply(chart));
		return chartDisplayMap.get(chart);
	}

	private List<TimeScale> scalesFrom(List<Ticket> tickets) {
		return tickets.stream()
				.map(Ticket::range)
				.flatMap(this::scalesInRange)
				.distinct()
				.sorted()
				.collect(toList());
	}

	private Stream<TimeScale> scalesInRange(Range range) {
		return rangeClosed(range.max().ordinal(), range.min().ordinal()).mapToObj(i -> TimeScale.values()[i]);
	}

	private Document export(Instant from, Instant to) {
		return chartDisplay().isPresent() ? chartDisplay().get().export(from, to) : null;
	}

	private boolean isFiltering() {
		return analyzeDisplay().isPresent() && analyzeDisplay().get().filterList().size() <= 0;
	}

	private boolean canFilter() {
		return !isDrilling() && analyzeDisplay().isPresent() && analyzeDisplay().get().filterList().size() > 0;
	}

	private boolean isDrilling() {
		return chartSpecHandler.drill() != null;
	}

	private boolean canDrill() {
		List<Ticket> selectedTickets = chartSpecHandler.selectedTickets();
		return indicators(selectedTickets).count() == 1 && chartDisplay().isPresent() && !chartDisplay().get().isShowingEvents();
	}

	private Ticket ticket(String label) {
		return platform().ticketList().stream().filter(b -> b.label().equals(label)).findFirst().orElse(null);
	}

	private void fillBuilders() {
		chartDisplayBuilders.put(Chart.TimeSeriesChart, this::buildTimeSeriesChartDisplay);
		chartDisplayBuilders.put(Chart.TimeCrossTable, this::buildTimeCrossTableDisplay);
		chartDisplayBuilders.put(Chart.TimeBarChart, this::buildTimeBarChartDisplay);
		chartDisplayBuilders.put(Chart.TimeScatterChart, this::buildTimeScatterChartDisplay);
	}

	private SumusChart buildTimeSeriesChartDisplay(Chart chart) {
		SumusTimeSeriesChart display = init(new SumusTimeSeriesChart(box));
		display.catalogs(catalogs());
		addAndPersonify(display);
		this.timeScaleHandler.onRangeChange(this::refreshOlapAndTemporalCategorizations);
		this.timeScaleHandler.onScaleChange(this::refreshOlapAndTemporalCategorizations);
		return display;
	}

	private SumusChart buildTimeCrossTableDisplay(Chart chart) {
		SumusChart display = addAndPersonify(init(new SumusTimeCrossTable(box)));
		updateTemporalCategorizations(timeScaleHandler.range());
		return display;
	}

	private SumusTimeBarChart buildTimeBarChartDisplay(Chart chart) {
		return addAndPersonify(init(new SumusTimeBarChart(box)));
	}

	private SumusTimeScatterChart buildTimeScatterChartDisplay(Chart chart) {
		return addAndPersonify(init(new SumusTimeScatterChart(box)));
	}

	private <D extends SumusChart> D init(D display) {
		display.timeScaleHandler(timeScaleHandler);
		display.chartSpecHandler(chartSpecHandler);
		display.nameSpace(nameSpaceHandler.selectedNameSpace(username()));
		display.instantFormatter(olap.instantFormatter());
		display.onShowDialog((value) -> notifier.openAnalyzeDialog());
		display.onSelect(instant -> notifySelectInstant((CatalogInstantBlock) instant));
		display.onLoading(value -> notifyLoading((Boolean) value));
		return display;
	}

	private <D extends SumusChart> D addAndPersonify(D display) {
		add(display);
		display.personifyOnce();
		return display;
	}

	private void notifySelectInstant(CatalogInstantBlock instant) {
		selectListeners.forEach(l -> l.accept(instant));
	}

	private List<Categorization> categorizations() {
		List<Concept> indicatorEntities = indicatorEntities();
		List<Concept> distributionEntities = distributionEntities();
		return box.graph().categorizationList().stream()
				.filter(c -> isContained(indicatorEntities, c.record()) || isContained(distributionEntities, c.record()))
				.collect(toList());
	}

	private boolean isContained(List<Concept> indicatorEntities, Concept concept) {
		for (Concept entity : indicatorEntities) if (entity.is(concept.name())) return true;
		return false;
	}

	private List<Concept> indicatorEntities() {
		return olap.tickets().stream()
				.map(ticket -> ticket.core$().findNode(MeasureIndicator.Formula.class))
				.map(formulas -> formulas.stream().map(MeasureIndicator.Formula::cube).collect(toList()))
				.flatMap(Collection::stream)
				.map(Cube::dimensionList)
				.flatMap(Collection::stream)
				.map(Cube.Dimension::entity)
				.distinct()
				.collect(toList());
	}

	private List<Concept> distributionEntities() {
		return olap.tickets().stream()
				.map(ticket -> ticket.core$().findNode(Distribution.Source.class))
				.flatMap(Collection::stream)
				.map(source -> source.property().core$().ownerAs(Cube.class).dimensionList())
				.flatMap(Collection::stream)
				.map(Cube.Dimension::entity)
				.distinct()
				.collect(toList());
	}

	private List<TemporalCategorization> temporalCategorizations(TimeRange range) {
		List<TimeScale> timeScales = scalesFrom(olap.tickets());
		return timeScales.stream().map(scale -> temporalCategorization(rangeForScale(range, scale))).flatMap(Collection::stream).collect(toList());
	}

	private TimeRange rangeForScale(TimeRange range, TimeScale scale) {
		TimeScale rangeScale = range.scale();
		if (scale.ordinal() <= rangeScale.ordinal()) return new TimeRange(range.from(), range.to(), scale);
		Instant instant = rangeScale.addTo(range.to(), 1);
		instant = rangeScale.next().addTo(instant, -1);
		return new TimeRange(range.from(), instant, scale);
	}

	private List<TemporalCategorization> temporalCategorization(TimeRange range) {
		TemporalCategorization result = new TemporalCategorizationFactory().get(range);
		result.helper(box.translatorHelper()).language(currentLanguage());
		return singletonList(result);
	}

	private TimeScaleHandler buildTimeScaleHandler(Olap olap, List<Olap.ZoomGroup.Zoom> zooms, List<TimeScale> scales) {
		TimeScaleHandler.Bounds bounds = new TimeScaleHandler.Bounds();
		String username = username();

		bounds.rangeLoader(() -> {
			Olap.Range range = olap.range();
			return new TimeRange(range.from(username), range.to(username), TimeScale.Minute);
		});
		bounds.mode(olap.select().range() == Mode.FromTheBeginning ? io.intino.konos.alexandria.activity.helpers.Bounds.Mode.FromTheBeginning : io.intino.konos.alexandria.activity.helpers.Bounds.Mode.ToTheLast);
		bounds.zooms(zooms.stream().filter(zoom -> scales.contains(scaleOf(zoom.scale()))).collect(toMap(zoom -> scaleOf(zoom.scale()), zoom -> new Bounds.Zoom() {
			@Override
			public int min() {
				return zoom.instantRange().min();
			}

			@Override
			public int max() {
				return zoom.instantRange().max();
			}
		})));

		TimeScaleHandler timeScaleHandler = new TimeScaleHandler(bounds, scales, scaleOf(olap.select().scale()));
		timeScaleHandler.availableScales(timeScaleHandler.scales());
		timeScaleHandler.onRangeChange(r -> chartSpecHandler.range(r));
		timeScaleHandler.onNotValidRange(r -> chartSpecHandler.range(r));
		timeScaleHandler.onScaleChange(r -> chartSpecHandler.range(r));

		return timeScaleHandler;
	}

	private ChartSpecHandler buildChartSpecHandler() {
		ChartSpecHandler result = new ChartSpecHandler();
		result.olapTickets(olap.tickets());
		result.olapCategorizations(categorizations());
		result.olapTemporalCategorizations(temporalCategorizations(timeScaleHandler.range()));
		result.onChange(this::refreshSelectedChartDesign);
		return result;
	}

	private void refreshSelectedChartDesign(ChartSpec chartSpec) {
		if (selectedChart == null) return;

		if (!chartSpec.equalsTickets(this.lastSelectedTickets))
			updateTickets();

		updateByCategorization();

		chartDisplay().ifPresent(d -> {
			if (!d.isValidSpecification(chartSpec))
				notifier.openAnalyzeDialog();
			d.specification(chartSpec);
		});
	}

	private void setupOlap() {
		List<Olap.ZoomGroup.Zoom> zooms = olap.zoomGroup() != null ? olap.zoomGroup().zoomList() : emptyList();
		List<TimeScale> scales = scalesFrom(olap.tickets());

		checkZoomsAndTicketsScales(zooms, scales);

		if (!scales.contains(scaleOf(olap.select().scale())))
			LOG.severe(String.format("Olap %s: Select scale %s not available", olap.name$(), olap.select().scale().toString()));

		this.timeScaleHandler = buildTimeScaleHandler(olap, zooms, scales);
		this.chartSpecHandler = buildChartSpecHandler();
		this.olap.onRefresh(() -> {
			sendRanges();
			chartDisplay().ifPresent(SumusChart::refresh);
		});
	}

}