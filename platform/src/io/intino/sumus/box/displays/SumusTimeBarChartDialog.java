package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeBarChartDialogNotifier;

import static io.intino.sumus.box.displays.SumusTimeBarChart.Tag;
import static io.intino.sumus.graph.rules.Chart.TimeBarChart;

public class SumusTimeBarChartDialog extends SumusTimeHistogramDialog<SumusTimeBarChartDialogNotifier> {
	public SumusTimeBarChartDialog(SumusBox box) {
		super(box, TimeBarChart, Tag);
	}

	@Override
	protected void sendTickets() {
	}

	@Override
	public void addNavigatorListeners(AlexandriaTimeNavigator display) {
		display.onMove(instant -> chartSpecHandler().update());
	}

	@Override
	protected void sendCategorizations() {
		notifier.refreshCategorizationList(ReferenceBuilder.buildList(categorizations()));
	}

	@Override
	protected void sendCategorization() {
		notifier.refreshCategorization(ReferenceBuilder.build(categorization()));
	}

	public void selectCategorization(String value) {
		super.selectCategorization(value);
	}
}