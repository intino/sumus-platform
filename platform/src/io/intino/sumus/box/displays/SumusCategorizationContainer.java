package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Categorization;

import java.util.*;

import static java.util.stream.Collectors.toList;

public abstract class SumusCategorizationContainer<DN extends AlexandriaDisplayNotifier> extends SumusDisplay<DN> {
    private List<Categorization> categorizationList = new ArrayList<>();
    private List<TemporalCategorization> temporalCategorizationList = new ArrayList<>();
    private final Map<String, SumusCategorization> displayByCategorization = new HashMap<>();
    private List<TemporalCategorization> activeCategorizations = new ArrayList<>();

    public SumusCategorizationContainer(SumusBox box) {
        super(box);
    }

    public void categorizations(List<Categorization> categorizations) {
        this.categorizationList = categorizations;
    }

    public void temporalCategorizations(List<TemporalCategorization> categorizations) {
        this.temporalCategorizationList = categorizations;
    }

    public void updateTemporalCategorizations(List<TemporalCategorization> activeCategorizations) {
        this.activeCategorizations = activeCategorizations;
        sendRefreshTemporalCategorizationList(temporalCategorizations());
    }

    @Override
    protected void init() {
        super.init();
        sendRefreshCategorizationList(categorizationList);
        sendRefreshTemporalCategorizationList(temporalCategorizationList);
        categorizationList.forEach(this::createCategorizationDisplay);
        temporalCategorizationList.forEach(this::createCategorizationDisplay);
    }

    protected abstract void sendRefreshCategorizationList(List<Categorization> categorizationList);
    protected abstract void sendRefreshTemporalCategorizationList(List<TemporalCategorization> categorizationList);
    protected abstract void addListeners(SumusCategorization categorizationDisplay);

    protected Collection<SumusCategorization> categorizationDisplays() {
        return displayByCategorization.values();
    }

    protected SumusCategorization categorizationDisplay(String name) {
        return displayByCategorization.get(name);
    }

    private List<TemporalCategorization> temporalCategorizations() {
        if (activeCategorizations.isEmpty()) return temporalCategorizationList;
        return activeCategorizations.stream().map(c -> (TemporalCategorization)c).collect(toList());
    }

    private void createCategorizationDisplay(Categorization categorization) {
        SumusCategorization display = new SumusCategorization(box);
        display.categorization(categorization);
        displayByCategorization.put(categorization.name$(), display);
        add(display);
        addListeners(display);
        display.personifyOnce(categorization.name$());
        display.refresh();
    }
}