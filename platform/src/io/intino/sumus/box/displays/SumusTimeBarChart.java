package io.intino.sumus.box.displays;

import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.HistogramBuilder;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeBarChartNotifier;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.helpers.ChartSpec;

public class SumusTimeBarChart extends SumusTimeHistogram<SumusTimeBarChartNotifier> {

	public static final String Tag = "time-bar-chart";

	public SumusTimeBarChart(SumusBox box) {
		super(box, Tag);
	}

	@Override
	protected void sendCategorization() {
		Categorization categorization = categorization();
		notifier.refreshCategorization(categorization != null ? ReferenceBuilder.build(categorization) : null);
	}

	@Override
	protected void sendTicketCount() {
		notifier.refreshTicketCount(specification().ticketList().size());
	}

	@Override
	protected void sendHistogram(io.intino.sumus.analytics.viewmodels.Histogram histogram) {
		notifier.refreshHistogram(HistogramBuilder.build(histogram));
	}

	@Override
	public boolean isValidSpecification(ChartSpec spec) {
		return spec.ticketList().size() > 0 && categorization() != null;
	}

	public void showDialog() {
		super.showDialog();
	}

}