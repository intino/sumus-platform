package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeCrossTableDialogNotifier;
import io.intino.sumus.box.schemas.CrossTableQuery;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static io.intino.sumus.box.displays.SumusTimeCrossTable.*;
import static io.intino.sumus.graph.rules.Chart.TimeCrossTable;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

public class SumusTimeCrossTableDialog extends SumusChartDialog<SumusTimeCrossTableDialogNotifier, AlexandriaTimeNavigator> {
	private List<Categorization> activeCategorizations = new ArrayList<>();

	public SumusTimeCrossTableDialog(SumusBox box) {
		super(box, TimeCrossTable, new AlexandriaTimeNavigator(box));
	}

	@Override
	protected void init() {
		super.init();
		sendCategorizations();
	}

	public void query(CrossTableQuery query) {
		Map<Categorization, List<String>> taggedCategorizationsMap = taggedCategorizationsMap(query);
		ChartSpecHandler chartSpecHandler = chartSpecHandler();
		chartSpecHandler.clearCategorizationsTag(ColumnTag);
		chartSpecHandler.clearCategorizationsTag(RowTag);
		chartSpecHandler.selectCategorizations(taggedCategorizationsMap);
		chartSpecHandler.heatMap(query.heatMap() != null ? new ChartSpec.HeatMap() {
			@Override
			public String minColor() {
				return query.heatMap().min();
			}

			@Override
			public String maxColor() {
				return query.heatMap().max();
			}
		} : null);
		chartSpecHandler.update();
	}

	public void updateTemporalCategorizations(List<TemporalCategorization> activeCategorizations) {
		TimeScale scale = timeScaleHandler().range().scale();
		this.activeCategorizations = activeCategorizations.stream().filter(c -> isAvailableForScale(c, scale)).collect(toList());
		notifier.refreshTemporalCategorizationList(ReferenceBuilder.buildList(temporalCategorizations()));
	}

	private Map<Categorization, List<String>> taggedCategorizationsMap(CrossTableQuery query) {
		Map<Categorization, List<String>> taggedCategorizationsMap = new LinkedHashMap<>();

		query.cols().forEach(col -> {
			Categorization categorization = categorizationOf(col);
			taggedCategorizationsMap.put(categorization, singletonList(ColumnTag));
		});
		query.rows().forEach(row -> {
			Categorization categorization = categorizationOf(row);
			taggedCategorizationsMap.put(categorization, singletonList(RowTag));
		});

		return taggedCategorizationsMap;
	}

	private void sendCategorizations() {
		notifier.refreshCategorizationList(ReferenceBuilder.buildList(categorizations()));
		notifier.refreshTemporalCategorizationList(ReferenceBuilder.buildList(temporalCategorizations()));
	}

	@Override
	protected List<TemporalCategorization> temporalCategorizations() {
		if (activeCategorizations.isEmpty()) return super.temporalCategorizations();
		return activeCategorizations.stream().map(c -> (TemporalCategorization)c).collect(toList());
	}

	@Override
	protected void sendTickets() {
	}

	@Override
	public void addNavigatorListeners(AlexandriaTimeNavigator display) {
		display.onMove(instant -> chartSpecHandler().update());
	}

}