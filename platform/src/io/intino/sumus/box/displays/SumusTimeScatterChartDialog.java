package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.builders.TicketBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeScatterChartDialogNotifier;
import io.intino.sumus.box.schemas.ScatterQuery;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static io.intino.sumus.box.displays.SumusTimeScatterChart.*;
import static io.intino.sumus.graph.rules.Chart.TimeScatterChart;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

public class SumusTimeScatterChartDialog extends SumusTimeHistogramDialog<SumusTimeScatterChartDialogNotifier> {
	private Ticket xAxis = null;
	private Ticket yAxis = null;
	private Ticket radius = null;
	private Ticket color = null;

	public SumusTimeScatterChartDialog(SumusBox box) {
		super(box, TimeScatterChart, ScatterTag);
	}

	@Override
	public void selectTickets(String[] tickets) {
		chartSpecHandler().selectTickets(tickets);
		resetTickets();
		refreshSpecificationTickets();
		chartSpecHandler().update();
	}

	@Override
	protected void init() {
		super.init();
		resetTickets();
		sendTickets();
	}

	@Override
	public void refresh() {
		super.refresh();
		sendTickets();
	}

	@Override
	protected void update() {
		resetTickets();
		super.update();
	}

	@Override
	protected void sendTickets() {
		notifier.refreshTicketList(TicketBuilder.buildList(new ArrayList<Ticket>() {{
			add(xAxis);
			add(yAxis);
			add(radius);
			add(color);
		}}));
	}

	@Override
	public void addNavigatorListeners(AlexandriaTimeNavigator display) {
		display.onMove(instant -> chartSpecHandler().update());
	}

	@Override
	protected void sendCategorizations() {
		notifier.refreshCategorizationList(ReferenceBuilder.buildList(categorizations()));
	}

	@Override
	protected void sendCategorization() {
		notifier.refreshCategorization(ReferenceBuilder.build(categorization()));
	}

	public void query(ScatterQuery query) {
		refreshSpecification(query);
		chartSpecHandler().update();
	}

	private Ticket ticketOf(String key) {
		return specification().ticketList().stream().filter(t -> t.name$().equals(key) || t.label().equals(key)).findFirst().orElse(null);
	}

	private void resetTickets() {
		ChartSpec specification = specification();
		List<Ticket> ticketList = specification.ticketList(XAxisTag);

		this.xAxis = ticketList.size() > 0 ? ticketList.get(0) : null;

		ticketList = specification.ticketList(YAxisTag);
		this.yAxis = ticketList.size() > 0 ? ticketList.get(0) : null;

		ticketList = specification.ticketList(RadiusTag);
		this.radius = ticketList.size() > 0 ? ticketList.get(0) : null;

		ticketList = specification.ticketList(ColorTag);
		this.color = ticketList.size() > 0 ? ticketList.get(0) : null;

		addNotQualifiedTickets();
		refreshTags();
	}

	private void addNotQualifiedTickets() {
		for (Ticket ticket : specification().ticketList()) {
			if (ticket == xAxis || ticket == yAxis || ticket == radius || ticket == color) continue;
			if (xAxis == null) xAxis = ticket;
			else if (yAxis == null) yAxis = ticket;
			else if (radius == null) radius = ticket;
			else if (color == null) color = ticket;
		}
	}

	private void refreshTags() {
		ChartSpecHandler chartSpecHandler = chartSpecHandler();
		if (xAxis != null) chartSpecHandler.setTicketTags(xAxis, singletonList(XAxisTag));
		if (yAxis != null) chartSpecHandler.setTicketTags(yAxis, singletonList(YAxisTag));
		if (radius != null) chartSpecHandler.setTicketTags(radius, singletonList(RadiusTag));
		if (color != null) chartSpecHandler.setTicketTags(color, singletonList(ColorTag));
	}

	private void refreshSpecification(ScatterQuery query) {
		refreshSpecificationTickets(query);
		refreshSpecificationCategorization(query);
		refreshSpecificationHeatMap(query);
	}

	private void refreshSpecificationTickets(ScatterQuery query) {
		xAxis = ticketOf(query.xAxis());
		yAxis = ticketOf(query.yAxis());
		radius = ticketOf(query.radius());
		color = ticketOf(query.color());

		refreshSpecificationTickets();
		sendTickets();
	}

	private void refreshSpecificationTickets() {
		ChartSpecHandler chartSpecHandler = chartSpecHandler();

		chartSpecHandler.clearTicketsTag(XAxisTag);
		chartSpecHandler.clearTicketsTag(YAxisTag);
		chartSpecHandler.clearTicketsTag(RadiusTag);
		chartSpecHandler.clearTicketsTag(ColorTag);

		chartSpecHandler.selectTickets(new LinkedHashMap<Ticket, List<String>>(){{
			if (xAxis != null) put(xAxis, singletonList(XAxisTag));
			if (yAxis != null) put(yAxis, singletonList(YAxisTag));
			if (radius != null) put(radius, singletonList(RadiusTag));
			if (color != null) put(color, singletonList(ColorTag));
		}});

		sendTickets();
	}

	private void refreshSpecificationCategorization(ScatterQuery query) {
		ChartSpecHandler chartSpecHandler = chartSpecHandler();
		Categorization categorization = categorizationOf(query.categorization());

		chartSpecHandler.clearCategorizationsTag(ScatterTag);
		chartSpecHandler.selectCategorizations(singletonMap(categorization, singletonList(ScatterTag)));

		sendCategorization();
	}

	private void refreshSpecificationHeatMap(ScatterQuery query) {
		chartSpecHandler().heatMap(query.heatMap() != null ? new ChartSpec.HeatMap() {
			@Override
			public String minColor() {
				return query.heatMap().min();
			}

			@Override
			public String maxColor() {
				return query.heatMap().max();
			}
		} : null);
	}
}