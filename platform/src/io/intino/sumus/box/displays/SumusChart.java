package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaNavigator;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.exporters.Document;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.schemas.CatalogInstant;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Olap;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class SumusChart<DN extends AlexandriaDisplayNotifier, N extends AlexandriaNavigator> extends SumusDisplay<DN> {
    private final N navigatorDisplay;
    private ChartSpec spec = null;
    private TimeScaleHandler timeScaleHandler;
    private ChartSpecHandler chartSpecHandler;
    private List<Consumer<Boolean>> loadingListeners = new ArrayList<>();
    private List<Consumer<Boolean>> showDialogListeners = new ArrayList<>();
    private List<Consumer<CatalogInstantBlock>> selectListeners = new ArrayList<>();
    private Olap.InstantFormatter formatter;
    private NameSpace nameSpace = null;

    public SumusChart(SumusBox box, N navigatorDisplay) {
        super(box);
        this.navigatorDisplay = navigatorDisplay;
    }

    public void onShowDialog(Consumer<Boolean> listener) {
        this.showDialogListeners.add(listener);
    }

    public void onSelect(Consumer<CatalogInstantBlock> listener) {
        this.selectListeners.add(listener);
    }

    public void nameSpace(NameSpace nameSpace) {
        this.nameSpace = nameSpace;
    }

    public N navigatorDisplay() {
        return navigatorDisplay;
    }

    public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
        this.timeScaleHandler = timeScaleHandler;
    }

    public TimeScaleHandler timeScaleHandler() {
        return this.timeScaleHandler;
    }

    public void chartSpecHandler(ChartSpecHandler chartSpecHandler) {
        this.chartSpecHandler = chartSpecHandler;
        this.spec = this.chartSpecHandler().specification();
    }

    public ChartSpecHandler chartSpecHandler() {
        return this.chartSpecHandler;
    }

    public List<Categorization> categorizations() {
        return chartSpecHandler.olapCategorizations();
    }

    public List<TemporalCategorization> temporalCategorizations() {
        return chartSpecHandler.olapTemporalCategorizations();
    }

    public void onLoading(Consumer<Boolean> listener) {
        loadingListeners.add(listener);
    }

    public void instantFormatter(Olap.InstantFormatter formatter) {
        this.formatter = formatter;
    }

    public ChartSpec specification() {
        return this.spec;
    }

    public void specification(ChartSpec spec) {
        this.spec = spec;
        update();
    }

    public abstract boolean isValidSpecification(ChartSpec spec);

    public abstract void addNavigatorListeners(N display);

    public abstract boolean allowMultipleTickets();

    public abstract boolean allowDrill();

    public abstract Document export(Instant from, Instant to);

    public abstract boolean isShowingEvents();

    public abstract boolean allowTemporalFiltering();

    public abstract Categorization byCategorization();

    @Override
    protected void init() {
        super.init();
        configureNavigatorDisplay();
    }

    protected abstract void update();

    protected NameSpace nameSpace() {
        return this.nameSpace;
    }

    protected List<TimeScale> scales() {
        return timeScaleHandler().availableScales();
    }

    protected Olap.InstantFormatter instantFormatter() {
        return formatter;
    }

    protected void notifyLoading(boolean value) {
        loadingListeners.forEach(l -> l.accept(value));
    }

    protected void configureNavigatorDisplay() {
        navigatorDisplay.timeScaleHandler(timeScaleHandler());
        addNavigatorListeners(navigatorDisplay);
        add(navigatorDisplay);
        navigatorDisplay.personify();
    }

    protected Categorization categorizationOf(String key) {
        return allCategorizations().stream().filter(c -> c.name$().equals(key) || c.label().equals(key)).findFirst().orElse(null);
    }

    protected List<Categorization> allCategorizations() {
        List<Categorization> result = new ArrayList<>();
        result.addAll(categorizations());
        result.addAll(temporalCategorizations());
        return result;
    }

    protected void notifySelectInstant(CatalogInstant instant, List<String> entities) {
        selectListeners.forEach(l -> l.accept(new CatalogInstantBlock() {
            @Override
            public String catalog() {
                return instant.catalog();
            }

            @Override
            public Instant instant() {
                return instant.value();
            }

            @Override
            public List<String> items() {
                return entities;
            }
        }));
    }

    public void showDialog() {
        showDialogListeners.forEach(l -> l.accept(true));
    }

}
