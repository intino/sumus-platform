package io.intino.sumus.box.displays.adapters;

import io.intino.sumus.graph.Olap;
import io.intino.sumus.box.schemas.Formatter;

public class FormatterAdapter {

    public static Formatter adapt(Formatter formatter, Olap.InstantFormatter.Formatter sumusFormatter) {
        return formatter.name(sumusFormatter.scale().name());
    }

}
