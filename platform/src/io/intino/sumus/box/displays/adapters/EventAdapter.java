package io.intino.sumus.box.displays.adapters;

import io.intino.sumus.box.schemas.Event;

import java.net.MalformedURLException;
import java.net.URL;

import static io.intino.konos.alexandria.activity.Asset.toResource;

public class EventAdapter {

    public static Event adapt(Event event, URL baseAssetUrl) {
        if (event.icon() == null) return event;
        return event.icon(toResource(baseAssetUrl, urlOf(event.icon())).toUrl().toString());
    }

    private static URL urlOf(String icon) {
        try {
            return new URL(icon);
        } catch (MalformedURLException e) {
            return null;
        }
    }

}
