package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.Formatter;
import io.intino.sumus.graph.Olap;

import java.util.List;

import static io.intino.sumus.box.displays.adapters.FormatterAdapter.adapt;
import static java.util.stream.Collectors.toList;

public class FormatterBuilder {

    public static Formatter build(Olap.InstantFormatter.Formatter formatter) {
        return new Formatter().name(formatter.name$()).format(formatter.format());
    }

    public static List<Formatter> buildList(List<Olap.InstantFormatter.Formatter> formatterList) {
        return formatterList.stream().map(formatter -> adapt(build(formatter), formatter)).collect(toList());
    }

}
