package io.intino.sumus.box.displays.builders;

import io.intino.sumus.Category;
import io.intino.sumus.box.schemas.Filter;
import io.intino.sumus.box.schemas.FilterCategorization;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FilterBuilder {

    public static Filter build(io.intino.sumus.graph.Filter filter) {
        return new Filter().name(filter.name$()).label(filter.label())
                           .filterCategorizationList(categorizationList(filter.categorizationList()));
    }

    public static List<Filter> buildList(List<io.intino.sumus.graph.Filter> filterList) {
        return filterList.stream().map(FilterBuilder::build).collect(toList());
    }

    private static List<FilterCategorization> categorizationList(List<io.intino.sumus.graph.Filter.Categorization> categorizations) {
        return categorizations.stream().map(c -> new FilterCategorization().name(c.name$()).categories(categories(c.categoryMap().values()))).collect(toList());
    }

    private static List<String> categories(Collection<Category> categories) {
        return categories.stream().map(Category::name).collect(toList());
    }

}
