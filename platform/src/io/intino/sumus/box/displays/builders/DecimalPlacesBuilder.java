package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.DecimalPlaces;

public class DecimalPlacesBuilder {

    public static DecimalPlaces build(io.intino.sumus.graph.Ticket.DecimalPlaces decimalPlaces) {
        return new DecimalPlaces().absolute(decimalPlaces.absolute()).percentage(decimalPlaces.percentage());
    }

}