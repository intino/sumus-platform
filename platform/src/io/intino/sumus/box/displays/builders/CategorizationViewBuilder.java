package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.CategorizationView;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CategorizationViewBuilder {

    public static CategorizationView build(io.intino.sumus.analytics.viewmodels.CategorizationView categorizationView) {
        return new CategorizationView().name(categorizationView.name())
                                       .label(categorizationView.label())
                                       .categoryViewList(CategoryViewBuilder.buildList(categorizationView.categories()));
    }

    public static List<CategorizationView> buildList(List<io.intino.sumus.analytics.viewmodels.CategorizationView> categorizationList) {
        return categorizationList.stream().map(CategorizationViewBuilder::build).collect(toList());
    }

}
