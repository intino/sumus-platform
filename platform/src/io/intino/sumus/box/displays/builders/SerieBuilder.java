package io.intino.sumus.box.displays.builders;

import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.box.schemas.Data;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class SerieBuilder {

    public static io.intino.sumus.box.schemas.Serie build(Serie serie) {
        io.intino.sumus.box.schemas.Serie result = new io.intino.sumus.box.schemas.Serie().label(serie.label()).metric(serie.metric())
                                 .color(serie.color())
                                 .style(serie.style())
                                 .decimalPlaces(DecimalPlacesBuilder.build(serie.decimalPlaces()))
                                 .dataList(dataList(serie.values()));

        if (serie.group() != null)
            result.group(serie.group());

        if (serie.min() != null)
            result.min(serie.min().value());

        if (serie.max() != null)
            result.max(serie.max().value());

        return result;
    }

    public static List<io.intino.sumus.box.schemas.Serie> buildList(List<Serie> serieList) {
        return serieList.stream().map(SerieBuilder::build).collect(toList());
    }

    private static List<Data> dataList(Map<Instant, Double> values) {
        return values.keySet().stream()
                .sorted()
                .map(instant -> data(instant, values.get(instant)))
                .collect(toList());
    }

    private static Data data(Instant instant, Double value) {
        return new Data().created(instant.toEpochMilli()).value( (!value.isNaN()) ? value : 0);
    }

}
