package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.Ticket;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class TicketBuilder {

    public static Ticket build(io.intino.sumus.graph.Ticket ticket) {
        if (ticket == null) return null;
        Ticket result = new Ticket().name(ticket.name$()).label(ticket.label()).shortLabel(ticket.shortLabel());
        result.color(ticket.style().color());
        return result;
    }

    public static List<Ticket> buildList(List<io.intino.sumus.graph.Ticket> ticketList) {
        return ticketList.stream().map(TicketBuilder::build).collect(toList());
    }

}
