package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.Olap;

public class OlapBuilder {

    public static Olap adapt(io.intino.sumus.graph.Olap olap) {
        return new Olap().label(olap.label()).type(olap.getClass().getSimpleName());
    }

}