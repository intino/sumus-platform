package io.intino.sumus.box.displays.builders;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.box.schemas.Points;

import java.util.List;

public class PointsBuilder {

    public static Points build(List<Serie> serieList, TimeScale timeScale, String language) {
        return new Points().serieList(SerieBuilder.buildList(serieList)).scale(ScaleBuilder.build(timeScale, language));
    }

}