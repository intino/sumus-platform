package io.intino.sumus.box.displays.builders;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.analytics.viewmodels.ZoomRange;
import io.intino.sumus.box.schemas.Range;
import io.intino.sumus.graph.Olap;

import java.time.Instant;

public class RangeBuilder {

    public static Range build(Olap.Range range, String username) {
        return new Range().min(range.from(username).toEpochMilli()).max(range.to(username).toEpochMilli());
    }

    public static Range build(TimeRange range) {
        Instant from = range.scale().normalise(range.from());
        Instant to = range.scale().normalise(range.to());
        return new Range().min(from.toEpochMilli()).max(to.toEpochMilli()).scale(range.scale().name());
    }

    public static Range build(ZoomRange range) {
        return new Range().min(range.min).max(range.max);
    }

}
