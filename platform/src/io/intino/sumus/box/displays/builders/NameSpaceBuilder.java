package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.NameSpace;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class NameSpaceBuilder {

    public static NameSpace build(io.intino.sumus.graph.NameSpace nameSpace) {
        return new NameSpace().name(nameSpace.name$()).label(nameSpace.label());
    }

    public static List<NameSpace> buildList(List<io.intino.sumus.graph.NameSpace> nameSpaceList) {
        return nameSpaceList.stream().map(NameSpaceBuilder::build).collect(toList());
    }

}
