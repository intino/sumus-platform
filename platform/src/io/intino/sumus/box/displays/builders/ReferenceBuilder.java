package io.intino.sumus.box.displays.builders;

import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.sumus.box.schemas.Reference;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Filter;
import io.intino.sumus.graph.Olap;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ReferenceBuilder {

    public static <T extends Categorization> Reference build(T categorization) {
        return new Reference().name(categorization.name$()).label(categorization.label());
    }

    public static <T extends Categorization> List<Reference> buildList(List<T> categorizationList) {
        return categorizationList.stream().map(ReferenceBuilder::build).collect(toList());
    }

    public static Reference build(String name, String label) {
        return new Reference().name(name).label(label);
    }

    public static Reference build(Olap olap) {
        return new Reference().name(olap.name$()).label(olap.label());
    }

    public static Reference build(Element element) {
        return new Reference().name(element.name()).label(element.label());
    }

    public static List<Reference> buildOlapList(List<Olap> olapList) {
        return olapList.stream().map(ReferenceBuilder::build).collect(toList());
    }

    public static List<Reference> buildCatalogList(List<Catalog> catalogList) {
        return catalogList.stream().map(ReferenceBuilder::build).collect(toList());
    }

    public static Reference build(Filter filter) {
        return new Reference().name(filter.name$()).label(filter.label());
    }

    public static List<Reference> buildFilterList(List<Filter> filterList) {
        return filterList.stream().map(ReferenceBuilder::build).collect(toList());
    }

}
