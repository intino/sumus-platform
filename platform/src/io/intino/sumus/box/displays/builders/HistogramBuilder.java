package io.intino.sumus.box.displays.builders;

import io.intino.sumus.Category;
import io.intino.sumus.analytics.Scaler;
import io.intino.sumus.analytics.viewmodels.CategoryView;
import io.intino.sumus.analytics.viewmodels.Histogram;
import io.intino.sumus.analytics.viewmodels.Histogram.HistogramIndicator;
import io.intino.sumus.analytics.viewmodels.Histogram.HistogramTicket;
import io.intino.sumus.box.schemas.Indicator;
import io.intino.sumus.box.schemas.Ticket;
import io.intino.sumus.graph.AbstractMetric;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class HistogramBuilder {

    public static io.intino.sumus.box.schemas.Histogram build(Histogram histogram) {
        Map<String, Scaler> scalerMap = new HashMap<>();
        List<Category> categories = histogram.categories();
        return new io.intino.sumus.box.schemas.Histogram().categoryViewList(CategoryViewBuilder.buildList(categories.stream().map(CategoryView::new).collect(toList())))
                .ticketList(histogram.tickets().stream().map(t -> ticket(t, categories, scalerMap)).collect(toList()));
    }

    private static Ticket ticket(HistogramTicket ticket, List<Category> categories, Map<String, Scaler> scalerMap) {
        Scaler scaler = scaler(ticket, scalerMap);
        return new Ticket().name(ticket.name()).label(ticket.label()).shortLabel(ticket.shortLabel())
                .values(scaleValues(ticket.values(), scaler))
                .indicatorList(indicators(ticket, categories, scalerMap))
                .unit(scaler.unitLabel());
    }

    private static List<Indicator> indicators(HistogramTicket ticket, List<Category> categories, Map<String, Scaler> scalerMap) {
        Scaler scaler = scaler(ticket, scalerMap);
        return ticket.indicators().stream().map(i -> indicator(i, scaler, categories)).collect(toList());
    }

    private static Indicator indicator(HistogramIndicator indicator, Scaler scaler, List<Category> categories) {
        return new Indicator().label(indicator.label())
                              .minValue(indicator.minValue())
                              .maxValue(indicator.maxValue())
                              .values(scaleValues(indicator.values(), scaler))
                              .color(indicator.color())
                              .decimalPlaces(DecimalPlacesBuilder.build(indicator.decimalPlaces()));
    }

    private static Scaler scaler(HistogramTicket ticket, Map<String, Scaler> scalerMap) {

        if (!scalerMap.containsKey(ticket.name())) {
            List<HistogramIndicator> indicators = ticket.indicators();
            double max = ticket.maxValue();
            AbstractMetric metric = indicators.get(0).unit().core$().ownerAs(AbstractMetric.class);
            Scaler scaler = metric.scaler().scaler(max, indicators.get(0).unit());
            scalerMap.put(ticket.name(), scaler);
        }

        return scalerMap.get(ticket.name());
    }

    private static List<Double> scaleValues(List<Double> values, Scaler scaler) {
        return values.stream().map(e -> e = scaler.scale(e)).collect(toList());
    }

}