package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.Categorization;
import io.intino.sumus.box.schemas.Category;
import io.intino.sumus.box.schemas.Drill;

import java.util.ArrayList;
import java.util.List;

public class DrillBuilder {

    public static Drill build(io.intino.sumus.analytics.viewmodels.Drill drill) {
        if (drill == null) return null;

        return new Drill().categorization(categorization(drill.categorization))
                .categoryList(build(drill.categories));
    }

    private static Categorization categorization(io.intino.sumus.graph.Categorization categorization) {
        return new Categorization().name(categorization.name$()).label(categorization.label());
    }

    private static List<Category> build(List<io.intino.sumus.Category> categories) {
        List<Category> result = new ArrayList<>();
        categories.forEach(category -> result.add(build(category)));
        return result;
    }

    private static Category build(io.intino.sumus.Category category) {
        return new Category().name(category.name()).label(category.label());
    }
}
