package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.displays.adapters.EventAdapter;
import io.intino.sumus.box.schemas.Event;

import java.net.URL;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class EventBuilder {

    // TODO mario

    public static List<Event> buildList(List<io.intino.sumus.graph.Event> eventList, URL baseAssetUrl) {
        return eventList.stream().map(event -> {
            Event schema = new Event().name(event.name$()).created(event.created());

//            schema.label(event.label());
//
//            URL icon = event.icon();
//            schema.icon(icon != null ? icon.toString() : null);

            return EventAdapter.adapt(schema, baseAssetUrl);
        }).collect(toList());
    }

}
