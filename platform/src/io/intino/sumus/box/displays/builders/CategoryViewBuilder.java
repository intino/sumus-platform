package io.intino.sumus.box.displays.builders;

import io.intino.sumus.box.schemas.CategoryView;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CategoryViewBuilder {

    public static CategoryView build(io.intino.sumus.analytics.viewmodels.CategoryView category) {
        return new CategoryView().name(category.name()).label(category.label());
    }

    public static List<CategoryView> buildList(List<io.intino.sumus.analytics.viewmodels.CategoryView> categoryList) {
        return categoryList.stream().map(CategoryViewBuilder::build).collect(toList());
    }

}
