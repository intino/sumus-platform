package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaTimeNavigator;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.exporters.Document;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.analytics.viewmodels.Histogram;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.queries.HistogramQuery;

import java.time.Instant;
import java.util.List;

public abstract class SumusTimeHistogram<DN extends AlexandriaDisplayNotifier> extends SumusChart<DN, AlexandriaTimeNavigator> {
    private final String tag;

    public SumusTimeHistogram(SumusBox box, String tag) {
        super(box, new AlexandriaTimeNavigator(box));
        this.tag = tag;
    }

    private void refresh(Instant instant) {
        doQuery();
    }

    @Override
    public void addNavigatorListeners(AlexandriaTimeNavigator display) {
        display.onMove(this::refresh);
    }

    @Override
    public boolean allowMultipleTickets() {
        return true;
    }

    @Override
    public boolean allowDrill() {
        return false;
    }

    @Override
    public Document export(Instant from, Instant to) {
        return null;
    }

    @Override
    public boolean isShowingEvents() {
        return false;
    }

    @Override
    public boolean allowTemporalFiltering() {
        return false;
    }

    @Override
    public void update() {
        sendCategorization();
        sendTicketCount();
        doQuery();
    }

    @Override
    public void refresh() {
        super.refresh();
        this.doQuery();
    }

    @Override
    public Categorization byCategorization() {
        return categorization();
    }

    protected abstract void sendCategorization();
    protected abstract void sendTicketCount();
    protected abstract void sendHistogram(Histogram histogram);

    protected void doQuery() {
        Categorization categorization = categorization();
        if (categorization == null) return;
        notifyLoading(true);
        Histogram histogram = queryEngine().histogram(buildQuery());
        sendHistogram(histogram);
        notifyLoading(false);
    }

    protected Categorization categorization() {
        List<Categorization> categorizationList = specification().categorizationList(tag);
        return categorizationList.size() > 0 ? categorizationList.get(0) : null;
    }

    private HistogramQuery buildQuery() {
        ChartSpec specification = specification();
        List<Ticket> tickets = specification.ticketList();
        List<FilterCondition> filters = specification.filterList();
        return new HistogramQuery.Builder().filter(filters)
                                 .<HistogramQuery.Builder>scope(specification.scope())
                                 .addTickets(tickets)
                                 .keys(keys())
                                 .build(nameSpace(), categorization(), range(), null);
    }

    private TimeRange range() {
        TimeScale scale = navigatorDisplay().scale();
        Instant date = navigatorDisplay().date();
        return new TimeRange(date, scale.addTo(date, 1), scale);
    }

}