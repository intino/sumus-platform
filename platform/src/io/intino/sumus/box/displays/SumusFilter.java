package io.intino.sumus.box.displays;

import io.intino.sumus.Category;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.viewmodels.FilterCondition;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.FilterBuilder;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusFilterNotifier;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Filter;
import io.intino.tara.magritte.Layer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class SumusFilter extends SumusCategorizationContainer<SumusFilterNotifier> {
	private Filter filter;
	private final List<Consumer<Filter>> applyListeners = new ArrayList<>();
	private final List<Consumer<Filter>> quitListeners = new ArrayList<>();
	private final List<Consumer<Filter>> changeListeners = new ArrayList<>();
	private final List<Consumer<Filter>> removeListeners = new ArrayList<>();
	private boolean refreshing = false;

	public SumusFilter(SumusBox box) {
		super(box);
	}

	public void filter(Filter filter) {
		this.filter = filter;
	}

	public void apply() {
		applyListeners.forEach(l -> l.accept(filter));
	}

	public void onApply(Consumer<Filter> consumer) {
		applyListeners.add(consumer);
	}

	public void quit() {
		quitListeners.forEach(l -> l.accept(filter));
	}

	public void onQuit(Consumer<Filter> consumer) {
		quitListeners.add(consumer);
	}

	public void remove() {
		filter.core$().delete();
		removeListeners.forEach(l -> l.accept(filter));
	}

	public void onRemove(Consumer<Filter> consumer) {
		removeListeners.add(consumer);
	}

	public void onChange(Consumer<Filter> consumer) {
		changeListeners.add(consumer);
	}

	public List<FilterCondition> conditions() {
		return children(SumusCategorization.class).stream()
				.filter(this::isFilteringByCategorization)
				.filter(SumusCategorization::hasFilterCondition)
				.map(SumusCategorization::asFilterCondition)
				.collect(toList());
	}

	public void clear() {
		filter.categorizationList().clear();
		categorizationDisplays().forEach(SumusCategorization::selectNone);
		sendRefreshFilter();
	}

	@Override
	protected void init() {
		sendRefreshFilter();
		super.init();
	}

	@Override
	protected void sendRefreshCategorizationList(List<Categorization> categorizationList) {
		notifier.refreshCategorizationList(ReferenceBuilder.buildList(categorizationList));
	}

	@Override
	protected void sendRefreshTemporalCategorizationList(List<TemporalCategorization> categorizationList) {
		categorizationList.forEach(c -> {
			SumusCategorization display = categorizationDisplay(c.name$());
			if (display == null) return;
			display.categorization(c);
			display.refresh();
		});
		notifier.refreshTemporalCategorizationList(ReferenceBuilder.buildList(categorizationList));
	}

	@Override
	public void refresh() {
		super.refresh();

		try {
			this.refreshing = true;
			sendRefreshFilter();
			refreshCategorizationSelection();
		} finally {
			this.refreshing = false;
		}
	}

	public void allowTemporalCategorizations(boolean value) {
		if (value)
			notifier.showTemporalCategorizations();
		else
			notifier.hideTemporalCategorizations();
	}

	public void addCategorization(String name) {
		SumusCategorization categorizationDisplay = categorizationDisplay(name);
		categorizationDisplay.selectAll();
	}

	public void removeCategorization(String name) {
		filter.categorizationList().stream().filter(c -> c.name$().equals(name)).findFirst().ifPresent(Layer::delete$);
		filter.save$();
		sendRefreshFilter();
		changeListeners.parallelStream().forEach(c -> c.accept(filter));
	}

	@Override
	protected void addListeners(SumusCategorization categorizationDisplay) {
		String categorization = categorizationDisplay.categorization().name$();
		categorizationDisplay.onSelect((categories) -> notifyFilterChange(categorization, categories));
	}

	private void notifyFilterChange(String categorizationName, List<Category> categories) {
		if (filter == null) return;

		Optional<Filter.Categorization> optionalCategorization = findCategorization(categorizationName);
		Filter.Categorization categorization = optionalCategorization.orElseGet(() -> filter.create(categorizationName).categorization(categorizationName));

		categorization.categoryMap().clear();
		categories.forEach(c -> categorization.categoryMap().put(c.name(), c));

		filter.save$();
		if (!refreshing) changeListeners.parallelStream().forEach(c -> c.accept(filter));
	}

	private void sendRefreshFilter() {
		notifier.refreshFilter(filter != null ? FilterBuilder.build(filter) : null);
	}

	private void refreshCategorizationSelection() {
		children(SumusCategorization.class).parallelStream().forEach(display -> {
			Filter.Categorization categorization = filter != null ? findCategorization(display.categorization().name$()).orElse(null) : null;
			Collection<Category> categories = categorization != null ? categorization.categoryMap().values() : emptyList();

			if (categories.size() > 0)
				display.select(categories.stream().map(Category::name).collect(toList()));
			else
				display.selectNone();
		});
	}

	private Optional<Filter.Categorization> findCategorization(String name) {
		return filter.categorizationList().stream().filter(c -> c.name$().equals(name)).findFirst();
	}

	private boolean isFilteringByCategorization(SumusCategorization display) {
		Categorization categorization = display.categorization();
		return this.filter.categorizationList().stream().filter(c -> c.name$().equals(categorization.name$())).count() > 0;
	}

}