package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeRangeNavigator;
import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.TimeStamp;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.exporters.Document;
import io.intino.sumus.analytics.exporters.XlsxDocument;
import io.intino.sumus.analytics.viewmodels.Serie;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.*;
import io.intino.sumus.box.displays.notifiers.SumusTimeSeriesChartNotifier;
import io.intino.sumus.box.schemas.CatalogInstant;
import io.intino.sumus.box.schemas.RequestRange;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.queries.TimeSeriesQuery;

import java.time.Instant;
import java.util.*;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class SumusTimeSeriesChart extends SumusChart<SumusTimeSeriesChartNotifier, AlexandriaTimeRangeNavigator> {
	private Map<Ticket, List<String>> ticketToSeries = new HashMap<>();
	private boolean shouldClearWidget = false;
	private List<Catalog> catalogs = new ArrayList<>();

	public SumusTimeSeriesChart(SumusBox box) {
		super(box, new AlexandriaTimeRangeNavigator(box));
	}

	@Override
	public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
		super.timeScaleHandler(timeScaleHandler);
		//timeScaleHandler.onNotValidRange(this::refreshChart);
		timeScaleHandler.onRangeChange(this::refreshRange);
		//timeScaleHandler.onScaleChange(this::refreshChart);
	}

	@Override
	public boolean isValidSpecification(ChartSpec spec) {
		return spec.ticketList().size() > 0;
	}

	public void refreshRange(TimeRange timeRange) {
		notifier.timeRange(RangeBuilder.build(timeRange));
	}

	public void refreshChart(TimeRange timeRange) {
		notifyLoading(true);
		shouldClearWidget = true;
		notifier.timeRange(RangeBuilder.build(timeRange));
		notifier.updateScale(ScaleBuilder.build(timeRange.scale(), currentLanguage()));
		specification().ticketList().forEach(this::addSeries);
		shouldClearWidget = false;
		notifyLoading(false);
		if (!isShowingEvents())
			loadMorePointsOnLeftAndRight(tickets());
	}

	public Document export(Instant from, Instant to) {
		TimeRange range = new TimeRange(from, to, timeScaleHandler().range().scale());
		TimeSeriesQuery query = buildQuery(range, tickets());
		return XlsxDocument.export(queryEngine().document(query), currentLanguage());
	}

	public void moveLeft(Long value) {
		TimeRange timeRange = timeScaleHandler().moveLeft(value);
		refresh(timeRange);
	}

	public void moveRight(Long value) {
		TimeRange timeRange = timeScaleHandler().moveRight(value);
		refresh(timeRange);
	}

	public void updateRangeAndScale(RequestRange range) {
		updateRangeAndScale(range.from(), range.to());
	}

	public void updateRangeAndScale(Instant from, Instant to) {
		updateRange(from, to, true);
	}

	public boolean isShowingEvents() {
		return tickets().length != 0 && tickets()[0].range().eventHorizon().ordinal() < timeScaleHandler().range().scale().ordinal();
	}

	@Override
	public boolean allowTemporalFiltering() {
		return false;
	}

	@Override
	public Categorization byCategorization() {
		return null;
	}

	@Override
	protected void init() {
		super.init();
		notifier.olapRange(RangeBuilder.build(timeScaleHandler().boundsRange()));
		notifier.timeRange(RangeBuilder.build(timeScaleHandler().range()));
		notifier.formats(FormatterBuilder.buildList(instantFormatter().formatterList()));
		notifier.updateScale(ScaleBuilder.build(timeScaleHandler().range().scale(), currentLanguage()));
		notifier.refreshCatalogList(ReferenceBuilder.buildCatalogList(catalogs));
	}

	@Override
	protected void update() {
		this.clearTickets();
		timeScaleHandler().resetLoadedPoints();
		notifier.refreshTicketCount(specification().ticketList().size());
		refreshChart(timeScaleHandler().range());
	}

	@Override
	public void addNavigatorListeners(AlexandriaTimeRangeNavigator display) {
		display.onFromChange(this::refreshChart);
		display.onToChange(this::refreshChart);
		display.onMove(this::refresh);
		display.onMoveNext(this::refresh);
		display.onMovePrevious(this::refresh);
	}

	@Override
	public boolean allowMultipleTickets() {
		return true;
	}

	@Override
	public boolean allowDrill() {
		return true;
	}

	@Override
	public void refresh() {
		super.refresh();
		this.refreshChart(timeScaleHandler().range());
	}

	public void showDialog() {
		super.showDialog();
	}

	public void catalogs(List<Catalog> catalogs) {
		this.catalogs = catalogs;
	}

	public void selectCatalogInstant(CatalogInstant instant) {
		List<String> entities = entitiesOf(instant);
		notifySelectInstant(instant, entities);
	}

	private List<String> entitiesOf(CatalogInstant instant) {
		TimeSeriesQuery query = buildQuery(new TimeStamp(instant.value(), timeScaleHandler().range().scale()), tickets());
		return queryEngine().entities(query);
	}

	private void addSeries(Ticket ticket) {
		if (shouldShowEvents(ticket)) {
			showEvents(ticket);
		} else {
			showIndicators(ticket);
		}
	}

	private void loadMorePointsOnLeftAndRight(Ticket... tickets) {
		showMorePoints(timeScaleHandler().leftRange(), tickets);
		showMorePoints(timeScaleHandler().rightRange(), tickets);
	}

	private void showMorePoints(TimeRange range, Ticket... tickets) {
		TimeSeriesQuery query = buildQuery(range, tickets);
		List<Serie> serieList = queryEngine().timeSeries(query);
		notifier.addPoints(PointsBuilder.build(serieList, timeScaleHandler().range().scale(), currentLanguage()));
	}

	private TimeSeriesQuery buildQuery(TimeRange timeRange, Ticket... tickets) {
		return createBuilder(tickets).build(nameSpace(), timeRange);
	}

	private TimeSeriesQuery buildQuery(TimeStamp timeStamp, Ticket... tickets) {
		return createBuilder(tickets).build(nameSpace(), timeStamp);
	}

	private TimeSeriesQuery.Builder createBuilder(Ticket... tickets) {
		TimeSeriesQuery.Builder builder = new TimeSeriesQuery.Builder().addTickets(asList(tickets));
		ChartSpec spec = specification();
		io.intino.sumus.analytics.viewmodels.Drill drill = spec.drill();

		if (drill != null)
			builder.drill(drill.categorization, drill.categories);

		spec.filterList().stream().filter(f -> !(f.categorization instanceof TemporalCategorization)).forEach(f -> builder.filter(f.categorization, f.categories));
		builder.includeGlobalSerie(spec.includeGlobalSerie());
		builder.scope(spec.scope());
		return builder;
	}

	private boolean shouldShowEvents() {
		Ticket[] tickets = tickets();
		if (tickets.length <= 0) return false;
		return shouldShowEvents(tickets[0]);
	}

	private boolean shouldShowEvents(Ticket ticket) {
		Ticket.Range range = ticket.range();
		return range.eventHorizon().ordinal() < timeScaleHandler().range().scale().ordinal();
	}

	private void showEvents(Ticket ticket) {
//		Map<Concept, List<io.intino.sumus.Event>> eventMap = loadEvents(timeScaleHandler().range(), nameSpace(), ticket.events());
//
//		ticketToSeries.put(ticket, Collections.emptyList());
//
//		if (shouldClearWidget) {
//			notifier.clear();
//			shouldClearWidget = false;
//		}
//
//		notifier.refreshEventListCount(eventMap.values().stream().mapToDouble(List::size).sum());
//		eventMap.values().forEach(events -> notifier.refreshEventList(EventBuilder.buildList(events, baseAssetUrl())));
	}

	private void showIndicators(Ticket ticket) {
		List<Serie> serieList = queryEngine().timeSeries(buildQuery(timeScaleHandler().range(), ticket));
		ticketToSeries.put(ticket, serieList.stream().map(Serie::label).collect(toList()));
		if (shouldClearWidget) {
			notifier.clear();
			shouldClearWidget = false;
		}
		notifier.addSeries(SerieBuilder.buildList(serieList));
	}

//	private Map<Concept, List<io.intino.sumus.Event>> loadEvents(TimeRange range, io.intino.sumus.NameSpace nameSpace, List<Concept> events) {
//		return queryEngine().events(new TemporalRecordQuery(events, specification().filterList(), range, nameSpace));
//	}

	private Ticket[] tickets() {
		List<Ticket> result = ticketToSeries.keySet().stream().filter(Objects::nonNull).collect(toList());
		return result.toArray(new Ticket[result.size()]);
	}

	private void updateRange(Instant from, Instant to, boolean updateScaleIfNeeded) {
		timeScaleHandler().updateRange(from, to, updateScaleIfNeeded);
		if (shouldShowEvents()) showEvents(tickets()[0]);
	}

	private void refresh(TimeRange timeRange) {
		if (shouldShowEvents())
			showEvents(tickets()[0]);
		else
			showMorePoints(timeRange, tickets());
	}

	private void removeTicket(Ticket ticket) {
		notifier.removeSeries(ticketToSeries.remove(ticket));
	}

	private void clearTickets() {
		Set<Ticket> tickets = new HashSet<>();
		tickets.addAll(ticketToSeries.keySet());
		tickets.forEach(this::removeTicket);
	}

}