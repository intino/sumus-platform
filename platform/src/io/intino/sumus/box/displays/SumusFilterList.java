package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.services.push.User;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusFilterListNotifier;
import io.intino.sumus.box.schemas.FilterDialog;
import io.intino.sumus.graph.Filter;
import io.intino.sumus.graph.SumusGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toList;

public class SumusFilterList extends SumusDisplay<SumusFilterListNotifier> {
	private List<Filter> filterList = null;
	private List<Consumer<Filter>> selectListeners = new ArrayList<>();
	private List<Consumer<Filter>> applyListeners = new ArrayList<>();
	private List<Consumer<Filter>> quitListeners = new ArrayList<>();
	private List<Consumer<Filter>> removeListeners = new ArrayList<>();
	private Filter selected = null;
	private List<String> appliedList = new ArrayList<>();

	public SumusFilterList(SumusBox box) {
		super(box);
	}

	public void onSelect(Consumer<Filter> listener) {
		this.selectListeners.add(listener);
	}

	public void onApply(Consumer<Filter> listener) {
		this.applyListeners.add(listener);
	}

	public void onQuit(Consumer<Filter> listener) {
		this.quitListeners.add(listener);
	}

	public void onRemoveFilter(Consumer<Filter> listener) {
		this.removeListeners.add(listener);
	}

	public List<Filter> filterList() {
		if (filterList == null)
			filterList = loadFilterList();
		return filterList;
	}

	@Override
	protected void init() {
		super.init();
		sendFilterList();
	}

	@Override
	public void refresh() {
		super.refresh();
		reload();
	}

	public void selectFilter(String name) {
		Filter filter = filterList().stream().filter(f -> f.name$().equals(name)).findFirst().orElse(null);
		if (selected != null && selected == filter) return;
		selected = filter;
		if (filter == null) return;
		notifier.refreshSelected(filter.name$());
		selectListeners.forEach(l -> l.accept(filter));
	}

	public void applyFilter(String name) {
		selectFilter(name);
		Filter filter = filterList().stream().filter(f -> f.name$().equals(name)).findFirst().orElse(null);
		applyListeners.forEach(l -> l.accept(filter));
		appliedList.add(name);
		notifier.refreshApplied(appliedList);
	}

	public void quitFilter(String name) {
		selectFilter(name);
		Filter filter = filterList().stream().filter(f -> f.name$().equals(name)).findFirst().orElse(null);
		quitListeners.forEach(l -> l.accept(filter));
		appliedList.remove(name);
		notifier.refreshApplied(appliedList);
	}

	public void addFilter() {
		SumusGraph sumus = box.graph();
		User user = session().user();
		Filter filter = sumus.create("filters").filter("", user != null ? user.username() : null);
		reload();
		selectFilter(filter.name$());
	}

	public void saveFilter(FilterDialog dialog) {
		Filter filter = filterOf(dialog.filter());
		filter.label(dialog.label());
		filter.save$();
	}

	public void removeFilter(String name) {
		Filter filter = filterOf(name);
		filter.delete$();
		removeListeners.forEach(l -> l.accept(filter));
		reload();
		if (filterList().size() > 0) selectFilter(filterList.get(0).name$());
	}

	private void sendFilterList() {
		notifier.refreshFilterList(ReferenceBuilder.buildFilterList(filterList()));
	}

	private List<Filter> loadFilterList() {
		User user = session().user();
		SumusGraph sumus = box.graph();
		return sumus.filterList().stream().filter(f -> userComposesFilter(user, f)).collect(toList());
	}

	private boolean userComposesFilter(User user, Filter filter) {
		return (user == null && filter.username() == null) || (user != null && user.username().equals(filter.username()));
	}

	private void reload() {
		filterList = null;
		selected = null;
		sendFilterList();
	}

	private Filter filterOf(String filter) {
		return filterList().stream().filter(f -> f.name$().equals(filter)).findFirst().orElse(null);
	}

}