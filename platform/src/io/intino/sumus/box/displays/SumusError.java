package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.notifiers.SumusErrorNotifier;

public class SumusError extends AlexandriaDisplay<SumusErrorNotifier> {
    private SumusBox box;

    public SumusError(SumusBox box) {
        super();
        this.box = box;
    }

}