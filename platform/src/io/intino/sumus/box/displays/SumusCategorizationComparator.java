package io.intino.sumus.box.displays;

import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.analytics.viewmodels.Drill;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.notifiers.SumusCategorizationComparatorNotifier;
import io.intino.sumus.graph.Categorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class SumusCategorizationComparator extends SumusCategorizationContainer<SumusCategorizationComparatorNotifier> {
	private List<BiConsumer<Drill, Boolean>> applyListeners = new ArrayList<>();
	private List<Consumer<Drill>> quitListeners = new ArrayList<>();
	private Drill drill;
	private Boolean includeGlobalSerie = true;
	private Timer executeDrillTimer = null;

	private static final int MaxCategoriesSelection = 5;

	public SumusCategorizationComparator(SumusBox box) {
		super(box);
	}

	@Override
	protected void init() {
		super.init();
		children(SumusCategorization.class).forEach(cd -> cd.maxSelection(MaxCategoriesSelection));
		notifier.refreshIncludeGlobalSerie(includeGlobalSerie);
	}

	@Override
	protected void sendRefreshCategorizationList(List<Categorization> categorizationList) {
		notifier.refreshCategorizationList(ReferenceBuilder.buildList(categorizationList));
	}

	@Override
	protected void sendRefreshTemporalCategorizationList(List<TemporalCategorization> categorizationList) {
	}

	public void apply() {
		applyListeners.forEach(l -> l.accept(clonedDrill(), includeGlobalSerie));
	}

	public void onApply(BiConsumer<Drill, Boolean> consumer) {
		applyListeners.add(consumer);
	}

	public void quit() {
		quitListeners.forEach(l -> l.accept(clonedDrill()));
	}

	public void onQuit(Consumer<Drill> consumer) {
		quitListeners.add(consumer);
	}

	public void includeGlobalSerie(Boolean value) {
		this.includeGlobalSerie = value;
		notifier.refreshIncludeGlobalSerie(includeGlobalSerie);
		applyDelayed();
	}

	@Override
	protected void addListeners(SumusCategorization categorizationDisplay) {
		categorizationDisplay.onSelect((categories) -> updateDrill(categorizationDisplay));
	}

	private void updateDrill(SumusCategorization categorizationDisplay) {
		this.drill = categorizationDisplay.asDrill();
		this.applyDelayed();
	}

	private void applyDelayed() {
		if (this.executeDrillTimer != null)
			this.executeDrillTimer.cancel();

		this.executeDrillTimer = new Timer();
		this.executeDrillTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				if (drill == null)
					quit();
				else
					apply();
				executeDrillTimer = null;
			}
		}, 1000);
	}

	private Drill clonedDrill() {
		if (drill == null) return null;
		return new Drill(drill.categorization, drill.categories);
	}

}