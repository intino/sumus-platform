package io.intino.sumus.box.displays;

import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.builders.HistogramBuilder;
import io.intino.sumus.box.displays.builders.ReferenceBuilder;
import io.intino.sumus.box.displays.builders.TicketBuilder;
import io.intino.sumus.box.displays.notifiers.SumusTimeScatterChartNotifier;
import io.intino.sumus.box.schemas.HeatMap;
import io.intino.sumus.box.schemas.Histogram;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.Ticket;
import io.intino.sumus.helpers.ChartSpec;

import java.util.ArrayList;

public class SumusTimeScatterChart extends SumusTimeHistogram<SumusTimeScatterChartNotifier> {
	public static final String XAxisTag = "xAxis";
	public static final String YAxisTag = "yAxis";
	public static final String RadiusTag = "radius";
	public static final String ColorTag = "color";
	public static final String ScatterTag = "scatter";

	public SumusTimeScatterChart(SumusBox box) {
		super(box, ScatterTag);
	}

	@Override
	public void update() {
		sendTicketList();
		super.update();
	}

	@Override
	public boolean isValidSpecification(ChartSpec spec) {
		return spec.ticketList(XAxisTag).size() == 1 && spec.ticketList(YAxisTag).size() == 1 && categorization() != null;
	}

	@Override
	protected void init() {
		super.init();
		sendTicketList();
	}

	@Override
	protected void sendCategorization() {
		Categorization categorization = categorization();
		notifier.refreshCategorization(categorization != null ? ReferenceBuilder.build(categorization) : null);
	}

	@Override
	protected void sendTicketCount() {
		sendTicketList();
	}

	@Override
	protected void sendHistogram(io.intino.sumus.analytics.viewmodels.Histogram histogram) {
		Histogram histogramSchema = HistogramBuilder.build(histogram);
		ChartSpec.HeatMap heatMap = specification().heatMap();
		histogramSchema.heatMap(heatMap != null ? heatMapOf(heatMap) : null);
		notifier.refreshHistogram(histogramSchema);
	}

	private HeatMap heatMapOf(ChartSpec.HeatMap heatMap) {
		return new HeatMap().min(heatMap.minColor()).max(heatMap.maxColor());
	}

	private void sendTicketList() {
		ChartSpec specification = specification();
		notifier.refreshTicketList(TicketBuilder.buildList(new ArrayList<Ticket>() {{
			add(specification.ticketList(XAxisTag).stream().findFirst().orElse(null));
			add(specification.ticketList(YAxisTag).stream().findFirst().orElse(null));
			add(specification.ticketList(RadiusTag).stream().findFirst().orElse(null));
			add(specification.ticketList(ColorTag).stream().findFirst().orElse(null));
		}}));
	}

	public void showDialog() {
		super.showDialog();
	}
}