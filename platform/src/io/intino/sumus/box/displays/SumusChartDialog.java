package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaNavigator;
import io.intino.konos.alexandria.activity.helpers.TimeScaleHandler;
import io.intino.sumus.analytics.categorization.TemporalCategorization;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.rules.Chart;
import io.intino.sumus.helpers.ChartSpec;
import io.intino.sumus.helpers.ChartSpecHandler;

import java.util.ArrayList;
import java.util.List;

public abstract class SumusChartDialog<DN extends AlexandriaDisplayNotifier, N extends AlexandriaNavigator> extends SumusDisplay<DN> {
    private final N navigatorDisplay;
    private final Chart type;
    private TimeScaleHandler timeScaleHandler;
    private ChartSpecHandler chartSpecHandler;
    private ChartSpec spec = null;

    public SumusChartDialog(SumusBox box, Chart type, N navigatorDisplay) {
        super(box);
        this.navigatorDisplay = navigatorDisplay;
        this.type = type;
    }

    public Chart type() {
        return type;
    }

    public void selectTickets(String[] tickets) {
        chartSpecHandler.selectTickets(tickets);
        chartSpecHandler.update();
    }

    @Override
    protected void init() {
        super.init();
        configureNavigatorDisplay();
    }

    protected void configureNavigatorDisplay() {
        navigatorDisplay.timeScaleHandler(timeScaleHandler());
        addNavigatorListeners(navigatorDisplay);
        addAndPersonify(navigatorDisplay);
    }

    protected TimeScaleHandler timeScaleHandler() {
        return timeScaleHandler;
    }

    public void timeScaleHandler(TimeScaleHandler timeScaleHandler) {
        this.timeScaleHandler = timeScaleHandler;
    }

    protected ChartSpecHandler chartSpecHandler() {
        return chartSpecHandler;
    }

    public void chartSpecHandler(ChartSpecHandler chartSpecHandler) {
        this.chartSpecHandler = chartSpecHandler;
        this.chartSpecHandler.onChange(this::specification);
    }

    protected List<Categorization> categorizations() {
        return chartSpecHandler.olapCategorizations();
    }

    protected List<TemporalCategorization> temporalCategorizations() {
        return chartSpecHandler.olapTemporalCategorizations();
    }

    protected ChartSpec specification() {
        return chartSpecHandler.specification();
    }

    public void specification(ChartSpec spec) {
        this.spec = spec;
        update();
    }

    protected void update() {
        sendTickets();
    }

    protected abstract void sendTickets();
    public abstract void addNavigatorListeners(N display);

    protected Categorization categorizationOf(String key) {
        return allCategorizations().stream().filter(c -> c.name$().equals(key) || c.label().equals(key)).findFirst().orElse(null);
    }

    protected List<Categorization> allCategorizations() {
        List<Categorization> result = new ArrayList<>();
        result.addAll(categorizations());
        result.addAll(temporalCategorizations());
        return result;
    }

}