package io.intino.sumus.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaTimeRangeNavigator;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.notifiers.SumusTimeSeriesChartDialogNotifier;

import static io.intino.sumus.graph.rules.Chart.TimeSeriesChart;

public class SumusTimeSeriesChartDialog extends SumusChartDialog<SumusTimeSeriesChartDialogNotifier, AlexandriaTimeRangeNavigator> {

    public SumusTimeSeriesChartDialog(SumusBox box) {
        super(box, TimeSeriesChart, new AlexandriaTimeRangeNavigator(box));
    }

    @Override
    protected void sendTickets() {
    }

    @Override
    public void addNavigatorListeners(AlexandriaTimeRangeNavigator display) {
        display.onMove(this::query);
        display.onFromChange(this::query);
        display.onToChange(this::query);
        display.onMoveNext(this::query);
        display.onMovePrevious(this::query);
    }

    private void query(TimeRange timeRange) {
        chartSpecHandler().range(timeRange);
        chartSpecHandler().update();
    }

}