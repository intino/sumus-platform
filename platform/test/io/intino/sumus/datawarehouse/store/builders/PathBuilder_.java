package io.intino.sumus.datawarehouse.store.builders;

import io.intino.sumus.TimeStamp;
import io.intino.sumus.datawarehouse.store.PathBuilder;
import io.intino.sumus.graph.Cube;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.helpers.UtcDateTime;
import io.intino.tara.magritte.Concept;
import io.intino.tara.magritte.Graph;
import org.junit.Test;

import java.time.Instant;

import static io.intino.konos.alexandria.activity.model.TimeScale.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PathBuilder_ {

    @Test
    public void digestsFileName() {
        SumusGraph graph = graph();
        NameSpace nameSpace = graph.nameSpace(0);
        Cube cube = graph.cube(0);
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Second)), is("TestNameSpace-digests/TestCube/2012/001/2320/A-00-Second"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Minute)), is("TestNameSpace-digests/TestCube/2012/001/A-2320-Minute"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), FifteenMinutes)), is("TestNameSpace-digests/TestCube/2012/001/B-2315-FifteenMinutes"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Hour)), is("TestNameSpace-digests/TestCube/2012/001/C-23-Hour"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), SixHours)), is("TestNameSpace-digests/TestCube/2012/001/D-18-SixHours"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Day)), is("TestNameSpace-digests/TestCube/2012/A-001-Day"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Week)), is("TestNameSpace-digests/TestCube/2011/B-52-Week"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Month)), is("TestNameSpace-digests/TestCube/2012/C-01-Month"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), QuarterOfYear)), is("TestNameSpace-digests/TestCube/2012/D-01-QuarterOfYear"));
        assertThat(PathBuilder.digestPath(nameSpace, cube, new TimeStamp(instant(), Year)), is("TestNameSpace-digests/TestCube/A-2012-Year"));
    }

    @Test
    public void eventFileNames() {
        SumusGraph graph = graph();
        NameSpace nameSpace = graph.nameSpace(0);
        Concept concept = graph.core$().concept("TestEvent");
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Second)), is("TestNameSpace-events/testevent/2012/001/2320/A-00-Second"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Minute)), is("TestNameSpace-events/testevent/2012/001/A-2320-Minute"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), FifteenMinutes)), is("TestNameSpace-events/testevent/2012/001/B-2315-FifteenMinutes"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Hour)), is("TestNameSpace-events/testevent/2012/001/C-23-Hour"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), SixHours)), is("TestNameSpace-events/testevent/2012/001/D-18-SixHours"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Day)), is("TestNameSpace-events/testevent/2012/A-001-Day"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Week)), is("TestNameSpace-events/testevent/2011/B-52-Week"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Month)), is("TestNameSpace-events/testevent/2012/C-01-Month"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), QuarterOfYear)), is("TestNameSpace-events/testevent/2012/D-01-QuarterOfYear"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Year)), is("TestNameSpace-events/testevent/A-2012-Year"));
    }

    @Test
    public void reportFileNames() {
        SumusGraph graph = graph();
        NameSpace nameSpace = graph.nameSpace(0);
        Concept concept = graph.core$().concept("TestReport");
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Second)), is("TestNameSpace-reports/testreport/2012/001/2320/A-00-Second"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Minute)), is("TestNameSpace-reports/testreport/2012/001/A-2320-Minute"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), FifteenMinutes)), is("TestNameSpace-reports/testreport/2012/001/B-2315-FifteenMinutes"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Hour)), is("TestNameSpace-reports/testreport/2012/001/C-23-Hour"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), SixHours)), is("TestNameSpace-reports/testreport/2012/001/D-18-SixHours"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Day)), is("TestNameSpace-reports/testreport/2012/A-001-Day"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Week)), is("TestNameSpace-reports/testreport/2011/B-52-Week"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Month)), is("TestNameSpace-reports/testreport/2012/C-01-Month"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), QuarterOfYear)), is("TestNameSpace-reports/testreport/2012/D-01-QuarterOfYear"));
        assertThat(PathBuilder.temporalRecordPath(nameSpace, concept, new TimeStamp(instant(), Year)), is("TestNameSpace-reports/testreport/A-2012-Year"));
    }

    public SumusGraph graph() {
        return new Graph().loadStashes("PathTest").as(SumusGraph.class);
    }

    private Instant instant() {
        return UtcDateTime.of(2012, 1, 1, 23, 20, 0).toInstant();
    }
}
