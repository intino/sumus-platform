package io.intino.sumus.analytics.matchers;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import io.intino.sumus.analytics.viewmodels.CategorizationView;
import io.intino.sumus.analytics.viewmodels.CategoryView;

import java.util.List;

public class CategorizationListMatcher extends BaseMatcher<List<CategorizationView>> {

	private final List<CategorizationView> expected;

	public CategorizationListMatcher(List<CategorizationView> expected) {
		this.expected = expected;
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(printCategorizationList(expected));
	}

	@SuppressWarnings({"unchecked", "ConstantConditions"})
	@Override
	public void describeMismatch(Object item, Description description) {
		description.appendText(printCategorizationList((List<CategorizationView>) item));
	}

	@SuppressWarnings({"unchecked", "ConstantConditions"})
	@Override
	public boolean matches(Object o) {
		return listIsExpected((List<CategorizationView>) o);
	}

	private boolean listIsExpected(List<CategorizationView> given) {
		return given.stream().allMatch(this::containsOneInstance);
	}

	private boolean containsOneInstance(CategorizationView categorizationView) {
		return expected.stream()
				.filter(c -> c.label().equals(categorizationView.label()) && categoriesAreEquals(c.categories(), categorizationView.categories()))
				.count() == 1;
	}

	private boolean categoriesAreEquals(List<CategoryView> expected, List<CategoryView> actual) {
		return expected.isEmpty() && actual.isEmpty() || expected.stream().allMatch(c -> isInList(c, actual));
	}

	private boolean isInList(CategoryView category, List<CategoryView> actual) {
		return actual.stream()
				.filter(c -> c.label().equals(category.label()) && categoriesAreEquals(category.subCategories(), c.subCategories()))
				.count() == 1;
	}

	private String printCategorizationList(List<CategorizationView> categorizations) {
		String message = "Categorization list (" + categorizations.size() + ")";
		for (CategorizationView categorization : categorizations) {
			message += "\n\tCategorization \"" + categorization.label() + "\"\n";
			message += "\t\t" + printCategories(categorization.categories(), "\t\t");
		}
		return message;
	}

	private String printCategories(List<CategoryView> categories, String indentation) {
		if (categories.isEmpty()) return "";
		String message = "Categories (" + categories.size() + ")\n";
		for (CategoryView category : categories) {
			message += indentation + "\tCategory " + category.label() + "\n";
		}
		return message;
	}
}
