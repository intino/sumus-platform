package io.intino.sumus.analytics.matchers;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.time.Instant;

public class InstantMatcher extends BaseMatcher<Instant> {

	private final Instant expected;

	public InstantMatcher(Instant expected) {
		this.expected = expected;
	}

	@Override
	public boolean matches(Object o) {
		if (!(o instanceof Instant)) {
			return false;
		}
		return expected.equals(o);
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(printDate(expected));
	}

	@Override
	public void describeMismatch(Object item, Description description) {
		description.appendText(printDate((Instant)item));
	}

	private String printDate(Instant instant) {
		return instant.toString();
	}
}
