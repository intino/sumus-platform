package io.intino.sumus.analytics.matchers;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.analytics.viewmodels.CategorizationView;
import org.hamcrest.Matcher;

import java.time.Instant;
import java.util.List;

public class SumusMatchers {

	public static Matcher<List<CategorizationView>> isCategorizationList(List<CategorizationView> expectedCategorizations) {
		return new CategorizationListMatcher(expectedCategorizations);
	}

	public static Matcher<TimeRange> is(TimeRange expectedTimeRange) {
		return new TimeRangeMatcher(expectedTimeRange);
	}

	public static Matcher<TimeScale> is(TimeScale expectedTimeScale) {
		return new TimeScaleMatcher(expectedTimeScale);
	}

	public static Matcher<Instant> is(Instant expectedInstant) {
		return new InstantMatcher(expectedInstant);
	}
}
