package io.intino.sumus.analytics.matchers;

import io.intino.konos.alexandria.activity.model.TimeRange;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class TimeRangeMatcher extends BaseMatcher<TimeRange> {

	private final TimeRange expected;

	public TimeRangeMatcher(TimeRange expected) {
		this.expected = expected;
	}

	@Override
	public boolean matches(Object o) {
		if (!(o instanceof TimeRange)) {
			return false;
		}
		TimeRange range = (TimeRange) o;
		return range.from().equals(expected.from()) &&
				range.to().equals(expected.to()) &&
				range.scale().equals(expected.scale());
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(printRange(expected));
	}

	@Override
	public void describeMismatch(Object item, Description description) {
		description.appendText(printRange((TimeRange)item));
	}

	private String printRange(TimeRange range) {
		return range.from() + " - " + range.to() + " - " + range.scale();
	}
}
