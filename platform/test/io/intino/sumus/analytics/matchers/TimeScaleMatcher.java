package io.intino.sumus.analytics.matchers;

import io.intino.konos.alexandria.activity.model.TimeScale;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class TimeScaleMatcher extends BaseMatcher<TimeScale> {

	private final TimeScale expected;

	public TimeScaleMatcher(TimeScale expected) {
		this.expected = expected;
	}

	@Override
	public boolean matches(Object o) {
		if (!(o instanceof TimeScale)) {
			return false;
		}
		return ((TimeScale)o).name().equals(expected.name());
	}

	@Override
	public void describeTo(Description description) {
		description.appendText(printScale(expected));
	}

	@Override
	public void describeMismatch(Object item, Description description) {
		description.appendText(printScale((TimeScale)item));
	}

	private String printScale(TimeScale scale) {
		return scale.name();
	}
}
