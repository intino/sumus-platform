package io.intino.sumus.analytics.categorization;

import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.TimeScale;
import org.junit.Test;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

import static io.intino.konos.alexandria.activity.model.TimeScale.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TemporalCategorization_ {

    @Test
    public void should_allow_ten_temporal_categorizations_if_scale_day() throws Exception {
        List<TemporalCategorization> categorizationList = temporalCategorizations();
        List<TemporalCategorization> availableCategorizations = categorizationList.stream().filter(c -> c.isAvailableForScale(Day)).collect(toList());

        assertThat(availableCategorizations.size(), is(10));
        assertThat(availableCategorizations.get(0).name$(), is("YearCategorization"));
        assertThat(availableCategorizations.get(1).name$(), is("QuarterCategorization"));
        assertThat(availableCategorizations.get(2).name$(), is("QuarterOfYearCategorization"));
        assertThat(availableCategorizations.get(3).name$(), is("MonthCategorization"));
        assertThat(availableCategorizations.get(4).name$(), is("MonthOfYearCategorization"));
        assertThat(availableCategorizations.get(5).name$(), is("WeekCategorization"));
        assertThat(availableCategorizations.get(6).name$(), is("WeekOfYearCategorization"));
        assertThat(availableCategorizations.get(7).name$(), is("DayOfWeekCategorization"));
        assertThat(availableCategorizations.get(8).name$(), is("DayOfMonthCategorization"));
        assertThat(availableCategorizations.get(9).name$(), is("DayOfYearCategorization"));
    }

    @Test
    public void should_allow_ten_temporal_categorizations_if_scale_month() throws Exception {
        List<TemporalCategorization> categorizationList = temporalCategorizations();
        List<TemporalCategorization> availableCategorizations = categorizationList.stream().filter(c -> c.isAvailableForScale(Month)).collect(toList());

        assertThat(availableCategorizations.size(), is(5));
        assertThat(availableCategorizations.get(0).name$(), is("YearCategorization"));
        assertThat(availableCategorizations.get(1).name$(), is("QuarterCategorization"));
        assertThat(availableCategorizations.get(2).name$(), is("QuarterOfYearCategorization"));
        assertThat(availableCategorizations.get(3).name$(), is("MonthCategorization"));
        assertThat(availableCategorizations.get(4).name$(), is("MonthOfYearCategorization"));
    }

    @Test
    public void should_allow_ten_temporal_categorizations_if_scale_minutes() throws Exception {
        List<TemporalCategorization> categorizationList = temporalCategorizations();
        List<TemporalCategorization> availableCategorizations = categorizationList.stream().filter(c -> c.isAvailableForScale(Minute)).collect(toList());

        assertThat(availableCategorizations.size(), is(14));
        assertThat(availableCategorizations.get(0).name$(), is("YearCategorization"));
        assertThat(availableCategorizations.get(1).name$(), is("QuarterCategorization"));
        assertThat(availableCategorizations.get(2).name$(), is("QuarterOfYearCategorization"));
        assertThat(availableCategorizations.get(3).name$(), is("MonthCategorization"));
        assertThat(availableCategorizations.get(4).name$(), is("MonthOfYearCategorization"));
        assertThat(availableCategorizations.get(5).name$(), is("WeekCategorization"));
        assertThat(availableCategorizations.get(6).name$(), is("WeekOfYearCategorization"));
        assertThat(availableCategorizations.get(7).name$(), is("DayOfWeekCategorization"));
        assertThat(availableCategorizations.get(8).name$(), is("DayOfMonthCategorization"));
        assertThat(availableCategorizations.get(9).name$(), is("DayOfYearCategorization"));
        assertThat(availableCategorizations.get(10).name$(), is("SixHoursCategorization"));
        assertThat(availableCategorizations.get(11).name$(), is("HourCategorization"));
        assertThat(availableCategorizations.get(12).name$(), is("FifteenMinutesCategorization"));
        assertThat(availableCategorizations.get(13).name$(), is("MinuteCategorization"));
    }

    private List<TemporalCategorization> temporalCategorizations() {
        List<TimeScale> timeScales = asList(Year, QuarterOfYear, Month, Week, Day, SixHours, Hour, FifteenMinutes, Minute, Second);
        return timeScales.stream().map(scale -> temporalCategorization(new TimeRange(Instant.now(), Instant.now(), scale))).flatMap(Collection::stream).collect(toList());
    }

    private List<TemporalCategorization> temporalCategorization(TimeRange range) {
        return singletonList(new TemporalCategorizationFactory().get(range));
    }

}