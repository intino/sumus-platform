package io.intino.sumus.test.graph;

import io.intino.tara.magritte.Graph;

public class SumusGraph extends io.intino.sumus.test.graph.AbstractGraph {

	public SumusGraph(Graph graph) {
		super(graph);
	}

	public SumusGraph(io.intino.tara.magritte.Graph graph, SumusGraph wrapper) {
	    super(graph, wrapper);
	}
}