package io.intino.sumus.rules;

import io.intino.konos.alexandria.activity.model.TimeScale;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static io.intino.sumus.analytics.matchers.SumusMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TimeScale_ {

    @Test
    public void should_have_correct_week() throws Exception {
        TimeScale scale = TimeScale.Week;
        assertThat(scale.normalise(instant(2016, 1, 2)), is(instant(2015, 12, 28)));
        assertThat(scale.normalise(instant(2016, 1, 6)), is(instant(2016, 1, 4)));
        assertThat(scale.normalise(instant(2017, 12, 26)), is(instant(2017, 12, 25)));
    }

    private Instant instant(int year, int month, int day) {
        return LocalDateTime.of(year, month, day, 0, 0).toInstant(ZoneOffset.UTC);
    }
}