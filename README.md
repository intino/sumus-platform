# Sumus #

Sumus is a framework for building business intelligence solutions based on Model Driven Engineering (MDE). Sumus is supported by [Tara](https://bitbucket.org/siani/tara), a framework that speeds up MDE developments.

Sumus is released under [GPL v3.0](http://www.gnu.org/licenses/gpl-3.0.en.html).