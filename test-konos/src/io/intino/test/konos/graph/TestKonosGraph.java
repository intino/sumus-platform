package io.intino.test.konos.graph;

import io.intino.tara.magritte.Graph;

public class TestKonosGraph extends io.intino.test.konos.graph.AbstractGraph {

	public TestKonosGraph(Graph graph) {
		super(graph);
	}

	public TestKonosGraph(io.intino.tara.magritte.Graph graph, TestKonosGraph wrapper) {
	    super(graph, wrapper);
	}
}