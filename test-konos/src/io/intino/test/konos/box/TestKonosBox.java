package io.intino.test.konos.box;

import io.intino.tara.magritte.Graph;
import io.intino.test.konos.graph.TestKonosGraph;

public class TestKonosBox extends AbstractBox {
	private TestKonosGraph graph;

	public TestKonosBox(String[] args) {
		super(args);
	}

	public TestKonosBox(TestKonosConfiguration configuration) {
		super(configuration);
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		if (o instanceof Graph) this.graph = ((Graph) o).as(TestKonosGraph.class);
		super.put(o);
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		return super.open();
	}

	public TestKonosGraph graph() {
		return graph;
	}

	public void close() {
		super.close();
	}
}