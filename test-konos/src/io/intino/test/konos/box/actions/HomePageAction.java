package io.intino.test.konos.box.actions;

import io.intino.konos.alexandria.activity.spark.actions.AlexandriaPageAction;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.TestHome;

public class HomePageAction extends AlexandriaPageAction {

	public TestKonosBox box;


	public HomePageAction() { super("test-konos"); }

	public String execute() {
		return super.template("homePage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
	    return new io.intino.konos.alexandria.activity.displays.Soul(session) {
			@Override
			public void personify() {
				TestHome display = new TestHome(box);
				register(display);
				display.personify();
			}
		};
	}

	@Override
	protected String title() {
		return "";
	}

	@Override
	protected java.net.URL favicon() {
		return null;
	}
}