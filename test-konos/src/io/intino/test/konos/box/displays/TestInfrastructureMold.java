package io.intino.test.konos.box.displays;

import io.intino.test.konos.box.TestKonosBox;

public class TestInfrastructureMold extends AbstractTestInfrastructureMold {

	public TestInfrastructureMold(TestKonosBox box) {
		super(box);
	}
	public static class Stamps {

		public static class Breadcrumbs {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}
		}
		public static class CatalogLink {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}


		}
		public static class Display {



			public static io.intino.konos.alexandria.activity.displays.AlexandriaStamp buildDisplay(TestKonosBox box, String name, String username) {
				return null;//TODO
			}
		}
		public static class EmbeddedCatalog {




		}
		public static class Highlight {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}

			public static String color(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return "";//TODO
			}
		}
		public static class Icon {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}

			public static String title(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return "";//TODO
			}
		}
		public static class ItemLinks {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}

			public static String title(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return "";//TODO
			}
		}
		public static class Coordinates {

			public static String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return station.coordinates();//TODO
			}

			public static java.net.URL icon(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return station.icon();
			}
		}


		public static class Label {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return station.label();
			}
		}
		public static class Description {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}
		}

		public static class Picture {

			public static java.util.List<java.net.URL> value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}
		}
		public static class Sequencia {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}
		}

		public static class Dummy {

			public static java.lang.String value(TestKonosBox box, io.intino.test.konos.graph.Station station, String username) {
				return null;//TODO
			}
		}

	}
}