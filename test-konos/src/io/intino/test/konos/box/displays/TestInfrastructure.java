package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.Resource;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Group;
import io.intino.konos.alexandria.activity.model.toolbar.TaskSelection;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.displays.SumusOlap;
import io.intino.sumus.graph.Entity;
import io.intino.sumus.helpers.ClusterHelper;
import io.intino.tara.magritte.Layer;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.graph.Station;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;

import static io.intino.sumus.box.SumusDisplayHelper.queryEngine;
import static io.intino.sumus.box.SumusDisplayHelper.scopeOf;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class TestInfrastructure extends AbstractTestInfrastructure {

	public TestInfrastructure(TestKonosBox box) {
		super(box);
	}

	public static class Toolbar {
		public static TaskSelection.Refresh taskSelection(TestKonosBox box, Element element, String option, List<Station> selection, String username) {
			return TaskSelection.Refresh.Catalog;
		}

		public static Resource download(TestKonosBox box, Element element, String option, String username) {
			return emptyResource("download.pdf");
		}

		public static Resource downloadSelection(TestKonosBox box, Element element, String option, List<Station> selection, String username) {
			return emptyResource("assets-info.pdf");
		}

		public static Resource export(TestKonosBox box, Element element, Instant from, Instant to, String username) {
			return emptyResource("export.pdf");
		}

		public static Resource exportSelection(TestKonosBox box, Element element, Instant from, Instant to, List<Station> selection, String username) {
			return emptyResource("export-selection.pdf");
		}

	}

	public static class Source {
		public static List<Station> stationList(TestKonosBox box, Scope scope, String condition, String username) {
			return box.graph().stationList();
		}

		public static Station station(TestKonosBox box, String id, String username) {
			return box.graph().stationList().get(0);
		}

		public static Station rootStation(TestKonosBox box, List<Station> stationList, String username) {
			return null;
		}

		public static Station defaultStation(TestKonosBox box, String id, String username) {
			return null;
		}

		public static String stationId(TestKonosBox box, Station station) {
			return station.core$().id();
		}

		public static String stationName(TestKonosBox box, Station station) {
			return station.name$();
		}
	}

	public static class Events {
		public static String onOpenDialogPath(TestKonosBox box, String record) {
			return "";
		}
	}

	public static class Views {
		public static AlexandriaDisplay olapDisplay(TestKonosBox box, io.intino.konos.alexandria.activity.model.Element context, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener, String username) {
			SumusOlap display = new SumusOlap(sumusBox(box));
			display.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox(box)));
			display.queryEngine(queryEngine(sumusBox(box), username));
			display.olap(box.graph().sampleOlap());
			display.onLoading(loadingListener);
			display.onSelect(instantListener::accept);
			return display;
		}

		public static void olapDisplayScope(TestKonosBox box, AlexandriaDisplay display, Scope scope) {
			((SumusOlap)display).scope(scopeOf(scope));
		}
	}

	public static class Arrangements {
		public static List<Group> units(TestKonosBox box, List<Station> items, String username) {
			return emptyList();
		}

		public static List<Group> status(TestKonosBox box, List<Station> items, String username) {
			return emptyList();
		}

		public static int dateComparator(Station item1, Station item2) {
			return 0;
		}

		public static void createGroup(TestKonosBox box, Catalog infrastructure, String grouping, Group group, String username) {
			SumusBox sumusBox = sumusBox(box);
			List<Entity> entities = group.objects().stream().map(s -> ((Layer)s).a$(Entity.class)).collect(toList());
			ClusterHelper.registerClusterGroup(sumusBox, infrastructure, grouping, group.label(), entities, username);
		}

		public static Catalog.ArrangementFilterer filterer(TestKonosBox box, String username) {
			return SumusDisplayHelper.createArrangementFilterer(username);
		}
	}

	private static SumusBox sumusBox(TestKonosBox box) {
		return (SumusBox)box.owner();
	}

	private static Resource emptyResource(String label) {
		return new Resource() {
			@Override
			public String label() {
				return label;
			}

			@Override
			public InputStream content() {
				return new ByteArrayInputStream(new byte[0]);
			}
		};
	}
}