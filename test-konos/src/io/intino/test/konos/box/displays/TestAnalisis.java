package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.SumusOlap;
import io.intino.test.konos.box.TestKonosBox;

import java.util.function.Consumer;

public class TestAnalisis extends AbstractTestAnalisis {

	public TestAnalisis(TestKonosBox box) {
		super(box);
	}

	public static class Views {
		public static AlexandriaDisplay olapDisplay(TestKonosBox box, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener) {
			SumusOlap display = new SumusOlap(sumusBox(box));
			display.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox(box)));
			display.olap(box.graph().sampleOlap());
			display.onLoading(loadingListener);
			display.onSelect(instantListener::accept);
			return display;
		}
	}

	private static SumusBox sumusBox(TestKonosBox box) {
		return (SumusBox) box.owner();
	}

}