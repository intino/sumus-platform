package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaLayout;
import io.intino.test.konos.box.TestKonosBox;

public class TestHome extends AbstractTestHome {

    public TestHome(TestKonosBox box) {
        super(box);
    }

    public static AlexandriaLayout layout(TestKonosBox box) {
        return new TestMenu(box);
    }

}