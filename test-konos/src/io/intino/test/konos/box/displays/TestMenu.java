package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.displays.SumusNameSpace;
import io.intino.test.konos.box.TestKonosBox;

public class TestMenu extends AbstractTestMenu {

	public TestMenu(TestKonosBox box) {
		super(box);
	}

	@Override
	protected void init() {
		super.init();
		SumusNameSpace nameSpaceDisplay = new SumusNameSpace(sumusBox());
		nameSpaceDisplay.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox()));
		addAndPersonify(nameSpaceDisplay);
	}

	private SumusBox sumusBox() {
		return (SumusBox) box.owner();
	}

	public static class Ddd {
		public static class Infrastructure {
			public static boolean filter(TestKonosBox box, Catalog catalog, Element context, Object target, Object object, String username) {
				return false;
			}
		}
	}
}