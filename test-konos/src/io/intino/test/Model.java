package io.intino.test;

import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.graph.NameSpace;
import io.intino.test.konos.graph.TestKonosGraph;

import java.util.Collections;
import java.util.List;

public class Model {
	public static List<NameSpace> sampleOlapNameSpace(io.intino.sumus.graph.Olap self, String username) {
		TestKonosGraph graph = self.graph().core$().as(TestKonosGraph.class);
		return Collections.singletonList(graph.defaultNameSpace());
	}

	public static io.intino.konos.alexandria.activity.model.TimeScale day(io.intino.sumus.graph.Olap.Select self) {
		return TimeScale.Day;
	}
}
