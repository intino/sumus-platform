package io.intino.test.konos.graph;

import io.intino.test.konos.graph.*;


public class Station extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected java.lang.String label;
	protected java.net.URL icon;
	protected java.lang.String coordinates;

	public Station(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public java.lang.String label() {
		return label;
	}

	public java.net.URL icon() {
		return icon;
	}

	public java.lang.String coordinates() {
		return coordinates;
	}

	public Station label(java.lang.String value) {
		this.label = value;
		return (Station) this;
	}

	public Station icon(java.net.URL url, String destiny) {
		this.icon = graph().core$().save(url, destiny, this.icon, core$());
		return (Station) this;
	}

	public Station icon(java.io.InputStream stream, String destiny) {
		this.icon = graph().core$().save(stream, destiny, this.icon, core$());
		return (Station) this;
	}

	public Station coordinates(java.lang.String value) {
		this.coordinates = value;
		return (Station) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		map.put("icon", new java.util.ArrayList(java.util.Collections.singletonList(this.icon)));
		map.put("coordinates", new java.util.ArrayList(java.util.Collections.singletonList(this.coordinates)));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("icon")) this.icon = io.intino.tara.magritte.loaders.ResourceLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("coordinates")) this.coordinates = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
		else if (name.equalsIgnoreCase("icon")) this.icon = (java.net.URL) values.get(0);
		else if (name.equalsIgnoreCase("coordinates")) this.coordinates = (java.lang.String) values.get(0);
	}


	public io.intino.test.konos.graph.TestKonosGraph graph() {
		return (io.intino.test.konos.graph.TestKonosGraph) core$().graph().as(io.intino.test.konos.graph.TestKonosGraph.class);
	}
}
