package io.intino.test.konos.graph;

import io.intino.tara.magritte.Graph;

public class AbstractGraph extends io.intino.tara.magritte.GraphWrapper {

	protected io.intino.tara.magritte.Graph graph;
	private java.util.List<io.intino.test.konos.graph.Station> stationList;
	private io.intino.sumus.graph.NameSpace defaultNameSpace;
	private io.intino.sumus.graph.Olap sampleOlap;

	public AbstractGraph(io.intino.tara.magritte.Graph graph) {
		this.graph = graph;
		this.graph.i18n().register("TestKonos");
	}

	public AbstractGraph(io.intino.tara.magritte.Graph graph, AbstractGraph wrapper) {
		this.graph = graph;
		this.graph.i18n().register("TestKonos");
		this.stationList = new java.util.ArrayList<>(wrapper.stationList);
		this.defaultNameSpace = wrapper.defaultNameSpace;
		this.sampleOlap = wrapper.sampleOlap;
	}

    @Override
	public void update() {
		stationList = this.graph.rootList(io.intino.test.konos.graph.Station.class);
		io.intino.tara.magritte.Node defaultNameSpaceNode = this.graph.load("TestKonos#defaultNameSpace");
		if(defaultNameSpaceNode != null) this.defaultNameSpace = defaultNameSpaceNode.as(io.intino.sumus.graph.NameSpace.class);
		io.intino.tara.magritte.Node sampleOlapNode = this.graph.load("TestKonos#sampleOlap");
		if(sampleOlapNode != null) this.sampleOlap = sampleOlapNode.as(io.intino.sumus.graph.Olap.class);
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		if (node.is("Station")) this.stationList.add(node.as(io.intino.test.konos.graph.Station.class));
		if(node.id().equals("TestKonos#defaultNameSpace")) this.defaultNameSpace = node.as(io.intino.sumus.graph.NameSpace.class);
		if(node.id().equals("TestKonos#sampleOlap")) this.sampleOlap = node.as(io.intino.sumus.graph.Olap.class);
	}

	@Override
	protected void removeNode$(io.intino.tara.magritte.Node node) {
		if (node.is("Station")) this.stationList.remove(node.as(io.intino.test.konos.graph.Station.class));
		if(node.id().equals("TestKonos#defaultNameSpace")) this.defaultNameSpace = null;
		if(node.id().equals("TestKonos#sampleOlap")) this.sampleOlap = null;
	}

	public java.net.URL resourceAsMessage$(String language, String key) {
		return graph.loadResource(graph.i18n().message(language, key));
	}

	public java.util.List<io.intino.test.konos.graph.Station> stationList() {
		return stationList;
	}

	public io.intino.sumus.graph.NameSpace defaultNameSpace() {
		return defaultNameSpace;
	}

	public io.intino.sumus.graph.Olap sampleOlap() {
		return sampleOlap;
	}

	public java.util.stream.Stream<io.intino.test.konos.graph.Station> stationList(java.util.function.Predicate<io.intino.test.konos.graph.Station> filter) {
		return stationList.stream().filter(filter);
	}

	public io.intino.test.konos.graph.Station station(int index) {
		return stationList.get(index);
	}

	public io.intino.tara.magritte.Graph core$() {
		return graph;
	}

	public io.intino.tara.magritte.utils.I18n i18n$() {
		return graph.i18n();
	}

	public Create create() {
		return new Create("Misc", null);
	}

	public Create create(String stash) {
		return new Create(stash, null);
	}

	public Create create(String stash, String name) {
		return new Create(stash, name);
	}

	public Clear clear() {
		return new Clear();
	}

	public class Create {
		private final String stash;
		private final String name;

		public Create(String stash, String name) {
			this.stash = stash;
			this.name = name;
		}

		public io.intino.test.konos.graph.Station station(java.lang.String label, java.net.URL icon, java.lang.String coordinates) {
			io.intino.test.konos.graph.Station newElement = AbstractGraph.this.graph.createRoot(io.intino.test.konos.graph.Station.class, stash, name).a$(io.intino.test.konos.graph.Station.class);
			newElement.core$().set(newElement, "label", java.util.Collections.singletonList(label));
			newElement.core$().set(newElement, "icon", java.util.Collections.singletonList(icon));
			newElement.core$().set(newElement, "coordinates", java.util.Collections.singletonList(coordinates));
			return newElement;
		}
	}

	public class Clear {
	    public void station(java.util.function.Predicate<io.intino.test.konos.graph.Station> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.stationList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }
	}
}