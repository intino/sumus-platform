package io.intino.test.konos.graph.natives.sampleolap;

import java.util.List;
import io.intino.sumus.graph.NameSpace;

/**sampleOlap#/Users/mcaballero/Proyectos/sumus-platform/test-konos/src/io/intino/test/Model.tara#10#17**/
public class NameSpaces_0 implements io.intino.sumus.graph.functions.NameSpacesLoader, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Olap self;

	@Override
	public List<NameSpace> nameSpaces(String username) {
		return io.intino.test.Model.sampleOlapNameSpace(self, username);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Olap) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Olap.class;
	}
}