package io.intino.test.konos.box;

public class TestKonosConfiguration extends io.intino.sumus.box.SumusConfiguration {

	TestKonosActivityConfiguration testKonosConfiguration;

	public TestKonosConfiguration(String[] args) {
		super(args);
		fillWithArgs();
	}

	private void fillWithArgs() {
		if (this.store == null && args.get("graph_store") != null)
			store = new java.io.File(args.remove("graph_store"));
		if (args.containsKey("testKonos_port")) {
			testKonosConfiguration(toInt(args.remove("testKonos_port")), args.remove("testKonos_webDirectory"));

		}
	}

	public java.io.File store() {
		return this.store;
	}

	public TestKonosConfiguration testKonosConfiguration(int port, String webDirectory) {
		this.testKonosConfiguration = new TestKonosActivityConfiguration();
		this.testKonosConfiguration.port = port;
		this.testKonosConfiguration.webDirectory = webDirectory == null ? "www/" : webDirectory;

		return this;
	}

	public TestKonosConfiguration testKonosConfiguration(int port) {
		return testKonosConfiguration(port, "www/");
	}

	public TestKonosActivityConfiguration testKonosConfiguration() {
		return testKonosConfiguration;
	}

	public static class TestKonosActivityConfiguration {
		public int port;
		public String webDirectory;
		public io.intino.konos.alexandria.activity.services.AuthService authService;



	}
}