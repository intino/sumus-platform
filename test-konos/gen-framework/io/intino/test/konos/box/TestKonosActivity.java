package io.intino.test.konos.box;

import io.intino.konos.alexandria.activity.ActivityAlexandriaSpark;
import io.intino.konos.alexandria.activity.services.push.PushService;
import io.intino.konos.alexandria.activity.spark.resources.AfterDisplayRequest;
import io.intino.konos.alexandria.activity.spark.resources.AssetResource;
import io.intino.konos.alexandria.activity.spark.resources.AuthenticateCallbackResource;
import io.intino.konos.alexandria.activity.spark.resources.BeforeDisplayRequest;
import io.intino.test.konos.box.displays.TestHomeDisplay;
import io.intino.test.konos.box.displays.TestInfrastructureDisplay;
import io.intino.test.konos.box.displays.TestInfrastructureMoldDisplay;
import io.intino.test.konos.box.displays.TestMenuDisplay;
import io.intino.test.konos.box.displays.notifiers.TestHomeDisplayNotifier;
import io.intino.test.konos.box.displays.notifiers.TestInfrastructureDisplayNotifier;
import io.intino.test.konos.box.displays.notifiers.TestInfrastructureMoldDisplayNotifier;
import io.intino.test.konos.box.displays.notifiers.TestMenuDisplayNotifier;
import io.intino.test.konos.box.displays.requesters.TestHomeDisplayRequester;
import io.intino.test.konos.box.displays.requesters.TestInfrastructureDisplayRequester;
import io.intino.test.konos.box.displays.requesters.TestInfrastructureMoldDisplayRequester;
import io.intino.test.konos.box.displays.requesters.TestMenuDisplayRequester;
import io.intino.test.konos.box.resources.HomePageResource;

public class TestKonosActivity extends io.intino.konos.alexandria.activity.Activity {

	public static ActivityAlexandriaSpark init(ActivityAlexandriaSpark spark, TestKonosBox box) {
		TestKonosConfiguration.TestKonosActivityConfiguration configuration = box.configuration().testKonosConfiguration;
		spark.route("/push").push(new PushService());
		spark.route("/authenticate-callback").get(manager -> new AuthenticateCallbackResource(manager, notifierProvider()).execute());
		spark.route("/asset/:name").get(manager -> new AssetResource(name -> new AssetResourceLoader(box).load(name), manager, notifierProvider()).execute());

		spark.route("").get(manager -> new HomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/").get(manager -> new HomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/index").get(manager -> new HomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/home").get(manager -> new HomePageResource(box, manager, notifierProvider()).execute());

		spark.route("/testhome/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testhome/:displayId").post(manager -> new TestHomeDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testhome/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testmenu/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testmenu/:displayId").post(manager -> new TestMenuDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testmenu/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testinfrastructure/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testinfrastructure/:displayId").post(manager -> new TestInfrastructureDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testinfrastructure/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testinfrastructuremold/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testinfrastructuremold/:displayId").post(manager -> new TestInfrastructureMoldDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testinfrastructuremold/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		registerNotifiers();
		return spark;
	}

	private static void registerNotifiers() {
		register(TestHomeDisplayNotifier.class).forDisplay(TestHomeDisplay.class);
		register(TestMenuDisplayNotifier.class).forDisplay(TestMenuDisplay.class);
		register(TestInfrastructureDisplayNotifier.class).forDisplay(TestInfrastructureDisplay.class);
		register(TestInfrastructureMoldDisplayNotifier.class).forDisplay(TestInfrastructureMoldDisplay.class);
	}
}