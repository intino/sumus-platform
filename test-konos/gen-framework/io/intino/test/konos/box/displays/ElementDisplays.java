package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.*;
import io.intino.konos.alexandria.activity.model.*;
import io.intino.test.konos.box.TestKonosBox;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ElementDisplays {
	private static Map<String, Class<? extends AlexandriaElementDisplay>> elementMap = new HashMap<>();

	public static void init(TestKonosBox box) {
		elementMap.put("testHome", TestHomeDisplay.class);
		elementMap.put("testMenu", TestMenuDisplay.class);
		elementMap.put("testInfrastructure", TestInfrastructureDisplay.class);
		elementMap.put("testInfrastructureMold", TestInfrastructureMoldDisplay.class);
		elementMap.put("testAnalisis", TestAnalisisDisplay.class);
	}

	public static AlexandriaElementDisplay displayFor(TestKonosBox box, String name) {
		if (!elementMap.containsKey(name)) return null;
		return displayFor(box, elementMap.get(name));
	}

	public static AlexandriaElementDisplay displayFor(TestKonosBox box, Element element) {
		if (!elementMap.containsKey(element.name())) return defaultElementDisplay(box, element);
		return displayFor(box, elementMap.get(element.name()));
	}

	public static Class<? extends AlexandriaElementDisplay> displayTypeFor(TestKonosBox box, Element element) {
		if (!elementMap.containsKey(element.name())) return defaultElementType(box, element);
		return elementMap.get(element.name());
	}

	private static AlexandriaElementDisplay displayFor(TestKonosBox box, Class<? extends AlexandriaElementDisplay> aClass) {
		Constructor<? extends AlexandriaElementDisplay> constructor = null;
		try {
			constructor = aClass.getConstructor(TestKonosBox.class);
			return constructor.newInstance(box);
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Class<? extends AlexandriaElementDisplay> defaultElementType(TestKonosBox box, Element element) {
		if (element instanceof Panel) return AlexandriaPanelDisplay.class;

		if (element instanceof TemporalCatalog)
			return ((TemporalCatalog)element).type() == TemporalCatalog.Type.Range ? AlexandriaTemporalRangeCatalogDisplay.class : AlexandriaTemporalTimeCatalogDisplay.class;

		if (element instanceof Catalog)
			return AlexandriaCatalogDisplay.class;

		if (element instanceof Layout)
			return ((Layout)element).mode() == Layout.Mode.Menu ? AlexandriaMenuLayoutDisplay.class : AlexandriaTabLayoutDisplay.class;

		if (element instanceof Desktop)
			return AlexandriaDesktopDisplay.class;

		return null;
	}

	private static AlexandriaElementDisplay defaultElementDisplay(TestKonosBox box, Element element) {
		if (element instanceof Panel) return new AlexandriaPanelDisplay(box);

		if (element instanceof TemporalCatalog)
			return ((TemporalCatalog)element).type() == TemporalCatalog.Type.Range ? new AlexandriaTemporalRangeCatalogDisplay(box) : new AlexandriaTemporalTimeCatalogDisplay(box);

		if (element instanceof Catalog)
			return new AlexandriaCatalogDisplay(box);

		if (element instanceof Layout)
			return ((Layout)element).mode() == Layout.Mode.Menu ? new AlexandriaMenuLayoutDisplay(box) : new AlexandriaTabLayoutDisplay(box);

		if (element instanceof Desktop)
			return new AlexandriaDesktopDisplay(box);

		return null;
	}

}
