package io.intino.test.konos.box.displays.requesters;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaCatalogDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;
import io.intino.test.konos.box.displays.TestInfrastructureDisplay;

public class TestInfrastructureDisplayRequester extends AlexandriaCatalogDisplayRequester {

	public TestInfrastructureDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestInfrastructureDisplay display = display();
		if (display == null) return;
		String operation = operation();
	}
}