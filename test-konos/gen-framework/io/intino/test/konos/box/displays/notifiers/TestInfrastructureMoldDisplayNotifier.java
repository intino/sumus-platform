package io.intino.test.konos.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaMoldDisplayNotifier;

public class TestInfrastructureMoldDisplayNotifier extends AlexandriaMoldDisplayNotifier {

	public TestInfrastructureMoldDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}