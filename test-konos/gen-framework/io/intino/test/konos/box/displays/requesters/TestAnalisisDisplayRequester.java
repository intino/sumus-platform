package io.intino.test.konos.box.displays.requesters;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaPanelDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;
import io.intino.test.konos.box.displays.TestAnalisisDisplay;

public class TestAnalisisDisplayRequester extends AlexandriaPanelDisplayRequester {

	public TestAnalisisDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestAnalisisDisplay display = display();
		if (display == null) return;
		String operation = operation();
	}
}