package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaPanelDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.model.AbstractView;
import io.intino.konos.alexandria.activity.model.Panel;
import io.intino.konos.alexandria.activity.model.Toolbar;
import io.intino.konos.alexandria.activity.model.panel.View;
import io.intino.konos.alexandria.activity.model.renders.RenderDisplay;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.notifiers.TestAnalisisDisplayNotifier;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class AbstractTestAnalisisDisplay extends AlexandriaPanelDisplay<TestAnalisisDisplayNotifier> {

	public AbstractTestAnalisisDisplay(TestKonosBox box) {
		super(box);
		element(buildPanel(box));
	}

	private static Panel buildPanel(TestKonosBox box) {
		Panel panel = new Panel();
		panel.name("testAnalisis");
		panel.label("Análisis");
		panel.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> panel.add(v));
		return panel;
	}

	private static Toolbar buildToolbar(TestKonosBox box) {
		Toolbar toolbar = new Toolbar();
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestKonosBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new View().render(new RenderDisplay().displayLoader(new RenderDisplay.DisplayLoader() {
			@Override
			public AlexandriaDisplay load(Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener) {
				return TestAnalisisDisplay.Views.olapDisplay(box, loadingListener, instantListener);
			}
		})).name("v1").label("Charts"));
		return result;
	}

}
