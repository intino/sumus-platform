package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaElementDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaMenuLayoutDisplay;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.Layout;
import io.intino.konos.alexandria.activity.model.Panel;
import io.intino.konos.alexandria.activity.model.layout.ElementOption;
import io.intino.konos.alexandria.activity.model.layout.options.Group;
import io.intino.konos.alexandria.activity.model.layout.options.Option;
import io.intino.konos.alexandria.activity.model.renders.RenderCatalog;
import io.intino.konos.alexandria.activity.model.renders.RenderPanel;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.notifiers.TestMenuDisplayNotifier;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestMenuDisplay extends AlexandriaMenuLayoutDisplay<TestMenuDisplayNotifier> {

	public AbstractTestMenuDisplay(TestKonosBox box) {
		super(box);
		element(buildLayout(box));
	}

	private static Layout buildLayout(TestKonosBox box) {
		Layout layout = new Layout();
		layout.mode(Layout.Mode.Menu);
		layout.elementDisplayBuilder(new Layout.ElementDisplayBuilder() {
			@Override
			public AlexandriaElementDisplay displayFor(Element element, Object o) {
				return ElementDisplays.displayFor(box, element);
			}

			@Override
			public Class<? extends AlexandriaElementDisplay> displayTypeFor(Element element, Object o) {
				return ElementDisplays.displayTypeFor(box, element);
			}
		});
		buildOptions(box).forEach(o -> layout.add(o));
		return layout;
	}

	private static List<ElementOption> buildOptions(TestKonosBox box) {
		List<ElementOption> result = new ArrayList<>();
		result.add(new Group().label("ddd").mode(Group.Mode.Expanded)
					.add(new Option().render(new RenderCatalog().catalog((Catalog) ElementDisplays.displayFor(box,"testInfrastructure").element())).label("infrastructure"))
					.add(new Option().render(new RenderPanel().panel((Panel) ElementDisplays.displayFor(box,"testAnalisis").element())).label("análisis")));
		return result;
	}

}
