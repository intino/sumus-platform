package io.intino.test.konos.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaMenuLayoutDisplayNotifier;
import io.intino.sumus.box.schemas.NameSpace;

public class TestMenuDisplayNotifier extends AlexandriaMenuLayoutDisplayNotifier {

	public TestMenuDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}