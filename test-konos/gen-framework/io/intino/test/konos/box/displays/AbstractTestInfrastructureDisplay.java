package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaCatalogDisplay;
import io.intino.konos.alexandria.activity.model.AbstractView;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.Toolbar;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Arrangement;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Grouping;
import io.intino.konos.alexandria.activity.model.catalog.events.OnClickRecord;
import io.intino.konos.alexandria.activity.model.catalog.events.OpenDialog;
import io.intino.konos.alexandria.activity.model.catalog.views.DisplayView;
import io.intino.konos.alexandria.activity.model.catalog.views.GridView;
import io.intino.konos.alexandria.activity.model.catalog.views.ListView;
import io.intino.konos.alexandria.activity.model.catalog.views.MapView;
import io.intino.konos.alexandria.activity.model.toolbar.*;
import io.intino.tara.magritte.Concept;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.notifiers.TestInfrastructureDisplayNotifier;
import io.intino.test.konos.graph.Station;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestInfrastructureDisplay extends AlexandriaCatalogDisplay<TestInfrastructureDisplayNotifier> {

	public AbstractTestInfrastructureDisplay(TestKonosBox box) {
		super(box);
		element(buildCatalog(box));
	}

	private static Catalog buildCatalog(TestKonosBox box) {
		Catalog catalog = new Catalog();
		catalog.name("testInfrastructure");
		catalog.label("Infraestructura");
		catalog.events().onClickRecord(new OnClickRecord().openDialog(new OpenDialog().height(100).width(100).path((item) -> TestInfrastructureDisplay.Events.openDialog(box, item))));
		catalog.objectsLoader((scope, condition, username) -> (List<Object>)(Object)TestInfrastructureDisplay.Source.stations(box, scope, condition, username));
		catalog.objectLoader((id, username) -> TestInfrastructureDisplay.Source.station(box, id, username));
		catalog.objectIdLoader((object) -> TestInfrastructureDisplay.Source.stationId(box, (Station)object));
		catalog.objectNameLoader((object) -> TestInfrastructureDisplay.Source.stationName(box, (Station)object));
		catalog.rootObjectLoader((objectList, username) -> TestInfrastructureDisplay.Source.rootStation(box, (List<Station>)(Object)objectList, username));
		catalog.defaultObjectLoader((id, username) -> TestInfrastructureDisplay.Source.defaultStation(box, id, username));
		catalog.clusterManager((element, grouping, group, username) -> TestInfrastructureDisplay.Arrangements.createGroup(box, element, grouping, group, username));
		catalog.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> catalog.add(v));
		buildArrangements(box).forEach(a -> catalog.add(a));
		return catalog;
	}

	private static Toolbar buildToolbar(TestKonosBox box) {
		Toolbar toolbar = new Toolbar();
		toolbar.add(new GroupingSelection().name("creargrupo").title("crear grupo").alexandriaIcon("content-copy"));
		toolbar.add(new TaskSelection().execute((element, option, selection, username) -> TestInfrastructureDisplay.Toolbar.removeElements(box, element, option, (List<Station>)(Object)selection, username)).name("removeelements").title("remove elements").alexandriaIcon("remove-circle-outline"));
		toolbar.add(new Download().execute((element, option, username) -> TestInfrastructureDisplay.Toolbar.download(box, element, option, username)).name("download").title("download").alexandriaIcon("file-download"));
		toolbar.add(new DownloadSelection().execute((element, option, selection, username) -> TestInfrastructureDisplay.Toolbar.downloadAssetsInfo(box, element, option, (List<Station>)(Object)selection, username)).name("downloadassetsinfo").title("download assets info").alexandriaIcon("file-download"));
		toolbar.add(new Export().execute((element, from, to, username) -> TestInfrastructureDisplay.Toolbar.export(box, element, from, to, username)).name("export").title("export").alexandriaIcon("archive"));
		toolbar.add(new ExportSelection().execute((element, from, to, selection, username) -> TestInfrastructureDisplay.Toolbar.exportSelection(box, element, from, to, (List<Station>)(Object)selection, username)).name("exportselection").title("export selection").alexandriaIcon("archive"));
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestKonosBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new ListView().width(90).mold((Mold) ElementDisplays.displayFor(box, "testInfrastructureMold").element()).name("v1").label("List"));
		result.add(new GridView().width(90).mold((Mold) ElementDisplays.displayFor(box, "testInfrastructureMold").element()).name("v2").label("Grid"));
		result.add(new DisplayView().hideNavigator(true)
								    .displayLoader((context, loadingListener, instantListener, username) -> TestInfrastructureDisplay.Views.olapDisplay(box, (io.intino.konos.alexandria.activity.model.Element)context, loadingListener, instantListener, username))
									.scopeManager((display, scope) -> TestInfrastructureDisplay.Views.olapDisplayScope(box, display, scope))
									.name("v3").label("Charts"));
		result.add(new MapView().center(new MapView.Center().latitude(28.146773457066104).longitude(-15.418557420532238)).zoom(new MapView.Zoom().defaultZoom(14).min(1).max(18)).mold((Mold) ElementDisplays.displayFor(box, "testInfrastructureMold").element()).name("v4").label("Mapa"));
		return result;
	}

	private static List<Arrangement> buildArrangements(TestKonosBox box) {
		List<Arrangement> arrangements = new ArrayList<>();
		arrangements.add(new Grouping().histogram(Grouping.Histogram.Percentage).groups((objects, username) -> TestInfrastructureDisplay.Arrangements.units(box, (List<Station>)(Object)objects, username)).name("units").label("units"));
		arrangements.add(new Grouping().histogram(Grouping.Histogram.Percentage).groups((objects, username) -> TestInfrastructureDisplay.Arrangements.status(box, (List<Station>)(Object)objects, username)).name("status").label("status"));
		arrangements.add(new io.intino.konos.alexandria.activity.model.catalog.arrangement.Sorting().comparator((object1, object2) -> TestInfrastructureDisplay.Arrangements.dateComparator((Station)object1, (Station)object2)).name("date").label("date"));
		return arrangements;
	}

}
