package io.intino.test.konos.box.displays.requesters;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaMoldDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;
import io.intino.test.konos.box.displays.TestInfrastructureMoldDisplay;

public class TestInfrastructureMoldDisplayRequester extends AlexandriaMoldDisplayRequester {

	public TestInfrastructureMoldDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestInfrastructureMoldDisplay display = display();
		if (display == null) return;
		String operation = operation();
	}
}