package io.intino.test.konos.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaPanelDisplayNotifier;

public class TestAnalisisDisplayNotifier extends AlexandriaPanelDisplayNotifier {

	public TestAnalisisDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}