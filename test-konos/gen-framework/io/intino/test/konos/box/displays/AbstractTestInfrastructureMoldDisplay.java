package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaMoldDisplay;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.mold.Block;
import io.intino.konos.alexandria.activity.model.mold.Stamp;
import io.intino.konos.alexandria.activity.model.mold.stamps.Location;
import io.intino.konos.alexandria.activity.model.mold.stamps.Picture;
import io.intino.konos.alexandria.activity.model.mold.stamps.Snippet;
import io.intino.konos.alexandria.activity.model.mold.stamps.Title;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.notifiers.TestInfrastructureMoldDisplayNotifier;
import io.intino.test.konos.graph.Station;

public abstract class AbstractTestInfrastructureMoldDisplay extends AlexandriaMoldDisplay<TestInfrastructureMoldDisplayNotifier> {

	public AbstractTestInfrastructureMoldDisplay(TestKonosBox box) {
		super(box);
		element(buildMold(box));
	}

	public Mold buildMold(TestKonosBox box) {
		Mold mold = new Mold();
		Block block1 = new Block();
		Block block11 = new Block();
		block11.add(new Location().icon((object, username) -> TestInfrastructureMoldDisplay.Stamps.coordinatesIcon((Station)object, username)).name("s1").label("coordinates").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.coordinates((Station)object, username)));
		block1.add(block11);
		Block block12 = new Block().expanded(true);
		block12.add(new Block().add(new Title().name("s2").label("label").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.label((Station)object, username)))
							   .add(new Title().name("s3").label("description").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.description((Station)object, username))));
		block12.add(new Block().add(Block.Layout.Horizontal)
							   .add(new Picture().defaultPicture(null).name("s4").label("chart").defaultStyle("margin-top:6px;").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.chart((Station)object, username)))
				               .add(new Snippet().name("s5").defaultStyle("height:155px;overflow:auto;margin-top:10px;margin-bottom:10px;").layout(Stamp.Layout.Flexible).label("sequencia").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.sequence((Station)object, username))));
		block12.add(new Block().add(new Snippet().name("s6").value((object, username) -> TestInfrastructureMoldDisplay.Stamps.stats((Station)object, username))));
		block1.add(block12);
		mold.add(block1);
		return mold;
	}

}
