package io.intino.test.konos.box.displays.requesters;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaMenuLayoutDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;
import io.intino.test.konos.box.displays.TestMenuDisplay;

public class TestMenuDisplayRequester extends AlexandriaMenuLayoutDisplayRequester {

	public TestMenuDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestMenuDisplay display = display();
		if (display == null) return;
		String operation = operation();

		if (operation.equals("logout")) display.logout();
		else if (operation.equals("selectItem")) display.selectItem(manager.fromQuery("value", String.class));
	}
}