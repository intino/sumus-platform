package io.intino.test.konos.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaCatalogDisplayNotifier;

public class TestInfrastructureDisplayNotifier extends AlexandriaCatalogDisplayNotifier {

	public TestInfrastructureDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}