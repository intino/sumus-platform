package io.intino.test.konos.box.displays;

import io.intino.konos.alexandria.activity.displays.AlexandriaDesktopDisplay;
import io.intino.konos.alexandria.activity.model.Desktop;
import io.intino.test.konos.box.TestKonosBox;
import io.intino.test.konos.box.displays.notifiers.TestHomeDisplayNotifier;

import java.net.MalformedURLException;
import java.net.URL;

public abstract class AbstractTestHomeDisplay extends AlexandriaDesktopDisplay<TestHomeDisplayNotifier> {

	public AbstractTestHomeDisplay(TestKonosBox box) {
		super(box);
		element(buildDesktop(box));
	}

	private static Desktop buildDesktop(TestKonosBox box) {
		Desktop desktop = new Desktop();
		desktop.title("Ejemplo");
		desktop.subtitle("Ejemplo de proyecto hecho con Konos");
		desktop.logo(null);
		desktop.favicon(null);
		desktop.authServiceUrl(null);
		//desktop.logo(new URL("logo.png"));
		//desktop.favicon(new URL("favicon.png"));
		//desktop.authServiceUrl(new URL("http://localhost"));
		desktop.layoutDisplayProvider(() -> TestHomeDisplay.Layout.layoutDisplay(box));
		return desktop;
	}

}