package io.intino.test.konos.box.displays.notifiers;

import io.intino.konos.alexandria.exceptions.*;
import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaDesktopDisplayNotifier;
import io.intino.test.konos.box.*;
import io.intino.test.konos.box.schemas.*;

public class TestHomeDisplayNotifier extends AlexandriaDesktopDisplayNotifier {

    public TestHomeDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
        super(display, carrier);
    }


}
