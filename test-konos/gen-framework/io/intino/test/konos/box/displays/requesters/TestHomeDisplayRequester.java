package io.intino.test.konos.box.displays.requesters;

import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaDesktopDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;
import io.intino.test.konos.box.displays.TestHomeDisplay;

public class TestHomeDisplayRequester extends AlexandriaDesktopDisplayRequester {

	public TestHomeDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestHomeDisplay display = display();
		if (display == null) return;
		String operation = operation();


	}
}