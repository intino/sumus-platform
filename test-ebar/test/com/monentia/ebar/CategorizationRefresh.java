package com.monentia.ebar;

import com.monentia.ebar.graph.Station;
import io.intino.sumus.Category;
import io.intino.sumus.SumusStore;
import io.intino.sumus.graph.Categorization;
import io.intino.sumus.graph.SumusGraph;
import io.intino.tara.magritte.Graph;
import org.junit.Test;

import java.io.File;
import java.util.Collections;

import static org.apache.commons.io.FileUtils.deleteDirectory;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class CategorizationRefresh {

	@Test
	public void should_refresh_categorization() throws Exception {
		File fileFolder = new File("temp");
		deleteDirectory(fileFolder);
		fileFolder.mkdir();
		SumusGraph sumus = new Graph(new SumusStore(fileFolder)).loadStashes("Model").as(SumusGraph.class);
		sumus.init();
		assertNotNull(category(sumus.categorization(0), "EB01. ZEC"));
		sumus.core$().load("Model#EB01").as(Station.class).label("EB01. NEW ZEC");
		assertNull(category(sumus.categorization(0), "EB01. ZEC"));
		assertNotNull(category(sumus.categorization(0), "EB01. NEW ZEC"));
	}

	private Category category(Categorization categorization, String name) {
		return categorization.calculate(Collections.emptyList()).stream().filter(c -> c.name().equals(name)).findFirst().orElse(null);
	}
}
