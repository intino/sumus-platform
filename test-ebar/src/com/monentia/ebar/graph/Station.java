package com.monentia.ebar.graph;

import com.monentia.ebar.SequenceExporter;
import io.intino.sumus.datawarehouse.store.PathBuilder;
import io.intino.tara.magritte.Graph;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;

import static com.monentia.ebar.graph.TestEbarGraph.SensorType.typeOf;
import static java.lang.String.format;

public class Station extends AbstractStation {

	public Station(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public Sensor sensor(String sensorId) {
		return sensorList().stream().filter(s -> s.name$().equalsIgnoreCase(sensorId)).findFirst().orElse(null);
	}

	public Sensor sensor(TestEbarGraph.SensorType type) {
		for (Sensor sensor : sensorList()) if (typeOf(sensor.type()) == type) return sensor;
		return null;
	}

	public InputStream export(Instant instant) {
		Graph clone = graph().core$().clone();
		TestEbarGraph ebar = clone.as(TestEbarGraph.class);
		clone.loadStashes(PathBuilder.temporalRecordPath(ebar.ebarNS(), StationSequence.class, ebar.eventScale(), instant));
		StringBuilder content = new StringBuilder("fecha;sensor;tipo;valor\n");
		ebar.stationSequenceList().stream()
				.filter(e -> e.station() == this)
				.forEach(e -> content.append(toCsv(e)).append("\n"));
		return new ByteArrayInputStream(content.toString().getBytes());
	}

	private String toCsv(StationSequence sequence) {
		StringBuilder result = new StringBuilder();
		sequence.sortedStates().forEach(s -> result.append(
				format("%s;%s;%s;%s\n", SequenceExporter.format(s.created()), s.sensor().name$(), s.sensor().type().label(), s.value() ? "1" : "0")));
		return result.toString();
	}

}