package com.monentia.ebar.graph.functions;

import com.monentia.ebar.graph.TestEbarGraph;
import com.monentia.ebar.graph.Station;

@FunctionalInterface
public interface SensorByType {
    Station.Sensor get(TestEbarGraph.SensorType type);
}
