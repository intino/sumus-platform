package com.monentia.ebar.graph.functions;

import java.io.InputStream;
import java.time.Instant;

@FunctionalInterface
public interface Exporter {
    InputStream export(Instant instant);
}
