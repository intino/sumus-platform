package com.monentia.ebar.graph.rules;


import io.intino.tara.lang.model.Rule;

public enum State implements Rule<Enum> {
    On, Off, Break;

    @Override
    public boolean accept(Enum value) {
        return value instanceof State;
    }
}
