package com.monentia.ebar.graph;

import com.monentia.ebar.helpers.DayEventLoader;
import io.intino.konos.alexandria.activity.model.TimeScale;
import io.intino.sumus.graph.Checker;
import io.intino.sumus.graph.SumusGraph;
import io.intino.tara.magritte.Graph;
import org.jfree.util.Log;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static io.intino.konos.alexandria.activity.model.TimeScale.Day;
import static java.time.LocalDateTime.ofInstant;
import static java.time.ZoneOffset.UTC;
import static java.util.stream.Collectors.toList;

public class TestEbarGraph extends AbstractGraph {

	private static final int SequenceLatencyDays = 5;
	private static DayEventLoader sequenceLoader = null;

	public TestEbarGraph(Graph graph) {
		super(graph);
	}

	public TestEbarGraph(io.intino.tara.magritte.Graph graph, TestEbarGraph wrapper) {
		super(graph, wrapper);
	}

	public static LocalDateTime utcTime(Instant instant) {
		return ofInstant(instant, ZoneId.of("UTC"));
	}

	public static Instant instant(LocalDateTime utcTime) {
		return utcTime.toInstant(UTC);
	}

	public void init(String... args) {
		core$().as(SumusGraph.class).init();
		initAlerts();
		sequenceLoader = new DayEventLoader(core$());
	}

	private void initAlerts() {
		core$().loadStashes("Alerts");
		if (alerts() == null) create("Alerts").alerts();
		stationList(s -> !existsAlertForStation(s)).forEach(s -> alerts().create().alert(s));
		alerts().clear().alert(a -> a.station() == null);
		alerts().save$();
	}

	private boolean existsAlertForStation(Station station) {
		return alerts().alertList(a -> a.station().name$().equals(station.name$())).size() > 0;
	}

	public Station station(String stationId) {
		Station station = stationList(s -> s.name$().equalsIgnoreCase(stationId)).findFirst().orElse(null);
		Log.info(station + " not found");
		return station;
	}

	public Checker checker(Class<?> aClass) {
		return graph.as(SumusGraph.class).checkerList(c -> c.record().layerClass() == aClass).collect(toList()).get(0);
	}

	public TimeScale eventScale() {
		return Day;
	}

	public ComponentType componentType(TestEbarGraph.SensorType sensorType) {
		return componentTypeList(c -> c.name$().equals(sensorType.name())).findFirst().orElse(null);
	}

	public enum SequenceType {All, WithFailures, WithoutFailures}

	public enum SequenceState {All, Running, Finished}

	public enum SensorType {
		PumpA, PumpB, BreakerA, BreakerB, Float1, Float2, Float3;

		public static TestEbarGraph.SensorType typeOf(ComponentType type) {
			return valueOf(type.name$());
		}

	}
}