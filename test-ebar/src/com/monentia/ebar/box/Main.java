package com.monentia.ebar.box;

import com.monentia.ebar.box.displays.ElementDisplays;
import io.intino.konos.alexandria.Box;
import io.intino.sumus.SumusStore;
import io.intino.sumus.graph.SumusGraph;
import io.intino.sumus.helpers.TranslatorHelper;
import io.intino.tara.magritte.Graph;

import java.util.Map;

public class Main {
	public static void main(String[] args) {
		TestEbarConfiguration configuration = new TestEbarConfiguration(args);
		Graph graph = new Graph(new SumusStore(configuration.store())).loadStashes(paths(configuration));
		Box box = new TestEbarBox(configuration).put(graph).put(new TranslatorHelper(graph.as(SumusGraph.class))).open();

		// TODO Octavio... se tiene que hacer al iniciar el activity
		ElementDisplays.init((TestEbarBox) box);

		Runtime.getRuntime().addShutdownHook(new Thread(box::close));
	}

	private static String[] paths(TestEbarConfiguration configuration) {
		Map<String, String> map = configuration.args();
		return map.containsKey("graph_paths") ? map.get("graph_paths").split(" ") : new String[]{"Sumus"};
	}
}