package com.monentia.ebar.box.actions;

import com.monentia.ebar.box.TestEbarBox;
import io.intino.konos.alexandria.exceptions.*;
import io.intino.konos.alexandria.activity.spark.actions.AlexandriaPageAction;
import java.time.*;
import java.util.*;

import com.monentia.ebar.box.displays.*;

public class TestEbarHomePageAction extends AlexandriaPageAction {

	public TestEbarBox box;


	public TestEbarHomePageAction() { super("test-ebar"); }

	public String execute() {
		return super.template("testEbarHomePage");
	}

	public io.intino.konos.alexandria.activity.displays.Soul prepareSoul(io.intino.konos.alexandria.activity.services.push.ActivityClient client) {
	    return new io.intino.konos.alexandria.activity.displays.Soul(session) {
			@Override
			public void personify() {
				TestEbarHomeDisplay display = new TestEbarHomeDisplay(box);
				register(display);
				display.personify();
			}
		};
	}

	@Override
	protected String title() {
		return "";
	}

	@Override
	protected java.net.URL favicon() {
		return null;
	}
}