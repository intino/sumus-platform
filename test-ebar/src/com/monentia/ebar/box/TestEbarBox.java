package com.monentia.ebar.box;

import com.monentia.ebar.graph.TestEbarGraph;
import io.intino.tara.magritte.Graph;

public class TestEbarBox extends AbstractBox {
	private TestEbarGraph graph;

	public TestEbarBox(String[] args) {
		super(args);
	}

	public TestEbarBox(TestEbarConfiguration configuration) {
		super(configuration);
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		if (o instanceof Graph) this.graph = ((Graph) o).as(TestEbarGraph.class);
		super.put(o);
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		return super.open();
	}

	public TestEbarGraph graph() {
		return graph;
	}

	public void close() {
		super.close();
	}

	static io.intino.konos.alexandria.activity.services.AuthService authService(java.net.URL authServiceUrl) {
		//TODO add your authService
		return null;
	}	
}