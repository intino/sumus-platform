package com.monentia.ebar.box.displays;

import com.monentia.ebar.Graph;
import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.*;
import com.monentia.ebar.graph.rules.State;
import io.intino.sumus.datawarehouse.store.Bucket;
import io.intino.sumus.datawarehouse.store.Digest;
import io.intino.sumus.graph.Record;

import java.net.URL;
import java.time.Instant;
import java.util.List;

import static io.intino.konos.alexandria.activity.model.TimeScale.Week;
import static java.util.Collections.singletonList;

public class TestEbarInfrastructureMoldDisplay extends AbstractTestEbarInfrastructureMoldDisplay {

	public TestEbarInfrastructureMoldDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Stamps {
		public static URL coordinatesIcon(Asset asset, String username) {
			Station station = asset.a$(Station.class);
			if (station.lastStationView() == null) return resourceOf(stationIcon(null));
			return resourceOf(station.core$().graph().clone().load(station.lastStationView()).as(StationView.class).icon());
		}

		public static String coordinates(Asset asset, String username) {
			return asset.coordinates();
		}

		public static String title(Asset asset, String username) {
			return asset.label();
		}

		public static String lastEpisode(Asset asset, String username) {
			Sequence sequence = sequenceOf(asset.a$(Station.class));
			return sequence == null ? "no hay episodio vivo" : "el episodio actual empezó el " + format(sequence.created());
		}

		public static List<URL> chart(Asset asset, String username) {
			StationSequence sequence = sequenceOf(asset.a$(Station.class));
			return sequence == null ? null : singletonList(sequence.icon());
		}

		public static String sequence(Asset asset, String username) {
			Sequence sequence = sequenceOf(asset.a$(Station.class));
			return sequence == null ? null : TestEbarInfrastructureMoldDisplay.sequence(sequence.a$(Record.class));
		}

		public static String stats(Asset asset, String username) {
			Station station = asset.a$(Station.class);
			TestEbarGraph ebarGraph = station.graph();
			Bucket bucket = ebarGraph.sequenceCube().getBucket(ebarGraph.ebarNS(), Week, Instant.now());
			Digest digest = bucket != null ? bucket.digest(station) : null;
			if (digest == null) return "No hay datos estadísticos de esta semana";
			return "Esta semana se han registrado " + digest.intOf("count") + " episodios. " + renderFails(digest);
		}
	}

	private static String stationIcon(StationView self) {
		if (self != null && self.online())
			return "ON-F" + floatLevel(self) + "A" + state(self.pumpA()) + "B" + state(self.pumpB()) + ".png";
		else
			return "OFF.png";
	}

	private static String state(State state) {
		if (state == State.On) return "1";
		if (state == State.Off) return "0";
		return "X";
	}

	private static String floatLevel(StationView self) {
		if (self.float3()) return "3";
		if (self.float2()) return "2";
		if (self.float1()) return "1";
		return "0";
	}

	private static URL resourceOf(String name) {
		return Graph.class.getResource("/images/" + name);
	}

	private static StationSequence sequenceOf(Station station) {
		String sequenceRef = station.currentSequence();
		return sequenceRef == null || sequenceRef.isEmpty() ? null : station.core$().graph().clone().load(sequenceRef).as(StationSequence.class);
	}

	private static String format(Instant created) {
		return String.format("date(%s, DD/MM/YYYY HH:mm:ss)", created.toEpochMilli());
	}

	private static String format(float value) {
		return String.format("%.2f", value * 100);
	}

	public static String sequence(Record record) {
		if (!record.i$(StationSequence.class)) return null;
		StationSequence sequence = record.a$(StationSequence.class);
		StringBuilder body = new StringBuilder();
		if (!sequence.failures().isEmpty()) {
			body.append("<ul class=\"layout vertical center-justified\" style=\"list-style:none;margin:0;padding-left:20px;font-size:11pt;color:red;margin-bottom:15px;\">");
			sequence.failures().forEach(f -> body.append("<li>").append(f).append("</li>"));
			body.append("</ul>");
		}
		body.append("<ul class=\"layout vertical center-justified\" style=\"list-style:none;margin:0;padding-left:20px;font-size:11pt;\">");
		sequence.sortedStates().forEach(s -> body.append("<li>").append(formatTimeStamp(s)).append(formatLabel(s)).append("</li>"));
		body.append("</ul>");
		return body.toString();
	}

	private static String formatTimeStamp(SensorState state) {
		return "<span style=\"width:140px;display:inline-block;font-size:9pt;color:grey\">" + format(state.created()) + "</span>";
	}

	private static String formatLabel(SensorState sensor) {
		return "<span style=\"width:80px;display:inline-block;color:blue;font-weight:bold\">" + sensor.sensor().type().label() + "</span>"
				+ "<span style=\"width:60px;display:inline-block;font-weight:bold;color:" + (sensor.value() ? "green" : "red") + "\">" + (sensor.value() ? "activo" : "inactivo") + "</span>";
	}

	private static String renderFails(Digest digest) {
		float failCount = digest.intOf("failCount");
		return failCount == 0 ? "" :
				"<font size=\"3\" color=\"red\">" + format(failCount / digest.intOf("count")) + "% de fallos</font>";
	}

}