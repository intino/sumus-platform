package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.Asset;
import io.intino.konos.alexandria.activity.Resource;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.sumus.graph.Entity;
import io.intino.sumus.queries.EntityQuery;
import io.intino.tara.magritte.Concept;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import static io.intino.sumus.box.SumusDisplayHelper.*;
import static java.util.stream.Collectors.toList;

public class TestEbarInfrastructureDisplay extends AbstractTestEbarInfrastructureDisplay {

	public TestEbarInfrastructureDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Toolbar {
	}

	public static class Source {
		public static List<Asset> assetList(TestEbarBox box, Scope scope, String condition, String username) {
			Concept concept = box.graph().core$().concept("StationSequence");
			EntityQuery.Builder builder = new EntityQuery.Builder();
			builder.scope(scopeOf(scope));
			builder.condition(condition);
			List<Entity> entities = queryEngine(sumusBox(box), username).entities(builder.build(concept, username));
			return entities.stream().map(e -> e.a$(Asset.class)).collect(toList());
		}

		public static Asset asset(TestEbarBox box, String id, String username) {
			return queryEngine(sumusBox(box), username).entity(id, username).a$(Asset.class);
		}

		public static Asset rootAsset(TestEbarBox box, List<Asset> assetList, String username) {
			return null;
		}

		public static Asset defaultAsset(TestEbarBox box, String id, String username) {
			return null;
		}

		public static String assetId(TestEbarBox box, Asset asset) {
			return asset.core$().id();
		}

		public static String assetName(TestEbarBox box, Asset asset) {
			return asset.name$();
		}
	}

	public static class Views {
	}

	public static class Arrangements {
	}

	private static Resource emptyResource(String label) {
		return new Resource() {
			@Override
			public String label() {
				return label;
			}

			@Override
			public InputStream content() {
				return new ByteArrayInputStream(new byte[0]);
			}
		};
	}
}