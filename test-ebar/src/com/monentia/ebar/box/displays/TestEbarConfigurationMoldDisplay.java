package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.Alerts;

public class TestEbarConfigurationMoldDisplay extends AbstractTestEbarConfigurationMoldDisplay {

	public TestEbarConfigurationMoldDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Stamps {
		public static String icono(Alerts.Alert object, String username) {
			return object.active() ? "social:notifications-active" : "social:notifications-off";
		}

		public static String title(Alerts.Alert object, String username) {
			return object.station().label();
		}

		public static String emails(Alerts.Alert object, String username) {
			return String.join(", ", object.mailingList());
		}

		public static String estado(Alerts.Alert object, String username) {
			return object.active() ? "Notificaciones activadas" : "Notificaciones desactivadas";
		}

		public static String estadoColor(Alerts.Alert object, String username) {
			return object.active() ? "#388e3c" : "#9d9d9d";
		}

		public static String dummy(Alerts.Alert object, String username) {
			return null;
		}

		public static String editAlert(String itemId, String username) {
			return "configuration/alert/" + itemId;
		}
	}

}