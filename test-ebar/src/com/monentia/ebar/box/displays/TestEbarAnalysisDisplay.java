package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.Asset;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.displays.SumusOlap;

import java.util.List;
import java.util.function.Consumer;

import static io.intino.sumus.box.SumusDisplayHelper.queryEngine;
import static io.intino.sumus.box.SumusDisplayHelper.scopeOf;
import static io.intino.sumus.box.SumusDisplayHelper.sumusBox;
import static java.util.Collections.emptyList;

public class TestEbarAnalysisDisplay extends AbstractTestEbarAnalysisDisplay {

	public TestEbarAnalysisDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Source {
		public static List<Asset> assetList(TestEbarBox box, Scope scope, String condition, String username) {
			return emptyList();
		}

		public static Asset asset(TestEbarBox box, String id, String username) {
			return null;
		}

		public static Asset rootAsset(TestEbarBox box, List<Asset> assetList, String username) {
			return null;
		}

		public static Asset defaultAsset(TestEbarBox box, String id, String username) {
			return null;
		}

		public static String assetId(TestEbarBox box, Asset asset) {
			return null;
		}

		public static String assetName(TestEbarBox box, Asset asset) {
			return null;
		}
	}

	public static class Views {
		public static AlexandriaDisplay olapDisplay(TestEbarBox box, Element context, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener, String username) {
			SumusOlap olapDisplay = new SumusOlap(sumusBox(box));
			olapDisplay.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox(box)));
			olapDisplay.queryEngine(queryEngine(sumusBox(box), username));
			olapDisplay.olap(box.graph().ebarOlap());
			olapDisplay.onLoading(loadingListener);
			olapDisplay.onSelect(instantListener::accept);
			return olapDisplay;
		}

		public static void olapDisplayScope(TestEbarBox box, AlexandriaDisplay display, Scope scope) {
			((SumusOlap)display).scope(scopeOf(scope));
		}
	}

}