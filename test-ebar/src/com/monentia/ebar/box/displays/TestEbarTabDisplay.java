package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.box.SumusBox;
import io.intino.sumus.box.displays.SumusNameSpace;

public class TestEbarTabDisplay extends AbstractTestEbarTabDisplay {

	public TestEbarTabDisplay(TestEbarBox box) {
		super(box);
	}

	@Override
	protected void init() {
		super.init();
		SumusNameSpace nameSpaceDisplay = new SumusNameSpace(sumusBox());
		nameSpaceDisplay.nameSpaceHandler(SumusDisplayHelper.nameSpaceHandler(sumusBox()));
		addAndPersonify(nameSpaceDisplay);
	}

	public static class Options {
		public static String label(Element element, Object object) {
			return element.label();
		}
		public static String icon(Element element, Object object) {
			return null;
		}
		public static int bubble(Element element, Object object) {
			return -1;
		}
	}

	private SumusBox sumusBox() {
		return (SumusBox)box.owner();
	}
}