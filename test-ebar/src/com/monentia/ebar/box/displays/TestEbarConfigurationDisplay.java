package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.Alerts;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.sumus.queries.EntityQuery;
import io.intino.tara.magritte.Concept;

import java.util.List;

import static io.intino.sumus.box.SumusDisplayHelper.*;
import static java.util.stream.Collectors.toList;

public class TestEbarConfigurationDisplay extends AbstractTestEbarConfigurationDisplay {

	public TestEbarConfigurationDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Source {
		public static List<Alerts.Alert> alertList(TestEbarBox box, Scope scope, String condition, String username) {
			Concept concept = box.graph().core$().concept("Alerts.Alert");
			EntityQuery.Builder builder = new EntityQuery.Builder();
			builder.scope(scopeOf(scope));
			builder.condition(condition);
			return queryEngine(sumusBox(box), username).entities(builder.build(concept, username)).stream().map(e -> e.a$(Alerts.Alert.class)).collect(toList());
		}

		public static Alerts.Alert alert(TestEbarBox box, String id, String username) {
			return queryEngine(sumusBox(box), username).entity(id, username).a$(Alerts.Alert.class);
		}

		public static Alerts.Alert rootAlert(TestEbarBox box, List<Alerts.Alert> alertList, String username) {
			return null;
		}

		public static Alerts.Alert defaultAlert(TestEbarBox box, String id, String username) {
			return null;
		}

		public static String alertId(TestEbarBox box, Alerts.Alert alert) {
			return alert.core$().id();
		}

		public static String alertName(TestEbarBox box, Alerts.Alert alert) {
			return alert.name$();
		}
	}

	public static class Views {
	}

}