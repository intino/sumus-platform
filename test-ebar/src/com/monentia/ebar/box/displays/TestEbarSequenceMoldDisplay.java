package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.StationSequence;
import io.intino.sumus.graph.Record;

import java.net.URL;
import java.util.List;

import static java.util.Collections.singletonList;

public class TestEbarSequenceMoldDisplay extends AbstractTestEbarSequenceMoldDisplay {

	public TestEbarSequenceMoldDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Stamps {

		public static URL icon(StationSequence object, String username) {
			return object.graph().core$().loadResource("/images/" + (object.closed() ? "grey.png" : "green.png"));
		}

		public static Object label(StationSequence object, String username) {
			return object.name$();
		}

		public static Object labelStyle(StationSequence object, String username) {
			return "color:" + (object.failures().isEmpty() ? "black" : "red");
		}

		public static List<URL> chart(StationSequence object, String username) {
			return singletonList(object.icon());
		}

		public static Object sequence(StationSequence object, String username) {
			return TestEbarInfrastructureMoldDisplay.sequence(object.a$(Record.class));
		}
	}

}