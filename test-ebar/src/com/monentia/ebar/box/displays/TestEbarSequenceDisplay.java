package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.graph.Sequence;
import com.monentia.ebar.graph.StationSequence;
import com.monentia.ebar.graph.TestEbarGraph;
import io.intino.konos.alexandria.activity.Resource;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.TimeRange;
import io.intino.konos.alexandria.activity.model.catalog.Scope;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Group;
import io.intino.sumus.Category;
import io.intino.sumus.box.SumusDisplayHelper;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.queries.TemporalRecordQuery;
import io.intino.tara.magritte.Concept;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.Instant;
import java.util.List;

import static com.monentia.ebar.SequenceExporter.rangeOfSequencesToCsv;
import static io.intino.sumus.box.SumusDisplayHelper.*;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

public class TestEbarSequenceDisplay extends AbstractTestEbarSequenceDisplay {

	public TestEbarSequenceDisplay(TestEbarBox box) {
		super(box);
	}

	public static class Temporal {
		public static TimeRange range(TestEbarBox box, String username) {
			List<NameSpace> nameSpaces = singletonList(box.graph().ebarNS());
			return queryEngine(sumusBox(box), username).temporalRecordRange(nameSpaces);
		}
	}

	public static class Toolbar {
		public static Resource export(TestEbarBox box, Element element, Instant from, Instant to, String username) {
			return new Resource() {
				@Override
				public String label() {
					return "episodios.csv";
				}

				@Override
				public InputStream content() {
					return rangeOfSequencesToCsv(from, to, box.graph().core$().as(TestEbarGraph.class).stationList());
				}
			};
		}
	}

	public static class Source {
		public static List<StationSequence> stationSequenceList(TestEbarBox box, Scope scope, String condition, TimeRange timeRange, String username) {
			Concept concept = box.graph().core$().concept("StationSequence");
			TemporalRecordQuery.Builder builder = new TemporalRecordQuery.Builder();
			builder.scope(scopeOf(scope));
			builder.condition(condition);
			builder.timeRange(timeRange);
			builder.nameSpace(box.graph().ebarNS());
			return queryEngine(sumusBox(box), username).temporalRecords(builder.build(concept, username)).stream().map(r -> r.a$(StationSequence.class)).collect(toList());
		}

		public static StationSequence stationSequence(TestEbarBox box, String id, String username) {
			return queryEngine(sumusBox(box), username).temporalRecord(id, username).a$(StationSequence.class);
		}

		public static String stationSequenceId(TestEbarBox box, StationSequence object) {
			return object.core$().id();
		}

		public static String stationSequenceName(TestEbarBox box, StationSequence object) {
			return object.name$();
		}

		public static Instant stationSequenceCreated(TestEbarBox box, StationSequence object) {
			return object.created();
		}

		public static StationSequence rootStationSequence(TestEbarBox box, List<StationSequence> objectList, TimeRange timeRange, String username) {
			return null;
		}

		public static StationSequence defaultStationSequence(TestEbarBox box, String id, TimeRange timeRange, String username) {
			return null;
		}
	}

	public static class Views {
	}

	public static class Arrangements {
		public static int dateComparator(StationSequence object1, StationSequence object2) {
			return object1.a$(Sequence.class).created().compareTo(object2.a$(Sequence.class).created());
		}

		public static List<Group> sequenceStations(TestEbarBox box, List<StationSequence> objects, String username) {
			List<Category> categoryList = box.graph().sequenceStations().calculate(objects, userKeys(sumusBox(box), username));
			return SumusDisplayHelper.groups(categoryList, objects);
		}

		public static List<Group> stationSequenceStates(TestEbarBox box, List<StationSequence> objects, String username) {
			List<Category> categoryList = box.graph().stationSequenceStates().calculate(objects, userKeys(sumusBox(box), username));
			return SumusDisplayHelper.groups(categoryList, objects);
		}

		public static List<Group> stationSequenceTypes(TestEbarBox box, List<StationSequence> objects, String username) {
			List<Category> categoryList = box.graph().stationSequenceTypes().calculate(objects, userKeys(sumusBox(box), username));
			return SumusDisplayHelper.groups(categoryList, objects);
		}

		public static Catalog.ArrangementFilterer filterer(TestEbarBox box, String username) {
			return SumusDisplayHelper.createArrangementFilterer(username);
		}
	}

	private static Resource emptyResource(String label) {
		return new Resource() {
			@Override
			public String label() {
				return label;
			}

			@Override
			public InputStream content() {
				return new ByteArrayInputStream(new byte[0]);
			}
		};
	}
}