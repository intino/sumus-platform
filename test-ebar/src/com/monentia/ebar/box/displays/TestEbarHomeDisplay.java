package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import io.intino.konos.alexandria.activity.displays.AlexandriaLayoutDisplay;

public class TestEbarHomeDisplay extends AbstractTestHomeDisplay {

    public TestEbarHomeDisplay(TestEbarBox box) {
        super(box);
    }

    public static class Layout {
        public static AlexandriaLayoutDisplay layoutDisplay(TestEbarBox box) {
            return new TestEbarTabDisplay(box);
        }
    }

}