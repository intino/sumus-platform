package com.monentia.ebar.helpers;

import com.monentia.ebar.graph.TestEbarGraph;
import io.intino.tara.magritte.Graph;
import io.intino.tara.magritte.Layer;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.monentia.ebar.graph.TestEbarGraph.instant;
import static com.monentia.ebar.graph.TestEbarGraph.utcTime;
import static io.intino.sumus.datawarehouse.store.PathBuilder.temporalRecordPath;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class DayEventLoader {
    private static final int DefaultCacheSize = 40;
    private static final int DefaultCacheTimeout = 3 * 60 * 1000;
    private final Graph originalGraph;
    private final int cacheSize;
    private final int cacheTimeout;
    private List<String> cache = new ArrayList<>();
    private Graph graph;

    public DayEventLoader(Graph graph) {
        this(graph, DefaultCacheSize, DefaultCacheTimeout);
    }

    public DayEventLoader(Graph graph, int cacheSize) {
        this(graph, cacheSize, DefaultCacheTimeout);
    }

    public DayEventLoader(Graph graph, int cacheSize, int cacheTimeout) {
        this.originalGraph = graph.clone();
        this.graph = graph.clone();
        this.cacheSize = cacheSize;
        this.cacheTimeout = cacheTimeout;
        init();
    }

    public synchronized <E extends Layer> List<E> load(Class<E> eventType, Instant instant) {
        LocalDateTime localDateTime = utcTime(instant);

        if (cache.size() > cacheSize)
            resetCache();

        List<String> stashes = filesForInstant(eventType, localDateTime);
        if (stashes.size() > 0) {
            graph = graph.loadStashes(stashes.toArray(new String[stashes.size()]));
            updateCache(stashes);
        }

        return graph.find(eventType).stream().map(event -> event.a$(eventType)).collect(toList());
    }

    private void resetCache() {
        cache.clear();
        graph = originalGraph.clone();
    }

    private <E extends Layer> List<String> filesForInstant(Class<E> eventType, LocalDateTime instant) {
        TestEbarGraph ebar = originalGraph.as(TestEbarGraph.class);
        return range(0, Math.round(cacheSize / 3)).mapToObj(instant::minusDays)
                .map(dayOfWeek -> temporalRecordPath(ebar.ebarNS(), eventType, ebar.eventScale(), instant(dayOfWeek)))
                .filter(stash -> !cache.contains(stash))
                .collect(toList());
    }

    private void updateCache(List<String> stashes) {
        cache.addAll(stashes);
    }

    private DayEventLoader init() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                resetCache();
            }
        }, 0, cacheTimeout);
        return this;
    }

}
