package com.monentia.ebar.helpers;

import com.monentia.ebar.graph.ComponentType;
import com.monentia.ebar.graph.SensorState;
import com.monentia.ebar.graph.StationSequence;
import com.monentia.ebar.graph.TestEbarGraph.SensorType;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.XYStepAreaRenderer;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.Layer;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

import java.awt.*;
import java.io.IOException;
import java.util.List;

import static com.monentia.ebar.Graph.sortedStates;
import static com.monentia.ebar.graph.TestEbarGraph.SensorType.*;

public class SequenceImageBuilder {

	private static final int STABILITY_FACTOR = 2;
	private final long offset;
	private final JFreeChart chart;
	private final XYDataset levels;
	private StationSequence sequence;

	public SequenceImageBuilder(StationSequence sequence) {
		this.sequence = sequence;
		this.offset = !sequence.sensorStateList().isEmpty() ? (long) (distance(firstState(), lastState()) * 0.1) : 0;
		this.levels = levelsIn();
		this.chart = createChart();
	}

	private static Color rgb(String hex) {
		return new Color(
				Integer.valueOf(hex.substring(1, 3), 16),
				Integer.valueOf(hex.substring(3, 5), 16),
				Integer.valueOf(hex.substring(5, 7), 16));
	}

	private long distance(SensorState state, SensorState state2) {
		return state2.created().getEpochSecond() - state.created().getEpochSecond();
	}

	public JFreeChart chart() {
		return chart;
	}

	private XYDataset levelsIn() {
		if (sequence.sensorStateList().isEmpty()) return new XYSeriesCollection();
		double lastValue = 0;
		XYSeries xySeries = new XYSeries("");
		xySeries.add(offset, lastValue);
		for (SensorState state : sortedStates(sequence)) {
			if (!isAFloat(state)) continue;
			lastValue = levelOf(state);
			long x = startOffset() + distance(firstState(), state);
			XYDataItem item = xySeries.getDataItem(xySeries.getItemCount() - 1);
			if (Math.abs(item.getXValue() - x) < STABILITY_FACTOR)
				if (state.value() && lastValue < item.getYValue()) lastValue = item.getYValue();
				else if (!state.value() && lastValue > item.getYValue()) lastValue = item.getYValue();
			xySeries.add(x, lastValue);
		}
		xySeries.add(startOffset() + distance(firstState(), lastState()) + endOffset(), lastValue);
		return new XYSeriesCollection(xySeries);
	}

	private long endOffset() {
		return offset;
	}

	private long startOffset() {
		return 2 * offset;
	}

	private boolean isAFloat(SensorState state) {
		return typeOf(state.sensor().type()) == Float1 || typeOf(state.sensor().type()) == Float2 || typeOf(state.sensor().type()) == Float3;
	}

	private double levelOf(SensorState state) {
		boolean value = state.value();
		ComponentType type = state.sensor().type();
		return typeOf(type) == Float1 ? value ? 1 : 0 :
				typeOf(type) == Float2 ? value ? 2 : minLevel(state) :
						value ? 3 : minLevel(state);
	}

	private double minLevel(SensorState state) {
		List<SensorState> sensorStates = sequence.sortedStates().subList(0, sequence.sortedStates().indexOf(state));
		for (int i = sensorStates.size() - 1; i >= 0; i--) {
			if (typeOf(state.sensor().type()) == Float2 && typeOf(sensorStates.get(i).sensor().type()) == Float1)
				return sensorStates.get(i).value() ? 1 : 0;
			if (typeOf(state.sensor().type()) == Float3 && typeOf(sensorStates.get(i).sensor().type()) == Float2)
				return sensorStates.get(i).value() ? 2 : 1;
		}
		return 0;
	}

	private SensorState firstState() {
		return sortedStates(sequence).get(0);
	}

	private SensorState lastState() {
		return sortedStates(sequence).get(sequence.sensorStateList().size() - 1);
	}

	private JFreeChart createChart() {
		JFreeChart chart = ChartFactory.createXYStepAreaChart(null, null, null, levels, PlotOrientation.VERTICAL, false, false, false);
		definePlot(chart.getXYPlot());
		return chart;
	}

	private void definePlot(XYPlot plot) {
		plot.getRenderer().setSeriesPaint(0, rgb("#15305B"));
		plot.getRenderer().setShape(new Rectangle(1, 1));
		plot.getDomainAxis().setVisible(false);
		plot.setDomainGridlinesVisible(false);
		defineRenderer((XYStepAreaRenderer) plot.getRenderer());
		defineRangeAxis((NumberAxis) plot.getRangeAxis());
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		eventMarker(plot);
	}

	private void eventMarker(XYPlot plot) {
		for (SensorState sensorState : sortedStates(sequence)) {
			if (isAFloat(sensorState)) continue;
			if (!sensorState.value()) continue;
			SensorState deactivationState = deactivationState(sensorState);
			if (deactivationState == null) markerWithoutEndAt(plot, sensorState);
			else markerWithEndAt(plot, sensorState, deactivationState);
		}
	}

	private void markerWithoutEndAt(XYPlot plot, SensorState state) {
		Marker marker = new ValueMarker(startOffset() + distance(firstState(), state));
		marker.setLabel(String.format(" %s ", format(typeOf(state.sensor().type()))));
		marker.setPaint(isBreaker(state) ? rgb("#D6652B") : Color.black);
		marker.setStroke(dashed(5));
		marker.setLabelAnchor(RectangleAnchor.TOP);
		marker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
		plot.addDomainMarker(marker, isBreaker(state) ? Layer.FOREGROUND : Layer.BACKGROUND);
	}

	private void markerWithEndAt(XYPlot plot, SensorState state, SensorState deactivationState) {
		long initialTime = startOffset() + distance(firstState(), state);
		long endTime = startOffset() + distance(firstState(), deactivationState);

		Marker marker = new IntervalMarker(initialTime, endTime);
		marker.setPaint(isBreaker(state) ? rgb("#D6652B") : rgb("#FBB634"));
		marker.setLabelAnchor(RectangleAnchor.TOP);
		marker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
		marker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		marker.setAlpha(isBreaker(state) ? 1f : 0.5f);
		plot.addDomainMarker(marker, Layer.BACKGROUND);

		marker = new ValueMarker(initialTime);
		marker.setLabel(String.format(" %s ", format(typeOf(state.sensor().type()))));
		marker.setPaint(isBreaker(state) ? Color.red : Color.black);
		marker.setStroke(dashed(5));
		marker.setLabelAnchor(RectangleAnchor.TOP);
		marker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
		marker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		plot.addDomainMarker(marker, Layer.BACKGROUND);


		marker = new ValueMarker(endTime);
		marker.setPaint(isBreaker(state) ? Color.red : Color.black);
		marker.setLabelAnchor(RectangleAnchor.TOP);
		marker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
		marker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		plot.addDomainMarker(marker, Layer.BACKGROUND);
	}

	private String format(SensorType type) {
		return type == BreakerA || type == PumpA ? "A" : "B";
	}

	private boolean isBreaker(SensorState sensorState) {
		return typeOf(sensorState.sensor().type()) == BreakerA || typeOf(sensorState.sensor().type()) == BreakerB;
	}

	private SensorState deactivationState(SensorState from) {
		if (isBreaker(from)) return null;
		List<SensorState> states = sortedStates(sequence);
		return states.subList(states.indexOf(from), states.size()).stream()
				.filter(s -> s.sensor().type() == from.sensor().type() && !s.value()).findFirst().orElse(null);
	}

	private BasicStroke dashed(float... v) {
		return new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, v, 0.0f);
	}

	private void defineRenderer(XYStepAreaRenderer renderer) {
		renderer.setShapesFilled(true);
	}

	private void defineRangeAxis(NumberAxis rangeAxis) {
		rangeAxis.setVisible(true);
		rangeAxis.setRange(0, 3.2);
		rangeAxis.setTickUnit(new NumberTickUnit(1));
	}

	public byte[] asPng() {
		try {
			return ChartUtilities.encodeAsPNG(chart.createBufferedImage(500, 270));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}