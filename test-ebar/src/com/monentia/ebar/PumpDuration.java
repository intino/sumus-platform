package com.monentia.ebar;

public class PumpDuration {

    double pumpADuration, pumpBDuration;

    public PumpDuration(double pumpADuration, double pumpBDuration) {
        this.pumpADuration = pumpADuration;
        this.pumpBDuration = pumpBDuration;
    }

    public double pumpADuration() {
        return pumpADuration;
    }

    public double pumpBDuration() {
        return pumpBDuration;
    }
}
