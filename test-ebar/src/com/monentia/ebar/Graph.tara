dsl Sumus

Palette

Olap("Olap") ebarOlap
	tickets = activationsCountTicket activationsDurationTicket totalUsageTicket averageDurationTicket
	nameSpaces = @ebarOlapNameSpaces
	Select(ticket = activationsDurationTicket, scale = Hour, range = ToTheLast)

	Range

	ZoomGroup
		Zoom(QuarterOfYear) > InstantRange(30, 90)
		Zoom(Month) > InstantRange(18, 50)
		Zoom(Week) > InstantRange(30, 75)
		Zoom(Day) > InstantRange(30, 90)
		Zoom(SixHours) > InstantRange(56 , 84)
		Zoom(Hour) > InstantRange(24, 168)
	InstantFormatter
		Formatter(QuarterOfYear, "qN quarter yyyy")
		Formatter(Month, "MM yyyy")
		Formatter(Week, "wN week yyyy")
		Formatter(Day, "dd/MM/yyyy")
		Formatter(SixHours, "dd/MM/yyyy qDS - qDEh")
		Formatter(Hour, "dd/MM/yyyy HHh")

Ticket("Número total de activaciones", shortLabel = "Total") activationsCountTicket
	events = empty
	Min(0)
	Style(line = ContinuousWithZeros)
	DecimalPlaces(0)
	Range(max = QuarterOfYear, eventHorizon = FifteenMinutes, min = Hour)
	MeasureIndicator activationsCountIndicator
    	label = "Número total de activaciones"
    	unit = countMetric.U
    	Formula(cube = Measure, calculate = 'return digests.stream().mapToDouble(d -> d.intOf("activationsCount")).sum();')


Ticket("Duración de las activaciones", shortLabel = "Duración") activationsDurationTicket
	events = empty
	Min(0)
	Style(line = ContinuousWithZeros)
	DecimalPlaces(2)
	Range(max = QuarterOfYear, eventHorizon = FifteenMinutes, min = Hour)
	MeasureIndicator activationsDurationIndicator
		label = "Duración de las activaciones"
		unit = timeMetric.s
		Formula(Measure, 'return digests.stream().mapToDouble(d -> d.floatOf("activationsDuration")).sum();')


Ticket("Uso total", shortLabel = "Uso") totalUsageTicket
	events = empty
	Min(0)
	Style(line = ContinuousWithZeros)
	DecimalPlaces(2)
	Range(max = QuarterOfYear, eventHorizon = FifteenMinutes, min = Hour)
	MeasureIndicator totalUsageIndicator
		label = "Uso total"
		unit = timeMetric.s
		Formula(Measure, 'return digests.stream().mapToDouble(d -> d.floatOf("totalUsage")).sum();')

Ticket("Duración promedio", shortLabel = "Promedio") averageDurationTicket
	events = empty
	Min(0)
	Style(line = ContinuousWithZeros)
	DecimalPlaces(2)
	Range(max = QuarterOfYear, eventHorizon = FifteenMinutes, min = Hour)
	MeasureIndicator averageDurationIndicator
		label = "Duración promedio"
		unit = timeMetric.s
		Formula(Measure)
			calculate
				---
				double activationsDuration = 0;
		double activationsCount = 0;
		for (Digest digest : digests) {
			activationsDuration += digest.floatOf("activationsDuration");
			activationsCount += digest.intOf("activationsCount");
		}
		return activationsCount == 0 ? 0 : activationsDuration / activationsCount;
				---

Categorization ("Estación", Station, 'return item.a$(Station.class).label();') stations
Categorization ("Tipo de componente", ComponentType, 'return item.a$(ComponentType.class).label();') componentTypes
Categorization ("Estación", StationSequence, 'return item.a$(StationSequence.class).station().label();') sequenceStations
Categorization ("Estado del episodio", StationSequence, 'return item.a$(StationSequence.class).closed() ? "Finalizado" : "En ejecución";') stationSequenceStates
Categorization ("Tipo de episodio", StationSequence, 'return item.a$(StationSequence.class).failures().isEmpty() ? "Sin fallos" : "Con fallos";') stationSequenceTypes

Metric countMetric
	Unit (convert = 'value', label = "") U
	Unit K > convert = 'value * 1e3'
	Unit M > convert = 'value * 1e6'
	Unit G > convert = 'value * 1e9'

TemporalMetric timeMetric
	Unit(limit = 60, conversionFactor = 1) s
	Unit(limit = 3600, conversionFactor = 60) m
	Unit(limit = '3600 * 24.0 * 7', conversionFactor = 3600) h
	Unit(limit = '3600 * 24 * 365.0', conversionFactor = '3600 * 24.0') d
	Unit(limit = '3600 * 24 * 365 * 20.0', conversionFactor = '3600 * 24 * 365.0') y

Report StationView
	var Station station
	var word:State pumpA
	var word:State pumpB
	var boolean float1
	var boolean float2
	var boolean float3
	var boolean online
	var string icon = @stationIcon is reactive

Entity Asset
	var string label
	var string coordinates
	var string timeZone = "Atlantic/Canary"

Entity Station extends Asset is decorable
	var boolean online = true
	var string currentSequence = empty
	var string lastStationView = empty
	Entity Sensor
		var ComponentType type
		var boolean lastValue = false
		var instant updated = empty

Entity ComponentType > var string label

Entity:{0..1} Alerts
	Entity Alert
		var boolean active = false
		var string[] mailingList = empty
		var Station station

Cube Measure
	Dimension(Station)
	Dimension(ComponentType)
	Property("activationsCount", Integer)
	Property("activationsDuration", Float)
	Property("totalUsage", Float)

Cube SequenceCube
	Dimension(Station)
	Property("failCount", Integer)
	Property("successCount", Integer)
	Property("count", Integer)

Episode Sequence
	sub StationSequence
		var Station station
		var string label = @sequenceLabel is reactive
		var resource icon = @sequenceIcon is reactive
		var string[] failures = empty
		var boolean closed = false
		var object:com.monentia.ebar.PumpDuration pumpsDuration = @pumpsDuration is reactive
		var SensorState[] sortedStates = @sortedStates is reactive
		has SensorState

Episode Activation
	var string label = "Activación"
	var Station station
	var ComponentType componentType
 	Event Active
 		var string label = "Activo"
 	Event Inactive
 		var string label = "Inactivo"

Event SensorState
	var string label = @sensorStateLabel is reactive
	var Station.Sensor sensor
	var boolean value

Checker(record = StationSequence)
	Rule(check = @float1WasFirst, message = "No se alcanzó boya 1")
	Rule(check = @float2WasActive, message = "No se alcanzó boya 2")
	Rule(check = @floatsAreNotSynced, message = "Se alcanzaron dos boyas a la vez")
	Rule(check = @twoPumpsIfFloat3Reached, message = "Se alcanzó la boya 3 pero sólo se activó una bomba")
	Rule(check = @float3IsOnWhenTwoPumpsAreActivated, message = "Se activaron dos bombas sin alcanzar la boya 3")
	Rule(check = @componentsWereStable, message = @unstableMessage)
	Rule(check = @thereWereNoAlarms, message = @noAlarmMessages)

NameSpace(label = "ebar") ebarNS