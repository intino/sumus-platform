package com.monentia.ebar;

import com.monentia.ebar.graph.*;
import com.monentia.ebar.graph.rules.State;
import com.monentia.ebar.helpers.SequenceImageBuilder;
import io.intino.sumus.graph.Checker;
import io.intino.sumus.graph.NameSpace;
import io.intino.sumus.graph.Olap;
import io.intino.sumus.graph.Record;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.monentia.ebar.graph.TestEbarGraph.SensorType.*;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class Graph {

    private static String format(float value) {
        return String.format("%.2f", value * 100);
    }

    private static Map<String, URL> sequenceUrls = createLRUMap();
    private static List<ComponentType> unstableComponents;

    public static String stationIcon(StationView self) {
        if (self != null && self.online())
            return "ON-F" + floatLevel(self) + "A" + state(self.pumpA()) + "B" + state(self.pumpB()) + ".png";
        else
            return "OFF.png";
    }

    private static String state(State state) {
        if (state == State.On) return "1";
        if (state == State.Off) return "0";
        return "X";
    }

    private static String floatLevel(StationView self) {
        if (self.float3()) return "3";
        if (self.float2()) return "2";
        if (self.float1()) return "1";
        return "0";
    }

    public static List<SensorState> sortedStates(StationSequence self) {
        return self.sensorStateList().stream().sorted(comparing(SensorState::created)).collect(toList());
    }

    public static boolean float1WasFirst(Checker.Rule self, Record record) {
        return typeOf(sortedStates(record.a$(StationSequence.class)).get(0).sensor().type()) == Float1;
    }

    public static boolean float2WasActive(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        SensorState float2 = firstActiveStateOf(Float2, sequence);
        if (float2 == null) return false;
        SensorState pumpA = firstActiveStateOf(PumpA, sequence);
        SensorState pumpB = firstActiveStateOf(PumpB, sequence);
        return (pumpA != null && float2.created().compareTo(pumpA.created()) < 1) ||
                (pumpB != null && float2.created().compareTo(pumpB.created()) < 1);
    }

    private static SensorState firstActiveStateOf(TestEbarGraph.SensorType type, StationSequence sequence) {
        return statesOf(sequence, type).stream().filter(SensorState::value).findFirst().orElse(null);
    }

    public static boolean floatsAreNotSynced(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        SensorState float1 = firstActiveStateOf(Float1, sequence);
        SensorState float2 = firstActiveStateOf(Float2, sequence);
        SensorState float3 = firstActiveStateOf(Float3, sequence);
        if (float1 != null)
            if (float2 != null && float1.created().equals(float2.created())) return false;
            else if (float3 != null && float1.created().equals(float3.created())) return false;
        if (float2 != null)
            if (float3 != null && float2.created().equals(float3.created())) return false;
        return true;
    }

    public static boolean twoPumpsIfFloat3Reached(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        return firstActiveStateOf(Float3, sequence) == null || firstActiveStateOf(PumpA, sequence) != null && firstActiveStateOf(PumpB, sequence) != null;
    }

    public static boolean float3IsOnWhenTwoPumpsAreActivated(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        return !(firstActiveStateOf(PumpA, sequence) != null && firstActiveStateOf(PumpB, sequence) != null) || firstActiveStateOf(Float3, sequence) != null;
    }

    public static boolean componentsWereStable(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        unstableComponents = self.core$().graph().as(TestEbarGraph.class).componentTypeList().stream()
                .filter(c -> statesOf(sequence, typeOf(c)).size() > 2).collect(toList());
        return unstableComponents.isEmpty();
    }

    public static String unstableMessage(Checker.Rule self) {
        String result = "";
        for (ComponentType component : unstableComponents) result += ", " + component.label();
        return result.isEmpty() ? "" : "Componentes inestables: " + result.substring(2);
    }

    public static boolean thereWereNoAlarms(Checker.Rule self, Record record) {
        StationSequence sequence = record.a$(StationSequence.class);
        return !(statesOf(sequence, BreakerA).stream().anyMatch(SensorState::value) ||
                statesOf(sequence, BreakerB).stream().anyMatch(SensorState::value));
    }

    public static String noAlarmMessages(Checker.Rule self) {
        return "Se activaron las alarmas";
    }

    public static String sensorStateLabel(SensorState self) {
        return self.sensor().type().label() + ": " + (self.value() ? "activo" : "inactivo");
    }

    public static String sequenceLabel(Sequence self) {
        return self.name$();
    }

    public static java.net.URL sequenceIcon(StationSequence self) {
        String id = self.core$().id() + self.sensorStateList().size();
        if (!sequenceUrls.containsKey(id)) sequenceUrls.put(id, writeIcon(self));
        return sequenceUrls.get(id);
    }

    private static URL writeIcon(StationSequence self) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new SequenceImageBuilder(self).asPng());
        return self.graph().core$().store()
                .writeResource(byteArrayInputStream, String.format("icons/%s.png", self.name$()), null, self.core$());
    }

    public static PumpDuration pumpsDuration(StationSequence self) {
        return new PumpDuration(activationTime(statesOf(self, PumpA)), activationTime(statesOf(self, PumpB)));
    }

    private static double activationTime(List<SensorState> sensorStates) {
        return sensorStates.size() == 2 && sensorStates.get(0).value() && !sensorStates.get(1).value() ?
                sensorStates.get(0).created().until(sensorStates.get(1).created(), SECONDS) : 0;
    }

    public static List<SensorState> statesOf(StationSequence sequence, TestEbarGraph.SensorType type) {
        return sortedStates(sequence).stream()
                .filter(s -> typeOf(s.sensor().type()) == type)
                .collect(toList());
    }

    private static <K, V> Map<K, V> createLRUMap() {
        return new LinkedHashMap<K, V>(2000 * 10 / 7, 0.7f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return size() > 2000;
            }
        };
    }

	public static List<NameSpace> ebarOlapNameSpaces(Olap self, String username) {
		return singletonList(self.graph().core$().as(TestEbarGraph.class).ebarNS());
	}
}
