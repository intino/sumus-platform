package com.monentia.ebar;

import com.monentia.ebar.graph.*;
import io.intino.tara.magritte.Graph;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.monentia.ebar.Graph.statesOf;
import static com.monentia.ebar.graph.TestEbarGraph.SensorType.typeOf;
import static com.monentia.ebar.graph.TestEbarGraph.utcTime;
import static io.intino.sumus.datawarehouse.store.PathBuilder.temporalRecordPath;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Arrays.asList;

public class SequenceExporter {


	public static ByteArrayInputStream rangeOfSequencesToCsv(Instant from, Instant to, Station... stations) {
		return rangeOfSequencesToCsv(from, to, asList(stations));
	}

	public static ByteArrayInputStream rangeOfSequencesToCsv(Instant from, Instant to, List<Station> stations) {
		if (stations.isEmpty()) return new ByteArrayInputStream(new byte[0]);
		Graph clone = graphWithSequences(stations.get(0).graph().core$().clone(), from, to);
		StringBuilder content = writeHeaders(new StringBuilder(), clone);
		clone.as(TestEbarGraph.class).stationSequenceList().stream()
				.filter(e -> stations.contains(e.station()))
				.forEach(e -> toCsv(e, content));
		return new ByteArrayInputStream(content.toString().getBytes());

	}

	private static Graph graphWithSequences(Graph graph, Instant from, Instant to) {
		while (!from.isAfter(to)) {
			TestEbarGraph ebar = graph.as(TestEbarGraph.class);
			graph.loadStashes(temporalRecordPath(ebar.ebarNS(), StationSequence.class, ebar.eventScale(), from));
			from = from.plus(1, ChronoUnit.DAYS);
		}
		return graph;
	}

	private static StringBuilder writeHeaders(StringBuilder content, Graph graph) {
		content.append("episodio;estación;inicio;fin;duración");
		graph.as(TestEbarGraph.class).componentTypeList().stream().map(ComponentType::label)
				.forEach(l -> content.append(";inicio ").append(l).append(";fin ").append(l).append(";duración ").append(l));
		return content.append("\n");
	}

	private static void toCsv(StationSequence sequence, StringBuilder content) {
		formatSequence(sequence, content);
		formatComponents(sequence, content);
		content.append("\n");
	}

	private static void formatSequence(StationSequence sequence, StringBuilder content) {
		List<SensorState> sensorStates = sequence.sortedStates();
		content.append(sequence.name$()).append(";")
				.append(sequence.station().label()).append(";")
				.append(format(sensorStates.get(0).created())).append(";")
				.append(format(sensorStates.get(sensorStates.size() - 1).created())).append(";")
				.append(durationOf(sensorStates.get(0).created(), sensorStates.get(sensorStates.size() - 1).created())).append(";");
	}

	private static void formatComponents(StationSequence sequence, StringBuilder content) {
		sequence.graph().componentTypeList().forEach(c -> {
			List<SensorState> states = statesOf(sequence, typeOf(c));
			SensorState active = states.stream().filter(SensorState::value).findFirst().orElse(null);
			SensorState inactive = states.stream().filter(s -> !s.value()).reduce((f, s) -> s).orElse(null);
			content.append(active != null ? format(active.created()) : "-").append(";")
					.append(inactive != null ? format(inactive.created()) : "-").append(";")
					.append(active != null && inactive != null ?
							durationOf(active.created(), inactive.created()) :
							"-").append(";");
		});
	}

	public static String format(Instant instant) {
		return ofPattern("dd-MM-yyyy HH:mm:ss").format(utcTime(instant));
	}

	private static String durationOf(Instant active, Instant inactive) {
		long until = active.until(inactive, SECONDS);
		long hours = until / 3600;
		long minutes = (until - hours * 3600) / 60;
		long seconds = until - hours * 3600 - minutes * 60;
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}
}
