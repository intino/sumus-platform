package com.monentia.ebar.graph.natives.stationsequencetypes;

import com.monentia.ebar.graph.StationSequence;

/**stationSequenceTypes#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#87#53**/
public class Attribute_0 implements io.intino.sumus.graph.functions.Attribute, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Categorization self;

	@Override
	public String get(io.intino.tara.magritte.Layer item) {
		return item.a$(StationSequence.class).failures().isEmpty() ? "Sin fallos" : "Con fallos";
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Categorization) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Categorization.class;
	}
}