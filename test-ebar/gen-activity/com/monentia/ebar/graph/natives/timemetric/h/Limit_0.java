package com.monentia.ebar.graph.natives.timemetric.h;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#98#6**/
public class Limit_0 implements io.intino.tara.magritte.Expression<Double> {
	private io.intino.sumus.graph.TemporalMetric.Unit self;

	@Override
	public Double value() {
		return 3600 * 24.0 * 7;
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.TemporalMetric.Unit) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.TemporalMetric.Unit.class;
	}
}