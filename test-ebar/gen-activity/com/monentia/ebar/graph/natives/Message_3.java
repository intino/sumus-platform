package com.monentia.ebar.graph.natives;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#176#40**/
public class Message_3 implements io.intino.tara.magritte.Expression<String> {
	private io.intino.sumus.graph.Checker.Rule self;

	@Override
	public String value() {
		return "Se alcanzó la boya 3 pero sólo se activó una bomba";
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Checker.Rule) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Checker.Rule.class;
	}
}