package com.monentia.ebar.graph.natives.stationsequence;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#155#2**/
public class SortedStates_0 implements io.intino.tara.magritte.Expression<java.util.List<com.monentia.ebar.graph.SensorState>> {
	private com.monentia.ebar.graph.StationSequence self;

	@Override
	public java.util.List<com.monentia.ebar.graph.SensorState> value() {
		return com.monentia.ebar.Graph.sortedStates(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.StationSequence) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.StationSequence.class;
	}
}