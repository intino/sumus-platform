package com.monentia.ebar.graph.natives.stationsequence;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#154#2**/
public class PumpsDuration_0 implements io.intino.tara.magritte.Expression<com.monentia.ebar.PumpDuration> {
	private com.monentia.ebar.graph.StationSequence self;

	@Override
	public com.monentia.ebar.PumpDuration value() {
		return com.monentia.ebar.Graph.pumpsDuration(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.StationSequence) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.StationSequence.class;
	}
}