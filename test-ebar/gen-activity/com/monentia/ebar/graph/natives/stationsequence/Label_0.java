package com.monentia.ebar.graph.natives.stationsequence;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#150#2**/
public class Label_0 implements io.intino.tara.magritte.Expression<String> {
	private com.monentia.ebar.graph.StationSequence self;

	@Override
	public String value() {
		return com.monentia.ebar.Graph.sequenceLabel(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.StationSequence) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.StationSequence.class;
	}
}