package com.monentia.ebar.graph.natives.stationsequence;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#151#2**/
public class Icon_0 implements io.intino.tara.magritte.Expression<java.net.URL> {
	private com.monentia.ebar.graph.StationSequence self;

	@Override
	public java.net.URL value() {
		return com.monentia.ebar.Graph.sequenceIcon(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.StationSequence) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.StationSequence.class;
	}
}