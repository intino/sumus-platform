package com.monentia.ebar.graph.natives.countmetric.u;



/**countMetric.U#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#90#7**/
public class Convert_0 implements io.intino.sumus.graph.functions.Converter, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Metric.Unit self;

	@Override
	public double convert(double value) {
		return value;
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Metric.Unit) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Metric.Unit.class;
	}
}