package com.monentia.ebar.graph.natives.countmetric.m;



/**countMetric.M#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#92#10**/
public class Convert_0 implements io.intino.sumus.graph.functions.Converter, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Metric.Unit self;

	@Override
	public double convert(double value) {
		return value * 1e6;
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Metric.Unit) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Metric.Unit.class;
	}
}