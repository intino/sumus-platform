package com.monentia.ebar.graph.natives.totalusageticket.totalusageindicator;

import java.util.List;
import io.intino.sumus.datawarehouse.store.Digest;

/**totalUsageTicket.totalUsageIndicator.[anonymous@Formula]#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#60#19**/
public class Calculate_0 implements io.intino.sumus.graph.functions.Calculate, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.MeasureIndicator.Formula self;

	@Override
	public double calculate(List<Digest> digests) {
		return digests.stream().mapToDouble(d -> d.floatOf("totalUsage")).sum();
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.MeasureIndicator.Formula) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.MeasureIndicator.Formula.class;
	}
}