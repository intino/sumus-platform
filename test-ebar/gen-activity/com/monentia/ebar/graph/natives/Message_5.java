package com.monentia.ebar.graph.natives;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#178#37**/
public class Message_5 implements io.intino.tara.magritte.Expression<String> {
	private io.intino.sumus.graph.Checker.Rule self;

	@Override
	public String value() {
		return com.monentia.ebar.Graph.unstableMessage(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Checker.Rule) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Checker.Rule.class;
	}
}