package com.monentia.ebar.graph.natives.averagedurationticket.averagedurationindicator;

import java.util.List;
import io.intino.sumus.datawarehouse.store.Digest;

/**averageDurationTicket.averageDurationIndicator.[anonymous@Formula]#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#72#3**/
public class Calculate_0 implements io.intino.sumus.graph.functions.Calculate, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.MeasureIndicator.Formula self;

	@Override
	public double calculate(List<Digest> digests) {
		double activationsDuration = 0;
				double activationsCount = 0;
				for (Digest digest : digests) {
					activationsDuration += digest.floatOf("activationsDuration");
					activationsCount += digest.intOf("activationsCount");
				}
				return activationsCount == 0 ? 0 : activationsDuration / activationsCount;
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.MeasureIndicator.Formula) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.MeasureIndicator.Formula.class;
	}
}