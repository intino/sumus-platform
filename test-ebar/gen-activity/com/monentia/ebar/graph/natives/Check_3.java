package com.monentia.ebar.graph.natives;

import io.intino.sumus.graph.Record;

/**[anonymous@Checker].[anonymous@Rule]#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#176#6**/
public class Check_3 implements io.intino.sumus.graph.functions.CheckRecord, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Checker.Rule self;

	@Override
	public boolean check(Record record) {
		return com.monentia.ebar.Graph.twoPumpsIfFloat3Reached(self, record);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Checker.Rule) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Checker.Rule.class;
	}
}