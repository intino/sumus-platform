package com.monentia.ebar.graph.natives.stationview;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#110#1**/
public class Icon_0 implements io.intino.tara.magritte.Expression<String> {
	private com.monentia.ebar.graph.StationView self;

	@Override
	public String value() {
		return com.monentia.ebar.Graph.stationIcon(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.StationView) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.StationView.class;
	}
}