package com.monentia.ebar.graph.natives.ebarolap;

import java.util.List;
import io.intino.sumus.graph.NameSpace;

/**ebarOlap#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#7#1**/
public class NameSpaces_0 implements io.intino.sumus.graph.functions.NameSpacesLoader, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Olap self;

	@Override
	public List<NameSpace> nameSpaces(String username) {
		return com.monentia.ebar.Graph.ebarOlapNameSpaces(self, username);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Olap) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Olap.class;
	}
}