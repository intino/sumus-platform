package com.monentia.ebar.graph.natives.stations;

import com.monentia.ebar.graph.Station;

/**stations#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#83#37**/
public class Attribute_0 implements io.intino.sumus.graph.functions.Attribute, io.intino.tara.magritte.Function {
	private io.intino.sumus.graph.Categorization self;

	@Override
	public String get(io.intino.tara.magritte.Layer item) {
		return item.a$(Station.class).label();
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (io.intino.sumus.graph.Categorization) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return io.intino.sumus.graph.Categorization.class;
	}
}