package com.monentia.ebar.graph.natives.sensorstate;



/**#/Users/mcaballero/Proyectos/sumus-platform/test-ebar/src/com/monentia/ebar/Graph.tara#168#1**/
public class Label_0 implements io.intino.tara.magritte.Expression<String> {
	private com.monentia.ebar.graph.SensorState self;

	@Override
	public String value() {
		return com.monentia.ebar.Graph.sensorStateLabel(self);
	}

	@Override
	public void self(io.intino.tara.magritte.Layer context) {
		self = (com.monentia.ebar.graph.SensorState) context;
	}

	@Override
	public Class<? extends io.intino.tara.magritte.Layer> selfClass() {
		return com.monentia.ebar.graph.SensorState.class;
	}
}