package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class ComponentType extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected java.lang.String label;

	public ComponentType(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public java.lang.String label() {
		return label;
	}

	public ComponentType label(java.lang.String value) {
		this.label = value;
		return (ComponentType) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
