package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public abstract class Sequence extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {

	protected io.intino.sumus.graph.Episode _metaType;

	public Sequence(io.intino.tara.magritte.Node node) {
		super(node);
		_metaType = a$(io.intino.sumus.graph.Episode.class);
	}

	public java.time.Instant created() {
		return _metaType.created();
	}

	public Sequence created(java.time.Instant value) {
		this._metaType.created(value);
		return (Sequence) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		core$().load(_metaType, name, values);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		core$().set(_metaType, name, values);
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create  {
		protected final java.lang.String name;

		public Create(java.lang.String name) {
			this.name = name;
		}



	}

	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
