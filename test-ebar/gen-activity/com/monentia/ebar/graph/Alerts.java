package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class Alerts extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {

	protected java.util.List<com.monentia.ebar.graph.Alerts.Alert> alertList = new java.util.ArrayList<>();

	public Alerts(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public java.util.List<com.monentia.ebar.graph.Alerts.Alert> alertList() {
		return java.util.Collections.unmodifiableList(alertList);
	}

	public com.monentia.ebar.graph.Alerts.Alert alert(int index) {
		return alertList.get(index);
	}

	public java.util.List<com.monentia.ebar.graph.Alerts.Alert> alertList(java.util.function.Predicate<com.monentia.ebar.graph.Alerts.Alert> predicate) {
		return alertList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}



	protected java.util.List<io.intino.tara.magritte.Node> componentList$() {
		java.util.Set<io.intino.tara.magritte.Node> components = new java.util.LinkedHashSet<>(super.componentList$());
		new java.util.ArrayList<>(alertList).forEach(c -> components.add(c.core$()));
		return new java.util.ArrayList<>(components);
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();

		return map;
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		super.addNode$(node);
		if (node.is("Alerts$Alert")) this.alertList.add(node.as(com.monentia.ebar.graph.Alerts.Alert.class));
	}

	@Override
    protected void removeNode$(io.intino.tara.magritte.Node node) {
        super.removeNode$(node);
        if (node.is("Alerts$Alert")) this.alertList.remove(node.as(com.monentia.ebar.graph.Alerts.Alert.class));
    }

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create  {
		protected final java.lang.String name;

		public Create(java.lang.String name) {
			this.name = name;
		}

		public com.monentia.ebar.graph.Alerts.Alert alert(com.monentia.ebar.graph.Station station) {
		    com.monentia.ebar.graph.Alerts.Alert newElement = core$().graph().concept(com.monentia.ebar.graph.Alerts.Alert.class).createNode(name, core$()).as(com.monentia.ebar.graph.Alerts.Alert.class);
			newElement.core$().set(newElement, "station", java.util.Collections.singletonList(station));
		    return newElement;
		}

	}

	public Clear clear() {
		return new Clear();
	}

	public class Clear  {
		public void alert(java.util.function.Predicate<com.monentia.ebar.graph.Alerts.Alert> filter) {
			new java.util.ArrayList<>(alertList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
		}
	}

	public static class Alert extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
		protected boolean active;
		protected java.util.List<java.lang.String> mailingList = new java.util.ArrayList<>();
		protected com.monentia.ebar.graph.Station station;

		public Alert(io.intino.tara.magritte.Node node) {
			super(node);
		}

		public boolean active() {
			return active;
		}

		public java.util.List<java.lang.String> mailingList() {
			return mailingList;
		}

		public java.lang.String mailingList(int index) {
			return mailingList.get(index);
		}

		public java.util.List<java.lang.String> mailingList(java.util.function.Predicate<java.lang.String> predicate) {
			return mailingList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
		}

		public com.monentia.ebar.graph.Station station() {
			return station;
		}

		public Alert active(boolean value) {
			this.active = value;
			return (Alert) this;
		}

		public Alert station(com.monentia.ebar.graph.Station value) {
			this.station = value;
			return (Alert) this;
		}

		@Override
		protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
			java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
			map.put("active", new java.util.ArrayList(java.util.Collections.singletonList(this.active)));
			map.put("mailingList", this.mailingList);
			map.put("station", this.station != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.station)) : java.util.Collections.emptyList());
			return map;
		}

		@Override
		protected void load$(java.lang.String name, java.util.List<?> values) {
			super.load$(name, values);
			if (name.equalsIgnoreCase("active")) this.active = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
			else if (name.equalsIgnoreCase("mailingList")) this.mailingList = io.intino.tara.magritte.loaders.StringLoader.load(values, this);
			else if (name.equalsIgnoreCase("station")) this.station = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.Station.class, this).get(0);
		}

		@Override
		protected void set$(java.lang.String name, java.util.List<?> values) {
			super.set$(name, values);
			if (name.equalsIgnoreCase("active")) this.active = (java.lang.Boolean) values.get(0);
			else if (name.equalsIgnoreCase("mailingList")) this.mailingList = new java.util.ArrayList<>((java.util.List<java.lang.String>) values);
			else if (name.equalsIgnoreCase("station")) this.station = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.Station.class) : null;
		}


		public com.monentia.ebar.graph.TestEbarGraph graph() {
			return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
		}
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
