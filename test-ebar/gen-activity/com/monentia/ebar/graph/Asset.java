package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class Asset extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected java.lang.String label;
	protected java.lang.String coordinates;
	protected java.lang.String timeZone;

	public Asset(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public java.lang.String label() {
		return label;
	}

	public java.lang.String coordinates() {
		return coordinates;
	}

	public java.lang.String timeZone() {
		return timeZone;
	}

	public Asset label(java.lang.String value) {
		this.label = value;
		return (Asset) this;
	}

	public Asset coordinates(java.lang.String value) {
		this.coordinates = value;
		return (Asset) this;
	}

	public Asset timeZone(java.lang.String value) {
		this.timeZone = value;
		return (Asset) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		map.put("coordinates", new java.util.ArrayList(java.util.Collections.singletonList(this.coordinates)));
		map.put("timeZone", new java.util.ArrayList(java.util.Collections.singletonList(this.timeZone)));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("coordinates")) this.coordinates = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("timeZone")) this.timeZone = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
		else if (name.equalsIgnoreCase("coordinates")) this.coordinates = (java.lang.String) values.get(0);
		else if (name.equalsIgnoreCase("timeZone")) this.timeZone = (java.lang.String) values.get(0);
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create  {
		protected final java.lang.String name;

		public Create(java.lang.String name) {
			this.name = name;
		}



	}

	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
