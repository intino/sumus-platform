package com.monentia.ebar.graph;

import io.intino.tara.magritte.Graph;

public class AbstractGraph extends io.intino.tara.magritte.GraphWrapper {

	protected io.intino.tara.magritte.Graph graph;
	private io.intino.sumus.graph.Olap ebarOlap;
	private io.intino.sumus.graph.Ticket activationsCountTicket;
	private io.intino.sumus.graph.Ticket activationsDurationTicket;
	private io.intino.sumus.graph.Ticket totalUsageTicket;
	private io.intino.sumus.graph.Ticket averageDurationTicket;
	private io.intino.sumus.graph.Categorization stations;
	private io.intino.sumus.graph.Categorization componentTypes;
	private io.intino.sumus.graph.Categorization sequenceStations;
	private io.intino.sumus.graph.Categorization stationSequenceStates;
	private io.intino.sumus.graph.Categorization stationSequenceTypes;
	private io.intino.sumus.graph.Metric countMetric;
	private io.intino.sumus.graph.TemporalMetric timeMetric;
	private java.util.List<com.monentia.ebar.graph.StationView> stationViewList;
	private java.util.List<com.monentia.ebar.graph.Asset> assetList;
	private java.util.List<com.monentia.ebar.graph.Station> stationList;
	private java.util.List<com.monentia.ebar.graph.ComponentType> componentTypeList;
	private com.monentia.ebar.graph.Alerts alerts;
	private io.intino.sumus.graph.Cube measure;
	private io.intino.sumus.graph.Cube sequenceCube;
	private java.util.List<com.monentia.ebar.graph.Sequence> sequenceList;
	private java.util.List<com.monentia.ebar.graph.StationSequence> stationSequenceList;
	private java.util.List<com.monentia.ebar.graph.Activation> activationList;
	private java.util.List<com.monentia.ebar.graph.SensorState> sensorStateList;
	private io.intino.sumus.graph.NameSpace ebarNS;

	public AbstractGraph(io.intino.tara.magritte.Graph graph) {
		this.graph = graph;
		this.graph.i18n().register("TestEbar");
	}

	public AbstractGraph(io.intino.tara.magritte.Graph graph, AbstractGraph wrapper) {
		this.graph = graph;
		this.graph.i18n().register("TestEbar");
		this.ebarOlap = wrapper.ebarOlap;
		this.activationsCountTicket = wrapper.activationsCountTicket;
		this.activationsDurationTicket = wrapper.activationsDurationTicket;
		this.totalUsageTicket = wrapper.totalUsageTicket;
		this.averageDurationTicket = wrapper.averageDurationTicket;
		this.stations = wrapper.stations;
		this.componentTypes = wrapper.componentTypes;
		this.sequenceStations = wrapper.sequenceStations;
		this.stationSequenceStates = wrapper.stationSequenceStates;
		this.stationSequenceTypes = wrapper.stationSequenceTypes;
		this.countMetric = wrapper.countMetric;
		this.timeMetric = wrapper.timeMetric;
		this.stationViewList = new java.util.ArrayList<>(wrapper.stationViewList);
		this.assetList = new java.util.ArrayList<>(wrapper.assetList);
		this.stationList = new java.util.ArrayList<>(wrapper.stationList);
		this.componentTypeList = new java.util.ArrayList<>(wrapper.componentTypeList);
		this.alerts = wrapper.alerts;
		this.measure = wrapper.measure;
		this.sequenceCube = wrapper.sequenceCube;
		this.sequenceList = new java.util.ArrayList<>(wrapper.sequenceList);
		this.stationSequenceList = new java.util.ArrayList<>(wrapper.stationSequenceList);
		this.activationList = new java.util.ArrayList<>(wrapper.activationList);
		this.sensorStateList = new java.util.ArrayList<>(wrapper.sensorStateList);
		this.ebarNS = wrapper.ebarNS;
	}

    @Override
	public void update() {
		io.intino.tara.magritte.Node ebarOlapNode = this.graph.load("TestEbar#ebarOlap");
		if(ebarOlapNode != null) this.ebarOlap = ebarOlapNode.as(io.intino.sumus.graph.Olap.class);
		io.intino.tara.magritte.Node activationsCountTicketNode = this.graph.load("TestEbar#activationsCountTicket");
		if(activationsCountTicketNode != null) this.activationsCountTicket = activationsCountTicketNode.as(io.intino.sumus.graph.Ticket.class);
		io.intino.tara.magritte.Node activationsDurationTicketNode = this.graph.load("TestEbar#activationsDurationTicket");
		if(activationsDurationTicketNode != null) this.activationsDurationTicket = activationsDurationTicketNode.as(io.intino.sumus.graph.Ticket.class);
		io.intino.tara.magritte.Node totalUsageTicketNode = this.graph.load("TestEbar#totalUsageTicket");
		if(totalUsageTicketNode != null) this.totalUsageTicket = totalUsageTicketNode.as(io.intino.sumus.graph.Ticket.class);
		io.intino.tara.magritte.Node averageDurationTicketNode = this.graph.load("TestEbar#averageDurationTicket");
		if(averageDurationTicketNode != null) this.averageDurationTicket = averageDurationTicketNode.as(io.intino.sumus.graph.Ticket.class);
		io.intino.tara.magritte.Node stationsNode = this.graph.load("TestEbar#stations");
		if(stationsNode != null) this.stations = stationsNode.as(io.intino.sumus.graph.Categorization.class);
		io.intino.tara.magritte.Node componentTypesNode = this.graph.load("TestEbar#componentTypes");
		if(componentTypesNode != null) this.componentTypes = componentTypesNode.as(io.intino.sumus.graph.Categorization.class);
		io.intino.tara.magritte.Node sequenceStationsNode = this.graph.load("TestEbar#sequenceStations");
		if(sequenceStationsNode != null) this.sequenceStations = sequenceStationsNode.as(io.intino.sumus.graph.Categorization.class);
		io.intino.tara.magritte.Node stationSequenceStatesNode = this.graph.load("TestEbar#stationSequenceStates");
		if(stationSequenceStatesNode != null) this.stationSequenceStates = stationSequenceStatesNode.as(io.intino.sumus.graph.Categorization.class);
		io.intino.tara.magritte.Node stationSequenceTypesNode = this.graph.load("TestEbar#stationSequenceTypes");
		if(stationSequenceTypesNode != null) this.stationSequenceTypes = stationSequenceTypesNode.as(io.intino.sumus.graph.Categorization.class);
		io.intino.tara.magritte.Node countMetricNode = this.graph.load("TestEbar#countMetric");
		if(countMetricNode != null) this.countMetric = countMetricNode.as(io.intino.sumus.graph.Metric.class);
		io.intino.tara.magritte.Node timeMetricNode = this.graph.load("TestEbar#timeMetric");
		if(timeMetricNode != null) this.timeMetric = timeMetricNode.as(io.intino.sumus.graph.TemporalMetric.class);
		stationViewList = this.graph.rootList(com.monentia.ebar.graph.StationView.class);
		assetList = this.graph.rootList(com.monentia.ebar.graph.Asset.class);
		stationList = this.graph.rootList(com.monentia.ebar.graph.Station.class);
		componentTypeList = this.graph.rootList(com.monentia.ebar.graph.ComponentType.class);
		alerts = this.graph.rootList(com.monentia.ebar.graph.Alerts.class).stream().findFirst().orElse(null);
		io.intino.tara.magritte.Node measureNode = this.graph.load("TestEbar#Measure");
		if(measureNode != null) this.measure = measureNode.as(io.intino.sumus.graph.Cube.class);
		io.intino.tara.magritte.Node sequenceCubeNode = this.graph.load("TestEbar#SequenceCube");
		if(sequenceCubeNode != null) this.sequenceCube = sequenceCubeNode.as(io.intino.sumus.graph.Cube.class);
		sequenceList = this.graph.rootList(com.monentia.ebar.graph.Sequence.class);
		stationSequenceList = this.graph.rootList(com.monentia.ebar.graph.StationSequence.class);
		activationList = this.graph.rootList(com.monentia.ebar.graph.Activation.class);
		sensorStateList = this.graph.rootList(com.monentia.ebar.graph.SensorState.class);
		io.intino.tara.magritte.Node ebarNSNode = this.graph.load("TestEbar#ebarNS");
		if(ebarNSNode != null) this.ebarNS = ebarNSNode.as(io.intino.sumus.graph.NameSpace.class);
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		if(node.id().equals("TestEbar#ebarOlap")) this.ebarOlap = node.as(io.intino.sumus.graph.Olap.class);
		if(node.id().equals("TestEbar#activationsCountTicket")) this.activationsCountTicket = node.as(io.intino.sumus.graph.Ticket.class);
		if(node.id().equals("TestEbar#activationsDurationTicket")) this.activationsDurationTicket = node.as(io.intino.sumus.graph.Ticket.class);
		if(node.id().equals("TestEbar#totalUsageTicket")) this.totalUsageTicket = node.as(io.intino.sumus.graph.Ticket.class);
		if(node.id().equals("TestEbar#averageDurationTicket")) this.averageDurationTicket = node.as(io.intino.sumus.graph.Ticket.class);
		if(node.id().equals("TestEbar#stations")) this.stations = node.as(io.intino.sumus.graph.Categorization.class);
		if(node.id().equals("TestEbar#componentTypes")) this.componentTypes = node.as(io.intino.sumus.graph.Categorization.class);
		if(node.id().equals("TestEbar#sequenceStations")) this.sequenceStations = node.as(io.intino.sumus.graph.Categorization.class);
		if(node.id().equals("TestEbar#stationSequenceStates")) this.stationSequenceStates = node.as(io.intino.sumus.graph.Categorization.class);
		if(node.id().equals("TestEbar#stationSequenceTypes")) this.stationSequenceTypes = node.as(io.intino.sumus.graph.Categorization.class);
		if(node.id().equals("TestEbar#countMetric")) this.countMetric = node.as(io.intino.sumus.graph.Metric.class);
		if(node.id().equals("TestEbar#timeMetric")) this.timeMetric = node.as(io.intino.sumus.graph.TemporalMetric.class);
		if (node.is("StationView")) this.stationViewList.add(node.as(com.monentia.ebar.graph.StationView.class));
		if (node.is("Asset")) this.assetList.add(node.as(com.monentia.ebar.graph.Asset.class));
		if (node.is("Station")) this.stationList.add(node.as(com.monentia.ebar.graph.Station.class));
		if (node.is("ComponentType")) this.componentTypeList.add(node.as(com.monentia.ebar.graph.ComponentType.class));
		if (node.is("Alerts")) this.alerts = node.as(com.monentia.ebar.graph.Alerts.class);
		if(node.id().equals("TestEbar#Measure")) this.measure = node.as(io.intino.sumus.graph.Cube.class);
		if(node.id().equals("TestEbar#SequenceCube")) this.sequenceCube = node.as(io.intino.sumus.graph.Cube.class);
		if (node.is("Sequence")) this.sequenceList.add(node.as(com.monentia.ebar.graph.Sequence.class));
		if (node.is("StationSequence")) this.stationSequenceList.add(node.as(com.monentia.ebar.graph.StationSequence.class));
		if (node.is("Activation")) this.activationList.add(node.as(com.monentia.ebar.graph.Activation.class));
		if (node.is("SensorState")) this.sensorStateList.add(node.as(com.monentia.ebar.graph.SensorState.class));
		if(node.id().equals("TestEbar#ebarNS")) this.ebarNS = node.as(io.intino.sumus.graph.NameSpace.class);
	}

	@Override
	protected void removeNode$(io.intino.tara.magritte.Node node) {
		if(node.id().equals("TestEbar#ebarOlap")) this.ebarOlap = null;
		if(node.id().equals("TestEbar#activationsCountTicket")) this.activationsCountTicket = null;
		if(node.id().equals("TestEbar#activationsDurationTicket")) this.activationsDurationTicket = null;
		if(node.id().equals("TestEbar#totalUsageTicket")) this.totalUsageTicket = null;
		if(node.id().equals("TestEbar#averageDurationTicket")) this.averageDurationTicket = null;
		if(node.id().equals("TestEbar#stations")) this.stations = null;
		if(node.id().equals("TestEbar#componentTypes")) this.componentTypes = null;
		if(node.id().equals("TestEbar#sequenceStations")) this.sequenceStations = null;
		if(node.id().equals("TestEbar#stationSequenceStates")) this.stationSequenceStates = null;
		if(node.id().equals("TestEbar#stationSequenceTypes")) this.stationSequenceTypes = null;
		if(node.id().equals("TestEbar#countMetric")) this.countMetric = null;
		if(node.id().equals("TestEbar#timeMetric")) this.timeMetric = null;
		if (node.is("StationView")) this.stationViewList.remove(node.as(com.monentia.ebar.graph.StationView.class));
		if (node.is("Asset")) this.assetList.remove(node.as(com.monentia.ebar.graph.Asset.class));
		if (node.is("Station")) this.stationList.remove(node.as(com.monentia.ebar.graph.Station.class));
		if (node.is("ComponentType")) this.componentTypeList.remove(node.as(com.monentia.ebar.graph.ComponentType.class));
		if (node.is("Alerts")) this.alerts = null;
		if(node.id().equals("TestEbar#Measure")) this.measure = null;
		if(node.id().equals("TestEbar#SequenceCube")) this.sequenceCube = null;
		if (node.is("Sequence")) this.sequenceList.remove(node.as(com.monentia.ebar.graph.Sequence.class));
		if (node.is("StationSequence")) this.stationSequenceList.remove(node.as(com.monentia.ebar.graph.StationSequence.class));
		if (node.is("Activation")) this.activationList.remove(node.as(com.monentia.ebar.graph.Activation.class));
		if (node.is("SensorState")) this.sensorStateList.remove(node.as(com.monentia.ebar.graph.SensorState.class));
		if(node.id().equals("TestEbar#ebarNS")) this.ebarNS = null;
	}

	public java.net.URL resourceAsMessage$(String language, String key) {
		return graph.loadResource(graph.i18n().message(language, key));
	}

	public io.intino.sumus.graph.Olap ebarOlap() {
		return ebarOlap;
	}

	public io.intino.sumus.graph.Ticket activationsCountTicket() {
		return activationsCountTicket;
	}

	public io.intino.sumus.graph.Ticket activationsDurationTicket() {
		return activationsDurationTicket;
	}

	public io.intino.sumus.graph.Ticket totalUsageTicket() {
		return totalUsageTicket;
	}

	public io.intino.sumus.graph.Ticket averageDurationTicket() {
		return averageDurationTicket;
	}

	public io.intino.sumus.graph.Categorization stations() {
		return stations;
	}

	public io.intino.sumus.graph.Categorization componentTypes() {
		return componentTypes;
	}

	public io.intino.sumus.graph.Categorization sequenceStations() {
		return sequenceStations;
	}

	public io.intino.sumus.graph.Categorization stationSequenceStates() {
		return stationSequenceStates;
	}

	public io.intino.sumus.graph.Categorization stationSequenceTypes() {
		return stationSequenceTypes;
	}

	public io.intino.sumus.graph.Metric countMetric() {
		return countMetric;
	}

	public io.intino.sumus.graph.TemporalMetric timeMetric() {
		return timeMetric;
	}

	public java.util.List<com.monentia.ebar.graph.StationView> stationViewList() {
		return stationViewList;
	}

	public java.util.List<com.monentia.ebar.graph.Asset> assetList() {
		return assetList;
	}

	public java.util.List<com.monentia.ebar.graph.Station> stationList() {
		return stationList;
	}

	public java.util.List<com.monentia.ebar.graph.ComponentType> componentTypeList() {
		return componentTypeList;
	}

	public com.monentia.ebar.graph.Alerts alerts() {
		return alerts;
	}

	public io.intino.sumus.graph.Cube measure() {
		return measure;
	}

	public io.intino.sumus.graph.Cube sequenceCube() {
		return sequenceCube;
	}

	public java.util.List<com.monentia.ebar.graph.Sequence> sequenceList() {
		return sequenceList;
	}

	public java.util.List<com.monentia.ebar.graph.StationSequence> stationSequenceList() {
		return stationSequenceList;
	}

	public java.util.List<com.monentia.ebar.graph.Activation> activationList() {
		return activationList;
	}

	public java.util.List<com.monentia.ebar.graph.SensorState> sensorStateList() {
		return sensorStateList;
	}

	public io.intino.sumus.graph.NameSpace ebarNS() {
		return ebarNS;
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.StationView> stationViewList(java.util.function.Predicate<com.monentia.ebar.graph.StationView> filter) {
		return stationViewList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.StationView stationView(int index) {
		return stationViewList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.Asset> assetList(java.util.function.Predicate<com.monentia.ebar.graph.Asset> filter) {
		return assetList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.Asset asset(int index) {
		return assetList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.Station> stationList(java.util.function.Predicate<com.monentia.ebar.graph.Station> filter) {
		return stationList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.Station station(int index) {
		return stationList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.ComponentType> componentTypeList(java.util.function.Predicate<com.monentia.ebar.graph.ComponentType> filter) {
		return componentTypeList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.ComponentType componentType(int index) {
		return componentTypeList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.Sequence> sequenceList(java.util.function.Predicate<com.monentia.ebar.graph.Sequence> filter) {
		return sequenceList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.Sequence sequence(int index) {
		return sequenceList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.StationSequence> stationSequenceList(java.util.function.Predicate<com.monentia.ebar.graph.StationSequence> filter) {
		return stationSequenceList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.StationSequence stationSequence(int index) {
		return stationSequenceList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.Activation> activationList(java.util.function.Predicate<com.monentia.ebar.graph.Activation> filter) {
		return activationList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.Activation activation(int index) {
		return activationList.get(index);
	}

	public java.util.stream.Stream<com.monentia.ebar.graph.SensorState> sensorStateList(java.util.function.Predicate<com.monentia.ebar.graph.SensorState> filter) {
		return sensorStateList.stream().filter(filter);
	}

	public com.monentia.ebar.graph.SensorState sensorState(int index) {
		return sensorStateList.get(index);
	}

	public io.intino.tara.magritte.Graph core$() {
		return graph;
	}

	public io.intino.tara.magritte.utils.I18n i18n$() {
		return graph.i18n();
	}

	public Create create() {
		return new Create("Misc", null);
	}

	public Create create(String stash) {
		return new Create(stash, null);
	}

	public Create create(String stash, String name) {
		return new Create(stash, name);
	}

	public Clear clear() {
		return new Clear();
	}

	public class Create {
		private final String stash;
		private final String name;

		public Create(String stash, String name) {
			this.stash = stash;
			this.name = name;
		}

		public com.monentia.ebar.graph.StationView stationView(com.monentia.ebar.graph.Station station, com.monentia.ebar.graph.rules.State pumpA, com.monentia.ebar.graph.rules.State pumpB, boolean float1, boolean float2, boolean float3, boolean online, java.time.Instant created) {
			com.monentia.ebar.graph.StationView newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.StationView.class, stash, name).a$(com.monentia.ebar.graph.StationView.class);
			newElement.core$().set(newElement, "station", java.util.Collections.singletonList(station));
			newElement.core$().set(newElement, "pumpA", java.util.Collections.singletonList(pumpA));
			newElement.core$().set(newElement, "pumpB", java.util.Collections.singletonList(pumpB));
			newElement.core$().set(newElement, "float1", java.util.Collections.singletonList(float1));
			newElement.core$().set(newElement, "float2", java.util.Collections.singletonList(float2));
			newElement.core$().set(newElement, "float3", java.util.Collections.singletonList(float3));
			newElement.core$().set(newElement, "online", java.util.Collections.singletonList(online));
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
			return newElement;
		}

		public com.monentia.ebar.graph.Asset asset(java.lang.String label, java.lang.String coordinates) {
			com.monentia.ebar.graph.Asset newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.Asset.class, stash, name).a$(com.monentia.ebar.graph.Asset.class);
			newElement.core$().set(newElement, "label", java.util.Collections.singletonList(label));
			newElement.core$().set(newElement, "coordinates", java.util.Collections.singletonList(coordinates));
			return newElement;
		}

		public com.monentia.ebar.graph.Station station(java.lang.String label, java.lang.String coordinates) {
			com.monentia.ebar.graph.Station newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.Station.class, stash, name).a$(com.monentia.ebar.graph.Station.class);
			newElement.core$().set(newElement, "label", java.util.Collections.singletonList(label));
			newElement.core$().set(newElement, "coordinates", java.util.Collections.singletonList(coordinates));
			return newElement;
		}

		public com.monentia.ebar.graph.ComponentType componentType(java.lang.String label) {
			com.monentia.ebar.graph.ComponentType newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.ComponentType.class, stash, name).a$(com.monentia.ebar.graph.ComponentType.class);
			newElement.core$().set(newElement, "label", java.util.Collections.singletonList(label));
			return newElement;
		}

		public com.monentia.ebar.graph.Alerts alerts() {
			com.monentia.ebar.graph.Alerts newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.Alerts.class, stash, name).a$(com.monentia.ebar.graph.Alerts.class);

			return newElement;
		}

		public com.monentia.ebar.graph.StationSequence stationSequence(com.monentia.ebar.graph.Station station, java.time.Instant created) {
			com.monentia.ebar.graph.StationSequence newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.StationSequence.class, stash, name).a$(com.monentia.ebar.graph.StationSequence.class);
			newElement.core$().set(newElement, "station", java.util.Collections.singletonList(station));
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
			return newElement;
		}

		public com.monentia.ebar.graph.Activation activation(com.monentia.ebar.graph.Station station, com.monentia.ebar.graph.ComponentType componentType, java.time.Instant created) {
			com.monentia.ebar.graph.Activation newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.Activation.class, stash, name).a$(com.monentia.ebar.graph.Activation.class);
			newElement.core$().set(newElement, "station", java.util.Collections.singletonList(station));
			newElement.core$().set(newElement, "componentType", java.util.Collections.singletonList(componentType));
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
			return newElement;
		}

		public com.monentia.ebar.graph.SensorState sensorState(com.monentia.ebar.graph.Station.Sensor sensor, boolean value, java.time.Instant created) {
			com.monentia.ebar.graph.SensorState newElement = AbstractGraph.this.graph.createRoot(com.monentia.ebar.graph.SensorState.class, stash, name).a$(com.monentia.ebar.graph.SensorState.class);
			newElement.core$().set(newElement, "sensor", java.util.Collections.singletonList(sensor));
			newElement.core$().set(newElement, "value", java.util.Collections.singletonList(value));
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
			return newElement;
		}
	}

	public class Clear {
	    public void stationView(java.util.function.Predicate<com.monentia.ebar.graph.StationView> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.stationViewList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void asset(java.util.function.Predicate<com.monentia.ebar.graph.Asset> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.assetList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void station(java.util.function.Predicate<com.monentia.ebar.graph.Station> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.stationList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void componentType(java.util.function.Predicate<com.monentia.ebar.graph.ComponentType> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.componentTypeList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void stationSequence(java.util.function.Predicate<com.monentia.ebar.graph.StationSequence> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.stationSequenceList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void activation(java.util.function.Predicate<com.monentia.ebar.graph.Activation> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.activationList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }

	    public void sensorState(java.util.function.Predicate<com.monentia.ebar.graph.SensorState> filter) {
	    	new java.util.ArrayList<>(AbstractGraph.this.sensorStateList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
	    }
	}
}