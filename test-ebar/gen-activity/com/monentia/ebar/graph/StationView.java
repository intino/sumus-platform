package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class StationView extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected com.monentia.ebar.graph.Station station;
	protected com.monentia.ebar.graph.rules.State pumpA;
	protected com.monentia.ebar.graph.rules.State pumpB;
	protected boolean float1;
	protected boolean float2;
	protected boolean float3;
	protected boolean online;
	protected io.intino.tara.magritte.Expression<java.lang.String> icon;
	protected io.intino.sumus.graph.Report _metaType;

	public StationView(io.intino.tara.magritte.Node node) {
		super(node);
		_metaType = a$(io.intino.sumus.graph.Report.class);
	}

	public com.monentia.ebar.graph.Station station() {
		return station;
	}

	public com.monentia.ebar.graph.rules.State pumpA() {
		return pumpA;
	}

	public com.monentia.ebar.graph.rules.State pumpB() {
		return pumpB;
	}

	public boolean float1() {
		return float1;
	}

	public boolean float2() {
		return float2;
	}

	public boolean float3() {
		return float3;
	}

	public boolean online() {
		return online;
	}

	public java.lang.String icon() {
		return icon.value();
	}

	public java.time.Instant created() {
		return _metaType.created();
	}

	public StationView station(com.monentia.ebar.graph.Station value) {
		this.station = value;
		return (StationView) this;
	}

	public StationView pumpA(com.monentia.ebar.graph.rules.State value) {
		this.pumpA = value;
		return (StationView) this;
	}

	public StationView pumpB(com.monentia.ebar.graph.rules.State value) {
		this.pumpB = value;
		return (StationView) this;
	}

	public StationView float1(boolean value) {
		this.float1 = value;
		return (StationView) this;
	}

	public StationView float2(boolean value) {
		this.float2 = value;
		return (StationView) this;
	}

	public StationView float3(boolean value) {
		this.float3 = value;
		return (StationView) this;
	}

	public StationView online(boolean value) {
		this.online = value;
		return (StationView) this;
	}

	public StationView icon(io.intino.tara.magritte.Expression<java.lang.String> value) {
		this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(value, this, io.intino.tara.magritte.Expression.class);
		return (StationView) this;
	}

	public StationView created(java.time.Instant value) {
		this._metaType.created(value);
		return (StationView) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("station", this.station != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.station)) : java.util.Collections.emptyList());
		map.put("pumpA", new java.util.ArrayList(java.util.Collections.singletonList(this.pumpA)));
		map.put("pumpB", new java.util.ArrayList(java.util.Collections.singletonList(this.pumpB)));
		map.put("float1", new java.util.ArrayList(java.util.Collections.singletonList(this.float1)));
		map.put("float2", new java.util.ArrayList(java.util.Collections.singletonList(this.float2)));
		map.put("float3", new java.util.ArrayList(java.util.Collections.singletonList(this.float3)));
		map.put("online", new java.util.ArrayList(java.util.Collections.singletonList(this.online)));
		map.put("icon", new java.util.ArrayList(java.util.Collections.singletonList(this.icon)));
		map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		core$().load(_metaType, name, values);
		if (name.equalsIgnoreCase("station")) this.station = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.Station.class, this).get(0);
		else if (name.equalsIgnoreCase("pumpA")) this.pumpA = io.intino.tara.magritte.loaders.WordLoader.load(values, com.monentia.ebar.graph.rules.State.class, this).get(0);
		else if (name.equalsIgnoreCase("pumpB")) this.pumpB = io.intino.tara.magritte.loaders.WordLoader.load(values, com.monentia.ebar.graph.rules.State.class, this).get(0);
		else if (name.equalsIgnoreCase("float1")) this.float1 = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("float2")) this.float2 = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("float3")) this.float3 = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("online")) this.online = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("icon")) this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		core$().set(_metaType, name, values);
		if (name.equalsIgnoreCase("station")) this.station = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.Station.class) : null;
		else if (name.equalsIgnoreCase("pumpA")) this.pumpA = (com.monentia.ebar.graph.rules.State) values.get(0);
		else if (name.equalsIgnoreCase("pumpB")) this.pumpB = (com.monentia.ebar.graph.rules.State) values.get(0);
		else if (name.equalsIgnoreCase("float1")) this.float1 = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("float2")) this.float2 = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("float3")) this.float3 = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("online")) this.online = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("icon")) this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
