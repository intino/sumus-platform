package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public abstract class AbstractStation extends com.monentia.ebar.graph.Asset implements io.intino.tara.magritte.tags.Terminal {
	protected boolean online;
	protected java.lang.String currentSequence;
	protected java.lang.String lastStationView;
	protected java.util.List<com.monentia.ebar.graph.Station.Sensor> sensorList = new java.util.ArrayList<>();

	public AbstractStation(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public boolean online() {
		return online;
	}

	public java.lang.String currentSequence() {
		return currentSequence;
	}

	public java.lang.String lastStationView() {
		return lastStationView;
	}

	public Station online(boolean value) {
		this.online = value;
		return (Station) this;
	}

	public Station currentSequence(java.lang.String value) {
		this.currentSequence = value;
		return (Station) this;
	}

	public Station lastStationView(java.lang.String value) {
		this.lastStationView = value;
		return (Station) this;
	}

	public java.util.List<com.monentia.ebar.graph.Station.Sensor> sensorList() {
		return java.util.Collections.unmodifiableList(sensorList);
	}

	public com.monentia.ebar.graph.Station.Sensor sensor(int index) {
		return sensorList.get(index);
	}

	public java.util.List<com.monentia.ebar.graph.Station.Sensor> sensorList(java.util.function.Predicate<com.monentia.ebar.graph.Station.Sensor> predicate) {
		return sensorList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}



	protected java.util.List<io.intino.tara.magritte.Node> componentList$() {
		java.util.Set<io.intino.tara.magritte.Node> components = new java.util.LinkedHashSet<>(super.componentList$());
		new java.util.ArrayList<>(sensorList).forEach(c -> components.add(c.core$()));
		return new java.util.ArrayList<>(components);
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>(super.variables$());
		map.put("online", new java.util.ArrayList(java.util.Collections.singletonList(this.online)));
		map.put("currentSequence", new java.util.ArrayList(java.util.Collections.singletonList(this.currentSequence)));
		map.put("lastStationView", new java.util.ArrayList(java.util.Collections.singletonList(this.lastStationView)));
		return map;
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		super.addNode$(node);
		if (node.is("Station$Sensor")) this.sensorList.add(node.as(com.monentia.ebar.graph.Station.Sensor.class));
	}

	@Override
    protected void removeNode$(io.intino.tara.magritte.Node node) {
        super.removeNode$(node);
        if (node.is("Station$Sensor")) this.sensorList.remove(node.as(com.monentia.ebar.graph.Station.Sensor.class));
    }

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		if (name.equalsIgnoreCase("online")) this.online = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("currentSequence")) this.currentSequence = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("lastStationView")) this.lastStationView = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		if (name.equalsIgnoreCase("online")) this.online = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("currentSequence")) this.currentSequence = (java.lang.String) values.get(0);
		else if (name.equalsIgnoreCase("lastStationView")) this.lastStationView = (java.lang.String) values.get(0);
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create extends com.monentia.ebar.graph.Asset.Create {


		public Create(java.lang.String name) {
			super(name);
		}

		public com.monentia.ebar.graph.Station.Sensor sensor(com.monentia.ebar.graph.ComponentType type) {
		    com.monentia.ebar.graph.Station.Sensor newElement = core$().graph().concept(com.monentia.ebar.graph.Station.Sensor.class).createNode(name, core$()).as(com.monentia.ebar.graph.Station.Sensor.class);
			newElement.core$().set(newElement, "type", java.util.Collections.singletonList(type));
		    return newElement;
		}

	}

	public Clear clear() {
		return new Clear();
	}

	public class Clear  {
		public void sensor(java.util.function.Predicate<com.monentia.ebar.graph.Station.Sensor> filter) {
			new java.util.ArrayList<>(sensorList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
		}
	}

	public static class Sensor extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
		protected com.monentia.ebar.graph.ComponentType type;
		protected boolean lastValue;
		protected java.time.Instant updated;

		public Sensor(io.intino.tara.magritte.Node node) {
			super(node);
		}

		public com.monentia.ebar.graph.ComponentType type() {
			return type;
		}

		public boolean lastValue() {
			return lastValue;
		}

		public java.time.Instant updated() {
			return updated;
		}

		public Sensor type(com.monentia.ebar.graph.ComponentType value) {
			this.type = value;
			return (Sensor) this;
		}

		public Sensor lastValue(boolean value) {
			this.lastValue = value;
			return (Sensor) this;
		}

		public Sensor updated(java.time.Instant value) {
			this.updated = value;
			return (Sensor) this;
		}

		@Override
		protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
			java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
			map.put("type", this.type != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.type)) : java.util.Collections.emptyList());
			map.put("lastValue", new java.util.ArrayList(java.util.Collections.singletonList(this.lastValue)));
			map.put("updated", new java.util.ArrayList(java.util.Collections.singletonList(this.updated)));
			return map;
		}

		@Override
		protected void load$(java.lang.String name, java.util.List<?> values) {
			super.load$(name, values);
			if (name.equalsIgnoreCase("type")) this.type = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.ComponentType.class, this).get(0);
			else if (name.equalsIgnoreCase("lastValue")) this.lastValue = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
			else if (name.equalsIgnoreCase("updated")) this.updated = io.intino.tara.magritte.loaders.InstantLoader.load(values, this).get(0);
		}

		@Override
		protected void set$(java.lang.String name, java.util.List<?> values) {
			super.set$(name, values);
			if (name.equalsIgnoreCase("type")) this.type = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.ComponentType.class) : null;
			else if (name.equalsIgnoreCase("lastValue")) this.lastValue = (java.lang.Boolean) values.get(0);
			else if (name.equalsIgnoreCase("updated")) this.updated = (java.time.Instant) values.get(0);
		}


		public com.monentia.ebar.graph.TestEbarGraph graph() {
			return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
		}
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
