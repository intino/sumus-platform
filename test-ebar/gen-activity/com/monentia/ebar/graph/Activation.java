package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class Activation extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected java.lang.String label;
	protected com.monentia.ebar.graph.Station station;
	protected com.monentia.ebar.graph.ComponentType componentType;
	protected java.util.List<com.monentia.ebar.graph.Activation.Active> activeList = new java.util.ArrayList<>();
	protected java.util.List<com.monentia.ebar.graph.Activation.Inactive> inactiveList = new java.util.ArrayList<>();
	protected io.intino.sumus.graph.Episode _metaType;

	public Activation(io.intino.tara.magritte.Node node) {
		super(node);
		_metaType = a$(io.intino.sumus.graph.Episode.class);
	}

	public java.lang.String label() {
		return label;
	}

	public com.monentia.ebar.graph.Station station() {
		return station;
	}

	public com.monentia.ebar.graph.ComponentType componentType() {
		return componentType;
	}

	public java.time.Instant created() {
		return _metaType.created();
	}

	public Activation label(java.lang.String value) {
		this.label = value;
		return (Activation) this;
	}

	public Activation station(com.monentia.ebar.graph.Station value) {
		this.station = value;
		return (Activation) this;
	}

	public Activation componentType(com.monentia.ebar.graph.ComponentType value) {
		this.componentType = value;
		return (Activation) this;
	}

	public Activation created(java.time.Instant value) {
		this._metaType.created(value);
		return (Activation) this;
	}

	public java.util.List<com.monentia.ebar.graph.Activation.Active> activeList() {
		return java.util.Collections.unmodifiableList(activeList);
	}

	public com.monentia.ebar.graph.Activation.Active active(int index) {
		return activeList.get(index);
	}

	public java.util.List<com.monentia.ebar.graph.Activation.Active> activeList(java.util.function.Predicate<com.monentia.ebar.graph.Activation.Active> predicate) {
		return activeList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}

	public java.util.List<com.monentia.ebar.graph.Activation.Inactive> inactiveList() {
		return java.util.Collections.unmodifiableList(inactiveList);
	}

	public com.monentia.ebar.graph.Activation.Inactive inactive(int index) {
		return inactiveList.get(index);
	}

	public java.util.List<com.monentia.ebar.graph.Activation.Inactive> inactiveList(java.util.function.Predicate<com.monentia.ebar.graph.Activation.Inactive> predicate) {
		return inactiveList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}





	protected java.util.List<io.intino.tara.magritte.Node> componentList$() {
		java.util.Set<io.intino.tara.magritte.Node> components = new java.util.LinkedHashSet<>(super.componentList$());
		new java.util.ArrayList<>(activeList).forEach(c -> components.add(c.core$()));
		new java.util.ArrayList<>(inactiveList).forEach(c -> components.add(c.core$()));
		return new java.util.ArrayList<>(components);
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		map.put("station", this.station != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.station)) : java.util.Collections.emptyList());
		map.put("componentType", this.componentType != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.componentType)) : java.util.Collections.emptyList());
		map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
		return map;
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		super.addNode$(node);
		if (node.is("Activation$Active")) this.activeList.add(node.as(com.monentia.ebar.graph.Activation.Active.class));
		if (node.is("Activation$Inactive")) this.inactiveList.add(node.as(com.monentia.ebar.graph.Activation.Inactive.class));
	}

	@Override
    protected void removeNode$(io.intino.tara.magritte.Node node) {
        super.removeNode$(node);
        if (node.is("Activation$Active")) this.activeList.remove(node.as(com.monentia.ebar.graph.Activation.Active.class));
        if (node.is("Activation$Inactive")) this.inactiveList.remove(node.as(com.monentia.ebar.graph.Activation.Inactive.class));
    }

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		core$().load(_metaType, name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("station")) this.station = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.Station.class, this).get(0);
		else if (name.equalsIgnoreCase("componentType")) this.componentType = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.ComponentType.class, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		core$().set(_metaType, name, values);
		if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
		else if (name.equalsIgnoreCase("station")) this.station = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.Station.class) : null;
		else if (name.equalsIgnoreCase("componentType")) this.componentType = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.ComponentType.class) : null;
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create  {
		protected final java.lang.String name;

		public Create(java.lang.String name) {
			this.name = name;
		}

		public com.monentia.ebar.graph.Activation.Active active(java.time.Instant created) {
		    com.monentia.ebar.graph.Activation.Active newElement = core$().graph().concept(com.monentia.ebar.graph.Activation.Active.class).createNode(name, core$()).as(com.monentia.ebar.graph.Activation.Active.class);
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
		    return newElement;
		}

		public com.monentia.ebar.graph.Activation.Inactive inactive(java.time.Instant created) {
		    com.monentia.ebar.graph.Activation.Inactive newElement = core$().graph().concept(com.monentia.ebar.graph.Activation.Inactive.class).createNode(name, core$()).as(com.monentia.ebar.graph.Activation.Inactive.class);
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
		    return newElement;
		}

	}

	public Clear clear() {
		return new Clear();
	}

	public class Clear  {
		public void active(java.util.function.Predicate<com.monentia.ebar.graph.Activation.Active> filter) {
			new java.util.ArrayList<>(activeList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
		}

		public void inactive(java.util.function.Predicate<com.monentia.ebar.graph.Activation.Inactive> filter) {
			new java.util.ArrayList<>(inactiveList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
		}
	}

	public static class Active extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
		protected java.lang.String label;
		protected io.intino.sumus.graph.Event _metaType;

		public Active(io.intino.tara.magritte.Node node) {
			super(node);
			_metaType = a$(io.intino.sumus.graph.Event.class);
		}

		public java.lang.String label() {
			return label;
		}

		public java.time.Instant created() {
			return _metaType.created();
		}

		public Active label(java.lang.String value) {
			this.label = value;
			return (Active) this;
		}

		public Active created(java.time.Instant value) {
			this._metaType.created(value);
			return (Active) this;
		}

		@Override
		protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
			java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
			map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
			map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
			return map;
		}

		@Override
		protected void load$(java.lang.String name, java.util.List<?> values) {
			super.load$(name, values);
			core$().load(_metaType, name, values);
			if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		}

		@Override
		protected void set$(java.lang.String name, java.util.List<?> values) {
			super.set$(name, values);
			core$().set(_metaType, name, values);
			if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
		}


		public com.monentia.ebar.graph.TestEbarGraph graph() {
			return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
		}
	}

	public static class Inactive extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
		protected java.lang.String label;
		protected io.intino.sumus.graph.Event _metaType;

		public Inactive(io.intino.tara.magritte.Node node) {
			super(node);
			_metaType = a$(io.intino.sumus.graph.Event.class);
		}

		public java.lang.String label() {
			return label;
		}

		public java.time.Instant created() {
			return _metaType.created();
		}

		public Inactive label(java.lang.String value) {
			this.label = value;
			return (Inactive) this;
		}

		public Inactive created(java.time.Instant value) {
			this._metaType.created(value);
			return (Inactive) this;
		}

		@Override
		protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
			java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
			map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
			map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
			return map;
		}

		@Override
		protected void load$(java.lang.String name, java.util.List<?> values) {
			super.load$(name, values);
			core$().load(_metaType, name, values);
			if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.StringLoader.load(values, this).get(0);
		}

		@Override
		protected void set$(java.lang.String name, java.util.List<?> values) {
			super.set$(name, values);
			core$().set(_metaType, name, values);
			if (name.equalsIgnoreCase("label")) this.label = (java.lang.String) values.get(0);
		}


		public com.monentia.ebar.graph.TestEbarGraph graph() {
			return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
		}
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
