package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class StationSequence extends com.monentia.ebar.graph.Sequence implements io.intino.tara.magritte.tags.Terminal {
	protected com.monentia.ebar.graph.Station station;
	protected io.intino.tara.magritte.Expression<java.lang.String> label;
	protected io.intino.tara.magritte.Expression<java.net.URL> icon;
	protected java.util.List<java.lang.String> failures = new java.util.ArrayList<>();
	protected boolean closed;
	protected io.intino.tara.magritte.Expression<com.monentia.ebar.PumpDuration> pumpsDuration;
	protected io.intino.tara.magritte.Expression<java.util.List<com.monentia.ebar.graph.SensorState>> sortedStates;
	protected java.util.List<com.monentia.ebar.graph.SensorState> sensorStateList = new java.util.ArrayList<>();

	public StationSequence(io.intino.tara.magritte.Node node) {
		super(node);
	}

	public com.monentia.ebar.graph.Station station() {
		return station;
	}

	public java.lang.String label() {
		return label.value();
	}

	public java.net.URL icon() {
		return icon.value();
	}

	public java.util.List<java.lang.String> failures() {
		return failures;
	}

	public java.lang.String failures(int index) {
		return failures.get(index);
	}

	public java.util.List<java.lang.String> failures(java.util.function.Predicate<java.lang.String> predicate) {
		return failures().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}

	public boolean closed() {
		return closed;
	}

	public com.monentia.ebar.PumpDuration pumpsDuration() {
		return pumpsDuration.value();
	}

	public java.util.List<com.monentia.ebar.graph.SensorState> sortedStates() {
		return sortedStates.value();
	}

	public java.time.Instant created() {
		return _metaType.created();
	}

	public StationSequence station(com.monentia.ebar.graph.Station value) {
		this.station = value;
		return (StationSequence) this;
	}

	public StationSequence label(io.intino.tara.magritte.Expression<java.lang.String> value) {
		this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(value, this, io.intino.tara.magritte.Expression.class);
		return (StationSequence) this;
	}

	public StationSequence icon(io.intino.tara.magritte.Expression<java.net.URL> value) {
		this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(value, this, io.intino.tara.magritte.Expression.class);
		return (StationSequence) this;
	}

	public StationSequence closed(boolean value) {
		this.closed = value;
		return (StationSequence) this;
	}

	public StationSequence pumpsDuration(io.intino.tara.magritte.Expression<com.monentia.ebar.PumpDuration> value) {
		this.pumpsDuration = io.intino.tara.magritte.loaders.FunctionLoader.load(value, this, io.intino.tara.magritte.Expression.class);
		return (StationSequence) this;
	}

	public StationSequence created(java.time.Instant value) {
		this._metaType.created(value);
		return (StationSequence) this;
	}

	public java.util.List<com.monentia.ebar.graph.SensorState> sensorStateList() {
		return java.util.Collections.unmodifiableList(sensorStateList);
	}

	public com.monentia.ebar.graph.SensorState sensorState(int index) {
		return sensorStateList.get(index);
	}

	public java.util.List<com.monentia.ebar.graph.SensorState> sensorStateList(java.util.function.Predicate<com.monentia.ebar.graph.SensorState> predicate) {
		return sensorStateList().stream().filter(predicate).collect(java.util.stream.Collectors.toList());
	}



	protected java.util.List<io.intino.tara.magritte.Node> componentList$() {
		java.util.Set<io.intino.tara.magritte.Node> components = new java.util.LinkedHashSet<>(super.componentList$());
		new java.util.ArrayList<>(sensorStateList).forEach(c -> components.add(c.core$()));
		return new java.util.ArrayList<>(components);
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>(super.variables$());
		map.put("station", this.station != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.station)) : java.util.Collections.emptyList());
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		map.put("icon", new java.util.ArrayList(java.util.Collections.singletonList(this.icon)));
		map.put("failures", this.failures);
		map.put("closed", new java.util.ArrayList(java.util.Collections.singletonList(this.closed)));
		map.put("pumpsDuration", new java.util.ArrayList(java.util.Collections.singletonList(this.pumpsDuration)));
		map.put("sortedStates", this.sortedStates != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.sortedStates)) : java.util.Collections.emptyList());
		return map;
	}

	@Override
	protected void addNode$(io.intino.tara.magritte.Node node) {
		super.addNode$(node);
		if (node.is("SensorState")) this.sensorStateList.add(node.as(com.monentia.ebar.graph.SensorState.class));
	}

	@Override
    protected void removeNode$(io.intino.tara.magritte.Node node) {
        super.removeNode$(node);
        if (node.is("SensorState")) this.sensorStateList.remove(node.as(com.monentia.ebar.graph.SensorState.class));
    }

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		if (name.equalsIgnoreCase("station")) this.station = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.Station.class, this).get(0);
		else if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
		else if (name.equalsIgnoreCase("icon")) this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
		else if (name.equalsIgnoreCase("failures")) this.failures = io.intino.tara.magritte.loaders.StringLoader.load(values, this);
		else if (name.equalsIgnoreCase("closed")) this.closed = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
		else if (name.equalsIgnoreCase("pumpsDuration")) this.pumpsDuration = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
		else if (name.equalsIgnoreCase("sortedStates")) this.sortedStates = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		if (name.equalsIgnoreCase("station")) this.station = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.Station.class) : null;
		else if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
		else if (name.equalsIgnoreCase("icon")) this.icon = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
		else if (name.equalsIgnoreCase("failures")) this.failures = new java.util.ArrayList<>((java.util.List<java.lang.String>) values);
		else if (name.equalsIgnoreCase("closed")) this.closed = (java.lang.Boolean) values.get(0);
		else if (name.equalsIgnoreCase("pumpsDuration")) this.pumpsDuration = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
		else if (name.equalsIgnoreCase("sortedStates")) this.sortedStates = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
	}

	public Create create() {
		return new Create(null);
	}

	public Create create(java.lang.String name) {
		return new Create(name);
	}

	public class Create extends com.monentia.ebar.graph.Sequence.Create {


		public Create(java.lang.String name) {
			super(name);
		}

		public com.monentia.ebar.graph.SensorState sensorState(com.monentia.ebar.graph.Station.Sensor sensor, boolean value, java.time.Instant created) {
		    com.monentia.ebar.graph.SensorState newElement = core$().graph().concept(com.monentia.ebar.graph.SensorState.class).createNode(name, core$()).as(com.monentia.ebar.graph.SensorState.class);
			newElement.core$().set(newElement, "sensor", java.util.Collections.singletonList(sensor));
			newElement.core$().set(newElement, "value", java.util.Collections.singletonList(value));
			newElement.core$().set(newElement, "created", java.util.Collections.singletonList(created));
		    return newElement;
		}

	}

	public Clear clear() {
		return new Clear();
	}

	public class Clear  {
		public void sensorState(java.util.function.Predicate<com.monentia.ebar.graph.SensorState> filter) {
			new java.util.ArrayList<>(sensorStateList()).stream().filter(filter).forEach(io.intino.tara.magritte.Layer::delete$);
		}
	}

	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
