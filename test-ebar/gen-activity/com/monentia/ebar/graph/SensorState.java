package com.monentia.ebar.graph;

import com.monentia.ebar.graph.*;


public class SensorState extends io.intino.tara.magritte.Layer implements io.intino.tara.magritte.tags.Terminal {
	protected io.intino.tara.magritte.Expression<java.lang.String> label;
	protected com.monentia.ebar.graph.Station.Sensor sensor;
	protected boolean value;
	protected io.intino.sumus.graph.Event _metaType;

	public SensorState(io.intino.tara.magritte.Node node) {
		super(node);
		_metaType = a$(io.intino.sumus.graph.Event.class);
	}

	public java.lang.String label() {
		return label.value();
	}

	public com.monentia.ebar.graph.Station.Sensor sensor() {
		return sensor;
	}

	public boolean value() {
		return value;
	}

	public java.time.Instant created() {
		return _metaType.created();
	}

	public SensorState label(io.intino.tara.magritte.Expression<java.lang.String> value) {
		this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(value, this, io.intino.tara.magritte.Expression.class);
		return (SensorState) this;
	}

	public SensorState sensor(com.monentia.ebar.graph.Station.Sensor value) {
		this.sensor = value;
		return (SensorState) this;
	}

	public SensorState value(boolean value) {
		this.value = value;
		return (SensorState) this;
	}

	public SensorState created(java.time.Instant value) {
		this._metaType.created(value);
		return (SensorState) this;
	}

	@Override
	protected java.util.Map<java.lang.String, java.util.List<?>> variables$() {
		java.util.Map<String, java.util.List<?>> map = new java.util.LinkedHashMap<>();
		map.put("label", new java.util.ArrayList(java.util.Collections.singletonList(this.label)));
		map.put("sensor", this.sensor != null ? new java.util.ArrayList(java.util.Collections.singletonList(this.sensor)) : java.util.Collections.emptyList());
		map.put("value", new java.util.ArrayList(java.util.Collections.singletonList(this.value)));
		map.put("created", new java.util.ArrayList(java.util.Collections.singletonList(this._metaType.created())));
		return map;
	}

	@Override
	protected void load$(java.lang.String name, java.util.List<?> values) {
		super.load$(name, values);
		core$().load(_metaType, name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(values, this, io.intino.tara.magritte.Expression.class).get(0);
		else if (name.equalsIgnoreCase("sensor")) this.sensor = io.intino.tara.magritte.loaders.NodeLoader.load(values, com.monentia.ebar.graph.Station.Sensor.class, this).get(0);
		else if (name.equalsIgnoreCase("value")) this.value = io.intino.tara.magritte.loaders.BooleanLoader.load(values, this).get(0);
	}

	@Override
	protected void set$(java.lang.String name, java.util.List<?> values) {
		super.set$(name, values);
		core$().set(_metaType, name, values);
		if (name.equalsIgnoreCase("label")) this.label = io.intino.tara.magritte.loaders.FunctionLoader.load(values.get(0), this, io.intino.tara.magritte.Expression.class);
		else if (name.equalsIgnoreCase("sensor")) this.sensor = values.get(0)!= null ? core$().graph().load(((io.intino.tara.magritte.Layer) values.get(0)).core$().id()).as(com.monentia.ebar.graph.Station.Sensor.class) : null;
		else if (name.equalsIgnoreCase("value")) this.value = (java.lang.Boolean) values.get(0);
	}


	public com.monentia.ebar.graph.TestEbarGraph graph() {
		return (com.monentia.ebar.graph.TestEbarGraph) core$().graph().as(com.monentia.ebar.graph.TestEbarGraph.class);
	}
}
