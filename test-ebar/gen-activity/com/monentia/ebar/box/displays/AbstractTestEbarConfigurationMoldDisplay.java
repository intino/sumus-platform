package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.TestEbarConfiguration;
import com.monentia.ebar.box.displays.notifiers.TestEbarConfigurationMoldDisplayNotifier;
import com.monentia.ebar.graph.Alerts;
import com.monentia.ebar.graph.StationSequence;
import io.intino.konos.alexandria.activity.displays.AlexandriaMoldDisplay;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.mold.Block;
import io.intino.konos.alexandria.activity.model.mold.Stamp;
import io.intino.konos.alexandria.activity.model.mold.stamps.*;
import io.intino.konos.alexandria.activity.model.mold.stamps.icons.AlexandriaIcon;
import io.intino.konos.alexandria.activity.model.mold.stamps.icons.ResourceIcon;
import io.intino.konos.alexandria.activity.model.mold.stamps.operations.OpenDialogOperation;

public abstract class AbstractTestEbarConfigurationMoldDisplay extends AlexandriaMoldDisplay<TestEbarConfigurationMoldDisplayNotifier> {

	public AbstractTestEbarConfigurationMoldDisplay(TestEbarBox box) {
		super(box);
		element(buildMold(box));
	}

	public Mold buildMold(TestEbarBox box) {
		Mold mold = new Mold();
		Block block1 = new Block().add(Block.Layout.Horizontal);
		Block block11 = new Block();
		block11.add(new AlexandriaIcon().name("icono").label("icono").value((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.icono((Alerts.Alert)object, username)));
		block1.add(block11);
		Block block12 = new Block().add(Block.Layout.Flexible);
		block12.add(new Title().name("title").label("estación").value((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.title((Alerts.Alert)object, username)));
		block12.add(new Description().name("emails").label("direcciones de correo").value((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.emails((Alerts.Alert)object, username)));
		block12.add(new Highlight().color((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.estadoColor((Alerts.Alert)object, username)).name("estado").label("estado").value((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.estado((Alerts.Alert)object, username)));
		block1.add(block12);
		mold.add(block1);
		Block block2 = new Block().add(Block.Layout.Horizontal).add(Block.Layout.Wrap).expanded(true);
		Block block21 = new Block();
		block21.add(new AlexandriaIcon().name("dummy").label("icono").value((object, username) -> TestEbarConfigurationMoldDisplay.Stamps.dummy((Alerts.Alert)object, username)));
		block2.add(block21);
		Block block22 = new Block().add(Block.Layout.Flexible);
		block22.add(new OpenDialogOperation().path((itemId, username) -> TestEbarConfigurationMoldDisplay.Stamps.editAlert((String)itemId, username)).name("editAlert").label("editar..."));
		block2.add(block22);
		mold.add(block2);
		return mold;
	}

}
