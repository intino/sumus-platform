package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarHomeDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaDesktopDisplay;
import io.intino.konos.alexandria.activity.model.Desktop;

public abstract class AbstractTestHomeDisplay extends AlexandriaDesktopDisplay<TestEbarHomeDisplayNotifier> {

	public AbstractTestHomeDisplay(TestEbarBox box) {
		super(box);
		element(buildDesktop(box));
	}

	private static Desktop buildDesktop(TestEbarBox box) {
		Desktop desktop = new Desktop();
		desktop.title("Ebar");
		desktop.subtitle("Ebar");
		desktop.logo(null);
		desktop.favicon(null);
		desktop.authServiceUrl(null);
		//desktop.logo(new URL("logo.png"));
		//desktop.favicon(new URL("favicon.png"));
		//desktop.authServiceUrl(new URL("http://localhost"));
		desktop.layoutDisplayProvider(() -> TestEbarHomeDisplay.Layout.layoutDisplay(box));
		return desktop;
	}

}