package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarTabDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaElementDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaTabLayoutDisplay;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Element;
import io.intino.konos.alexandria.activity.model.Layout;
import io.intino.konos.alexandria.activity.model.layout.ElementOption;
import io.intino.konos.alexandria.activity.model.layout.options.Options;
import io.intino.konos.alexandria.activity.model.renders.RenderCatalogs;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestEbarTabDisplay extends AlexandriaTabLayoutDisplay<TestEbarTabDisplayNotifier> {

	public AbstractTestEbarTabDisplay(TestEbarBox box) {
		super(box);
		element(buildLayout(box));
	}

	private static Layout buildLayout(TestEbarBox box) {
		Layout layout = new Layout();
		layout.mode(Layout.Mode.Tab);
		layout.elementDisplayBuilder(new Layout.ElementDisplayBuilder() {
			@Override
			public AlexandriaElementDisplay displayFor(Element element, Object o) {
				return ElementDisplays.displayFor(box, element);
			}

			@Override
			public Class<? extends AlexandriaElementDisplay> displayTypeFor(Element element, Object o) {
				return ElementDisplays.displayTypeFor(box, element);
			}
		});
		buildOptions(box).forEach(o -> layout.add(o));
		return layout;
	}

	private static List<ElementOption> buildOptions(TestEbarBox box) {
		List<ElementOption> result = new ArrayList<>();
		result.add(new Options().render(new RenderCatalogs().catalogs(new ArrayList<Catalog>() {{
			add((Catalog) ElementDisplays.displayFor(box,"testEbarInfrastructure").element());
			add((Catalog) ElementDisplays.displayFor(box,"testEbarSequence").element());
			add((Catalog) ElementDisplays.displayFor(box,"testEbarAnalysis").element());
			add((Catalog) ElementDisplays.displayFor(box,"testEbarConfiguration").element());
		}})).label((element, object) -> TestEbarTabDisplay.Options.label(element, object))
			.icon((element, object) -> TestEbarTabDisplay.Options.icon(element, object))
			.bubble((element, object) -> TestEbarTabDisplay.Options.bubble(element, object)));
		return result;
	}

}
