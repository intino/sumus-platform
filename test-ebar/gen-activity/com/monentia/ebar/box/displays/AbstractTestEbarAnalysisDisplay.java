package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarAnalysisDisplayNotifier;
import com.monentia.ebar.graph.Asset;
import io.intino.konos.alexandria.activity.displays.AlexandriaCatalogDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplay;
import io.intino.konos.alexandria.activity.displays.CatalogInstantBlock;
import io.intino.konos.alexandria.activity.model.AbstractView;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Toolbar;
import io.intino.konos.alexandria.activity.model.catalog.views.DisplayView;
import io.intino.konos.alexandria.activity.model.mold.stamps.Display;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class AbstractTestEbarAnalysisDisplay extends AlexandriaCatalogDisplay<TestEbarAnalysisDisplayNotifier> {

	public AbstractTestEbarAnalysisDisplay(TestEbarBox box) {
		super(box);
		element(buildCatalog(box));
	}

	private static Catalog buildCatalog(TestEbarBox box) {
		Catalog catalog = new Catalog();
		catalog.name("testEbarAnalisis");
		catalog.label("Análisis");
		catalog.objectsLoader((scope, condition, username) -> (List<Object>)(Object)TestEbarAnalysisDisplay.Source.assetList(box, scope, condition, username));
		catalog.objectLoader((id, username) -> TestEbarAnalysisDisplay.Source.asset(box, id, username));
		catalog.objectIdLoader((object) -> TestEbarAnalysisDisplay.Source.assetId(box, (Asset)object));
		catalog.objectNameLoader((object) -> TestEbarAnalysisDisplay.Source.assetName(box, (Asset)object));
		catalog.rootObjectLoader((objectList, username) -> TestEbarAnalysisDisplay.Source.rootAsset(box, (List<Asset>)(Object)objectList, username));
		catalog.defaultObjectLoader((id, username) -> TestEbarAnalysisDisplay.Source.defaultAsset(box, id, username));
		catalog.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> catalog.add(v));
		return catalog;
	}

	private static Toolbar buildToolbar(TestEbarBox box) {
		Toolbar toolbar = new Toolbar();
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestEbarBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new DisplayView().displayLoader(new DisplayView.DisplayLoader() {
			@Override
			public AlexandriaDisplay load(Object context, Consumer<Boolean> loadingListener, Consumer<CatalogInstantBlock> instantListener, String username) {
				return TestEbarAnalysisDisplay.Views.olapDisplay(box, (io.intino.konos.alexandria.activity.model.Element) context, loadingListener, instantListener, username);
			}
		}).scopeManager((display, scope) -> TestEbarAnalysisDisplay.Views.olapDisplayScope(box, display, scope)).name("v1").label("análisis"));
		return result;
	}

}
