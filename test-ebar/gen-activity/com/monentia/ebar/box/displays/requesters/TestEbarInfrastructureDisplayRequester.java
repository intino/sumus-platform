package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarInfrastructureDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaCatalogDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarInfrastructureDisplayRequester extends AlexandriaCatalogDisplayRequester {

	public TestEbarInfrastructureDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarInfrastructureDisplay display = display();
		if (display == null) return;
		String operation = this.operation();
		super.execute();
	}
}