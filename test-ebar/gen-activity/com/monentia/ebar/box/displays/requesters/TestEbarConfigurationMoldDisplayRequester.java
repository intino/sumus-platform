package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarConfigurationMoldDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaMoldDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarConfigurationMoldDisplayRequester extends AlexandriaMoldDisplayRequester {

	public TestEbarConfigurationMoldDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarConfigurationMoldDisplay display = display();
		if (display == null) return;
		super.execute();
	}
}