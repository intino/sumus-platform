package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarTabDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaTabLayoutDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarTabDisplayRequester extends AlexandriaTabLayoutDisplayRequester {

	public TestEbarTabDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarTabDisplay display = display();
		if (display == null) return;
		String operation = operation();

		if (operation.equals("logout")) display.logout();
		else if (operation.equals("selectItem")) display.selectItem(manager.fromQuery("value", String.class));
	}
}