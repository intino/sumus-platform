package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarHomeDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaDesktopDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarHomeDisplayRequester extends AlexandriaDesktopDisplayRequester {

	public TestEbarHomeDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarHomeDisplay display = display();
		if (display == null) return;
		String operation = operation();
	}
}