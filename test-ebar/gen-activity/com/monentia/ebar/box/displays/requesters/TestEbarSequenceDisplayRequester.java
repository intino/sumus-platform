package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarSequenceDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaTemporalRangeCatalogDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarSequenceDisplayRequester extends AlexandriaTemporalRangeCatalogDisplayRequester {

	public TestEbarSequenceDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarSequenceDisplay display = display();
		if (display == null) return;
		super.execute();
	}
}