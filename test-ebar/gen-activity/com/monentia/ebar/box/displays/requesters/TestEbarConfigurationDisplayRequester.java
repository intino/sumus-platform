package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarConfigurationDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaCatalogDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarConfigurationDisplayRequester extends AlexandriaCatalogDisplayRequester {

	public TestEbarConfigurationDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarConfigurationDisplay display = display();
		if (display == null) return;
		super.execute();
	}
}