package com.monentia.ebar.box.displays.requesters;

import com.monentia.ebar.box.displays.TestEbarInfrastructureMoldDisplay;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaMoldDisplayRequester;
import io.intino.konos.alexandria.activity.spark.ActivitySparkManager;
import io.intino.konos.alexandria.exceptions.AlexandriaException;

public class TestEbarInfrastructureMoldDisplayRequester extends AlexandriaMoldDisplayRequester {

	public TestEbarInfrastructureMoldDisplayRequester(ActivitySparkManager manager, AlexandriaDisplayNotifierProvider notifierProvider) {
		super(manager, notifierProvider);
	}

	@Override
	public void execute() throws AlexandriaException {
		TestEbarInfrastructureMoldDisplay display = display();
		if (display == null) return;
		super.execute();
	}
}