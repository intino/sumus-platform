package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import io.intino.konos.alexandria.activity.displays.*;
import io.intino.konos.alexandria.activity.model.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class ElementDisplays {
	private static Map<String, Class<? extends AlexandriaElementDisplay>> elementMap = new HashMap<>();

	public static void init(TestEbarBox box) {
		elementMap.put("testEbarHome", TestEbarHomeDisplay.class);
		elementMap.put("testEbarTab", TestEbarTabDisplay.class);
		elementMap.put("testEbarInfrastructure", TestEbarInfrastructureDisplay.class);
		elementMap.put("testEbarInfrastructureMold", TestEbarInfrastructureMoldDisplay.class);
		elementMap.put("testEbarSequence", TestEbarSequenceDisplay.class);
		elementMap.put("testEbarSequenceMold", TestEbarSequenceMoldDisplay.class);
		elementMap.put("testEbarAnalysis", TestEbarAnalysisDisplay.class);
		elementMap.put("testEbarConfiguration", TestEbarConfigurationDisplay.class);
		elementMap.put("testEbarConfigurationMold", TestEbarConfigurationMoldDisplay.class);
	}

	public static AlexandriaElementDisplay displayFor(TestEbarBox box, String name) {
		if (!elementMap.containsKey(name)) return null;
		return displayFor(box, elementMap.get(name));
	}

	public static AlexandriaElementDisplay displayFor(TestEbarBox box, Element element) {
		if (!elementMap.containsKey(element.name())) return defaultElementDisplay(box, element);
		return displayFor(box, elementMap.get(element.name()));
	}

	public static Class<? extends AlexandriaElementDisplay> displayTypeFor(TestEbarBox box, Element element) {
		if (!elementMap.containsKey(element.name())) return defaultElementType(box, element);
		return elementMap.get(element.name());
	}

	private static AlexandriaElementDisplay displayFor(TestEbarBox box, Class<? extends AlexandriaElementDisplay> aClass) {
		Constructor<? extends AlexandriaElementDisplay> constructor = null;
		try {
			constructor = aClass.getConstructor(TestEbarBox.class);
			return constructor.newInstance(box);
		} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Class<? extends AlexandriaElementDisplay> defaultElementType(TestEbarBox box, Element element) {
		if (element instanceof Panel) return AlexandriaPanelDisplay.class;

		if (element instanceof TemporalCatalog)
			return ((TemporalCatalog)element).type() == TemporalCatalog.Type.Range ? AlexandriaTemporalRangeCatalogDisplay.class : AlexandriaTemporalTimeCatalogDisplay.class;

		if (element instanceof Catalog)
			return AlexandriaCatalogDisplay.class;

		if (element instanceof Layout)
			return ((Layout)element).mode() == Layout.Mode.Menu ? AlexandriaMenuLayoutDisplay.class : AlexandriaTabLayoutDisplay.class;

		if (element instanceof Desktop)
			return AlexandriaDesktopDisplay.class;

		return null;
	}

	private static AlexandriaElementDisplay defaultElementDisplay(TestEbarBox box, Element element) {
		if (element instanceof Panel) return new AlexandriaPanelDisplay(box);

		if (element instanceof TemporalCatalog)
			return ((TemporalCatalog)element).type() == TemporalCatalog.Type.Range ? new AlexandriaTemporalRangeCatalogDisplay(box) : new AlexandriaTemporalTimeCatalogDisplay(box);

		if (element instanceof Catalog)
			return new AlexandriaCatalogDisplay(box);

		if (element instanceof Layout)
			return ((Layout)element).mode() == Layout.Mode.Menu ? new AlexandriaMenuLayoutDisplay(box) : new AlexandriaTabLayoutDisplay(box);

		if (element instanceof Desktop)
			return new AlexandriaDesktopDisplay(box);

		return null;
	}

}
