package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarSequenceMoldDisplayNotifier;
import com.monentia.ebar.graph.Asset;
import com.monentia.ebar.graph.StationSequence;
import io.intino.konos.alexandria.activity.displays.AlexandriaMoldDisplay;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.mold.Block;
import io.intino.konos.alexandria.activity.model.mold.Stamp;
import io.intino.konos.alexandria.activity.model.mold.stamps.*;
import io.intino.konos.alexandria.activity.model.mold.stamps.icons.ResourceIcon;

public abstract class AbstractTestEbarSequenceMoldDisplay extends AlexandriaMoldDisplay<TestEbarSequenceMoldDisplayNotifier> {

	public AbstractTestEbarSequenceMoldDisplay(TestEbarBox box) {
		super(box);
		element(buildMold(box));
	}

	public Mold buildMold(TestEbarBox box) {
		Mold mold = new Mold();
		Block block1 = new Block().add(Block.Layout.Horizontal);
		block1.add(new ResourceIcon().name("icon").label("icon").value((object, username) -> TestEbarSequenceMoldDisplay.Stamps.icon((StationSequence)object, username)));
		block1.add(new Block().add(new Title().name("label").label("label").value((object, username) -> TestEbarSequenceMoldDisplay.Stamps.label((StationSequence)object, username)).style((object, username) -> TestEbarSequenceMoldDisplay.Stamps.labelStyle((StationSequence)object, username))));
		mold.add(block1);
		Block block2 = new Block().add(Block.Layout.Horizontal).add(Block.Layout.Wrap).expanded(true);
		block2.add(new Picture().defaultPicture(null).name("chart").label("sequencia").defaultStyle("margin-top:6px;").value((object, username) -> TestEbarSequenceMoldDisplay.Stamps.chart((StationSequence)object, username)));
		block2.add(new Snippet().name("sequence").label("sequencia").defaultStyle("height:270px;overflow:auto;margin-top:10px;margin-bottom:10px;").layout(Stamp.Layout.Flexible).value((object, username) -> TestEbarSequenceMoldDisplay.Stamps.sequence((StationSequence)object, username)));
		mold.add(block2);
		return mold;
	}

}
