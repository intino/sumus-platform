package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarConfigurationDisplayNotifier;
import com.monentia.ebar.graph.Alerts;
import io.intino.konos.alexandria.activity.displays.AlexandriaCatalogDisplay;
import io.intino.konos.alexandria.activity.model.AbstractView;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.Toolbar;
import io.intino.konos.alexandria.activity.model.catalog.views.ListView;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestEbarConfigurationDisplay extends AlexandriaCatalogDisplay<TestEbarConfigurationDisplayNotifier> {

	public AbstractTestEbarConfigurationDisplay(TestEbarBox box) {
		super(box);
		element(buildCatalog(box));
	}

	private static Catalog buildCatalog(TestEbarBox box) {
		Catalog catalog = new Catalog();
		catalog.name("testEbarConfiguration");
		catalog.label("Configuración");
		catalog.objectsLoader((scope, condition, username) -> (List<Object>)(Object)TestEbarConfigurationDisplay.Source.alertList(box, scope, condition, username));
		catalog.objectLoader((id, username) -> TestEbarConfigurationDisplay.Source.alert(box, id, username));
		catalog.objectIdLoader((object) -> TestEbarConfigurationDisplay.Source.alertId(box, (Alerts.Alert)object));
		catalog.objectNameLoader((object) -> TestEbarConfigurationDisplay.Source.alertName(box, (Alerts.Alert)object));
		catalog.rootObjectLoader((objectList, username) -> TestEbarConfigurationDisplay.Source.rootAlert(box, (List<Alerts.Alert>)(Object)objectList, username));
		catalog.defaultObjectLoader((id, username) -> TestEbarConfigurationDisplay.Source.defaultAlert(box, id, username));
		catalog.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> catalog.add(v));
		return catalog;
	}

	private static Toolbar buildToolbar(TestEbarBox box) {
		Toolbar toolbar = new Toolbar();
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestEbarBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new ListView().mold((Mold) ElementDisplays.displayFor(box, "testEbarConfigurationMold").element()).name("v1").label("alertas"));
		return result;
	}

}
