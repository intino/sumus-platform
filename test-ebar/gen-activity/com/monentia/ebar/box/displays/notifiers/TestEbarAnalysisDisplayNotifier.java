package com.monentia.ebar.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaCatalogDisplayNotifier;

public class TestEbarAnalysisDisplayNotifier extends AlexandriaCatalogDisplayNotifier {

	public TestEbarAnalysisDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}