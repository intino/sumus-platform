package com.monentia.ebar.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaCatalogDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaTemporalRangeCatalogDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.requesters.AlexandriaTemporalRangeCatalogDisplayRequester;

public class TestEbarSequenceDisplayNotifier extends AlexandriaTemporalRangeCatalogDisplayNotifier {

	public TestEbarSequenceDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}