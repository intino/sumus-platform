package com.monentia.ebar.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaMoldDisplayNotifier;

public class TestEbarInfrastructureMoldDisplayNotifier extends AlexandriaMoldDisplayNotifier {

	public TestEbarInfrastructureMoldDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}