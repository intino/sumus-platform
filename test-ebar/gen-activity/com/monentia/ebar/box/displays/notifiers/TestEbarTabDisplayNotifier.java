package com.monentia.ebar.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaTabLayoutDisplayNotifier;

public class TestEbarTabDisplayNotifier extends AlexandriaTabLayoutDisplayNotifier {

	public TestEbarTabDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}