package com.monentia.ebar.box.displays.notifiers;

import io.intino.konos.alexandria.activity.displays.notifiers.AlexandriaMoldDisplayNotifier;

public class TestEbarSequenceMoldDisplayNotifier extends AlexandriaMoldDisplayNotifier {

	public TestEbarSequenceMoldDisplayNotifier(io.intino.konos.alexandria.activity.displays.AlexandriaDisplay display, io.intino.konos.alexandria.activity.displays.MessageCarrier carrier) {
		super(display, carrier);
	}

}