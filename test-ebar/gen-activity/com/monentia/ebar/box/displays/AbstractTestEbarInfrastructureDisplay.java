package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarInfrastructureDisplayNotifier;
import com.monentia.ebar.graph.Asset;
import io.intino.konos.alexandria.activity.displays.AlexandriaCatalogDisplay;
import io.intino.konos.alexandria.activity.model.AbstractView;
import io.intino.konos.alexandria.activity.model.Catalog;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.Toolbar;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Arrangement;
import io.intino.konos.alexandria.activity.model.catalog.views.MapView;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestEbarInfrastructureDisplay extends AlexandriaCatalogDisplay<TestEbarInfrastructureDisplayNotifier> {

	public AbstractTestEbarInfrastructureDisplay(TestEbarBox box) {
		super(box);
		element(buildCatalog(box));
	}

	private static Catalog buildCatalog(TestEbarBox box) {
		Catalog catalog = new Catalog();
		catalog.name("testEbarInfrastructure");
		catalog.label("Infrastructure");
		catalog.objectsLoader((scope, condition, username) -> (List<Object>)(Object)TestEbarInfrastructureDisplay.Source.assetList(box, scope, condition, username));
		catalog.objectLoader((id, username) -> TestEbarInfrastructureDisplay.Source.asset(box, id, username));
		catalog.objectIdLoader((object) -> TestEbarInfrastructureDisplay.Source.assetId(box, (Asset)object));
		catalog.objectNameLoader((object) -> TestEbarInfrastructureDisplay.Source.assetName(box, (Asset)object));
		catalog.rootObjectLoader((objectList, username) -> TestEbarInfrastructureDisplay.Source.rootAsset(box, (List<Asset>)(Object)objectList, username));
		catalog.defaultObjectLoader((id, username) -> TestEbarInfrastructureDisplay.Source.defaultAsset(box, id, username));
		catalog.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> catalog.add(v));
		buildArrangements(box).forEach(a -> catalog.add(a));
		return catalog;
	}

	private static Toolbar buildToolbar(TestEbarBox box) {
		Toolbar toolbar = new Toolbar();
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestEbarBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new MapView().center(new MapView.Center().latitude(28.146773457066104).longitude(-15.418557420532238)).zoom(new MapView.Zoom().defaultZoom(14).min(1).max(18)).mold((Mold) ElementDisplays.displayFor(box, "testEbarInfrastructureMold").element()).name("v4").label("mapa"));
		return result;
	}

	private static List<Arrangement> buildArrangements(TestEbarBox box) {
		List<Arrangement> arrangements = new ArrayList<>();
		return arrangements;
	}

}
