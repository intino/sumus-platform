package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarSequenceDisplayNotifier;
import com.monentia.ebar.graph.Station;
import com.monentia.ebar.graph.StationSequence;
import io.intino.konos.alexandria.activity.displays.AlexandriaTemporalRangeCatalogDisplay;
import io.intino.konos.alexandria.activity.model.*;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Arrangement;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Group;
import io.intino.konos.alexandria.activity.model.catalog.arrangement.Grouping;
import io.intino.konos.alexandria.activity.model.catalog.views.ListView;
import io.intino.konos.alexandria.activity.model.toolbar.Export;
import io.intino.konos.alexandria.activity.services.push.ActivitySession;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTestEbarSequenceDisplay extends AlexandriaTemporalRangeCatalogDisplay<TestEbarSequenceDisplayNotifier> {

	public AbstractTestEbarSequenceDisplay(TestEbarBox box) {
		super(box);
		element(buildCatalog(box));
	}

	private static TemporalCatalog buildCatalog(TestEbarBox box) {
		TemporalCatalog catalog = new TemporalCatalog();
		catalog.name("testEbarSequence");
		catalog.label("Episodios EBAR");
		catalog.type(TemporalCatalog.Type.Range);
		catalog.scales(new ArrayList<TimeScale>() {{ add(TimeScale.Day); }});
		catalog.objectsLoader((scope, condition, timeRange, username) -> (List<Object>)(Object)TestEbarSequenceDisplay.Source.stationSequenceList(box, scope, condition, timeRange, username));
		catalog.objectLoader((id, username) -> TestEbarSequenceDisplay.Source.stationSequence(box, id, username));
		catalog.objectIdLoader((object) -> TestEbarSequenceDisplay.Source.stationSequenceId(box, (StationSequence)object));
		catalog.objectNameLoader((object) -> TestEbarSequenceDisplay.Source.stationSequenceName(box, (StationSequence)object));
		catalog.objectCreatedLoader((object) -> TestEbarSequenceDisplay.Source.stationSequenceCreated(box, (StationSequence)object));
		catalog.rootObjectLoader((objectList, timeRange, username) -> TestEbarSequenceDisplay.Source.rootStationSequence(box, (List<StationSequence>)(Object)objectList, timeRange, username));
		catalog.defaultObjectLoader((id, timeRange, username) -> TestEbarSequenceDisplay.Source.defaultStationSequence(box, id, timeRange, username));
		catalog.rangeLoader((username) -> TestEbarSequenceDisplay.Temporal.range(box, username));
		catalog.arrangementFiltererLoader((username) -> TestEbarSequenceDisplay.Arrangements.filterer(box, username));
		catalog.toolbar(buildToolbar(box));
		buildViews(box).forEach(v -> catalog.add(v));
		buildArrangements(box).forEach(a -> catalog.add(a));
		return catalog;
	}

	private static Toolbar buildToolbar(TestEbarBox box) {
		Toolbar toolbar = new Toolbar();
		toolbar.add(new Export().execute((element, from, to, username) -> TestEbarSequenceDisplay.Toolbar.export(box, element, from, to, username)).name("export").title("Descarga los episodios del último mes").alexandriaIcon("archive"));
		return toolbar;
	}

	private static List<AbstractView> buildViews(TestEbarBox box) {
		List<AbstractView> result = new ArrayList<>();
		result.add(new ListView().mold((Mold) ElementDisplays.displayFor(box, "testEbarSequenceMold").element()).name("v1").label("listado"));
		return result;
	}

	private static List<Arrangement> buildArrangements(TestEbarBox box) {
		List<Arrangement> arrangements = new ArrayList<>();
		arrangements.add(new io.intino.konos.alexandria.activity.model.catalog.arrangement.Sorting().comparator((object1, object2) -> TestEbarSequenceDisplay.Arrangements.dateComparator((StationSequence) object1, (StationSequence)object2)).name("date").label("fecha"));
		arrangements.add(new Grouping().groups((objects, username) -> TestEbarSequenceDisplay.Arrangements.sequenceStations(box, (List<StationSequence>)(Object)objects, username)).name("sequenceStations").label("Estación"));
		arrangements.add(new Grouping().groups((objects, username) -> TestEbarSequenceDisplay.Arrangements.stationSequenceStates(box, (List<StationSequence>)(Object)objects, username)).name("stationSequenceStates").label("Estado del episodio"));
		arrangements.add(new Grouping().groups((objects, username) -> TestEbarSequenceDisplay.Arrangements.stationSequenceTypes(box, (List<StationSequence>)(Object)objects, username)).name("stationSequenceTypes").label("Tipo de episodio"));
		return arrangements;
	}

}
