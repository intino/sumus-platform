package com.monentia.ebar.box.displays;

import com.monentia.ebar.box.TestEbarBox;
import com.monentia.ebar.box.displays.notifiers.TestEbarInfrastructureMoldDisplayNotifier;
import com.monentia.ebar.graph.Asset;
import io.intino.konos.alexandria.activity.displays.AlexandriaMoldDisplay;
import io.intino.konos.alexandria.activity.model.Mold;
import io.intino.konos.alexandria.activity.model.mold.Block;
import io.intino.konos.alexandria.activity.model.mold.Stamp;
import io.intino.konos.alexandria.activity.model.mold.stamps.*;

public abstract class AbstractTestEbarInfrastructureMoldDisplay extends AlexandriaMoldDisplay<TestEbarInfrastructureMoldDisplayNotifier> {

	public AbstractTestEbarInfrastructureMoldDisplay(TestEbarBox box) {
		super(box);
		element(buildMold(box));
	}

	public Mold buildMold(TestEbarBox box) {
		Mold mold = new Mold();
		Block block1 = new Block();
		Block block11 = new Block();
		block11.add(new Location().icon((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.coordinatesIcon((Asset)object, username)).name("s1").label("coordinates").value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.coordinates((Asset)object, username)));
		block1.add(block11);
		Block block12 = new Block().expanded(true);
		block12.add(new Block().add(new Title().name("title").label("label").value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.title((Asset)object, username)))
							   .add(new Description().name("lastEpisode").label("Estado").value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.lastEpisode((Asset)object, username))));
		block12.add(new Block().add(Block.Layout.Horizontal)
							   .add(new Picture().defaultPicture(null).name("chart").label("chart").defaultStyle("margin-top:6px;").value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.chart((Asset)object, username)))
				               .add(new Snippet().name("sequence").label("sequencia").defaultStyle("height:155px;overflow:auto;margin-top:10px;margin-bottom:10px;").layout(Stamp.Layout.Flexible).value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.sequence((Asset)object, username))));
		block12.add(new Block().add(new Snippet().name("stats").label("").value((object, username) -> TestEbarInfrastructureMoldDisplay.Stamps.stats((Asset)object, username))));
		block1.add(block12);
		mold.add(block1);
		return mold;
	}

}
