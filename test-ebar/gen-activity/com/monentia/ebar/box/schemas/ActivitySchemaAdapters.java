package com.monentia.ebar.box.schemas;

public class ActivitySchemaAdapters {


	public static com.monentia.ebar.box.schemas.Dummy dummyFromLayer(io.intino.tara.magritte.Layer layer) {
		return dummyFromNode(layer.core$());
	}

	private static com.monentia.ebar.box.schemas.Dummy dummyFromNode(io.intino.tara.magritte.Node node) {
		com.monentia.ebar.box.schemas.Dummy schema = new com.monentia.ebar.box.schemas.Dummy();
		final java.util.Map<String, java.util.List<?>> variables = node.variables();
		variables.put("name", java.util.Collections.singletonList(node.name()));
		variables.put("id", java.util.Collections.singletonList(node.id()));


		return schema;
	}

}

