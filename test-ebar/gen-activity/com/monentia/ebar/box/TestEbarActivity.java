package com.monentia.ebar.box;

import com.monentia.ebar.box.displays.*;
import com.monentia.ebar.box.displays.notifiers.*;
import com.monentia.ebar.box.displays.requesters.*;
import com.monentia.ebar.box.resources.*;

import io.intino.konos.alexandria.activity.ActivityAlexandriaSpark;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifier;
import io.intino.konos.alexandria.activity.displays.AlexandriaDisplayNotifierProvider;
import io.intino.konos.alexandria.activity.services.push.PushService;
import io.intino.konos.alexandria.activity.spark.resources.AfterDisplayRequest;
import io.intino.konos.alexandria.activity.spark.resources.AssetResource;
import io.intino.konos.alexandria.activity.spark.resources.AuthenticateCallbackResource;
import io.intino.konos.alexandria.activity.spark.resources.BeforeDisplayRequest;

import java.net.MalformedURLException;
import java.net.URL;

public class TestEbarActivity extends io.intino.konos.alexandria.activity.Activity {

	public static ActivityAlexandriaSpark init(ActivityAlexandriaSpark spark, TestEbarBox box) {
		TestEbarConfiguration.TestEbarActivityConfiguration configuration = box.configuration().testEbarConfiguration;
		spark.route("/push").push(new PushService());
		spark.route("/authenticate-callback").get(manager -> new AuthenticateCallbackResource(manager, notifierProvider()).execute());
		spark.route("/asset/:name").get(manager -> new AssetResource(name -> new AssetResourceLoader(box).load(name), manager, notifierProvider()).execute());

		spark.route("").get(manager -> new TestEbarHomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/").get(manager -> new TestEbarHomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/index").get(manager -> new TestEbarHomePageResource(box, manager, notifierProvider()).execute());
		spark.route("/home").get(manager -> new TestEbarHomePageResource(box, manager, notifierProvider()).execute());

		spark.route("/testhome/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testhome/:displayId").post(manager -> new TestEbarHomeDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testhome/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebartab/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebartab/:displayId").post(manager -> new TestEbarTabDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebartab/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarinfrastructure/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarinfrastructure/:displayId").post(manager -> new TestEbarInfrastructureDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarinfrastructure/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarinfrastructuremold/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarinfrastructuremold/:displayId").post(manager -> new TestEbarInfrastructureMoldDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarinfrastructuremold/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarsequence/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarsequence/:displayId").post(manager -> new TestEbarSequenceDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarsequence/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarsequencemold/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarsequencemold/:displayId").post(manager -> new TestEbarSequenceMoldDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarsequencemold/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebaranalysis/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebaranalysis/:displayId").post(manager -> new TestEbarAnalysisDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebaranalysis/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarconfiguration/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarconfiguration/:displayId").post(manager -> new TestEbarConfigurationDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarconfiguration/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		spark.route("/testebarconfigurationmold/:displayId").before(manager -> new BeforeDisplayRequest(manager).execute());
		spark.route("/testebarconfigurationmold/:displayId").post(manager -> new TestEbarConfigurationMoldDisplayRequester(manager, notifierProvider()).execute());
		spark.route("/testebarconfigurationmold/:displayId").after(manager -> new AfterDisplayRequest(manager).execute());

		registerNotifiers();
		return spark;
	}

	private static void registerNotifiers() {
		register(TestEbarHomeDisplayNotifier.class).forDisplay(TestEbarHomeDisplay.class);

	}
}