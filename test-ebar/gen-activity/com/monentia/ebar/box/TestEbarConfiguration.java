package com.monentia.ebar.box;

public class TestEbarConfiguration extends io.intino.sumus.box.SumusConfiguration {

	TestEbarActivityConfiguration testEbarConfiguration;

	public TestEbarConfiguration(String[] args) {
		super(args);
		fillWithArgs();
	}

	private void fillWithArgs() {
		if (this.store == null && args.get("graph_store") != null)
			store = new java.io.File(args.remove("graph_store"));
		if (args.containsKey("testEbar_port")) {
			testEbarConfiguration(toInt(args.remove("testEbar_port")), args.remove("testEbar_webDirectory"));

		}
	}

	public java.io.File store() {
		return this.store;
	}

	public TestEbarConfiguration testEbarConfiguration(int port, String webDirectory) {
		this.testEbarConfiguration = new TestEbarActivityConfiguration();
		this.testEbarConfiguration.port = port;
		this.testEbarConfiguration.webDirectory = webDirectory == null ? "www/" : webDirectory;

		return this;
	}

	public TestEbarConfiguration testEbarConfiguration(int port) {
		return testEbarConfiguration(port, "www/");
	}

	public TestEbarActivityConfiguration testEbarConfiguration() {
		return testEbarConfiguration;
	}

	public static class TestEbarActivityConfiguration {
		public int port;
		public String webDirectory;
		public io.intino.konos.alexandria.activity.services.AuthService authService;



	}
}