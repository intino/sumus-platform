package com.monentia.ebar.box;

import io.intino.konos.alexandria.activity.ActivityBox;
import io.intino.konos.alexandria.activity.ActivityElementsActivity;
import io.intino.konos.alexandria.activity.displays.Soul;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

import static org.slf4j.Logger.ROOT_LOGGER_NAME;

public abstract class AbstractBox extends ActivityBox {
	private static Logger logger = LoggerFactory.getLogger(ROOT_LOGGER_NAME);
	protected TestEbarConfiguration configuration;
    protected Map<String, Soul> activitySouls = new java.util.HashMap<>();

	public AbstractBox(String[] args) {
		this(new TestEbarConfiguration(args));
	}

	public AbstractBox(TestEbarConfiguration configuration) {
		owner = new io.intino.sumus.box.SumusBox(configuration);
		this.configuration = configuration;
		if (configuration().testEbarConfiguration != null)
			io.intino.konos.alexandria.activity.ActivityAlexandriaSpark.setup(configuration().testEbarConfiguration.port, configuration().testEbarConfiguration.webDirectory, configuration().testEbarConfiguration.authService);
	}

	public TestEbarConfiguration configuration() {
		return (TestEbarConfiguration) configuration;
	}

	@Override
	public io.intino.konos.alexandria.Box put(Object o) {
		owner.put(o);
		return this;
	}

	public io.intino.konos.alexandria.Box open() {
		if(owner != null) owner.open();
		initActivities();
		initRESTServices();
		initJMXServices();
		initJMSServices();
		initDataLake();
		initTasks();
		initSlackBots();
		return this;
	}

	public void close() {
		if(owner != null) owner.close();
		io.intino.konos.alexandria.activity.ActivityAlexandriaSpark.instance().stop();


	}

	public void registerSoul(String clientId, Soul soul) {
	if(owner != null) ((io.intino.sumus.box.SumusBox) owner).registerSoul(clientId, soul);
	activitySouls.put(clientId, soul);
}

public void unRegisterSoul(String clientId) {
	if(owner != null) ((io.intino.sumus.box.SumusBox) owner).unRegisterSoul(clientId);
	activitySouls.remove(clientId);
}







	private void initRESTServices() {

	}

	private void initJMSServices() {


	}

	private void initJMXServices() {

	}

	private void initSlackBots() {

	}

	private void initActivities() {
		if (configuration().testEbarConfiguration == null) return;
		TestEbarActivity.init(io.intino.konos.alexandria.activity.ActivityAlexandriaSpark.instance(), (TestEbarBox) this).start();
		ActivityElementsActivity.init(io.intino.konos.alexandria.activity.ActivityAlexandriaSpark.instance(), (ActivityBox)this).start();
		logger.info("Activity testEbar: started!");
	}

	private void initDataLake() {

	}

	private void initTasks() {

	}

	private void initLogger() {
		final java.util.logging.Logger logger = java.util.logging.Logger.getGlobal();
		final ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(Level.INFO);
		handler.setFormatter(new io.intino.konos.alexandria.LogFormatter("log"));
		logger.addHandler(handler);
	}
}